/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.06.17.
//

#ifndef HGLIB_ISDERIVEDFROM_H
#define HGLIB_ISDERIVEDFROM_H

#include <type_traits>
#include "SfinaeBase.h"

namespace hglib {

/**
 * Struct to check if a type is derived from a class.
 *
 * @tparam Base
 * @tparam Derived
 */
template <template <typename...> class Parent, typename Derived>
struct is_parent_of : public sfinae_base {
    /// This method is called if Derived* can be converted to Parent<Args...>
    template <typename... Args>
    static yes& test(Parent<Args...>*);
    /// This method is called otherwise (SFINAE)
    static no& test(...);

    /// Value of the test if Derived* can be converted to Parent<Args...>
    static bool const value =
            sizeof(test(std::declval<Derived*>())) == sizeof(yes);
};
}  // namespace hglib
#endif  // HGLIB_ISDERIVEDFROM_H
