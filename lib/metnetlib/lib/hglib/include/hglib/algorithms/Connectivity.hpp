/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 03.08.17.
//

#ifndef HGLIB_CONNECTIVITY_HPP
#define HGLIB_CONNECTIVITY_HPP

#include <list>

namespace hglib {

/**
 * \@brief Computes the vertices that are reachable from a source vertex by a
 * simple path.
 *
 * Implements the Visit procedure of the paper Gallo et al:
 * "Directed hypergraphs and applications",
 * Discrete Applied Mathematics 42 (1993) 177-201
 *
 * @tparam DirectedHypergraph Type of directed hypergraph
 * @param g DirectedHypergraph
 * @param source Id of the source vertex
 * @return
 */
template<typename DirectedHypergraph>
std::vector<hglib::VertexIdType>
verticesConnectedToSource(const DirectedHypergraph& g,
                          const hglib::VertexIdType& source) {
  // Check if source vertex is part of the hypergrapgh
  std::list<hglib::VertexIdType> verticesRemainsToVisit;
  try {
    const auto& vertex = g.vertexById(source);
    if (vertex == nullptr) {
      throw std::invalid_argument("The source vertex with the id "
                                  + std::to_string(source)
                                  + " is not declared in the hypergraph.");
    }
    verticesRemainsToVisit.push_back(source);
  } catch (const std::out_of_range& oor) {
    std::cerr << "The source vertex with the id " << std::to_string(source)
             << " is not declared in the hypergraph." << std::endl;
  }

  // Init containers of already visited vertices and hyperarcs
  auto lastVertex = --g.verticesEnd();
  std::vector<bool> alreadyVisitedVertices((*lastVertex)->id() + 1,
                                           false);
  alreadyVisitedVertices[source] = true;

  auto lastHyperarc = --g.hyperarcsEnd();
  std::vector<bool> alreadyVisitedHyperarcs((*lastHyperarc)->id() + 1,
                                            false);

  // From source reachable vertices
  std::vector<hglib::VertexIdType> reachableVertices = {source};
  do {
    auto& nextVertex = verticesRemainsToVisit.front();
    verticesRemainsToVisit.pop_front();
    auto outArcsIt = g.outHyperarcsBegin(nextVertex);
    auto outArcsEnd = g.outHyperarcsEnd(nextVertex);
    for (; outArcsIt != outArcsEnd; ++outArcsIt) {
      const auto& outArcId = (*outArcsIt)->id();
      if (not alreadyVisitedHyperarcs[outArcId]) {
        alreadyVisitedHyperarcs[outArcId] = true;
        auto headsIt = g.headsBegin(outArcId);
        auto headsEnd = g.headsEnd(outArcId);
        for (; headsIt != headsEnd; ++headsIt) {
          const auto& headVertexId = (*headsIt)->id();
          if (not alreadyVisitedVertices[headVertexId]) {
            alreadyVisitedVertices[headVertexId] = true;
            verticesRemainsToVisit.push_back(headVertexId);
            reachableVertices.push_back(headVertexId);
          }
        }
      }
    }
  } while (not verticesRemainsToVisit.empty());
  return reachableVertices;
}
}  // namespace hglib
#endif  // HGLIB_CONNECTIVITY_HPP
