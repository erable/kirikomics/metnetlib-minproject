/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 04.08.17.
//

#ifndef HGLIB_NOPATHEXEPTION_H
#define HGLIB_NOPATHEXEPTION_H

namespace hglib {
/**
 * Exception class to signal that there is no path
 * between a source (set) and a target (set)
 *
 */
class no_path_exception : public std::exception {
 public:
  // Exception
  explicit no_path_exception() {}

  /// Return error message
  virtual const char* what() const throw() {
    return "No path exists";
  }
};
}  // namespace hglib
#endif  // HGLIB_NOPATHEXEPTION_H
