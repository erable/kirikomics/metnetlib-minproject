/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.06.17.
//

#ifndef HGLIB_UNDIRECTEDHYPEREDGE_H
#define HGLIB_UNDIRECTEDHYPEREDGE_H

#include "ArgumentsToCreateUndirectedHyperedge.h"
#include "UndirectedHyperedgeBase.h"
#include "hglib/utils/types.h"
#include "hglib/vertex/undirected/UndirectedVertex.h"

namespace hglib {
/**
 * UndirectedHyperedge class
 */
class UndirectedHyperedge final :
        public UndirectedHyperedgeBase<UndirectedVertex<UndirectedHyperedge>>{
  template <template <typename> class DirectedVertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedHypergraph;
  template <template <typename> class DirectedVertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedHypergraphInterface;
  template <template <typename> class Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedSubHypergraph;

  /// Alias for the vertex type
  typedef UndirectedVertex<UndirectedHyperedge> Vertex_t;

 public:
  /**
   * SpecificAttributes class
   *
   * A basic hyperedge has no specific arguments, but the class is needed to
   * call the addHyperedge function of a Undirected(Sub)Hypergraph.
   */
  class SpecificAttributes {};

 protected:
  /// UndirectedHyperedge constructor
  UndirectedHyperedge(int id,
                      std::shared_ptr<
                              GraphElementContainer<Vertex_t*>> vertices,
                      const SpecificAttributes& specificAttributes);
  /// Copy constructor
  UndirectedHyperedge(const UndirectedHyperedge& hyperedge);
  /// Get arguments to create this Hyperedge
  ArgumentsToCreateUndirectedHyperedge<
                    UndirectedHyperedge> argumentsToCreateHyperedge() const;

 public:
  /// Print the hyperedge
  void print(std::ostream& out) const;

 protected:
  /// Specific attributes (none)
  SpecificAttributes specificAttributes_;
};

}  // namespace hglib
#endif  // HGLIB_UNDIRECTEDHYPEREDGE_H
