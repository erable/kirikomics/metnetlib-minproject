/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.06.17.
//

#ifndef HGLIB_UNDIRECTEDHYPEREDGEBASE_H
#define HGLIB_UNDIRECTEDHYPEREDGEBASE_H

#include <memory>
#include <vector>

#include "hglib/utils/types.h"
#include "hglib/vertex/undirected/UndirectedVertex.h"

namespace hglib {

template <typename Hyperedge_t>
class UndirectedVertex;

/**
 * UndirectedHyperedgeBase class
 *
 * @tparam UndirectedVertex_t
 */
template <typename UndirectedVertex_t>
class UndirectedHyperedgeBase {
  template <template <typename> class Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedHypergraph;
  template <template <typename> class Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedSubHypergraph;

 protected:
  /// UndirectedHyperedgeBase constructor
  UndirectedHyperedgeBase(
          HyperedgeIdType id,
          std::shared_ptr<
                  GraphElementContainer<UndirectedVertex_t*>> vertices);
  /// Copy constructor
  UndirectedHyperedgeBase(const UndirectedHyperedgeBase& hyperedge);

 public:
  /// Get Id of the hyperedge
  HyperedgeIdType id() const;
  /// Print the hyperedge
  void print(std::ostream& out) const;

 protected:
  /// Remove a vertex from the hyperedge.
  void removeVertex(const UndirectedVertex_t& vertexToBeDeleted);
  /// Compare if this hyperedge has the provided vertex names
  bool compare(const std::vector<const UndirectedVertex_t*>& vertices) const;
  /// Assign an ID to the hyperedge
  void setId(const HyperedgeIdType& id);
  /// Print vertex names of the hyperedge
  void printVertices(std::ostream& out) const;
  /// Add a pointer to vertex to the vertex list
  void addVertex(UndirectedVertex_t* vertex);

 protected:
  /// Numerical identifier of the hyperedge
  HyperedgeIdType id_;
  /// Vertices of the hyperedge
  std::shared_ptr<GraphElementContainer<UndirectedVertex_t*>> vertices_;
};

template <typename UndirectedVertex_t>
std::ostream& operator<< (
        std::ostream& out,
        const UndirectedHyperedgeBase<UndirectedVertex_t>& hyperedge) {
  hyperedge.print(out);
  return out;
}

}  // namespace hglib

#include "UndirectedHyperedgeBase.hpp"
#endif  // HGLIB_UNDIRECTEDHYPEREDGEBASE_H
