/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#ifndef HGLIB_DIRECTEDHYPEREDGEBASE_H
#define HGLIB_DIRECTEDHYPEREDGEBASE_H

#include <memory>
#include <vector>
#include <tuple>
#include <utility>
#include <string>

#include "hglib/vertex/directed/DirectedVertex.h"
#include "hglib/utils/types.h"

namespace hglib {

template <typename Hyperarc_t>
class DirectedVertex;

/**
 * Base of a hyperarc which contains a set of tails and head vertices.
 * The type of a vertex can be specified by the template argument.
 *
 * @tparam DirectedVertex_t Type of a directed vertex
 */
template <typename DirectedVertex_t>
class DirectedHyperedgeBase {
  template <template <typename> class Vertex_t, typename Hyperarc_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedHypergraph;
  template <template <typename> class Vertex_t, typename Hyperarc_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedSubHypergraph;

 public:
  /// \brief Alias for the data container that stores the names of tails and
  /// heads of a hyperarc
  typedef std::pair<std::vector<std::string>, std::vector<std::string>>
          TailAndHeadNames;

 protected:
  /// HyperarcBase constructor
  DirectedHyperedgeBase(HyperedgeIdType id,
               std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> tails,
               std::shared_ptr<
                       GraphElementContainer<DirectedVertex_t*>> heads);
  /// Copy constructor
  DirectedHyperedgeBase(const DirectedHyperedgeBase& hyperarc);

 public:
  /// Get Id of the hyperarc
  HyperedgeIdType id() const;
  /// Print the hyperarc
  void print(std::ostream& out) const;

 protected:
  /// Remove a vertex from the tail vertices of the hyperarc.
  void removeTailVertex(const DirectedVertex_t& vertexToBeDeleted);
  /// Remove a vertex from the head vertices of the hyperarc.
  void removeHeadVertex(const DirectedVertex_t& vertexToBeDeleted);
  /// Compare if this hyperarc has the provided tail and head vertex names
  bool compare(const std::vector<const DirectedVertex_t*>& tails,
               const std::vector<const DirectedVertex_t*>& heads) const;
  /// Assign an ID to the hyperarc
  void setId(const HyperedgeIdType& id);
  /// Print tail and head vertex names of the hyperarc
  void printTailsAndHeads(std::ostream& out) const;
  /// Add a pointer to a tail vertex
  void addTailVertex(DirectedVertex_t* vertex);
  /// Add a pointer to a head vertex
  void addHeadVertex(DirectedVertex_t* vertex);

 protected:
  /// Numerical identifier of the hyperarc
  HyperedgeIdType id_;
  /// Tail vertices of the hyperarc
  std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> tails_;
  /// Head vertices of the hyperarc
  std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> heads_;
};

template <typename DirectedVertex_t>
std::ostream& operator<< (
        std::ostream& out,
        const DirectedHyperedgeBase<DirectedVertex_t>& hyperarc) {
  hyperarc.print(out);
  return out;
}
}  // namespace hglib

#include "DirectedHyperedgeBase.hpp"
#endif  // HGLIB_DIRECTEDHYPEREDGEBASE_H
