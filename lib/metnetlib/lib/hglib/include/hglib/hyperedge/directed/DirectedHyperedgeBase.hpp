/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#include "DirectedHyperedgeBase.h"

#include <algorithm>
#include <memory>
#include <vector>
#include <set>

#include "filtered_vector.h"

namespace hglib {

/**
 * HyperarcBase constructor
 *
 * @param id Identifier of the hyperarc
 * @param tails Tail vertices of the hyperarc
 * @param heads Head vertices of the hyperarc
 */
template <typename DirectedVertex_t>
DirectedHyperedgeBase<DirectedVertex_t>::
DirectedHyperedgeBase(HyperedgeIdType id,
             std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> tails,
             std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> heads) :
        id_(id), tails_(tails), heads_(heads) {}


/// Copy constructor
template <typename DirectedVertex_t>
DirectedHyperedgeBase<DirectedVertex_t>::
DirectedHyperedgeBase(const DirectedHyperedgeBase& hyperarc) :
        id_(hyperarc.id_) {
  tails_ = create_filtered_vector<DirectedVertex_t*>();
  heads_ = create_filtered_vector<DirectedVertex_t*>();
}

/**
 * Get Id of the hyperarc
 *
 * @return Id of the hyperarc
 */
template <typename DirectedVertex_t>
HyperedgeIdType DirectedHyperedgeBase<DirectedVertex_t>::
id() const {
  return id_;
}


/**
 * Print the hyperarc
 *
 * Call the print method of the derived class.
 *
 * @param out std::ostream
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
print(std::ostream& out) const {
  static_cast<typename DirectedVertex_t::HyperarcType const&>(*this).print(
          out);
}

/* ****************************************************************************
 * ****************************************************************************
 *                        Protected methods
 * ****************************************************************************
 */

/**
 * Remove a vertex from the tail vertices of the hyperarc.
 *
 * @param vertexToBeDeleted Vertex reference
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
removeTailVertex(const DirectedVertex_t& vertexToBeDeleted) {
  // We suppose that there is at most one occurrence of the given vertex
  // in the tails.
  auto newEnd = std::remove(tails_->data(),
                            tails_->data() + tails_->size(),
                            &vertexToBeDeleted);
  // new end is different from
  if (newEnd != (tails_->data() + tails_->size())) {
    tails_->resize(tails_->size() - 1);
  }
}


/**
 * Remove a vertex from the head vertices of the hyperarc.
 *
 * @param vertexToBeDeleted Vertex reference
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
removeHeadVertex(const DirectedVertex_t& vertexToBeDeleted) {
  // We suppose that there is at most one occurrence of the given vertex
  // in the heads.
  auto newEnd = std::remove(heads_->data(),
                            heads_->data() + heads_->size(),
                            &vertexToBeDeleted);
  // new end is different from
  if (newEnd != (heads_->data() + heads_->size())) {
    heads_->resize(heads_->size() - 1);
  }
}

/**
 * Compare if this hyperarc has the provided tail and head vertex names
 *
 * @param tails_heads names of tail and head vertices
 * @return true if the hyperarc has the same tail and head vertex names,
 * false otherwise
 */
template <typename DirectedVertex_t>
bool DirectedHyperedgeBase<DirectedVertex_t>::
compare(const std::vector<const DirectedVertex_t*>& tails,
        const std::vector<const DirectedVertex_t*>& heads) const {
  // Compare first the number of tail and head vertices (names).
  // If different, return false.
  // Compare the names of the tail vertices by adding the provided tail vertex
  // names to a set, and then removing the tail vertex names of this hyperarc.
  // If the set is empty this hyperarc has the same tail vertex names as the
  // provided ones. Otherwise return false. Repeat the procedure with the
  // names of the head vertices. Return true if the set is empty.
  if (tails.size() != tails_->size() or heads.size() != heads_->size()) {
    return false;
  }
  // Compare names of tail vertices
  std::set<VertexIdType> vertexIds;
  for (auto& tail : tails) {
    vertexIds.insert(tail->id());
  }
  for (const auto& tail : *(tails_.get())) {
    vertexIds.erase(tail->id());
  }
  if (not vertexIds.empty()) {
    return false;
  }
  // Compare names of head vertices
  for (auto& head : heads) {
    vertexIds.insert(head->id());
  }
  for (const auto& head : *(heads_.get())) {
    vertexIds.erase(head->id());
  }
  return vertexIds.empty();
}


/**
 * Assign an ID to the hyperarc
 *
 * @param id Id to be assigned
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
setId(const HyperedgeIdType& id) {
  id_ = id;
}


/**
 * Print tail and head vertices of the hyperarc
 *
 * Prints tail and head vertices of the hyperarc in the following format:
 * Tail_1 + ... + Tail_n -> Head_1 + ... + Head_m
 *
 * @param out std::ostream
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
printTailsAndHeads(std::ostream& out) const {
  // print tails
  const char *padding = "";
  for (const auto &tail : *(tails_.get())) {
    out << padding << *tail;
    padding = " + ";
  }
  out << " -> ";
  // print heads
  padding = "";
  for (const auto &head : *(heads_.get())) {
    out << padding << *head;
    padding = " + ";
  }
}


/**
 * Add a pointer to a tail vertex
 *
 * @param vertex Pointer to vertex
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
addTailVertex(DirectedVertex_t* vertex) {
  tails_->push_back(vertex);
}


/**
 * Add a pointer to a head vertex
 *
 * @param vertex Pointer to vertex
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
addHeadVertex(DirectedVertex_t* vertex) {
  heads_->push_back(vertex);
}
}  // namespace hglib
