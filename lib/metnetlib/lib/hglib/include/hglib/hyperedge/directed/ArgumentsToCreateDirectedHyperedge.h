/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 20.09.17.
//

#ifndef HGLIB_ARGUMENTSTOCREATEDIRECTEDHYPEREDGE_H
#define HGLIB_ARGUMENTSTOCREATEDIRECTEDHYPEREDGE_H

#include <vector>
#include <utility>

#include "hglib/utils/types.h"

namespace hglib {

/**
 * A wrapper class that holds the arguments needed to create a
 * hyperarc of the template parameter type.
 *
 * Every hyperarc type has tail and head vertices, and attributes that are
 * specific to the hyperarc type (possibly empty).
 *
 * @tparam Hyperarc_t
 */
template<typename Hyperarc_t>
class ArgumentsToCreateDirectedHyperedge {
  friend Hyperarc_t;

  /// Alias for data structure to provide tail and head ids
  typedef std::pair<std::vector<hglib::VertexIdType>,
                    std::vector<hglib::VertexIdType>> TailAndHeadIds;

 protected:
  /// Constructor
  ArgumentsToCreateDirectedHyperedge(
          const TailAndHeadIds& tailAndHeadIds,
          const typename Hyperarc_t::SpecificAttributes& specificAttributes);

 public:
  /// Get tail and head ids
  const TailAndHeadIds& tailAndHeadIds() const;
  /// Get specific arguments
  const typename Hyperarc_t::SpecificAttributes& specificAttributes() const;
  /// Set tail and head ids
  void setTailsAndHeadIds(const TailAndHeadIds&);
  /// Set arguments that are specific to the hyperarc type (Hyperarc_t)
  void setSpecificArguments(const typename Hyperarc_t::SpecificAttributes&);

 protected:
  /// Tail and head ids
  TailAndHeadIds tailAndHeadIds_;
  /// Specific arguments for the hyperarc type
  typename Hyperarc_t::SpecificAttributes specificAttributes_;
};
}  // namespace hglib

#include "ArgumentsToCreateDirectedHyperedge.hpp"
#endif  // HGLIB_ARGUMENTSTOCREATEDIRECTEDHYPEREDGE_H
