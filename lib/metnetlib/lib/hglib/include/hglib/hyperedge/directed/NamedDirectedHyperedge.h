/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.06.17.
//

#ifndef HGLIB_NAMEDDIRECTEDHYPEREDGE_H
#define HGLIB_NAMEDDIRECTEDHYPEREDGE_H

#include <memory>
#include <tuple>
#include <utility>
#include <vector>
#include <string>

#include "DirectedHyperedgeBase.h"
#include "hglib/vertex/directed/DirectedVertex.h"
#include "hglib/utils/types.h"
#include "ArgumentsToCreateDirectedHyperedge.h"

namespace hglib {
/**
 * NamedHyperarc class
 */
class NamedDirectedHyperedge final :
        public DirectedHyperedgeBase<DirectedVertex<NamedDirectedHyperedge>>{
  template <template <typename> class DirectedVertex_t, typename Hyperarc_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedHypergraphInterface;
  template <template <typename> class Vertex_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
  friend class DirectedHypergraph;
  template <template <typename> class Vertex_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
  friend class DirectedSubHypergraph;

  /// Alias for the vertex type
  typedef DirectedVertex<NamedDirectedHyperedge> DirectedVertex_t;

 public:
  /**
   * SpecificAttributes class
   *
   * A named hyperarc has a name as specific arguments. The class is needed to
   * call the addHyperarc function of a Directed(Sub)Hypergraph.
   */
  class SpecificAttributes {
   public:
    /// Constructor
    SpecificAttributes(const std::string& name) : name_(name) {}
    /// Name of the hyperarc
    std::string name_;
  };

 protected:
  /// NamedHyperarc constructor
  NamedDirectedHyperedge(
          int id,
          std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> tails,
          std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> heads,
          const SpecificAttributes& specificAttributes);
  /// Copy constructor
  NamedDirectedHyperedge(const NamedDirectedHyperedge& hyperarc);
  /// Get arguments to create this NamedHyperarc
  ArgumentsToCreateDirectedHyperedge<
          NamedDirectedHyperedge> argumentsToCreateHyperarc() const;

 public:
  /// Get name of the hyperedge
  inline std::string name() const;
  /// Print the named hyperarc
  void print(std::ostream& out) const;

 protected:
  /**
   * \brief Compare if the provided name, and the tail and head vertex names
   * are identical to the name, tail and head vertex names of this
   * NamedHyperarc
   */
  bool compare(const std::vector<const DirectedVertex_t*>& tails,
               const std::vector<const DirectedVertex_t*>& heads,
               const std::string& name) const;

 protected:
  /// Specific attributes (name)
  SpecificAttributes specificAttributes_;
};

std::string NamedDirectedHyperedge::name() const {
  return specificAttributes_.name_;
}

}  // namespace hglib


#endif  // HGLIB_NAMEDDIRECTEDHYPEREDGE_H
