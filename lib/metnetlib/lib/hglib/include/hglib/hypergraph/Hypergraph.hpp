/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#include "Hypergraph.h"

#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <memory>

#include "filtered_vector.h"

#include "hglib/utils/HasInsertMemberFunction.h"
#include "hglib/utils/types.h"

namespace hglib {

/**
 * Hypergraph constructor
 *
 * @param allowMultiHyperedges flag to signal if multi-hyperarcs are allowed
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
Hypergraph(bool allowMultiHyperedges) :
        HypergraphInterface<VertexTemplate_t, Hyperedge_t, VertexProperty,
                EdgeProperty, HypergraphProperty>(),
        vertexMaxId_(-1),
        allowMultiHyperedges_(allowMultiHyperedges) {
  // create vertices_
  vertices_ = create_filtered_vector<Vertex_t*>(
          create_filtered_vector<Vertex_t*>(),
          std::make_unique<NullptrFilter<Vertex_t*>>());

  // create vertexProperties_
  vertexProperties_ = create_filtered_vector<VertexProperty*>(
          create_filtered_vector<VertexProperty*>(),
          std::make_unique<NullptrFilter<VertexProperty*>>());
  // Create HypergraphProperty object
  hypergraphProperty_ = new HypergraphProperty();
}

/**
 * Hypergraph copy constructor
 *
 * @param allowMultiHyperedges flag to signal if multi-hyperarcs are allowed
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
    typename VertexProperty, typename EdgeProperty,
    typename HypergraphProperty>
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
    HypergraphProperty>::Hypergraph(const Hypergraph& rhs) :
    vertexMaxId_(-1),
    allowMultiHyperedges_(rhs.allowMultiHyperedges_) {
  try {
    // create vertices_
    vertices_ = create_filtered_vector<Vertex_t*>(
            create_filtered_vector<Vertex_t*>(),
            std::make_unique<NullptrFilter<Vertex_t*>>());
    // create vertexProperties_
    vertexProperties_ = create_filtered_vector<VertexProperty*>(
            create_filtered_vector<VertexProperty*>(),
            std::make_unique<NullptrFilter<VertexProperty*>>());

    // copy vertices and associated vertex properties
    for (std::size_t i = 0; i < rhs.vertices_->size(); ++i) {
      Vertex_t* vertex = rhs.vertices_->operator[](i);
      ++vertexMaxId_;
      // vertex was deleted in directedHypergraph
      if (vertex == nullptr) {
        vertices_->push_back(nullptr);
        vertexProperties_->push_back(nullptr);
      } else {
        // create new vertex
        Vertex_t* vertexCopy = new Vertex_t(*vertex);
        vertices_->push_back(vertexCopy);
        // Copy vertex property
        VertexProperty* vertexPropertyCopy = new VertexProperty(
                *(rhs.vertexProperties_->operator[](i)));
        vertexProperties_->push_back(vertexPropertyCopy);
      }
    }
    // copy graph properties
    hypergraphProperty_ =
            new HypergraphProperty(*(rhs.hypergraphProperty_));
  } catch (std::bad_alloc &e) {
    for (auto& vertex : *(vertices_.get())) {
      delete vertex;
    }
    for (auto& vertexProperty : *(vertexProperties_.get())) {
      delete vertexProperty;
    }
    delete hypergraphProperty_;
    throw;
  }
}


/**
 * Hypergraph destructor
 *
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
~Hypergraph() {
  for (auto& vertex : *(vertices_.get())) {
    delete vertex;
  }
  for (auto& vertexProperty : *(vertexProperties_.get())) {
    delete vertexProperty;
  }
  delete hypergraphProperty_;
}


/// Assignment operator
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>& Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
operator=(const Hypergraph& source) {
  // check for self assignment
  if (this != &source) {
    // deallocate vertices_
    for (auto &vertex : *(vertices_.get())) {
      delete vertex;
    }
    vertices_->clear();

    // deallocate vertexProperties_
    for (auto& vProp : *(vertexProperties_.get())) {
      delete vProp;
    }
    vertexProperties_->clear();

    // deallocate hypergraph property
    delete hypergraphProperty_;

    // re-init the vertex max Id
    vertexMaxId_ = -1;

    // copy of flag
    allowMultiHyperedges_ = source.allowMultiHyperedges_;

    // copy vertices and associated vertex properties
    try {
      for (std::size_t i = 0; i < source.vertices_->size(); ++i) {
        Vertex_t* vertex = source.vertices_->operator[](i);
        ++vertexMaxId_;
        // vertex was deleted in directedHypergraph
        if (vertex == nullptr) {
          vertices_->push_back(nullptr);
          vertexProperties_->push_back(nullptr);
        } else {
          // Get arguments to build a vertex
          /*auto args = vertex->getArgsToBuildDirectedVertex();
          // create new vertex
          callAddVertex(args);*/
          Vertex_t* vertexCopy = new Vertex_t(*vertex);
          vertices_->push_back(vertexCopy);
          // Copy vertex property
          VertexProperty* vertexPropertyCopy = new VertexProperty(
                  *source.vertexProperties_->operator[](i));
          vertexProperties_->push_back(vertexPropertyCopy);
        }
      }

      // copy graph properties
      hypergraphProperty_ = new HypergraphProperty(
              *(source.hypergraphProperty_));
    } catch (std::bad_alloc &e) {
      for (auto& vertex : *(vertices_.get())) {
        delete vertex;
      }
      for (auto& vertexProperty : *(vertexProperties_.get())) {
        delete vertexProperty;
      }
      delete hypergraphProperty_;
      throw;
    }
  }
  return *this;
}


/* ****************************************************************************
 * ****************************************************************************
 *                          Public methods
 * ****************************************************************************
 *****************************************************************************/

/**
 * Number of vertices already added to the hypergraph.
 *
 * The return value does not necessary corresponds to the number of vertices
 * present in the hypergraph, e.g. after the removal of a vertex the return
 * value of this function does not change. The return value of this function
 * corresponds to the number of vertices present in the hypergraph if any
 * vertex was removed and when the function resetVertexIds/resetIds was
 * called.
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
size_t Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
vertexContainerSize() const {
  return vertices_->size();
}

/**
 * Number of vertices in the hypergraph
 *
 * Complexity: Linear
 *
 * @return number of vertices in the hypergraph
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
size_t Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
nbVertices() const {
  size_t nb = 0;
  for (auto& vertex : *(vertices_.get())) {
    if (vertex != nullptr) {
      ++nb;
    }
  }
  return nb;
}


/**
 * Search vertex list for a vertex with the provided name
 *
 * @param vertexName name of the vertex to find
 * @return pointer to vertex if found, nullptr otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::Vertex_t*
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
vertexByName(const std::string& vertexName) const {
  for (const auto& vertex : *(vertices_.get())) {
    if (vertex != nullptr and vertex->name_.compare(vertexName) == 0) {
      return vertex;
    }
  }
  return nullptr;
}


/**
 * Search vertex list for a vertex with the provided id
 *
 * Complexity: Constant
 *
 * @param vertexId if of the vertex to find
 * @return pointer to vertex if found, nullptr if vertexId is between 0
 * and maximum attributed vertex id in this hypergraph, throw out_of_range
 * exception otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::Vertex_t*
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
vertexById(const VertexIdType & vertexId) const {
  try {
    return vertices_->at(vertexId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in Hypergraph::vertexById: "
              "index = " << vertexId << "; min = 0; max = " <<
              vertices_->size() << '\n';
    throw;
  }
}

/**
 * Function to add a vertex.
 *
 * A vertex has at least a name and can have additional arguments that are
 * specific to the vertex type. As this function do not require a vertex name
 * a default name will be created.
 *
 * @param attributes specific attributes of the vertex type
 * @return const pointer to added vertex object if the vertex was successfully
 * added to the hypergraph. Throws an exception otherwise, e.g. when a vertex
 * with the same name exists already in the hypergraph.
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::Vertex_t*
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
addVertex(const typename Vertex_t::SpecificAttributes& attributes) {
  // As we do not know if the name was passed as an argument, we first create
  // the vertex and check then if there is already vertex of the same name. If
  // yes an exception is thrown, otherwise the vertex is added to the
  // hpergraph.
  auto vertex = new Vertex_t(++vertexMaxId_, attributes);
  // Check if a vertex with the same name exists
  for (auto& v : *(vertices_.get())) {
    if (v != nullptr and v->name().compare(vertex->name()) == 0) {
      --vertexMaxId_;
      auto vname = vertex->name();
      delete vertex;
      throw std::invalid_argument("Vertex " + vname +
                                  " already exists in this hypergraph.");
    }
  }
  vertices_->push_back(vertex);

  // build entry in vertexProperties
  auto vertexProperty = new VertexProperty();
  vertexProperties_->push_back(vertexProperty);
  return vertex;
}


/**
 * Function to add a vertex.
 *
 * A vertex has at least a name and can have additional arguments that are
 * specific to the vertex type.
 *
 * @param name vertex name
 * @param attributes specific attributes of the vertex type
 * @return const pointer to added vertex object if the vertex was successfully
 * added to the hypergraph. Throws an exception otherwise, e.g. when a vertex
 * with the same name exists already in the hypergraph.
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::Vertex_t*
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
addVertex(const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes) {
  // Check if a vertex with the same name exists
  for (auto& v : *(vertices_.get())) {
    if (v->name().compare(name) == 0) {
      throw std::invalid_argument("Vertex " + name +
                                  " already exists in this hypergraph.");
    }
  }
  auto vertex = new Vertex_t(++vertexMaxId_, name, attributes);
  vertices_->push_back(vertex);

  // build entry in vertexProperties
  auto vertexProperty = new VertexProperty();
  vertexProperties_->push_back(vertexProperty);
  return vertex;
}


/**
 * Establish consecutive vertex ids
 *
 * After deleting vertices from the hypergraph, the vertex ids
 * are not consecutive anymore. This function reassigns
 * consecutive ids to the vertices.
 *
 * Complexity: Linear
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
void Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
resetVertexIds() {
  // Notify subgraphs of this change
  this->NotifyObservers(ObservableEvent::RESET_VERTEX_IDS);
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(vertices_->data(),
                                     vertices_->data() + vertices_->size(),
                                     static_cast<Vertex_t*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (vertices_->data() + vertices_->size()) - posFirstNullptr;
  // Resize
  vertices_->resize(vertices_->size() - nbNullptr);

  // assign new Ids
  hglib::VertexIdType id(-1);
  for (auto it = vertices_->begin(); it != vertices_->end(); ++it) {
    (*it)->setId(++id);  // assign new Id
  }

  // set vertexMaxId_ to the highest assigned id
  vertexMaxId_ = id;

  // remove nullptr entries in vertexProperties_
  removeNullEntriesFromVertexProperties();
}


/**
 * Get arguments to create the vertex
 *
 * @param vertexId Id of the vertex
 * @return a variadic tuple with the arguments needed to add
 * the vertex to a hypergraph.
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
ArgumentsToCreateVertex<typename Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::Vertex_t>
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
argumentsToCreateVertex(const hglib::VertexIdType& vertexId) const {
  try {
    const auto &vertex = vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called Hypergraph::"
                                       "argumentsToCreateVertex "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->argumentsToCreateVertex();
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Get properties of the vertex with the provided id.
 *
 * Complexity: Constant
 *
 * Throws an out_of_range exception if the vertexId is smaller than 0
 * or greater than the highest vertex id attributed in this hypergraph.
 *
 * @param vertexId id of the vertex
 * @return pointer to property struct of the vertex if the vertex exists,
 * nullptr otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const VertexProperty* Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
getVertexProperties(const VertexIdType& vertexId) const {
  try {
    return vertexProperties_->at(vertexId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in Hypergraph::"
            "getVertexProperties: "
            "index = " << vertexId << "; min = 0; max = " <<
              vertexProperties_->size() << '\n';
    throw;
  }
}

/**
 * Get properties of the vertex with the provided id.
 *
 * Complexity: Constant
 *
 * Throws an out_of_range exception if the vertexId is smaller than 0
 * or greater than the highest vertex id attributed in this hypergraph.
 *
 * @param vertexId id of the vertex
 * @return pointer to property struct of the vertex if the vertex exists,
 * nullptr otherwise
 *
 * Non-const version of getVertexProperties
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
VertexProperty* Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
getVertexProperties_(const VertexIdType& vertexId) const {
  try {
    return vertexProperties_->at(vertexId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in Hypergraph::"
            "getVertexProperties_: index = " << vertexId << "; min = 0; max = "
              << vertexProperties_->size() << '\n';
    throw;
  }
}


/**
 * Get properties of the hypergraph.
 *
 * Complexity: Constant
 *
 * @return Pointer to HypergraphProperty struct/class.
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const HypergraphProperty* Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
getHypergraphProperties() const {
  return hypergraphProperty_;
}


/**
 * Get properties of the hypergraph.
 *
 * Complexity: Constant
 *
 * @return Pointer to HypergraphProperty struct/class.
 *
 * Non-const version of getHypergraphProperties()
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
HypergraphProperty* Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
getHypergraphProperties_() const {
  return const_cast<HypergraphProperty*>(getHypergraphProperties());
}


/**
 * Const vertex iterator pointing to begin of vertices
 *
 * @return Const vertex iterator pointing to begin of vertices
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::vertex_iterator
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
verticesBegin() const {
  return vertices_->cbegin();
}

/**
 * Const vertex iterator pointing to end of vertices
 *
 * @return Const vertex iterator pointing to end of vertices
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::vertex_iterator
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
verticesEnd() const {
  return vertices_->cend();
}


/**
 * Const vertex iterator pointing to begin and end of vertices
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
std::pair<typename Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::vertex_iterator,
        typename Hypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, EdgeProperty,
                HypergraphProperty>::vertex_iterator>
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
verticesBeginEnd() const {
  return std::make_pair(vertices_->cbegin(), vertices_->cend());
}


/**
 * Return a reference to the container of vertices.
 *
 * Can be used in a ranged based for loop as follows
 \verbatim
 for (const auto& vertex : graph.vertices() {
    ...
 }
 \endverbatim
 *
 * @return Reference to vertices container
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::VertexContainer&
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
vertices() const {
  return *(vertices_.get());
}

/* ****************************************************************************
 * ****************************************************************************
 *                          Protected methods
 * ****************************************************************************
 * ***************************************************************************/

/**
 * Search vertex list for a vertex with the provided name
 *
 * Complexity: Linear
 *
 * @param vertexName name of the vertex to find
 * @return pointer to vertex if found, nullptr otherwise
 *
 * Non-const version of vertexByName
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::Vertex_t*
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
vertexByName_(const std::string& vertexName) const {
  return const_cast<Vertex_t*>(vertexByName(vertexName));
}


/**
 * Remove nullptr entries from vector of vertex properties.
 *
 * Complexity: Linear
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
void Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
removeNullEntriesFromVertexProperties() {
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(vertexProperties_->data(),
                                     vertexProperties_->data() +
                                             vertexProperties_->size(),
                                     static_cast<VertexProperty*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (vertexProperties_->data() + vertexProperties_->size()) -
          posFirstNullptr;
  // Resize
  vertexProperties_->resize(vertexProperties_->size() - nbNullptr);
}


/**
 * Get Ids of vertices for given vertex names
 *
 * @param vertexNames Vector of vertex names
 * @return vector of ids of vertices
 * @throw Throws an invalid_argument exception if no vertex with a given name
 * is found in the hypergraph.
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
std::vector<hglib::VertexIdType> Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
getVertexIdsFromNames(const std::vector<std::string>& vertexNames) const {
  std::vector<hglib::VertexIdType> ids;
  for (const auto& vertexName : vertexNames) {
    const Vertex_t* vertex = vertexByName(vertexName);
    if (vertex == nullptr) {
      throw std::invalid_argument("Vertex " + vertexName +
                                  " is not declared in this hypergraph.");
    } else {
      ids.push_back(vertex->id());
    }
  }
  return ids;
}


/// Set vertex and vertex property entry at given position to nullptr
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
void Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
setVertexAndVertexPropertyEntryToNullptr(const VertexIdType& idx) {
  // assign NULL in vertices_ at the index that corresponds
  // to the id of the vertex
  delete vertices_->operator[](idx);
  vertices_->operator[](idx) = nullptr;

  // assign NULL in vertexProperties_ at the index that corresponds
  // to the id of the vertex
  delete vertexProperties_->operator[](idx);
  vertexProperties_->operator[](idx) = nullptr;
}


/// Return pointer to vertices container
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::VertexContainerPtr
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
getRootVerticesContainerPtr() const {
  return vertices_;
}
}  // namespace hglib
