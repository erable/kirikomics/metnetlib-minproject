/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#ifndef HGLIB_DIRECTEDHYPERGRAPH_H
#define HGLIB_DIRECTEDHYPERGRAPH_H

#include <memory>
#include <type_traits>

#include "filtered_vector.h"

#include "DirectedHypergraphInterface.h"
#include "hglib/hypergraph/Hypergraph.h"
#include "hglib/utils/observation/Observable.h"
#include "hglib/hyperedge/directed/ArgumentsToCreateDirectedHyperedge.h"
#include "hglib/hyperedge/directed/DirectedHyperedgeBase.h"
#include "hglib/hyperedge/directed/DirectedHyperedge.h"
#include "hglib/utils/IsParentOf.h"
#include "hglib/utils/types.h"

namespace hglib {

 /**
  * DirectedHypergraph class.
  *
  * @tparam Vertex_t
  * @tparam Hyperarc_t
  * @tparam VertexProperty
  * @tparam EdgeProperty
  * @tparam HypergraphProperty
  */
template <template <typename> class VertexTemplate_t = DirectedVertex,
          typename Hyperarc_t = DirectedHyperedge,
          typename VertexProperty = emptyProperty,
          typename HyperarcProperty = emptyProperty,
          typename HypergraphProperty = emptyProperty>
class DirectedHypergraph : public DirectedHypergraphInterface<
        VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
        HypergraphProperty>,
                                public Hypergraph<VertexTemplate_t,
                                        Hyperarc_t, VertexProperty,
                                        HyperarcProperty, HypergraphProperty> {
  // Check that the vertex and hyperarc type is correct
  // A directed vertex is needed.
  static_assert(std::is_base_of<DirectedVertex<Hyperarc_t>,
                    VertexTemplate_t<Hyperarc_t>>::value,
                "A vertex of a directed hypergraph must be derived from "
                    "DirectedVertex<T>, where T is derived from "
                    "DirectedHyperedgeBase, e.g. "
                    "DirectedVertex<DirectedHyperedge> or "
                    "DirectedVertex<NamedDirectedHyperedge>");
  // A hyperarc is needed
  static_assert(
      std::is_base_of<DirectedHyperedgeBase<VertexTemplate_t<Hyperarc_t>>,
          Hyperarc_t>::value, "A hyperarc of a directed hypergraph must be "
          "derived from DirectedHyperedgeBase, e.g. use as second template "
          "parameter DirectedHyperedge or NamedDirectedHyperedge");


 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename DirectedHypergraphInterface<VertexTemplate_t,
          Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::Vertex_t;
  /// Alias for Hyperarc type
  using Hyperarc_type = Hyperarc_t;
  /// Alias for Hyperarc property type
  using HyperarcProperty_type = HyperarcProperty;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperarc_t ptr
  using HyperarcContainer = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty,
          HyperarcProperty, HypergraphProperty>::HyperarcContainer;
  ///
  using vertex_iterator = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::vertex_iterator;
  ///
  using hyperarc_iterator = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::hyperarc_iterator;
  /// \brief Alias for the data container that stores the names of tails and
  /// heads of a hyperarc
  typedef std::pair<std::vector<std::string>, std::vector<std::string>>
          TailAndHeadNames;
  /// \brief Alias for the data container that stores the ids of tails and
  /// heads of a hyperarc
  typedef std::pair<std::vector<VertexIdType>, std::vector<VertexIdType>>
          TailAndHeadIds;
  /// Alias for unique_ptr of GraphElementContainer<Hyperarc_t*>
  using HyperarcContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::HyperarcContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<HyperarcProperty*>
  using HyperarcPropertyContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::HyperarcPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;

  /* **************************************************************************
   * **************************************************************************
   *                        Constructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// DirectedHypergraph constructor
  explicit DirectedHypergraph(bool allowMultiHyperarcs = true);
  /// Copy constructor
  explicit DirectedHypergraph(
          const DirectedHypergraph& directedHypergraph);
  /// DirectedHypergraph destructor
  virtual ~DirectedHypergraph();

  /// Assignment operator
  DirectedHypergraph& operator=(const DirectedHypergraph& source);

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if multi-hyperarcs are allowed in this hypergraph
  bool allowMultiHyperarcs() const override;

  /* **************************************************************************
  * **************************************************************************
  *                        DirectedHyperedge access
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Number of hyperarcs in the hypergraph
  size_t nbHyperarcs() const override;
  /// Highest assigned hyperarc id of the hypergraph
  size_t hyperarcContainerSize() const override;

  /**
    * \brief Add a hyperarc with the provided tails, heads, and additional
    * parameters (if needed to create a hyperarc of type Hyperarc_t) to the
    * hypergraph
    */
  const Hyperarc_t* addHyperarc(
          decltype(hglib::NAME),
          const TailAndHeadNames& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {})
    override;
  /**
  * \brief Add a hyperarc with the provided tail ids, head ids, and additional
  * parameters (if needed to create a hyperarc of type Hyperarc_t) to the
  * hypergraph
  */
  const Hyperarc_t* addHyperarc(
          const TailAndHeadIds& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {})
    override;

  /// Get arguments to create the hyperarc
  ArgumentsToCreateDirectedHyperedge<Hyperarc_t> argumentsToCreateHyperarc(
          const hglib::HyperedgeIdType& hyperarcId) const override;

  /// Remove a hyperarc with the given id from the hypergraph.
  void removeHyperarc(const HyperedgeIdType& hyperarcId) override;

  /// Search hyperarc list for an hyperarc with the provided id
  const Hyperarc_t* hyperarcById(const HyperedgeIdType & hyperarcId) const
    override;

  /// Const hyperarc iterator pointing to begin of the hyperarcs
  hyperarc_iterator hyperarcsBegin() const override;
  /// Const hyperarc iterator pointing to end of the hyperarcs
  hyperarc_iterator hyperarcsEnd() const override;
  /// \brief Pair of hyperarc iterators pointing to begin and end of the
  /// hyperarcs
  std::pair<hyperarc_iterator, hyperarc_iterator> hyperarcsBeginEnd() const
    override;
  /// Return a reference to the container of hyperarcs.
  const HyperarcContainer& hyperarcs() const override;

  // Tails
  /// Return number of tail vertices in the hyperarc with given id.
  size_t nbTailVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Check if hyperarc with given id has tail vertices.
  bool hasTailVertices(const HyperedgeIdType & hyperarcId) const override;
  /// \brief Const vertex iterator pointing to the begin of the tail vertices
  /// of the hyperarc with the given id
  vertex_iterator tailsBegin(const HyperedgeIdType & hyperarcId) const
    override;
  /// \brief Const vertex iterator pointing to the end of the tail vertices of
  /// the hyperarc with the given id
  vertex_iterator tailsEnd(const HyperedgeIdType & hyperarcId) const override;
  /// \brief Pair of vertex iterators pointing to begin and end of the
  /// tails
  std::pair<vertex_iterator, vertex_iterator> tailsBeginEnd(
          const HyperedgeIdType & hyperarcId) const override;
  /// Get container of tail vertices of given hyperarc id
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> tails(
          const HyperedgeIdType& hyperarcId) const override;

  // Heads
  /// Return number of head vertices in the hyperarc with given id.
  size_t nbHeadVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Check if hyperarc with given id has head vertices.
  bool hasHeadVertices(const HyperedgeIdType & hyperarcId) const override;
  /// \brief Const vertex iterator pointing to the begin of the head vertices
  /// of the hyperarc with the given id
  vertex_iterator headsBegin(const HyperedgeIdType & hyperarcId) const
    override;
  //// \brief Const vertex iterator pointing to the end of the head vertices
  /// of the hyperarc with the given id
  vertex_iterator headsEnd(const HyperedgeIdType & hyperarcId) const override;
  /// \brief Pair of vertex iterators pointing to begin and end of the
  /// heads
  std::pair<vertex_iterator, vertex_iterator> headsBeginEnd(
          const HyperedgeIdType & hyperarcId) const override;
  /// Get container of head vertices of given hyperarc id
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> heads(
          const HyperedgeIdType& hyperarcId) const override;

  /// Establish consecutive hyperarc ids
  void resetHyperarcIds();
  /// Establish consecutive vertex and hyperarc ids
  void resetIds();

  /* **************************************************************************
   * **************************************************************************
   *                       DirectedVertex access
   * **************************************************************************
   * *************************************************************************/
 public:
  // incoming hyperarcs
  /// Number of incoming hyperarcs of the vertex with given id
  size_t inDegree(const VertexIdType& vertexId) const override;
  /// Check if vertex has incoming hyperarcs
  bool hasInHyperarcs(const VertexIdType& vertexId) const override;
  /// \brief Const hyperarc iterator pointing to the begin of the incoming
  /// hyperarcs of the vertex with the given id
  hyperarc_iterator inHyperarcsBegin(const VertexIdType& vertexId) const
    override;
  /// \brief Const hyperarc iterator pointing to the end of the incoming
  /// hyperarcs of the vertex with the given id
  hyperarc_iterator inHyperarcsEnd(const VertexIdType& vertexId) const
    override;
  /// \brief Pair of hyperarc iterators pointing to begin and end of the
  /// incoming hyperarcs
  std::pair<hyperarc_iterator, hyperarc_iterator> inHyperarcsBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get container of incoming hyperarcs of given vertex id
  std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> inHyperarcs(
      const VertexIdType& vertexId) const override;

  // out-going hyperarcs
  /// Number of outgoing hyperarcs of the vertex with given id
  size_t outDegree(const VertexIdType& vertexId) const override;
  /// Check if vertex has outgoing hyperarcs
  bool hasOutHyperarcs(const VertexIdType& vertexId) const override;
  /// \brief Const hyperarc iterator pointing to the begin of the outgoing
  /// hyperarcs of the vertex with the given id
  hyperarc_iterator outHyperarcsBegin(const VertexIdType& vertexId) const
    override;
  /// \brief Const hyperarc iterator pointing to the end of the outgoing
  /// hyperarcs of the vertex with the given id
  hyperarc_iterator outHyperarcsEnd(const VertexIdType& vertexId) const
    override;
  /// \brief Pair of hyperarc iterators pointing to begin and end of the
  /// outgoing hyperarcs
  std::pair<hyperarc_iterator, hyperarc_iterator> outHyperarcsBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get container of outgoing hyperarcs of given vertex id
  std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> outHyperarcs(
          const VertexIdType& vertexId) const override;

  /// Remove a vertex with given name from the directed hypergraph.
  void removeVertex(const std::string& vertexName,
                    bool removeImpliedHyperarcs = true) override;
  /// Remove a vertex with given id from the directed hypergraph.
  void removeVertex(const VertexIdType& vertexId,
                    bool removeImpliedHyperarcs = true) override;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/DirectedHyperedge dependent access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if given vertex is a tail vertex of given hyperarc.
  bool isTailVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const override;
  /// Check if given vertex is a head vertex of given hyperarc.
  bool isHeadVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const override;
  /// Check if given vertex is a tail or head vertex of given hyperarc.
  bool isVertexOfHyperarc(const VertexIdType& vertexId,
                          const HyperedgeIdType& hyperarcId) const override;

  /* **************************************************************************
  * **************************************************************************
  *                        HyperarcProperty access
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Get properties of the hyperarc with the provided id.
  const HyperarcProperty* getHyperarcProperties(
          const HyperedgeIdType& hyperarcId) const override;
  /// Get properties of the hyperarc with the provided id.
  HyperarcProperty* getHyperarcProperties_(const HyperedgeIdType& hyperarcId)
  const override;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get root hypergraph
  const DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
          VertexProperty, HyperarcProperty,
          HypergraphProperty>* getRootHypergraph() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected methods
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Construct a hyperarc and a HyperarcProperty object
  const Hyperarc_t* buildHyperarcAndItsDependencies(
          std::unique_ptr<GraphElementContainer<Vertex_t*>> tails,
          std::unique_ptr<GraphElementContainer<Vertex_t*>> heads,
          const typename Hyperarc_t::SpecificAttributes& attributes);
  /// Remove vertex dependencies.
  void removeVertexDependencies(const Vertex_t& vertex);
  /// Remove hyperarc dependencies.
  void removeHyperarcDependencies(const Hyperarc_t& hyperarc);
  /// Remove nullptr entries from vector of hyperarc properties.
  void removeNullEntriesFromHyperarcProperties();
  /// Return pointer to hyperarc container
  HyperarcContainerPtr getRootHyperarcContainerPtr() const override;


  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Hyperarcs of the graph
  HyperarcContainerPtr hyperarcs_;
  /// Property per hyperarc
  HyperarcPropertyContainerPtr hyperarcProperties_;
  /// Hyperarc max Id
  HyperedgeIdType hyperarcMaxId_;
};

/// Operator<<
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::ostream& operator<< (std::ostream& out,
                          const DirectedHypergraph<VertexTemplate_t,
                                  Hyperarc_t, VertexProperty, HyperarcProperty,
                                  HypergraphProperty>& hypergraph) {
  // print isolated vertices
  for (const auto& vertex : hypergraph.vertices()) {
    if (not hypergraph.hasInHyperarcs(vertex->id()) and
            not hypergraph.hasOutHyperarcs(vertex->id())) {
      out << *vertex << std::endl;
    }
  }

  // print hyperarcs
  for (const auto& hyperarc : hypergraph.hyperarcs()) {
    out << *hyperarc << std::endl;
  }

  return out;
}
}  // namespace hglib

#include "DirectedHypergraph.hpp"
#endif  // HGLIB_DIRECTEDHYPERGRAPH_H
