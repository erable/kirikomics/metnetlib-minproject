/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 01.08.17.
//

#include "DirectedSubHypergraph.h"

#include "filtered_vector.h"

namespace hglib {

template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
DirectedSubHypergraph(DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
                       VertexProperty, HyperarcProperty,
                       HypergraphProperty>* parent,
                       const std::unordered_set<VertexIdType>&
                       whitelistedVertexIds,
                       const std::unordered_set<HyperedgeIdType>&
                       whitelistedHyperarcIds) :
        DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>(),
        Observer(),
        parent_(parent) {
  // build whitelisted vertex FilteredVector
  std::vector<Vertex_t*> whitelistFilterVertices;
  for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
    whitelistFilterVertices.push_back(nullptr);
  }
  for (VertexIdType vertexId : whitelistedVertexIds) {
    try {
      const auto& vertex = parent_->vertexById(vertexId);
      if (vertex != nullptr) {
        whitelistFilterVertices[vertexId] = const_cast<Vertex_t*>(vertex);
      }
    } catch (const std::out_of_range& oor) {
      std::string errorMessage("");
      if (parent_->hyperarcContainerSize() == 0) {
        errorMessage = "There is no vertex in the parent graph. You cannot"
                " add vertices to the subgraph\n";
      } else {
        errorMessage = "Vertex with id: " + std::to_string(vertexId)
                       + " is out of range -> min=0; max="
                       + std::to_string((parent_->vertexContainerSize() - 1))
                       + '\n';
      }
      std::cerr << errorMessage;
    }
  }
  // create actual vertices list
  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(whitelistFilterVertices));

  // build whitelisted hyperarc FilteredVector
  std::vector<Hyperarc_t*> whitelistFilterHyperarcs;
  for (size_t i = 0; i < parent_->hyperarcContainerSize(); ++i) {
    whitelistFilterHyperarcs.push_back(nullptr);
  }
  for (HyperedgeIdType hyperarcId : whitelistedHyperarcIds) {
    try {
      const auto& hyperarc = parent_->hyperarcById(hyperarcId);
      if (hyperarc != nullptr) {
        whitelistFilterHyperarcs[hyperarcId] = const_cast<
                Hyperarc_t*>(hyperarc);
      }
    } catch (const std::out_of_range& oor) {
      std::string errorMessage("");
      if (parent_->hyperarcContainerSize() == 0) {
        errorMessage = "There is no hyperarc in the parent graph. You cannot"
                " add hyperarcs to the subgraph\n";
      } else {
        errorMessage = "DirectedHyperedge with id: "
                       + std::to_string(hyperarcId)
                       + " is out of range -> min=0; max="
                       + std::to_string((parent_->hyperarcContainerSize() - 1))
                       + '\n';
      }
      std::cerr << errorMessage;
    }
  }
  // create actual hyperarc list
  whitelistedHyperarcsList_ = create_filtered_vector<Hyperarc_t*>(
      parent->getRootHyperarcContainerPtr(),
      std::make_unique<WhitelistFilter<Hyperarc_t*>>(whitelistFilterHyperarcs));

  // add itself as observer of the parent
  parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);

  // Create HypergraphProperty object
  subHypergraphProperty_ = new HypergraphProperty(
          *parent_->getHypergraphProperties_());
}

/**
 * Copy constructor
 *
 * @param subDirectedHypergraph
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
DirectedSubHypergraph(const DirectedSubHypergraph& other) :
        Observer(),
        parent_(other.parent_) {
  // copy whitelisted vertex FilteredVector
  std::vector<Vertex_t*> whitelistFilterVertices;
  for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
    whitelistFilterVertices.push_back(nullptr);
  }
  for (const auto& vertex : other.vertices()) {
    whitelistFilterVertices[vertex->id()] =
                const_cast<Vertex_t*>(vertex);
  }
  // create actual vertices list
  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(whitelistFilterVertices));


  // copy whitelisted hyperarc FilteredVector
  std::vector<Hyperarc_t*> whitelistFilterHyperarcs;
  for (size_t i = 0; i < parent_->hyperarcContainerSize(); ++i) {
    whitelistFilterHyperarcs.push_back(nullptr);
  }
  for (const auto& hyperarc : other.hyperarcs()) {
    whitelistFilterHyperarcs[hyperarc->id()] =
            const_cast<Hyperarc_t*>(hyperarc);
  }
  // create actual vertices list
  whitelistedHyperarcsList_ = create_filtered_vector<Hyperarc_t*>(
      parent_->getRootHyperarcContainerPtr(),
      std::make_unique<WhitelistFilter<Hyperarc_t*>>(whitelistFilterHyperarcs));


  // Attach itself as observer to parent graph
  parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);

  // Create HypergraphProperty object
  subHypergraphProperty_ = new HypergraphProperty(
          *other.getHypergraphProperties_());
}


/// Destructor
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
~DirectedSubHypergraph() {
  delete subHypergraphProperty_;
  // Detach itself as observer of the parent graph
  if (parent_ != nullptr) {
    parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);

    // Subgraphs whose parent (this subgraph) becomes the current grand-parent.
    // Add subgraphs as observers to the current grand-parent.
    // Clear list of observers of this subgraph.
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver = itEvent->second.begin();
           itObserver != itEvent->second.end(); ++itObserver) {
        auto subgraph = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
                Hyperarc_t, VertexProperty, HyperarcProperty,
                HypergraphProperty> *>(*itObserver);
        subgraph->parent_ = parent_;
        subgraph->parent_->AddObserver(subgraph, itEvent->first);
      }
      this->observers_[itEvent->first].clear();
    }
  }
}

/**
 * Assignment operator
 *
 * @param source
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>& DirectedSubHypergraph<
        VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
operator=(const DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>& source) {
  // check for self assignment
  if (this != &source) {
    // Set the parent of all childs in the subgraph-hierarchy to null
    std::unordered_set<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
            VertexProperty, HyperarcProperty, HypergraphProperty>*> observers;
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver : itEvent->second) {
        auto subgraph = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
                Hyperarc_t, VertexProperty, HyperarcProperty,
                HypergraphProperty>*>(itObserver);
        observers.insert(subgraph);
      }
    }
    for (auto& subgraph : observers) {
      subgraph->setParentOfChildsToNull();
      this->DeleteObserver(subgraph, ObservableEvent::VERTEX_REMOVED);
      this->DeleteObserver(subgraph, ObservableEvent::HYPEREDGE_REMOVED);
      this->DeleteObserver(subgraph, ObservableEvent::RESET_VERTEX_IDS);
      this->DeleteObserver(subgraph, ObservableEvent::RESET_EDGE_IDS);
    }

    // Detach itself as observer from parent graph
    parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);
    // Assign parent
    parent_ = source.parent_;

    // copy whitelisted vertex FilteredVector
    std::vector<Vertex_t*> whitelistFilterVertices;
    for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
      whitelistFilterVertices.push_back(nullptr);
    }
    for (const auto& vertex : source.vertices()) {
      whitelistFilterVertices[vertex->id()] =
              const_cast<Vertex_t*>(vertex);
    }
    // create actual vertices list
    whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
        parent_->getRootVerticesContainerPtr(),
        std::make_unique<WhitelistFilter<Vertex_t*>>(whitelistFilterVertices));


    // copy whitelisted hyperarc FilteredVector
    std::vector<Hyperarc_t*> whitelistFilterHyperarcs;
    for (size_t i = 0; i < parent_->hyperarcContainerSize(); ++i) {
      whitelistFilterHyperarcs.push_back(nullptr);
    }
    for (const auto& hyperarc : source.hyperarcs()) {
      whitelistFilterHyperarcs[hyperarc->id()] =
              const_cast<Hyperarc_t*>(hyperarc);
    }
    // create actual vertices list
    whitelistedHyperarcsList_ = create_filtered_vector<Hyperarc_t*>(
        parent_->getRootHyperarcContainerPtr(),
        std::make_unique<WhitelistFilter<Hyperarc_t*>>(
            whitelistFilterHyperarcs));


    // init hypergraph property
    delete subHypergraphProperty_;
    subHypergraphProperty_ = new HypergraphProperty(
            *source.getHypergraphProperties_());

    // Attach itself as observer to parent graph
    parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);
  }
  return *this;
}


/// Check if multi-hyperarcs are allowed in this hypergraph
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
allowMultiHyperarcs() const {
  throwExceptionOnNullptrParent();
  return parent_->allowMultiHyperarcs();
}


/**
 * Number of hyperarcs in the hypergraph
 *
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
nbHyperarcs() const {
  throwExceptionOnNullptrParent();
  size_t nbHyperarcs(0);
  for (auto it = whitelistedHyperarcsList_->begin();
       it != whitelistedHyperarcsList_->end(); ++it) {
    ++nbHyperarcs;
  }
  return nbHyperarcs;
}


/**
 * Highest assigned hyperarc id of the hypergraph
 *
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarcContainerSize() const {
  throwExceptionOnNullptrParent();
  return parent_->hyperarcContainerSize();
}


/**
 * \brief Add a hyperarc with the provided tails, heads, and additional
 * parameters (if needed to create a hyperarc of type Hyperarc_type) to the
 * sub- and parent hypergraph
 *
 * @param tails_heads
 * @param args
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Hyperarc_type*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
addHyperarc(decltype(hglib::NAME),
            const TailAndHeadNames& tails_heads,
            const typename Hyperarc_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  /*
   * Try to add the hyperarc in the parent hypergraph. If this was done
   * with success, then the hyperarc is added to the whitelist of the sub-
   * hypergraph.
   */
  try {
    const auto hyperarc = parent_->addHyperarc(hglib::NAME, tails_heads,
                                               attributes);
    whitelistedHyperarcsList_->removeFilteredOutItem(
            const_cast<Hyperarc_type*>(hyperarc));
    return hyperarc;
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument(ia.what());
  }
}


/**
 * \brief Add a hyperarc with the provided tail ids, head ids, and additional
 * parameters (if needed to create a hyperarc of type Hyperarc_type) to the
 * sub- and parent hypergraph
 *
 * @param tails_heads
 * @param args
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Hyperarc_type*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
addHyperarc(const TailAndHeadIds& tails_heads,
            const typename Hyperarc_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  /*
   * Try to add the hyperarc in the parent hypergraph. If this was done
   * with success, then the hyperarc is added to the whitelist of the sub-
   * hypergraph.
   */
  try {
    const auto hyperarc = parent_->addHyperarc(tails_heads, attributes);
    whitelistedHyperarcsList_->removeFilteredOutItem(
            const_cast<Hyperarc_type*>(hyperarc));
    return hyperarc;
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument(ia.what());
  }
}


/**
 * Get arguments to create the hyperarc
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
ArgumentsToCreateDirectedHyperedge<Hyperarc_t> DirectedSubHypergraph<
        VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
        HypergraphProperty>::
argumentsToCreateHyperarc(const hglib::HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  return parent_->argumentsToCreateHyperarc(hyperarcId);
}


/**
 * Search whitelisted hyperarc list for an hyperarc with the provided id
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Hyperarc_type*
DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarcById(const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check in constant time if the hyperarc id is in the whitelist of the sub-
   * hypergraph. If yes, we get the ptr to hyperarc from teh parent hypergraph.
   * If no, we return a nullptr.
   *
   * A out_of_range exception is thrown if the given hyperarc id is out of
   * range.
   */
  try {
    const auto& hyperarc = whitelistedHyperarcsList_->at(hyperarcId);
    return whitelistedHyperarcsList_->filter_out(hyperarc) ?
           nullptr : hyperarc;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hyperarcById: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  }
}


/**
 * Remove a hyperarc with the given id from the sub-hypergraph.
 *
 * @param hyperarcId
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeHyperarc(const HyperedgeIdType& hyperarcId) {
  throwExceptionOnNullptrParent();
  /*
   * Check if there exists a hyperarc in the sub-hypergraph with the given id.
   * If yes, remove it from the whitelist.
   * If no, an exception is thrown.
   */
  try {
    const auto hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::removeHyperarc"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }

    // Notify observers of change
    HyperedgeIdType nonConstHyperarcId(hyperarcId);
    this->NotifyObservers(ObservableEvent::HYPEREDGE_REMOVED,
                          &nonConstHyperarcId);
    whitelistedHyperarcsList_->addFilteredOutItem(
            const_cast<Hyperarc_t*>(hyperarc));
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hyperarcById: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * Const hyperarc iterator pointing to begin of the hyperarcs
 *
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hyperarcsBegin() const {
  throwExceptionOnNullptrParent();
  return whitelistedHyperarcsList_->cbegin();
}


/**
 * Const hyperarc iterator pointing to end of the hyperarcs
 *
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hyperarcsEnd() const {
  throwExceptionOnNullptrParent();
  return whitelistedHyperarcsList_->cend();
}


/**
 *
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator, typename DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator> DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarcsBeginEnd() const {
  throwExceptionOnNullptrParent();
  return std::make_pair(whitelistedHyperarcsList_->cbegin(),
                        whitelistedHyperarcsList_->cend());
}

/**
 * Return a reference to the container of hyperarcs.
 *
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty,
        HypergraphProperty>::HyperarcContainer& DirectedSubHypergraph<
        VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
        HypergraphProperty>::
hyperarcs() const {
  throwExceptionOnNullptrParent();
  return *(whitelistedHyperarcsList_.get());
}


/**
 * Return number of tail vertices in the hyperarc with given id.
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
nbTailVertices(const HyperedgeIdType & hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::nbTailVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of tail vertices in this subgraph
    size_t nbTails(0);
    auto it = tailsBegin(hyperarcId);
    auto end = tailsEnd(hyperarcId);
    for (; it != end; ++it) {
      ++nbTails;
    }
    return nbTails;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "nbTailVertices: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * Check if hyperarc with given id has tail vertices.
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasTailVertices(const HyperedgeIdType & hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::hasTailVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // compare begin and end
    auto begin = tailsBegin(hyperarcId);
    auto end = tailsEnd(hyperarcId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hasTailVertices: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the begin of the tail vertices
 * of the hyperarc with the given id
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tailsBegin(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::tailsBegin"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }

    // create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperarc->tails_,
                    whitelistedVerticesList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "tailsBegin: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the end of the tail vertices
 * of the hyperarc with the given id
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tailsEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::tailsEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperarc->tails_,
                    whitelistedVerticesList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "tailsEnd: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator,
        typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>::
        vertex_iterator> DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
tailsBeginEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::tailsBeginEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator pair
    auto begin = tailsBegin(hyperarcId);
    auto end = tailsEnd(hyperarcId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "tailsBeginEnd: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}

/**
 * Allow a ranged based for loop of the tails
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename DirectedSubHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::Vertex_t*>>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tails(const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::tails"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
     errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    return create_filtered_vector(hyperarc->tails_,
                                  whitelistedVerticesList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "tails: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * Return number of head vertices in the hyperarc with given id.
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
nbHeadVertices(const HyperedgeIdType & hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::nbHeadVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of head vertices in this subgraph
    size_t nbHeads(0);
    auto it = headsBegin(hyperarcId);
    auto end = headsEnd(hyperarcId);
    for (; it != end; ++it) {
      ++nbHeads;
    }
    return nbHeads;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "nbHeadVertices: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * Check if hyperarc with given id has head vertices.
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasHeadVertices(const HyperedgeIdType & hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::hasHeadVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }

    auto begin = headsBegin(hyperarcId);
    auto end = headsEnd(hyperarcId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hasHeadVertices: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the begin of the head vertices
 * of the hyperarc with the given id
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
headsBegin(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto &hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::headsBegin"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // Create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperarc->heads_,
                    whitelistedVerticesList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range &oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "headsBegin: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument &ia) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the end of the head vertices
 * of the hyperarc with the given id
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
headsEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  throwExceptionOnNullptrParent();
  try {
    const auto &hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::headsEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperarc->heads_,
                    whitelistedVerticesList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range &oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "headsEnd: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument &ia) {
    throw;
  }
}


/**
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator,
        typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>::
        vertex_iterator> DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
headsBeginEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::headsBeginEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create pair of iterators
    auto begin = headsBegin(hyperarcId);
    auto end = headsEnd(hyperarcId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range &oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "headsBeginEnd: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument &ia) {
    throw;
  }
}


/**
 * Allow a ranged based for loop of the heads
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename DirectedSubHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::Vertex_t*>> DirectedSubHypergraph<VertexTemplate_t,
    Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
heads(const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::heads"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
     errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create FilteredVector
    return create_filtered_vector(hyperarc->heads_,
                                  whitelistedVerticesList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "heads: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * Highest assigned vertex id of the hypergraph
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
vertexContainerSize() const {
  throwExceptionOnNullptrParent();
  return parent_->vertexContainerSize();
}


/**
 * Number of vertices in the hypergraph
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
nbVertices() const {
  throwExceptionOnNullptrParent();
  size_t nbVertices(0);
  for (auto it = whitelistedVerticesList_->begin();
       it != whitelistedVerticesList_->end(); ++it) {
    ++nbVertices;
  }
  return nbVertices;
}

/**
 * Search vertex list for a vertex with the provided name
 *
 * @param vertexName
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Vertex_t*
DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
vertexByName(const std::string& vertexName) const {
  throwExceptionOnNullptrParent();
  for (const auto& vertex : *(whitelistedVerticesList_.get())) {
    if (vertex->name().compare(vertexName) == 0) {
      return vertex;
    }
  }
  return nullptr;
}


/**
 * Search vertex list for a vertex with the provided id
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Vertex_t*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
vertexById(const VertexIdType & vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = whitelistedVerticesList_->at(vertexId);
    return whitelistedVerticesList_->filter_out(vertex) ? nullptr : vertex;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in SubHypergraph::vertexById: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/**
 * Return the begin of the container of vertices.
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
verticesBegin() const {
  throwExceptionOnNullptrParent();
  return whitelistedVerticesList_->cbegin();
}


/**
 * Return the end of the container of vertices.
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
verticesEnd() const {
  throwExceptionOnNullptrParent();
  return whitelistedVerticesList_->cend();
}


/**
 * Const vertex iterator pointing to begin and end of vertices
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator,
        typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
verticesBeginEnd() const {
  throwExceptionOnNullptrParent();
  return std::make_pair(whitelistedVerticesList_->cbegin(),
                        whitelistedVerticesList_->cend());
}


/**
 * Return reference to whitelisted vertex container
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::VertexContainer&
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
vertices() const {
  throwExceptionOnNullptrParent();
  return *(whitelistedVerticesList_.get());
}


/**
 * Add a vertex to the sub- and parent hpergraph
 *
 * @param args
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Vertex_t*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
addVertex(const typename Vertex_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  // TODO(mw): Do we allow to add a vertex that is already in the parent
  // hypergraph but not yet in the whitelist of the sub-hypergraph? Currently
  // we don't.
  try {
    // Add new vertex to parent
    const auto vertex = parent_->addVertex(attributes);
    whitelistedVerticesList_->removeFilteredOutItem(
            const_cast<Vertex_t*>(vertex));
    return vertex;
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument(ia.what());
  }
}


template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Vertex_t*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
addVertex(const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  // TODO(mw): Do we allow to add a vertex that is already in the parent
  // hypergraph but not yet in the whitelist of the sub-hypergraph? Currently
  // we don't.
  try {
    // Add new vertex to parent
    const auto vertex = parent_->addVertex(name, attributes);
    whitelistedVerticesList_->removeFilteredOutItem(
            const_cast<Vertex_t*>(vertex));
    return vertex;
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument(ia.what());
  }
}

/**
 * Get arguments to create the vertex
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
ArgumentsToCreateVertex<typename DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty,
        HypergraphProperty>::Vertex_t> DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
argumentsToCreateVertex(const hglib::VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = whitelistedVerticesList_->at(vertexId);
    if (not whitelistedVerticesList_->filter_out(vertex)) {
      return whitelistedVerticesList_->operator[](
              vertexId)->argumentsToCreateVertex();
    } else {
      throw std::invalid_argument("Vertex with " + std::to_string(vertexId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in SubHypergraph::vertexById: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}





/**
 * Number of incoming hyperarcs of the vertex with given id
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
inDegree(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::inDegree"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of in-arcs of given vertex in this subgraph
    size_t inDegree(0);
    auto it = inHyperarcsBegin(vertexId);
    auto end = inHyperarcsEnd(vertexId);
    for (; it != end; ++it) {
      ++inDegree;
    }
    return inDegree;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "inDegree: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * Check if vertex has incoming hyperarcs
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasInHyperarcs(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::hasInHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // check if there are in-arcs
    auto begin = inHyperarcsBegin(vertexId);
    auto end = inHyperarcsEnd(vertexId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hasInHyperarcs: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the begin of the incoming
 * hyperarcs of the vertex with the given id
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
inHyperarcsBegin(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::inHyperarcsBegin"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperarc_t*>> tmp =
            create_filtered_vector<Hyperarc_t*>(
                    vertex->inHyperarcs_,
                    whitelistedHyperarcsList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "inHyperarcsBegin: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the end of the incoming
 * hyperarcs of the vertex with the given id
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
inHyperarcsEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::inHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperarc_t*>> tmp =
            create_filtered_vector<Hyperarc_t*>(
                    vertex->inHyperarcs_,
                    whitelistedHyperarcsList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "inHyperarcsEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}



template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator, typename DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator> DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
inHyperarcsBeginEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "inHyperarcsBeginEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create pair of iterators
    auto begin = inHyperarcsBegin(vertexId);
    auto end = inHyperarcsEnd(vertexId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "inHyperarcsBeginEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * Get reference to container of incoming hyperarcs of given vertex id
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<Hyperarc_t*>>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
    HyperarcProperty, HypergraphProperty>::
inHyperarcs(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::inHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
     errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create FilteredVector
    return create_filtered_vector(vertex->inHyperarcs_,
                                  whitelistedHyperarcsList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "inHyperarcs: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}

/**
 * Number of outgoing hyperarcs of the vertex with given id
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
outDegree(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::outDegree"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of out-arcs of given vertex in this subgraph
    size_t outDegree(0);
    auto it = outHyperarcsBegin(vertexId);
    auto end = outHyperarcsEnd(vertexId);
    for (; it != end; ++it) {
      ++outDegree;
    }
    return outDegree;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "outDegree: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * Check if vertex has outgoing hyperarcs
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasOutHyperarcs(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::hasOutHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // check if there are out-arcs
    auto begin = outHyperarcsBegin(vertexId);
    auto end = outHyperarcsEnd(vertexId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hasOutHyperarcs: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the begin of the outgoing
 * hyperarcs of the vertex with the given id
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
outHyperarcsBegin(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::outHyperarcsBegin"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperarc_t*>> tmp =
            create_filtered_vector<Hyperarc_t*>(
                    vertex->outHyperarcs_,
                    whitelistedHyperarcsList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "outHyperarcsBegin: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the end of the outgoing
 * hyperarcs of the vertex with the given id
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
outHyperarcsEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::outHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperarc_t*>> tmp =
            create_filtered_vector<Hyperarc_t*>(
                    vertex->outHyperarcs_,
                    whitelistedHyperarcsList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "outHyperarcsEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator, typename DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator> DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
outHyperarcsBeginEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "outHyperarcsBeginEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create pair of iterators
    auto begin = outHyperarcsBegin(vertexId);
    auto end = outHyperarcsEnd(vertexId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "outHyperarcsBeginEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}

/**
 * Get reference to container of outgoing hyperarcs of given vertex id
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<Hyperarc_t*>>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
      HyperarcProperty, HypergraphProperty>::
outHyperarcs(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::outHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
     errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create FilteredVector
    return create_filtered_vector(vertex->outHyperarcs_,
                                  whitelistedHyperarcsList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "outHyperarcs: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}

/**
 * Remove a vertex with given name from the directed sub-hypergraph.
 *
 * @param vertexName
 * @param removeImpliedHyperarcs
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeVertex(const std::string& vertexName,
             bool removeImpliedHyperarcs) {
  throwExceptionOnNullptrParent();
  // Find vertex by name in the hypergraph and call the method removeVertex
  // that takes the id of the vertex
  const auto& vertex = this->vertexByName(vertexName);
  if (vertex == nullptr) {
    std::string errorMessage("Called DirectedSubHypergraph::removeVertex"
                                     " with the vertex name " + vertexName +
                             " which does not exists in the directed "
                                     "subhypergraph.\n");
    throw std::invalid_argument(errorMessage);
  }

  removeVertex(vertex->id(), removeImpliedHyperarcs);
}


/**
 * Remove a vertex with given id from the directed sub-hypergraph.
 *
 * @param vertexId
 * @param removeImpliedHyperarcs
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeVertex(const VertexIdType& vertexId,
             bool removeImpliedHyperarcs) {
  throwExceptionOnNullptrParent();
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before, or does not take part of the
   * sub-hypergraph.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove all implied hyperarcs from the sub-hypergraph if flag is set to
   * true (default).
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::removeVertex"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    if (removeImpliedHyperarcs) {
      // hyperarcs that will be removed from the hypergraph
      std::vector<Hyperarc_type*> hyperarcsToBeRemoved;
      for (auto& hyperarc : *(whitelistedHyperarcsList_.get())) {
        // all hyperarcs that contain the vertex will be removed from the
        // hypergraph
        if (isVertexOfHyperarc(vertexId, hyperarc->id())) {
          hyperarcsToBeRemoved.emplace_back(hyperarc);
        }
      }
      // remove hyperarcs from hypergraph
      for (const auto& hyperarc : hyperarcsToBeRemoved) {
        removeHyperarc(hyperarc->id());
      }
    }

    // Notify observers of change
    Remove_vertex_args args;
    args.vertexId_ = vertexId;
    args.removeImpliedHyperarcs_ = removeImpliedHyperarcs;
    this->NotifyObservers(ObservableEvent::VERTEX_REMOVED, &args);

    whitelistedVerticesList_->addFilteredOutItem(
            const_cast<Vertex_t*>(vertex));
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "removeVertex:"
            " index = " << vertexId << "; min = 0; max = "
              << this->whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}

/**
 * Check if given vertex is a tail vertex of given hyperarc.
 *
 * @param vertexId
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isTailVertexOfHyperarc(const VertexIdType& vertexId,
                       const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex and hyperarc are part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "isTailVertexOfHyperarc"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "isTailVertexOfHyperarc: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }

  try {
    const auto hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "isTailVertexOfHyperarc"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "isTailVertexOfHyperarc: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }

  auto it = tailsBegin(hyperarcId);
  auto end = tailsEnd(hyperarcId);
  for (; it != end; ++it) {
     if ((*it)->id() == vertexId) {
      return true;
    }
  }
  return false;
}


/**
 * Check if given vertex is a head vertex of given hyperarc.
 *
 * @param vertexId
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isHeadVertexOfHyperarc(const VertexIdType& vertexId,
                       const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex and hyperarc are part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "isHeadVertexOfHyperarc"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "isHeadVertexOfHyperarc: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }

  try {
    const auto hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "isHeadVertexOfHyperarc"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "isHeadVertexOfHyperarc: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
  auto it = headsBegin(hyperarcId);
  auto end = headsEnd(hyperarcId);
  for (; it != end; ++it) {
    if ((*it)->id() == vertexId) {
      return true;
    }
  }
  return false;
}


/**
 * Check if given vertex is a tail or head vertex of given hyperarc.
 *
 * @param vertexId
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isVertexOfHyperarc(const VertexIdType& vertexId,
                   const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  return (isTailVertexOfHyperarc(vertexId, hyperarcId) or
          isHeadVertexOfHyperarc(vertexId, hyperarcId));
}


/**
 * Get properties of the hyperarc with the provided id.
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const HyperarcProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHyperarcProperties(const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarc = whitelistedHyperarcsList_->at(hyperarcId);
    if (not whitelistedHyperarcsList_->filter_out(hyperarc)) {
      return parent_->getHyperarcProperties(hyperarcId);
    } else {
      throw std::invalid_argument("DirectedHyperedge with " +
                                          std::to_string(hyperarcId) +
                                          " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in"
            " DirectedSubHypergraph::getHyperarcProperties: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  }
}


/**
 * Get properties of the hyperarc with the provided id.
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
HyperarcProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHyperarcProperties_(const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarc = whitelistedHyperarcsList_->at(hyperarcId);
    if (not whitelistedHyperarcsList_->filter_out(hyperarc)) {
      return parent_->getHyperarcProperties_(hyperarcId);
    } else {
      throw std::invalid_argument("DirectedHyperedge with " +
                                  std::to_string(hyperarcId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in"
            " DirectedSubHypergraph::getHyperarcProperties: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  }
}


/**
 * Get properties of the vertex with the provided id.
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const VertexProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getVertexProperties(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = whitelistedVerticesList_->at(vertexId);
    if (not whitelistedVerticesList_->filter_out(vertex)) {
      return parent_->getVertexProperties(vertexId);
    } else {
      throw std::invalid_argument("vertex with " +
                                  std::to_string(vertexId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in"
            " DirectedSubHypergraph::getVertexProperties: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/**
 * Get properties of the vertex with the provided id.
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
VertexProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getVertexProperties_(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = whitelistedVerticesList_->at(vertexId);
    if (not whitelistedVerticesList_->filter_out(vertex)) {
      return parent_->getVertexProperties_(vertexId);
    } else {
      throw std::invalid_argument("vertex with " +
                                  std::to_string(vertexId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in"
            " DirectedSubHypergraph::getVertexProperties_: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/**
 * Get properties of the subhypergraph.
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const HypergraphProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHypergraphProperties() const {
  throwExceptionOnNullptrParent();
  return subHypergraphProperty_;
}


/**
 * Get properties of the subhypergraph.
 *
 * Non-const version of getHypergraphProperties()
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
HypergraphProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHypergraphProperties_() const {
  throwExceptionOnNullptrParent();
  return const_cast<HypergraphProperty*>(getHypergraphProperties());
}


/**
 * Observer update method
 *
 * @param o Observable
 * @param e Event
 * @param arg optional arguments
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
Update(const Observable& o, ObservableEvent e, void* arg) {
  throwExceptionOnNullptrParent();
  if (&o == parent_) {
    switch (e) {
      case ObservableEvent::VERTEX_REMOVED: {
        Remove_vertex_args* args = static_cast<Remove_vertex_args*>(arg);
        try {
          const auto& vertex = whitelistedVerticesList_->at(args->vertexId_);
          if (not whitelistedVerticesList_->filter_out(vertex)) {
            removeVertex(args->vertexId_, args->removeImpliedHyperarcs_);
          }
        } catch (const std::out_of_range& oor) {
          std::cerr << "IndexOutOfRangeException in"
                  " DirectedSubHypergraph::Update: "
                  "index = " << args->vertexId_ << "; min = 0; max = " <<
                    whitelistedVerticesList_->size() << '\n';
          throw;
        }
        break;
      }
      case ObservableEvent::HYPEREDGE_REMOVED: {
        HyperedgeIdType* hyperarcId = static_cast<HyperedgeIdType*>(arg);
        try {
          const auto& hyperarc = whitelistedHyperarcsList_->at(*hyperarcId);
          if (not whitelistedHyperarcsList_->filter_out(hyperarc)) {
            removeHyperarc(*hyperarcId);
          }
        } catch (const std::out_of_range& oor) {
          std::cerr << "IndexOutOfRangeException in"
                  " DirectedSubHypergraph::Update: "
                  "index = " << *hyperarcId << "; min = 0; max = " <<
                    whitelistedHyperarcsList_->size() << '\n';
          throw;
        }
        break;
      }
      case ObservableEvent::RESET_VERTEX_IDS: {
        updateVertexContainerUponResetVertexIds();
        break;
      }
      case ObservableEvent::RESET_EDGE_IDS: {
        updateHyperarcContainerUponResetHyperarcIds();
        break;
      }
    }
  }
}


/**
 * Get root hypergraph
 *
 * @return
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t, VertexProperty,
HyperarcProperty, HypergraphProperty>* DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
getRootHypergraph() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootHypergraph();
}


/* ****************************************************************************
 * ****************************************************************************
 *            Protected methods
 * ****************************************************************************
 * ***************************************************************************/

/**
 * \brief If the dtor of the root directed hypergraph was called we invalidate
 * all parents of its child hierarchy
 *
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
setParentOfChildsToNull() {
  throwExceptionOnNullptrParent();
  std::unordered_set<Observer*> observers;
  for (auto itEvent = this->observers_.begin();
       itEvent != this->observers_.end(); ++itEvent) {
    for (auto itObserver : itEvent->second) {
      observers.insert(itObserver);
    }
  }
  for (auto& observer : observers) {
    auto subgraph = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
            Hyperarc_t, VertexProperty, HyperarcProperty,
            HypergraphProperty>*>(observer);
    subgraph->setParentOfChildsToNull();
  }

  // Detach as observer of parent graph
  parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);
  // Set parent to nullptr
  parent_ = nullptr;
}


/**
 * \brief Throw a exception if one tries to use a member function on a
 * subgraph whose parent was set to nullptr previously
 *
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
throwExceptionOnNullptrParent() const {
  if (parent_ == nullptr) {
    throw std::invalid_argument("Previously another hypergraph was assigned"
                                       " to the original parent of this"
                                       " sub-hypergraph. This sub-hypergraph"
                                       " has no parent hypergraph anymore and"
                                       " is thus in an invalid state.");
  }
}


/**
 * \brief In response to the call resetVertexIds in the root hypergraph, the
 * content of the whitelisted vertices must be adapted.
 *
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
updateVertexContainerUponResetVertexIds() {
  /*
   * Consider the containers of vertices in the root and sub-graphs, where
   * X corresponds to a nullptr and a number to the id of the vertex pointed
   * by the entry:
   *
   * graph:          |X|1|2|3|X|
   * subgraph1:      |X|X|2|3|X|
   * subsubgraph1_1: |X|X|X|3|X|
   * subgraph2:      |X|1|X|X|X|
   *
   * If the method resetVertexIds was called on the graph we expect the
   * following:
   *
   * graph:          |0|1|2|
   * subgraph1:      |X|1|2|
   * subsubgraph1_1: |X|X|2|
   * subgraph2:      |0|X|X|
   *
   * The method updateVertexContainerUponResetVertexIds in
   * DirectedSubHypergraph notify its subgraphs about this change,
   * before changing its own container of vertices.
   */
  throwExceptionOnNullptrParent();

  // Notify subgraphs of this change
  this->NotifyObservers(ObservableEvent::RESET_VERTEX_IDS);

  if (whitelistedVerticesList_->empty()) {
    return;
  }
  std::vector<Vertex_t*> newWhitelistedVertexContainer;
  auto it = whitelistedVerticesList_->data();
  auto end = whitelistedVerticesList_->data() +
          whitelistedVerticesList_->size();
  for (; it != end; ++it) {
    const auto& vertex = *it;
    if (vertex != nullptr) {
      if (whitelistedVerticesList_->filter_out(vertex)) {
        newWhitelistedVertexContainer.push_back(nullptr);
      } else {
        newWhitelistedVertexContainer.push_back(vertex);
      }
    }
  }

  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(
          newWhitelistedVertexContainer));
}


/**\brief Adapt the content of container whitelistedHyperarcsList_ in
  * response to the call resetHyperarcIds in the root hypergraph
  */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
updateHyperarcContainerUponResetHyperarcIds() {
  /*
   * Consider the containers of hyperarcs in the root and sub-graphs, where
   * X corresponds to a nullptr and a number to the id of the hyperarc pointed
   * by the entry:
   *
   * graph:          |X|1|2|3|X|
   * subgraph1:      |X|X|2|3|X|
   * subsubgraph1_1: |X|X|X|3|X|
   * subgraph2:      |X|1|X|X|X|
   *
   * If the method resetHyperarcIds was called on the graph we expect the
   * following:
   *
   * graph:          |0|1|2|
   * subgraph1:      |X|1|2|
   * subsubgraph1_1: |X|X|2|
   * subgraph2:      |0|X|X|
   *
   * The method updateHyperarcContainerUponResetHyperarcIds in
   * DirectedSubHypergraph notify its subgraphs about this change, before
   * changing its own container of vertices.
   */
  throwExceptionOnNullptrParent();

  // Notify subgraphs of this change
  this->NotifyObservers(ObservableEvent::RESET_EDGE_IDS);

  if (whitelistedHyperarcsList_->empty()) {
    return;
  }
  std::vector<Hyperarc_t*> newWhitelistedHyperarcContainer;
  auto it = whitelistedHyperarcsList_->data();
  auto end = whitelistedHyperarcsList_->data() +
             whitelistedHyperarcsList_->size();
  for (; it != end; ++it) {
    const auto& hyperarc = *it;
    if (hyperarc != nullptr) {
      if (whitelistedHyperarcsList_->filter_out(hyperarc)) {
        newWhitelistedHyperarcContainer.push_back(nullptr);
      } else {
        newWhitelistedHyperarcContainer.push_back(hyperarc);
      }
    }
  }

  whitelistedHyperarcsList_ = create_filtered_vector<Hyperarc_t*>(
      parent_->getRootHyperarcContainerPtr(),
      std::make_unique<WhitelistFilter<Hyperarc_t*>>(
          newWhitelistedHyperarcContainer));
}


/// Return pointer to vertices container
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::VertexContainerPtr
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
getRootVerticesContainerPtr() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootVerticesContainerPtr();
}


/// Return pointer to hyperarc container
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::HyperarcContainerPtr
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
getRootHyperarcContainerPtr() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootHyperarcContainerPtr();
}
}  // namespace hglib
