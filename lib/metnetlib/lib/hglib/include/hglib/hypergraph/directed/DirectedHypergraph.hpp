/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#include "DirectedHypergraph.h"

#include <algorithm>
#include <stdexcept>
#include <string>
#include <unordered_set>
#include <vector>

#include "filtered_vector.h"

#include "DirectedSubHypergraph.h"

namespace hglib {

 /**
  * DirectedHypergraph constructor
  *
  * @param allowMultiHyperarcs flag to signal if multi-hyperarcs are allowed
  */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
DirectedHypergraph(bool allowMultiHyperarcs) :
        DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
                VertexProperty, EdgeProperty, HypergraphProperty>(),
        Hypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
                EdgeProperty, HypergraphProperty>(allowMultiHyperarcs),
        hyperarcMaxId_(-1) {
  // create hyperarcs_
  hyperarcs_ = create_filtered_vector<Hyperarc_t*>(
           create_filtered_vector<Hyperarc_t*>(),
           std::make_unique<NullptrFilter<Hyperarc_t*>>());
  // create hyperarcProperties_
  hyperarcProperties_ = create_filtered_vector<HyperarcProperty_type*>(
          create_filtered_vector<HyperarcProperty_type*>(),
          std::make_unique<NullptrFilter<HyperarcProperty_type*>>());
}


/**
 * Copy constructor
 *
 * @param directedHypergraph to be copied directed hypergraph
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
DirectedHypergraph(const DirectedHypergraph& directedHypergraph) :
        DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>(),
        Hypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
                HyperarcProperty, HypergraphProperty>(
                directedHypergraph),
        hyperarcMaxId_(-1) {
  try {
    // create hyperarcs_
    hyperarcs_ = create_filtered_vector<Hyperarc_t*>(
            create_filtered_vector<Hyperarc_t*>(),
            std::make_unique<NullptrFilter<Hyperarc_t*>>());
    // create hyperarcProperties_
    hyperarcProperties_ =
            create_filtered_vector<HyperarcProperty_type*>(
            create_filtered_vector<HyperarcProperty_type*>(),
            std::make_unique<NullptrFilter<HyperarcProperty_type*>>());
    // copy edges and associated edge properties
    for (std::size_t i = 0; i < directedHypergraph.hyperarcs_->size(); ++i) {
      Hyperarc_t* hyperarc = directedHypergraph.hyperarcs_->operator[](i);
      ++hyperarcMaxId_;
      // hyperarc was deleted in directedHypergraph
      if (hyperarc == nullptr) {
        hyperarcs_->push_back(nullptr);
        hyperarcProperties_->push_back(nullptr);
      } else {
        Hyperarc_t* copy = new Hyperarc_t(*hyperarc);
        // add tails to hyperarc
        for (const auto& vertex : *(hyperarc->tails_.get())) {
          auto& tail = this->vertices_->operator[](vertex->id());
          copy->addTailVertex(tail);
          tail->addOutHyperarc(copy);
        }
        // add heads to hyperarc
        for (const auto& vertex : *(hyperarc->heads_.get())) {
          auto& head = this->vertices_->operator[](vertex->id());
          copy->addHeadVertex(head);
          head->addInHyperarc(copy);
        }
        // add hyperarc to hypergraph
        hyperarcs_->push_back(copy);
        // copy hyperarc property
        HyperarcProperty* hyperarcPropertyCopy = new HyperarcProperty(
                *directedHypergraph.hyperarcProperties_->operator[](i));
        hyperarcProperties_->push_back(hyperarcPropertyCopy);
      }
    }
  } catch (std::bad_alloc& e) {
    for (auto& hyperarc : *(hyperarcs_.get())) {
      delete hyperarc;
    }
    for (auto& hyperarcProperty : *(hyperarcProperties_.get())) {
      delete hyperarcProperty;
    }
    throw;
  }
}


template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
~DirectedHypergraph() {
  // Set the parent of all childs in the subgraph-hierarchy to null
  std::unordered_set<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
      VertexProperty, HyperarcProperty, HypergraphProperty>*> observers;
  for (auto itEvent = this->observers_.begin();
       itEvent != this->observers_.end(); ++itEvent) {
    for (auto itObserver : itEvent->second) {
      auto subgraph = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
          Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>*>(itObserver);
      observers.insert(subgraph);
    }
  }
  for (auto& subgraph : observers) {
    subgraph->setParentOfChildsToNull();
  }

  // Delete pointers to hyperarcs
  for (auto& hyperarc : *(hyperarcs_.get())) {
    delete hyperarc;
  }
  // Delete pointers to hyperarc properties
  for (auto& hyperarcProperty : *(hyperarcProperties_.get())) {
    delete hyperarcProperty;
  }
}


/**
 * Assignment operator
 *
 * @param source DirectedHypergraph
 * @return reference to DirectedHypergraph
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>&
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
operator=(const DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>& source) {
  // check for self assignment
  if (this != &source) {
    // Call assignment oeprator of Hypergraph
    Hypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
            HyperarcProperty, HypergraphProperty>::operator=(source);

    // Set the parent of all childs in the subgraph-hierarchy to null
    std::unordered_set<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
            VertexProperty, HyperarcProperty, HypergraphProperty>*> observers;
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver : itEvent->second) {
        auto subgraph = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
                Hyperarc_t, VertexProperty, HyperarcProperty,
                HypergraphProperty>*>(itObserver);
        observers.insert(subgraph);
      }
    }
    for (auto& subgraph : observers) {
      subgraph->setParentOfChildsToNull();
    }

    // deallocate hyperarcs_
    for (auto& hyperarc : *(hyperarcs_.get())) {
      delete hyperarc;
    }
    hyperarcs_->clear();

    // deallocate hyperarcProperties_
    for (auto& eProp : *(hyperarcProperties_.get())) {
      delete eProp;
    }
    hyperarcProperties_->clear();


    // re-init the arc max Id
    hyperarcMaxId_ = -1;

    try {
      // copy arcs and associated arc properties
      for (std::size_t i = 0; i < source.hyperarcs_->size(); ++i) {
        Hyperarc_t* hyperarc = source.hyperarcs_->operator[](i);
        ++hyperarcMaxId_;
        // hyperarc was deleted in directedHypergraph
        if (hyperarc == nullptr) {
          hyperarcs_->push_back(nullptr);
          hyperarcProperties_->push_back(nullptr);
        } else {
          Hyperarc_t* copy = new Hyperarc_t(*hyperarc);
          // add tails to hyperarc
          for (const auto& vertex : *(hyperarc->tails_.get())) {
            auto& tail = this->vertices_->operator[](vertex->id());
            copy->addTailVertex(tail);
            tail->addOutHyperarc(copy);
          }
          // add heads to hyperarc
          for (const auto& vertex : *(hyperarc->heads_.get())) {
            auto& head = this->vertices_->operator[](vertex->id());
            copy->addHeadVertex(head);
            head->addInHyperarc(copy);
          }
          // add hyperarc to hypergraph
          hyperarcs_->push_back(copy);
          // copy hyperarc property
          HyperarcProperty* hyperarcPropertyCopy = new HyperarcProperty(
                  *source.hyperarcProperties_->operator[](i));
          hyperarcProperties_->push_back(hyperarcPropertyCopy);
        }
      }
    } catch (std::bad_alloc& e) {
      for (auto& hyperarc : *(hyperarcs_.get())) {
        delete hyperarc;
      }
      for (auto& hyperarcProperty : *(hyperarcProperties_.get())) {
        delete hyperarcProperty;
      }
      throw;
    }
  }
  return *this;
}


/**
 * Check if this directed hypergraph supports multi-hyperarcs.
 *
 * @return true if multi-hyperarcs are supported, false otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
allowMultiHyperarcs() const {
  return this->allowMultiHyperedges_;
}


/**
 * Number of hyperarcs in the hypergraph
 *
 * Complexity: Linear
 *
 * @return number of hyperarcs in the hypergraph
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
nbHyperarcs() const {
  size_t nbHyperarcs = 0;
  auto it = hyperarcs_->cbegin();
  auto end = hyperarcs_->cend();
  for (; it != end; ++it) {
    ++nbHyperarcs;
  }
  return nbHyperarcs;
}


/**
 * Highest assigned hyperarc id of the hypergraph
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarcContainerSize() const {
  return hyperarcs_->size();
}


/**
 * Search hyperarc list for an hyperarc with the provided id
 *
 * @param hyperarcId Identifier of the hyperarc
 * @return pointer to hyperarc if found, nullptr if hyperarcId is between 0
 * and maximum attributed hyperarc id in this hypergraph, throw out_of_range
 * exception otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const Hyperarc_t* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
hyperarcById(const HyperedgeIdType & hyperarcId) const {
  try {
    return hyperarcs_->at(hyperarcId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "hyperarcById: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  }
}


/**
 * Const hyperarc iterator pointing to begin of the hyperarcs
 *
 * @return Const hyperarc iterator pointing to begin of the hyperarcs
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
hyperarcsBegin() const {
  return hyperarcs_->cbegin();
}


/**
 * Const hyperarc iterator pointing to end of the hyperarcs
 *
 * @return Const hyperarc iterator pointing to end of the hyperarcs
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
hyperarcsEnd() const {
  return hyperarcs_->cend();
}


/**
 * \brief Pair of hyperarc iterators pointing to begin and end of the hyperarcs
 *
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
std::pair<typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::hyperarc_iterator,
        typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, EdgeProperty, HypergraphProperty>::
        hyperarc_iterator> DirectedHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, EdgeProperty, HypergraphProperty>::
hyperarcsBeginEnd() const {
  return std::make_pair(hyperarcsBegin(), hyperarcsEnd());
}


/**
 * Return a reference to the container of hyperarcs.
 *
 * Can be used in a ranged based for loop as follows
 \verbatim
 for (const auto& hyperarc : graph.hyperarcs() {
    ...
 }
 \endverbatim
 *
 * @return Reference to hyperarc container
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::HyperarcContainer&
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
hyperarcs() const {
  return *(hyperarcs_.get());
}


/**
 * Return number of tail vertices in the hyperarc with given id.
 *
 * @param hyperarcId hyperarc id
 * @return number of tail vertices in the hyperarc with given id.
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
nbTailVertices(const HyperedgeIdType& hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::nbTailVertices "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->tails_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "nbTailVertices: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Check if hyperarc with given id has tail vertices.
 *
 * @param hyperarcId hyperarc id
 * @return true if hyperarc with given id has tail vertices,
 * false otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasTailVertices(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "hasTailVertices "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not hyperarc->tails_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "nbTailVertices: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Const vertex iterator pointing to the begin of the tail vertices of
 * the hyperarc with the given id
 *
 * @param hyperarcId hyperarc id
 * @return Const vertex iterator pointing to the begin of the tail vertices of
 * the hyperarc with the given id
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tailsBegin(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::tailsBegin "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->tails_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "tailsBegin: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Const vertex iterator pointing to the end of the tail vertices of
 * the hyperarc with the given id
 *
 * @param hyperarcId hyperarc id
 * @return Const vertex iterator pointing to the end of the tail vertices of
 * the hyperarc with the given id
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tailsEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::tailsEnd "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->tails_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "tailsEnd: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Pair of vertex iterators pointing to begin and end of the tails
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator,
        typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>::
        vertex_iterator> DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
tailsBeginEnd(const HyperedgeIdType& hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::tailsEnd "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(hyperarc->tails_->cbegin(),
                          hyperarc->tails_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "tailsBeginEnd: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Get container of tail vertices of given hyperarc id
 *
 *  * Can be used in a ranged based for loop as follows
 \verbatim
 for (const auto& vertex : graph.tails(hyperarcId) {
    ...
 }
 \endverbatim
 *
 * @param hyperarcId hyperarc id
 * @return reference to container of tail vertices of given hyperarc id
 * @throw std::out_of_range if the provided hyperarc id was never assigned.
 * @throw std::invalid_argument if the hyperarc with the provided id was
 * removed earlier from the hypergraph.
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename DirectedHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::Vertex_t*>>
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tails(const HyperedgeIdType& hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::tails "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->tails_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "tails: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}

/**
 * Return number of head vertices in the hyperarc with given id.
 *
 * @param hyperarcId hyperarc id
 * @return number of head vertices in the hyperarc with given id.
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
nbHeadVertices(const HyperedgeIdType& hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::nbHeadVertices "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->heads_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "nbHeadVertices: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Check if hyperarc with given id has head vertices.
 *
 * @param hyperarcId hyperarc id
 * @return true if hyperarc with given id has head vertices,
 * false otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasHeadVertices(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "hasHeadVertices "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not hyperarc->heads_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "hasHeadVertices: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Const vertex iterator pointing to the begin of the head vertices of
 * the hyperarc with the given id
 *
 * @param hyperarcId hyperarc id
 * @return Const vertex iterator pointing to the begin of the head vertices of
 * the hyperarc with the given id
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
headsBegin(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::headsBegin"
                                       " with "
                                       "the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->heads_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "headsBegin: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Const vertex iterator pointing to the end of the head vertices of
 * the hyperarc with the given id
 *
 * @param hyperarcId hyperarc id
 * @return Const vertex iterator pointing to the end of the head vertices of
 * the hyperarc with the given id
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
headsEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::headsEnd"
                                       " with the "
                                       "hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->heads_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "headsEnd: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Pair of vertex iterators pointing to begin and end of the heads
 *
 * @param hyperarcId
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator,
        typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>::
        vertex_iterator> DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
headsBeginEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::headsEnd"
                                       " with the "
                                       "hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(hyperarc->heads_->cbegin(),
                          hyperarc->heads_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "headsEnd: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}

/**
 * Get container of head vertices of given hyperarc id
 *
 \verbatim
 for (const auto& vertex : graph.heads(hyperarcId) {
    ...
 }
 \endverbatim
 *
 * @param hyperarcId hyperarc id
 * @return reference to container of head vertices of given hyperarc id
 * @throw std::out_of_range if the provided hyperarc id was never assigned.
 * @throw std::invalid_argument if the hyperarc with the provided id was
 * removed earlier from the hypergraph.
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename DirectedHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::Vertex_t*>> DirectedHypergraph<VertexTemplate_t,
    Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
heads(const HyperedgeIdType& hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::heads with the "
                                       "hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->heads_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "heads: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Establish consecutive hyperarc ids
 *
 * After deleting hyperarcs from the hypergraph, the hyperarc ids
 * are not consecutive anymore. This function reassigns
 * consecutive ids to the hyperarcs.
 *
 * Complexity: Linear
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
resetHyperarcIds() {
  // Notify observers
  this->NotifyObservers(ObservableEvent::RESET_EDGE_IDS);
  // erase all nullptrs
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(hyperarcs_->data(),
                                     hyperarcs_->data() +
                                             hyperarcs_->size(),
                                     static_cast<Hyperarc_t*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (hyperarcs_->data() + hyperarcs_->size()) -
          posFirstNullptr;
  // Resize
  hyperarcs_->resize(hyperarcs_->size() - nbNullptr);

  // assign new Ids
  HyperedgeIdType id(-1);
  for (auto it = hyperarcs_->begin(); it != hyperarcs_->end();
       ++it) {
    (*it)->setId(++id);  // assign new Id
  }

  // set vertexMaxId_ to the highest assigned id
  hyperarcMaxId_ = id;

  // remove nullptr entries from vector of hyperarc properties
  removeNullEntriesFromHyperarcProperties();
}


/**
 * Establish consecutive vertex and hyperarc ids
 *
 * After deleting vertices and hyperarcs from the hypergraph the vertex and
 * hyperarc ids are not consecutive anymore. This function reassigns
 * consecutive ids to the vertices and the hyperarcs.
 */

template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
resetIds() {
  this->resetVertexIds();
  resetHyperarcIds();
}


/**
 * Number of incoming hyperarcs of the vertex with given id
 *
 * @param vertexId Id of a vertex
 * @return Number of incoming hyperarcs of the vertex with given id
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
inDegree(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::inDegree"
                                       " with the "
                                       "vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->inHyperarcs_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "inDegree: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Check if vertex has incoming hyperarcs
 *
 * @param vertexId Id of a vertex
 * @return true if vertex with given id has incoming hyperarcs,
 * false otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hasInHyperarcs(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::hasInHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not vertex->inHyperarcs_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "hasInHyperarcs: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Const hyperarc iterator pointing to the begin of the incoming hyperarcs
 * of the vertex with the given id
 *
 * @param vertexId Id of a vertex
 * @return Const hyperarc iterator pointing to the begin of the incoming
 * hyperarcs of the vertex with the given id
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
inHyperarcsBegin(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "inHyperarcsBegin"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->inHyperarcs_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "inHyperarcsBegin: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Const hyperarc iterator pointing to the end of the incoming hyperarcs
 * of the vertex with the given id
 *
 * @param vertexId Id of a vertex
 * @return Const hyperarc iterator pointing to the end of the incoming
 * hyperarcs of the vertex with the given id
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
inHyperarcsEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::inHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->inHyperarcs_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "inHyperarcsEnd: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Pair of hyperarc iterators pointing to begin and end of the
 * incoming hyperarcs
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator, typename DirectedHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator> DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
inHyperarcsBeginEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::inHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(vertex->inHyperarcs_->cbegin(),
                          vertex->inHyperarcs_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "inHyperarcsBeginEnd: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Get container of incoming hyperarcs of given vertex id
 *
 \verbatim
 auto inArcsPtr = graph.inHyperarcs(vertexId);
 for (const auto& hyperarc : *inArcsPtr) {
    ...
 }
 \endverbatim
 *
 * @param vertexId Id of a vertex
 * @return reference to container of incoming hyperarcs of given vertex id
 * @throw std::out_of_range if the provided vertex id was never assigned.
 * @throw std::invalid_argument if the vertex with the provided id was
 * removed earlier from the hypergraph.
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> DirectedHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::
inHyperarcs(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::inHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->inHyperarcs_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "inHyperarcs: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Number of outgoing hyperarcs of the vertex with given id
 *
 * @param vertexId Id of a vertex
 * @return Number of outgoing hyperarcs of the vertex with given id
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
outDegree(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::outDegree"
                                       " with the "
                                       "vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->outHyperarcs_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "outDegree: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Check if vertex has outgoing hyperarcs
 *
 * @param vertexId Id of a vertex
 * @return true if vertex with given id has outgoing hyperarcs,
 * false otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hasOutHyperarcs(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "hasOutHyperarcs "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not vertex->outHyperarcs_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "hasOutHyperarcs: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Const hyperarc iterator pointing to the begin of the outgoing hyperarcs
 * of the vertex with the given id
 *
 * @param vertexId Id of a vertex
 * @return Const hyperarc iterator pointing to the begin of the outgoing
 * hyperarcs of the vertex with the given id
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
outHyperarcsBegin(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "outHyperarcsBegin"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->outHyperarcs_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "outHyperarcsBegin: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Const hyperarc iterator pointing to the end of the outgoing hyperarcs
 * of the vertex with the given id
 *
 * @param vertexId Id of a vertex
 * @return Const hyperarc iterator pointing to the end of the outgoing
 * hyperarcs of the vertex with the given id
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
outHyperarcsEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "outHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->outHyperarcs_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "outHyperarcsEnd: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Pair of hyperarc iterators pointing to begin and end of the
 * outgoing hyperarcs
 *
 * @param vertexId
 * @return
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator, typename DirectedHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator> DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
outHyperarcsBeginEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "outHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(vertex->outHyperarcs_->cbegin(),
                          vertex->outHyperarcs_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "outHyperarcsBeginEnd: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Get container of outgoing hyperarcs of given vertex id
 *
 \verbatim
 auto outArcsPtr = g.outHyperarcs(id);
 for (const auto& outHyperarc : *outArcsPtr) {
    ...
 }
 \endverbatim
 *
 * @param vertexId Id of a vertex
 * @return reference to container of outgoing hyperarcs of given vertex id
 * @throw std::out_of_range if the provided vertex id was never assigned.
 * @throw std::invalid_argument if the vertex with the provided id was
 * removed earlier from the hypergraph.
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> DirectedHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::
outHyperarcs(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::outHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->outHyperarcs_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "outHyperarcs: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Remove a vertex with given name from the directed hypergraph.
 *
 * @param vertexName Name of a vertex
 * @param removeImpliedHyperarcs flag that shows whether hyperarcs that contain
 * the vertex with the given name should be removed from the hypergraph
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeVertex(const std::string& vertexName,
             bool removeImpliedHyperarcs) {
  // Find vertex by name in the hypergraph and call the method removeVertex
  // that takes the id of the vertex
  const auto& vertex = this->vertexByName(vertexName);
  if (vertex == nullptr) {
    std::string errorMessage("Called DirectedHypergraph::removeVertex"
                                     " with the vertex name " + vertexName +
                                     " which does not exists in the directed "
                                             "hypergraph.\n");
    throw std::invalid_argument(errorMessage);
  }

  removeVertex(vertex->id(), removeImpliedHyperarcs);
}


/**
 * Remove a vertex with given id from the directed hypergraph.
 *
 * @param vertexId Identifier of a vertex
 * @param removeImpliedHyperarcs flag that shows whether hyperarcs that contain
 * the vertex with the given id should be removed from the hypergraph
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeVertex(const VertexIdType& vertexId,
             bool removeImpliedHyperarcs) {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove all implied hyperarcs if flag is set to true (default).
   * Otherwise, remove only the vertex v with given id 'vertexId' from the
   * tails and heads of the outgoing and incoming hyperarcs of v. Remove
   * hyperarcs that have neither tails nor heads.
   *
   * Delete the pointer at index vertexId in vertices_ and vertexProperties_
   * and assign nullptr.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::removeVertex"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }

    if (removeImpliedHyperarcs) {
      // hyperarcs that will be removed from the hypergraph
      std::vector<Hyperarc_t*> hyperarcsToBeRemoved;
      for (auto& hyperarc : *(hyperarcs_.get())) {
        // all hyperarcs that contain the vertex will be removed from the
        // hypergraph
        if (hyperarc != nullptr and isVertexOfHyperarc(vertexId,
                                                       hyperarc->id())) {
          hyperarcsToBeRemoved.emplace_back(hyperarc);
        }
      }
      // remove hyperarcs from hypergraph
      for (auto& hyperarc : hyperarcsToBeRemoved) {
        removeHyperarc(hyperarc->id());
      }
    } else {
      // remove vertex from tails_ (heads_) of its outgoing (incoming)
      // hyperarcs
      removeVertexDependencies(*vertex);
      // remove hyperarcs without tails and heads
      for (auto& hyperarc : *(hyperarcs_.get())) {
        if (hyperarc != nullptr and
            (not hasTailVertices(hyperarc->id()) and
             not hasHeadVertices(hyperarc->id()))) {
          removeHyperarc(hyperarc->id());
        }
      }
    }

    VertexIdType idx(vertexId);
    // Notify observers of change
    Remove_vertex_args args;
    args.vertexId_ = idx;
    args.removeImpliedHyperarcs_ = removeImpliedHyperarcs;
    this->NotifyObservers(ObservableEvent::VERTEX_REMOVED, &args);

    // assign NULL in vertices_ and vertexProperties_ at the index that
    // corresponds to the id of the vertex
    this->setVertexAndVertexPropertyEntryToNullptr(idx);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "removeVertex:"
            " index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
      throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Check if given vertex is a tail vertex of given hyperarc.
 *
 * @param vertexId The id od a vertex
 * @param hyperarcId The id of a hyperarc
 * @return true if vertex is a tail vertex of hyperarc, false
 * otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isTailVertexOfHyperarc(const VertexIdType& vertexId,
                       const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the vertex/hyperarc with the given ids.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex/hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "isTailVertexOfHyperarc with the "
                                       "hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "isTailVertexOfHyperarc with the "
                                       "vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    for (const auto& tail : *(hyperarc->tails_.get())) {
      if (tail->id() == vertex->id()) {
        return true;
      }
    }
    return false;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "isTailVertexOfHyperarc: " << '\n' <<
              "index vertex = " << vertexId << "; min = 0; max = " <<
              this->vertices_->size() << '\n' <<
              "index hyperarc = " << hyperarcId << "; min = 0; max = " <<
              hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Check if given vertex is a head vertex of given hyperarc.
 *
 * @param vertexId The id od a vertex
 * @param hyperarcId The id of a hyperarc
 * @return true if vertex is a head vertex of hyperarc, false
 * otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isHeadVertexOfHyperarc(const VertexIdType& vertexId,
                       const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the vertex/hyperarc with the given ids.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex/hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "isHeadVertexOfHyperarc with the "
                                       "hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "isHeadVertexOfHyperarc with the "
                                       "vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    for (const auto& tail : *(hyperarc->heads_.get())) {
      if (tail->id() == vertex->id()) {
        return true;
      }
    }
    return false;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "isTailVertexOfHyperarc: " << '\n' <<
              "index vertex = " << vertexId << "; min = 0; max = " <<
              this->vertices_->size() << '\n' <<
              "index hyperarc = " << hyperarcId << "; min = 0; max = " <<
              hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Check if given vertex is a tail or head vertex of given hyperarc.
 *
 * @param vertexId The id od a vertex
 * @param hyperarcId The id of a hyperarc
 * @return true if vertex is a tail or head vertex of hyperarc, false
 * otherwise
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isVertexOfHyperarc(const VertexIdType& vertexId,
                   const HyperedgeIdType & hyperarcId) const {
  try {
    if (isTailVertexOfHyperarc(vertexId, hyperarcId) or
          isHeadVertexOfHyperarc(vertexId, hyperarcId)) {
      return true;
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "Out of range in DirectedHypergraph::"
            "isVertexOfHyperarc\n";
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    std::cerr << "Invalid argument exception in DirectedHypergraph::"
            "isVertexOfHyperarc\n";
    throw;
  }
  return false;
}


/**
 * Get properties of the hyperarc with the provided id.
 *
 * @param hyperarcId Id of the hyperarc
 * @return Pointer to hyperarc property struct
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const HyperarcProperty* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHyperarcProperties(const HyperedgeIdType& hyperarcId) const {
  try {
    return hyperarcProperties_->at(hyperarcId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "getHyperarcProperties: index = " << hyperarcId <<
              "; min = 0; " <<
              "max = " << hyperarcProperties_->size() << '\n';
    throw;
  }
}

/**
 * Get properties of the hyperarc with the provided id.
 *
 * @param hyperarcId Id of the hyperarc
 * @return Pointer to hyperarc property struct
 *
 * Non-const version of getHyperarcProperties
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
HyperarcProperty* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHyperarcProperties_(const HyperedgeIdType& hyperarcId) const {
  try {
    return const_cast<HyperarcProperty*>(this->getHyperarcProperties(
            hyperarcId));
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "getHyperarcProperties_: index = " << hyperarcId <<
              "; min = 0; " <<
              "max = " << hyperarcProperties_->size() << '\n';
    throw;
  }
}


/**
 * Get root hypergraph
 *
 * @return the current object
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>*
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
getRootHypergraph() const {
  return this;
}


/**
 * \brief Add a hyperarc with the provided tails, heads, and additional
 * parameters (if needed to create a hyperarc of type Hyperarc_t) to the
 * hypergraph
 *
 * @param tails_heads tail and head vertex names
 * @param args additional arguments to create a hyperarc of type Hyperarc_t
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const Hyperarc_t* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
 VertexProperty, HyperarcProperty, HypergraphProperty>::
addHyperarc(decltype(hglib::NAME),
            const TailAndHeadNames& tails_heads,
            const typename Hyperarc_t::SpecificAttributes& attributes) {
  /*
   * Get first the ids of the vertices with given names. An invalid_argurment
   * exception is thrown if a vertex name does not exists in the hypergraph.
   * Then, call addHyperarc(TailAndHeadIds, args) to add the hyperarc.
   */
  std::vector<VertexIdType> tailIds =
          this->getVertexIdsFromNames(tails_heads.first);
  std::vector<VertexIdType> headIds =
          this->getVertexIdsFromNames(tails_heads.second);
  return addHyperarc(std::make_pair(tailIds, headIds), attributes);
}


/**
 * \brief Add a hyperarc with the provided tail ids, head ids, and additional
 * parameters (if needed to create a hyperarc of type Hyperarc_t) to the
 * hypergraph
 *
 * @param tails_heads tail and head vertex ids
 * @param args additional arguments to create a hyperarc of type Hyperarc_t
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const Hyperarc_t* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
addHyperarc(const TailAndHeadIds& tails_heads,
            const typename Hyperarc_t::SpecificAttributes& attributes) {
  // Check if all tail and head vertices are declared in the hypergraph.
  // If not, an exception is thrown.
  // In the case that multi-hyperarcs are not allowed we check if there exists
  // already a hyperarc with the same tails and heads. If this is the case we
  // throw an exception. Otherwise and in case multi-hyperarcs are allowed we
  // add the hyperarc to the hypergraph.

  // The following 2 vectors will hold pointers to the tail (resp. head)
  // DirectedVertex_t corresponding to the provided vertex ids
  std::vector<const Vertex_t*> tails_p_const, heads_p_const;
  for (const auto& vertexId : tails_heads.first) {
    tails_p_const.push_back(this->vertexById(vertexId));
  }
  for (const auto& vertexId : tails_heads.second) {
    heads_p_const.push_back(this->vertexById(vertexId));
  }

  if (not this->allowMultiHyperedges_) {
    // check if there exists a hyperarc with the same tails and heads
    for (const auto& hyperarc : *(hyperarcs_.get())) {
      if (hyperarc != nullptr and
          hyperarc->DirectedHyperedgeBase<Vertex_t>::
                  compare(tails_p_const, heads_p_const)) {
        // build error message
        std::string tails("");
        for (auto& tailId : tails_heads.first) {
          tails += std::to_string(tailId) + ", ";
        }
        std::string heads("");
        for (auto& headId : tails_heads.second) {
          heads += std::to_string(headId) + ", ";
        }
        std::string errorMessage("Multiple hyperarcs are not allowed in this "
                                         "hypergraph, but the hyperarc with "
                                         "tail ids: " + tails + " and"
                                         " head ids: " + heads +
                                 " already exists.\n");
        throw std::invalid_argument(errorMessage);
      }
    }
  }

  // Non-const pointers are needed for the Hyperarc_t ctor
  std::unique_ptr<GraphElementContainer<Vertex_t*>> tails_p =
          create_filtered_vector<Vertex_t*>();
  std::unique_ptr<GraphElementContainer<Vertex_t*>> heads_p =
          create_filtered_vector<Vertex_t*>();
  for (const Vertex_t* vPtr : tails_p_const) {
    tails_p->push_back(const_cast<Vertex_t*>(vPtr));
  }
  for (const Vertex_t* vPtr : heads_p_const) {
    heads_p->push_back(const_cast<Vertex_t*>(vPtr));
  }
  // add hyperarc
  const auto hyperarc =
          buildHyperarcAndItsDependencies(std::move(tails_p),
                                          std::move(heads_p),
                                          attributes);
  return hyperarc;
}


/**
 * Get arguments to create the hyperarc
 *
 * Returns a tuple that can be used to add a hyperarc via
 * addHyperarc(const std::tuple<Args...>&).
 *
 * @param hyperarcId
 * @return a tuple of arguments to build a hyperarc
 */
template <template <typename> class Vertex_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
ArgumentsToCreateDirectedHyperedge<Hyperarc_t> DirectedHypergraph<Vertex_t,
        Hyperarc_t, VertexProperty, EdgeProperty, HypergraphProperty>::
argumentsToCreateHyperarc(const hglib::HyperedgeIdType& hyperarcId) const {
  try {
    const auto &hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "argumentsToCreateHyperarc with "
                                       "the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->argumentsToCreateHyperarc();
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * Remove a hyperarc with the given id from the hypergraph.
 *
 * @param hyperarcId Identifier of the hyperarc
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
removeHyperarc(const HyperedgeIdType& hyperarcId) {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove hyperarc h with given id from the incoming/outgoing hyperarc list
   * of the head/tail vertices of h.
   * Delete the pointer at index hyperarcId in hyperarcs_ and
   * hyperarcProperties_ and assign nullptr.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::removeHyperarc"
                                       " with "
                                       "the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }

    HyperedgeIdType idx(hyperarcId);
    removeHyperarcDependencies(*hyperarc);
    // Notify observers of change
    this->NotifyObservers(ObservableEvent::HYPEREDGE_REMOVED, &idx);

    // delete hyperarc
    delete hyperarcs_->operator[](idx);
    hyperarcs_->operator[](idx) = nullptr;
    // assign nullptr in hyperarcProperties_ at the index that corresponds
    // to the id of the hyperarc
    delete hyperarcProperties_->operator[](idx);
    hyperarcProperties_->operator[](idx) = nullptr;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "removeHyperarc: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}

/* ****************************************************************************
 ******************************************************************************
 *                           Protected methods
 ******************************************************************************
 *****************************************************************************/


/**
 * Construct a hyperarc and a HyperarcProperty object
 *
 * Add a hyperarc if all tail/head vertices were declared before in this
 * hypergraph. Otherwise an exception is thrown.
 * A HyperarcProperty object is created via the default constructor. Both, the
 * hyperarc (of type Hyperarc_t) and the HyperarcProperty are added to the
 * directed hypergraph.
 *
 * @param tails_heads names of tail and head vertices
 * @param args additional parameters that are needed to build a hyperarc of
 * type Hyperarc_t.
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const Hyperarc_t* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
buildHyperarcAndItsDependencies(
        std::unique_ptr<GraphElementContainer<Vertex_t*>> tails,
        std::unique_ptr<GraphElementContainer<Vertex_t*>> heads,
        const typename Hyperarc_t::SpecificAttributes& attributes) {
  // A hyperarc x is created and a
  // pointer to this hyperarc is added to the list of hyperarcs of the
  // hypergraph. DirectedHyperedge dependencies are solved:
  // 1. Pointer to hyperarc x is added to the out-hyperarcs list of the tail
  // vertices of x.
  // 2. Pointer to hyperarc x is added to the in-hyperarcs list of the head
  // vertices of x.
  // A hyperarc property object is created using the default ctor.

  // create the hyperarc and add it to the list of hyperarcs of the hypergraph
  auto hyperarc = new Hyperarc_t(++hyperarcMaxId_, std::move(tails),
                                 std::move(heads), attributes);
  hyperarcs_->push_back(hyperarc);

  // Solve hyperarc dependencies
  // add hyperarc to outHyperarcs_ of tail vertices of the hyperarc
  for (auto&& tail : *(hyperarc->tails_.get())) {
    tail->addOutHyperarc(hyperarc);
  }
  // add hyperarc to inHyperarcs_ of head vertices of the hyperarc
  for (auto&& head : *(hyperarc->heads_.get())) {
    head->addInHyperarc(hyperarc);
  }

  // build hyperarc property and add it to the list of hyperarc properties
  auto hyperarcProperty = new HyperarcProperty();
  hyperarcProperties_->push_back(hyperarcProperty);
  return hyperarc;
}


/**
 * Remove vertex dependencies.
 *
 * Vertex dependencies of vertex v are removed, that is:
 * (i) the vertex v is removed from the list of tail vertices of the outgoing
 * hyperarcs of v,
 * (ii) the vertex v is removed from the list of head vertices of the incoming
 * hyperarcs of v.
 *
 * @param vertex
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeVertexDependencies(const Vertex_t& vertex) {
  // remove the vertex from the list of tails of the vertex's outgoing hyperarcs
  for (auto&& edge : *(vertex.outHyperarcs_.get())) {
    edge->removeTailVertex(vertex);
  }
  // remove the vertex from the list of heads of the vertex's incoming hyperarcs
  for (auto&& edge : *(vertex.inHyperarcs_.get())) {
    edge->removeHeadVertex(vertex);
  }
}

/**
 * Remove hyperarc dependencies.
 *
 * Removes hyperarc dependencies of hyperarc h, that is:
 * (i) the hyperarc h is removed from the outgoing hyperarc of the tail
 * vertices of the hyperarc h,
 * (ii) the hyperarc h is removed from the incoming hyperarc of the head
 * vertices of the hyperarc h.
 *
 * @param hyperarc
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeHyperarcDependencies(const Hyperarc_t& hyperarc) {
  // remove hyperarc from outgoing hyperarc of tails
  for (auto&& tail : *(hyperarc.tails_.get())) {
    tail->removeOutHyperarc(hyperarc);
  }
  // remove hyperarc from incoming hyperarc of heads
  for (auto&& head : *(hyperarc.heads_.get())) {
    head->removeInHyperarc(hyperarc);
  }
}


/**
 * Remove nullptr entries from vector of hyperarc properties.
 *
 * Complexity: Linear
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeNullEntriesFromHyperarcProperties() {
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(hyperarcProperties_->data(),
                                     hyperarcProperties_->data() +
                                             hyperarcProperties_->size(),
                                     static_cast<HyperarcProperty*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (hyperarcProperties_->data() +
          hyperarcProperties_->size()) - posFirstNullptr;
  // Resize
  hyperarcProperties_->resize(hyperarcProperties_->size() - nbNullptr);
}


/// Return pointer to hyperarc container
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::HyperarcContainerPtr
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
getRootHyperarcContainerPtr() const {
  return hyperarcs_;
}
}  // namespace hglib
