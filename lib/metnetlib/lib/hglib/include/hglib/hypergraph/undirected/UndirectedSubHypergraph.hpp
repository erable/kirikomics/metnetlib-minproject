/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 04.10.17.
//

#include "UndirectedSubHypergraph.h"

#include <vector>

#include "filtered_vector.h"

namespace hglib {

/// Constructor
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
UndirectedSubHypergraph(UndirectedHypergraphInterface<VertexTemplate_t,
                        Hyperedge_t, VertexProperty, HyperedgeProperty,
                        HypergraphProperty>* parent,
                        const std::unordered_set<VertexIdType>&
                        whitelistedVertexIds,
                        const std::unordered_set<HyperedgeIdType>&
                        whitelistedHyperedgeIds) :
        UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty, HypergraphProperty>(),
        Observer(),
        parent_(parent) {
  // build whitelisted vertex filtered_vector
  std::vector<Vertex_t*> whitelistFilterVertices;
  for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
    whitelistFilterVertices.push_back(nullptr);
  }
  for (VertexIdType vertexId : whitelistedVertexIds) {
    try {
      const auto& vertex = parent_->vertexById(vertexId);
      if (vertex != nullptr) {
        whitelistFilterVertices[vertexId] = const_cast<Vertex_t *>(vertex);
      }
    } catch (const std::out_of_range& oor) {
      std::string errorMessage("");
      if (parent_->vertexContainerSize() == 0) {
        errorMessage = "There is no vertex in the parent graph. You cannot"
                " add vertices to the subgraph\n";
      } else {
        errorMessage = "Vertex with id: " + std::to_string(vertexId)
                       + " is out of range -> min=0; max="
                       + std::to_string((parent_->vertexContainerSize() - 1))
                       + '\n';
      }
      std::cerr << errorMessage;
    }
  }
  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(whitelistFilterVertices));


  // build whitelisted hyperedge filtered_vector
  std::vector<Hyperedge_t*> whitelistFilterHyperedges;
  for (size_t i = 0; i < parent_->hyperedgeContainerSize(); ++i) {
    whitelistFilterHyperedges.push_back(nullptr);
  }
  for (HyperedgeIdType hyperedgeId : whitelistedHyperedgeIds) {
    try {
      const auto& hyperedge = parent_->hyperedgeById(hyperedgeId);
      if (hyperedge != nullptr) {
        whitelistFilterHyperedges[hyperedgeId] = const_cast<
                Hyperedge_t*>(hyperedge);
      }
    } catch (const std::out_of_range& oor) {
      std::string errorMessage("");
      if (parent_->hyperedgeContainerSize() == 0) {
        errorMessage = "There is no hyperedge in the parent graph. You cannot"
                " add hyperedges to the subgraph\n";
      } else {
        errorMessage = "Hyperedge with id: " + std::to_string(hyperedgeId)
                       + " is out of range -> min=0; max="
                       + std::to_string((parent_->hyperedgeContainerSize() - 1))
                       + '\n';
      }
      std::cerr << errorMessage;
    }
  }
  // create actual hyperedge list
  whitelistedHyperedgesList_ = create_filtered_vector<Hyperedge_t*>(
      parent->getRootHyperedgeContainerPtr(),
      std::make_unique<WhitelistFilter<Hyperedge_t*>>(
          whitelistFilterHyperedges));

  // add itself as observer of the parent
  parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);

  // Create HypergraphProperty object
  subHypergraphProperty_ = new HypergraphProperty(
          *parent_->getHypergraphProperties_());
}


/// Copy constructor
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
UndirectedSubHypergraph(const UndirectedSubHypergraph& other) :
        Observer(),
        parent_(other.parent_) {
  // copy whitelisted vertex filtered_vector
  std::vector<Vertex_t*> whitelistFilterVertices;
  for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
    whitelistFilterVertices.push_back(nullptr);
  }
  for (const auto& vertex : other.vertices()) {
    whitelistFilterVertices[vertex->id()] =
            const_cast<Vertex_t*>(vertex);
  }
  // create actual vertices list
  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(whitelistFilterVertices));


  // copy whitelisted hyperarc filtered_vector
  std::vector<Hyperedge_t*> whitelistFilterHyperedges;
  for (size_t i = 0; i < parent_->hyperedgeContainerSize(); ++i) {
    whitelistFilterHyperedges.push_back(nullptr);
  }
  for (const auto& hyperedge : other.hyperedges()) {
    whitelistFilterHyperedges[hyperedge->id()] =
            const_cast<Hyperedge_t*>(hyperedge);
  }
  // create actual vertices list
  whitelistedHyperedgesList_ = create_filtered_vector<Hyperedge_t*>(
      parent_->getRootHyperedgeContainerPtr(),
      std::make_unique<WhitelistFilter<Hyperedge_t*>>(
          whitelistFilterHyperedges));


  // Attach itself as observer to parent graph
  parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);

  // Create HypergraphProperty object
  subHypergraphProperty_ = new HypergraphProperty(
          *other.getHypergraphProperties_());
}


/// Destructor
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
~UndirectedSubHypergraph() {
  delete subHypergraphProperty_;
  // Detach itself as observer of the parent graph
  if (parent_ != nullptr) {
    parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);

    // Subgraphs whose parent (this subgraph) becomes the current grand-parent.
    // Add subgraphs as observers to the current grand-parent.
    // Clear list of observers of this subgraph.
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver = itEvent->second.begin();
           itObserver != itEvent->second.end(); ++itObserver) {
        auto subgraph = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
                Hyperedge_t, VertexProperty, HyperedgeProperty,
                HypergraphProperty>*>(*itObserver);
        subgraph->parent_ = parent_;
        subgraph->parent_->AddObserver(subgraph, itEvent->first);
      }
      this->observers_[itEvent->first].clear();
    }
  }
}


/// Assignment operator
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>& UndirectedSubHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
operator=(const UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>& source) {
  // check for self assignment
  if (this != &source) {
    // Set the parent of all childs in the subgraph-hierarchy to null
    std::unordered_set<UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
            VertexProperty, HyperedgeProperty, HypergraphProperty>*> observers;
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver : itEvent->second) {
        auto subgraph = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
                Hyperedge_t, VertexProperty, HyperedgeProperty,
                HypergraphProperty>*>(itObserver);
        observers.insert(subgraph);
      }
    }
    for (auto& subgraph : observers) {
      subgraph->setParentOfChildsToNull();
      this->DeleteObserver(subgraph, ObservableEvent::VERTEX_REMOVED);
      this->DeleteObserver(subgraph, ObservableEvent::HYPEREDGE_REMOVED);
      this->DeleteObserver(subgraph, ObservableEvent::RESET_VERTEX_IDS);
      this->DeleteObserver(subgraph, ObservableEvent::RESET_EDGE_IDS);
    }

    // Detach itself as observer from parent graph
    parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);

    // Assign parent
    parent_ = source.parent_;

    // copy whitelisted vertex filtered_vector
    std::vector<Vertex_t*> whitelistFilterVertices;
    for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
      whitelistFilterVertices.push_back(nullptr);
    }
    for (const auto& vertex : source.vertices()) {
      whitelistFilterVertices[vertex->id()] =
              const_cast<Vertex_t*>(vertex);
    }
    // create actual vertices list
    whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
            parent_->getRootVerticesContainerPtr(),
            std::make_unique<WhitelistFilter<Vertex_t*>>(
                whitelistFilterVertices));


    // copy whitelisted hyperarc filtered_vector
    std::vector<Hyperedge_t*> whitelistFilterHyperedges;
    for (size_t i = 0; i < parent_->hyperedgeContainerSize(); ++i) {
      whitelistFilterHyperedges.push_back(nullptr);
    }
    for (const auto& hyperedge : source.hyperedges()) {
      whitelistFilterHyperedges[hyperedge->id()] =
              const_cast<Hyperedge_t*>(hyperedge);
    }
    // create actual vertices list
    whitelistedHyperedgesList_ = create_filtered_vector<Hyperedge_t*>(
            parent_->getRootHyperedgeContainerPtr(),
            std::make_unique<WhitelistFilter<Hyperedge_t*>>(
                whitelistFilterHyperedges));

    // init hypergraph property
    delete subHypergraphProperty_;
    subHypergraphProperty_ = new HypergraphProperty(
            *source.getHypergraphProperties_());

    // Attach itself as observer to parent graph
    parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);
  }
  return *this;
}


/// Check if multi-hyperedges are allowed in this hypergraph
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
allowMultiHyperedges() const {
  throwExceptionOnNullptrParent();
  return parent_->allowMultiHyperedges();
}


/// Number of hyperedges in the hypergraph
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbHyperedges() const {
  throwExceptionOnNullptrParent();
  size_t nbHyperedges = 0;
  auto it = whitelistedHyperedgesList_->cbegin();
  auto end = whitelistedHyperedgesList_->cend();
  for (; it != end; ++it) {
    ++nbHyperedges;
  }
  return nbHyperedges;
}


/// Highest assigned hyperedge id of the hypergraph
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgeContainerSize() const {
  throwExceptionOnNullptrParent();
  return parent_->hyperedgeContainerSize();
}


/**
  * \brief Add a hyperedge with the provided vertices, and additional
  * parameters (if needed to create a hyperedge of type Hyperedge_t) to the
  * hypergraph
  */
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
addHyperedge(decltype(hglib::NAME),
             const VertexNames& vertices,
             const typename Hyperedge_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  /*
   * Try to add the hyperedge in the parent hypergraph. If this was done
   * with success, then the hyperedge is added to the whitelist of the sub-
   * hypergraph.
   */
  try {
    const auto hyperedge = parent_->addHyperedge(hglib::NAME, vertices,
                                               attributes);
    whitelistedHyperedgesList_->removeFilteredOutItem(
            const_cast<Hyperedge_t*>(hyperedge));
    return hyperedge;
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument(ia.what());
  }
}


/**
* \brief Add a hyperedge with the provided vertex ids, and additional
* parameters (if needed to create a hyperedge of type Hyperedge_t) to the
* hypergraph
*/
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
addHyperedge(const VertexIds& vertices,
             const typename Hyperedge_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  /*
   * Try to add the hyperedge in the parent hypergraph. If this was done
   * with success, then the hyperedge is added to the whitelist of the sub-
   * hypergraph.
   */
  try {
    const auto hyperedge = parent_->addHyperedge(vertices, attributes);
    whitelistedHyperedgesList_->removeFilteredOutItem(
            const_cast<Hyperedge_t*>(hyperedge));
    return hyperedge;
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument(ia.what());
  }
}


/// Get arguments to create the hyperedge
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
ArgumentsToCreateUndirectedHyperedge<Hyperedge_t> UndirectedSubHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
argumentsToCreateHyperedge(const hglib::HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  return parent_->argumentsToCreateHyperedge(hyperedgeId);
}


/// Remove a hyperedge with the given id from the hypergraph.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
removeHyperedge(const HyperedgeIdType& hyperedgeId) {
  throwExceptionOnNullptrParent();
  /*
   * Check if there exists a hyperedge in the sub-hypergraph with the given id.
   * If yes, remove it from the whitelist.
   * If no, an exception is thrown.
   */
  try {
    const auto hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "removeHyperedge"
                                       " with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }

    // Notify observers of change
    HyperedgeIdType nonConstHyperedgeId(hyperedgeId);
    this->NotifyObservers(ObservableEvent::HYPEREDGE_REMOVED,
                          &nonConstHyperedgeId);
    whitelistedHyperedgesList_->addFilteredOutItem(
            const_cast<Hyperedge_t*>(hyperedge));
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedgeById: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// Search hyperedge list for an hyperedge with the provided id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
hyperedgeById(const HyperedgeIdType & hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check in constant time if the hyperedge id is in the whitelist of the sub-
   * hypergraph. If yes, we get the ptr to hyperedge from the parent hypergraph.
   * If no, we return a nullptr.
   *
   * A out_of_range exception is thrown if the given hyperedge id is out of
   * range.
   */
  try {
    const auto& hyperedge = whitelistedHyperedgesList_->at(hyperedgeId);
    return whitelistedHyperedgesList_->filter_out(hyperedge) ?
           nullptr : hyperedge;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedgeById: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  }
}


/// Const hyperedge iterator pointing to begin of the hyperedges
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::hyperedge_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBegin() const {
  throwExceptionOnNullptrParent();
  return whitelistedHyperedgesList_->cbegin();
}


/// Const hyperedge iterator pointing to end of the hyperedges
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::hyperedge_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesEnd() const {
  throwExceptionOnNullptrParent();
  return whitelistedHyperedgesList_->cend();
}


/// \brief Pair of const hyperedge iterator pointing to begin and end
/// of the hyperedges
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::pair<typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator,
        typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::hyperedge_iterator>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBeginEnd() const {
  throwExceptionOnNullptrParent();
  return std::make_pair(whitelistedHyperedgesList_->cbegin(),
                        whitelistedHyperedgesList_->cend());
}


/// Return a reference to the container of hyperedges.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::HyperedgeContainer&
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedges() const {
  throwExceptionOnNullptrParent();
  return *(whitelistedHyperedgesList_.get());
}


/// Return number of vertices in the hyperedge with given id.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbImpliedVertices(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::nbVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of implied vertices in this hyperedge in the subgraph
    size_t nbVerticesInHyperedge(0);
    auto it = impliedVerticesBegin(hyperedgeId);
    auto end = impliedVerticesEnd(hyperedgeId);
    for (; it != end; ++it) {
      ++nbVerticesInHyperedge;
    }
    return nbVerticesInHyperedge;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "nbVertices: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// Check if hyperedge with given id has vertices.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hasVertices(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::hasVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // compare begin and end
    auto begin = impliedVerticesBegin(hyperedgeId);
    auto end = impliedVerticesEnd(hyperedgeId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hasVertices: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// \brief Const vertex iterator pointing to the begin of the vertices
/// of the hyperedge with the given id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesBegin(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "impliedVerticesBegin"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperedge->vertices_,
                    whitelistedVerticesList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "impliedVerticesBegin: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// \brief Const vertex iterator pointing to the end of the vertices
/// of the hyperedge with the given id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesEnd(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "impliedVerticesEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperedge->vertices_,
                    whitelistedVerticesList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "impliedVerticesEnd: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// Const vertex iterator pointing to begin and end of the vertices of
/// the hyperedge with the given id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::pair<typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::vertex_iterator,
        typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::vertex_iterator>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesBeginEnd(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "impliedVerticesBeginEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator pair
    auto begin = impliedVerticesBegin(hyperedgeId);
    auto end = impliedVerticesEnd(hyperedgeId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "impliedVerticesBeginEnd: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// Get container of vertices of given hyperedge id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename UndirectedSubHypergraph<
    VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
    HypergraphProperty>::Vertex_t*>> UndirectedSubHypergraph<VertexTemplate_t,
    Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
impliedVertices(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "impliedVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-"
          "hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    // create FilteredVector
    return create_filtered_vector(hyperedge->vertices_,
                                  whitelistedVerticesList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "impliedVertices: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// Highest assigned vertex id of the hypergraph
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
vertexContainerSize() const {
  throwExceptionOnNullptrParent();
  return parent_->vertexContainerSize();
}


/// Number of vertices in the hypergraph
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbVertices() const {
  throwExceptionOnNullptrParent();
  size_t nbVertices(0);
  auto it = whitelistedVerticesList_->cbegin();
  auto end = whitelistedVerticesList_->cend();
  for (; it != end; ++it) {
    ++nbVertices;
  }
  return nbVertices;
}


/// Search vertex list for a vertex with the provided name
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t*
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
vertexByName(const std::string& vertexName) const {
  throwExceptionOnNullptrParent();
  for (const auto& vertex : *(whitelistedVerticesList_.get())) {
    if (vertex->name().compare(vertexName) == 0) {
      return vertex;
    }
  }
  return nullptr;
}


/// Search vertex list for a vertex with the provided id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t*
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
vertexById(const VertexIdType & vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = whitelistedVerticesList_->at(vertexId);
    return whitelistedVerticesList_->filter_out(vertex) ? nullptr : vertex;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "vertexById: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/// Const vertex iterator pointing to begin of vertices
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
verticesBegin() const {
  throwExceptionOnNullptrParent();
  return whitelistedVerticesList_->cbegin();
}


/// Const vertex iterator pointing to end of vertices
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
verticesEnd() const {
  throwExceptionOnNullptrParent();
  return whitelistedVerticesList_->cend();
}


/// Const vertex iterator pointing to begin and end of vertices
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::pair<typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::vertex_iterator,
        typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::vertex_iterator>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
verticesBeginEnd() const {
  throwExceptionOnNullptrParent();
  return std::make_pair(whitelistedVerticesList_->cbegin(),
                        whitelistedVerticesList_->cend());
}



/// Return a reference to the container of vertices.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::VertexContainer&
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
vertices() const {
  throwExceptionOnNullptrParent();
  return *(whitelistedVerticesList_.get());
}


/// Add a vertex
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t*
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
addVertex(const typename Vertex_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  // TODO(mw): Do we allow to add a vertex that is already in the parent
  // hypergraph but not yet in the whitelist of the sub-hypergraph?
  // Currently we don't.
  try {
    // Add new vertex to parent
    const auto vertex = parent_->addVertex(attributes);
    whitelistedVerticesList_->removeFilteredOutItem(
        const_cast<Vertex_t*>(vertex));
    return vertex;
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument(ia.what());
  }
}


/// Add a vertex with a given name
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t*
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
addVertex(const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  // TODO(mw): Do we allow to add a vertex that is already in the parent
  // hypergraph but not yet in the whitelist of the sub-hypergraph?
  // Currently we don't.
  try {
    // Add new vertex to parent
    const auto vertex = parent_->addVertex(name, attributes);
    whitelistedVerticesList_->removeFilteredOutItem(
        const_cast<Vertex_t*>(vertex));
    return vertex;
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument(ia.what());
  }
}


/// Get arguments to create the vertex
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
ArgumentsToCreateVertex<typename UndirectedSubHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::Vertex_t> UndirectedSubHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
argumentsToCreateVertex(const hglib::VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = whitelistedVerticesList_->at(vertexId);
    if (not whitelistedVerticesList_->filter_out(vertex)) {
      return whitelistedVerticesList_->operator[](
              vertexId)->argumentsToCreateVertex();
    } else {
      throw std::invalid_argument("Vertex with " + std::to_string(vertexId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "vertexById: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/// Number of hyperedges that contain the vertex with given id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbHyperedges(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::nbHyperedges"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of hyperedges
    size_t nbHyperedgesWithGivenVertex(0);
    auto it = hyperedgesBegin(vertexId);
    auto end = hyperedgesEnd(vertexId);
    for (; it != end; ++it) {
      ++nbHyperedgesWithGivenVertex;
    }
    return nbHyperedgesWithGivenVertex;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "nbHyperedges: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// Check if the vertex is part of at least one hyperedge
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
isContainedInAnyHyperedge(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "isContainedInAnyHyperedge"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // check if there hyperedges
    auto begin = hyperedgesBegin(vertexId);
    auto end = hyperedgesEnd(vertexId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "isContainedInAnyHyperedge: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// \brief Const hyperedge iterator pointing to the begin of the
/// hyperedges that contain the vertex with the given id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::hyperedge_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBegin(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "hyperedgesBegin"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperedge_t*>> tmp =
            create_filtered_vector<Hyperedge_t*>(
                    vertex->hyperedges_,
                    whitelistedHyperedgesList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedgesBegin: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// \brief Const hyperedge iterator pointing to the end of the
/// hyperedges that contain the vertex with the given id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::hyperedge_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "hyperedgesEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperedge_t*>> tmp =
            create_filtered_vector<Hyperedge_t*>(
                    vertex->hyperedges_,
                    whitelistedHyperedgesList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedgesEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// \brief Const hyperedge iterator pointing to begin and end of the
/// hyperedges that contain the vertex with the given id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::pair<typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator,
        typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::hyperedge_iterator>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBeginEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "hyperedgesBeginEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create pair of iterators
    auto begin = hyperedgesBegin(vertexId);
    auto end = hyperedgesEnd(vertexId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedgesBeginEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}



/// Get container of hyperedges that contain the vertex with the given id
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<
    Hyperedge_t*>> UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>::
hyperedges(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "hyperedges"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed"
          " sub-hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    // create FilteredVector
    return create_filtered_vector(vertex->hyperedges_,
                                  whitelistedHyperedgesList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedges: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// Remove a vertex with given name from the hypergraph.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
removeVertex(const std::string& vertexName,
             bool removeImpliedHyperedges) {
  throwExceptionOnNullptrParent();
  // Find vertex by name in the hypergraph and call the method removeVertex
  // that takes the id of the vertex
  const auto& vertex = this->vertexByName(vertexName);
  if (vertex == nullptr) {
    std::string errorMessage("Called UndirectedSubHypergraph::removeVertex"
                                     " with the vertex name " + vertexName +
                             " which does not exists in the undirected "
                                     "subhypergraph.\n");
    throw std::invalid_argument(errorMessage);
  }

  removeVertex(vertex->id(), removeImpliedHyperedges);
}



/// Remove a vertex with given id from the hypergraph.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
removeVertex(const VertexIdType& vertexId,
             bool removeImpliedHyperedges) {
  throwExceptionOnNullptrParent();
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before, or does not take part of the
   * sub-hypergraph.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove all implied hyperedges from the sub-hypergraph if flag is set to
   * true (default).
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::removeVertex"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    if (removeImpliedHyperedges) {
      // hyperedges that will be removed from the hypergraph
      std::vector<Hyperedge_t*> hyperedgesToBeRemoved;
      for (auto& hyperedge : *(whitelistedHyperedgesList_.get())) {
        // all hyperedges that contain the vertex will be removed from the
        // hypergraph
        if (isVertexOfHyperedge(vertexId, hyperedge->id())) {
          hyperedgesToBeRemoved.emplace_back(hyperedge);
        }
      }
      // remove hyperedges from hypergraph
      for (const auto& hyperedge : hyperedgesToBeRemoved) {
        removeHyperedge(hyperedge->id());
      }
    }

    // Notify observers of change
    Remove_vertex_args args;
    args.vertexId_ = vertexId;
    args.removeImpliedHyperarcs_ = removeImpliedHyperedges;
    this->NotifyObservers(ObservableEvent::VERTEX_REMOVED, &args);

    // remove vertex from sub-hypergraph
    whitelistedVerticesList_->addFilteredOutItem(
            const_cast<Vertex_t*>(vertex));
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "removeVertex:"
            " index = " << vertexId << "; min = 0; max = "
              << this->whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Check if given vertex is a vertex of given hyperedge.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
isVertexOfHyperedge(const VertexIdType& vertexId,
                    const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex and hyperedge are part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "isVertexOfHyperedge"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "isVertexOfHyperedge: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }

  try {
    const auto hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "isVertexOfHyperedge"
                                       " with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected sub-"
              "hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "isVertexOfHyperedge: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }

  auto it = impliedVerticesBegin(hyperedgeId);
  auto end = impliedVerticesEnd(hyperedgeId);
  for (; it != end; ++it) {
    if ((*it)->id() == vertexId) {
      return true;
    }
  }
  return false;
}


/// Get properties of the hyperedge with the provided id.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const HyperedgeProperty* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHyperedgeProperties(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  try {
    if (whitelistedHyperedgesList_->at(hyperedgeId) != nullptr) {
      return parent_->getHyperedgeProperties(hyperedgeId);
    } else {
      throw std::invalid_argument("Hyperedge with " +
                                  std::to_string(hyperedgeId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in"
            " UndirectedSubHypergraph::getHyperedgeProperties: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  }
}


/// Get properties of the hyperedge with the provided id.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
HyperedgeProperty* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHyperedgeProperties_(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  try {
    if (whitelistedHyperedgesList_->at(hyperedgeId) != nullptr) {
      return parent_->getHyperedgeProperties_(hyperedgeId);
    } else {
      throw std::invalid_argument("Hyperedge with " +
                                  std::to_string(hyperedgeId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in"
            " UndirectedSubHypergraph::getHyperedgeProperties_: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  }
}


/// Get properties of the vertex with the provided id.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const VertexProperty* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
getVertexProperties(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    if (whitelistedVerticesList_->at(vertexId) != nullptr) {
      return parent_->getVertexProperties(vertexId);
    } else {
      throw std::invalid_argument("vertex with " +
                                  std::to_string(vertexId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in"
            " UndirectedSubHypergraph::getVertexProperties: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/// Get properties of the vertex with the provided id.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
VertexProperty* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
getVertexProperties_(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    if (whitelistedVerticesList_->at(vertexId) != nullptr) {
      return parent_->getVertexProperties_(vertexId);
    } else {
      throw std::invalid_argument("vertex with " +
                                  std::to_string(vertexId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in"
            " UndirectedSubHypergraph::getVertexProperties_: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/// Get properties of the hypergraph.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const HypergraphProperty* UndirectedSubHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHypergraphProperties() const {
  throwExceptionOnNullptrParent();
  return subHypergraphProperty_;
}


/// Get properties of the hypergraph.
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
HypergraphProperty* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHypergraphProperties_() const {
  throwExceptionOnNullptrParent();
  return const_cast<HypergraphProperty*>(getHypergraphProperties());
}


/// Get root hypergraph
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
      VertexProperty, HyperedgeProperty, HypergraphProperty>*
      UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
              HyperedgeProperty, HypergraphProperty>::
getRootHypergraph() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootHypergraph();
}



/// Observer update method
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
Update(const Observable& o, ObservableEvent e, void* arg) {
  throwExceptionOnNullptrParent();
  if (&o == parent_) {
    switch (e) {
      case ObservableEvent::VERTEX_REMOVED: {
        Remove_vertex_args* args = static_cast<Remove_vertex_args*>(arg);
        try {
          const auto& vertex = whitelistedVerticesList_->at(args->vertexId_);
          if (not whitelistedVerticesList_->filter_out(vertex)) {
            removeVertex(args->vertexId_, args->removeImpliedHyperarcs_);
          }
        } catch (const std::out_of_range& oor) {
          std::cerr << "IndexOutOfRangeException in"
                  " UndirectedSubHypergraph::Update: "
                  "index = " << args->vertexId_ << "; min = 0; max = " <<
                    whitelistedVerticesList_->size() << '\n';
          throw;
        }
        break;
      }
      case ObservableEvent::HYPEREDGE_REMOVED: {
        HyperedgeIdType* hyperedgeId = static_cast<HyperedgeIdType*>(arg);
        try {
          const auto& hyperedge = whitelistedHyperedgesList_->at(*hyperedgeId);
          if (not whitelistedHyperedgesList_->filter_out(hyperedge)) {
            removeHyperedge(*hyperedgeId);
          }
        } catch (const std::out_of_range& oor) {
          std::cerr << "IndexOutOfRangeException in"
                  " UndirectedSubHypergraph::Update: "
                  "index = " << *hyperedgeId << "; min = 0; max = " <<
                    whitelistedHyperedgesList_->size() << '\n';
          throw;
        }
        break;
      }
      case ObservableEvent::RESET_VERTEX_IDS: {
        updateVertexContainerUponResetVertexIds();
        break;
      }
      case ObservableEvent::RESET_EDGE_IDS: {
        updateHyperedgeContainerUponResetHyperedgeIds();
        break;
      }
    }
  }
}


/** \brief If the dtor of the root directed hypergraph was called we
 * invalidate all parents of its child hierarchy
 */
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
setParentOfChildsToNull() {
  throwExceptionOnNullptrParent();
  std::unordered_set<Observer*> observers;
  for (auto itEvent = this->observers_.begin();
       itEvent != this->observers_.end(); ++itEvent) {
    for (auto itObserver : itEvent->second) {
      observers.insert(itObserver);
    }
  }
  for (auto& observer : observers) {
    auto subgraph = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
            Hyperedge_t, VertexProperty, HyperedgeProperty,
            HypergraphProperty>*>(observer);
    subgraph->setParentOfChildsToNull();
  }

  // Detach as observer of parent graph
  parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);
  // Set parent to nullptr
  parent_ = nullptr;
}



/**\brief Throw a exception if one tries to use a member function on a
 * subgraph whose parent was set to nullptr previously
 */
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
throwExceptionOnNullptrParent() const {
  if (parent_ == nullptr) {
    throw std::invalid_argument("Previously another hypergraph was assigned"
                                        " to the original parent of this"
                                        " sub-hypergraph. This sub-hypergraph"
                                        " has no parent hypergraph anymore and"
                                        " is thus in an invalid state.");
  }
}



/**\brief Adapt the content of container whitelistedVerticesList_ in
 * response to the call resetVertexIds in the root hypergraph
 */
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
updateVertexContainerUponResetVertexIds() {
  /*
   * Consider the containers of vertices in the root and sub-graphs, where
   * X corresponds to a nullptr and a number to the id of the vertex pointed
   * by the entry:
   *
   * graph:          |X|1|2|3|X|
   * subgraph1:      |X|X|2|3|X|
   * subsubgraph1_1: |X|X|X|3|X|
   * subgraph2:      |X|1|X|X|X|
   *
   * If the method resetVertexIds was called on the graph we expect the
   * following:
   *
   * graph:          |0|1|2|
   * subgraph1:      |X|1|2|
   * subsubgraph1_1: |X|X|2|
   * subgraph2:      |0|X|X|
   *
   * The method updateVertexContainerUponResetVertexIds in
   * UndirectedSubHypergraph notify its subgraphs about this change,
   * before changing its own container of vertices.
   */
  throwExceptionOnNullptrParent();

  // Notify subgraphs of this change
  this->NotifyObservers(ObservableEvent::RESET_VERTEX_IDS);

  if (whitelistedVerticesList_->empty()) {
    return;
  }
  std::vector<Vertex_t*> newWhitelistedVertexContainer;
  auto it = whitelistedVerticesList_->data();
  auto end = whitelistedVerticesList_->data() +
             whitelistedVerticesList_->size();
  for (; it != end; ++it) {
    const auto& vertex = *it;
    if (vertex != nullptr) {
      if (whitelistedVerticesList_->filter_out(vertex)) {
        newWhitelistedVertexContainer.push_back(nullptr);
      } else {
        newWhitelistedVertexContainer.push_back(vertex);
      }
    }
  }

  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(
          newWhitelistedVertexContainer));
}


/**\brief Adapt the content of container whitelistedHyperedgesList_ in
 * response to the call resetHyperedgeIds in the root hypergraph
 */
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
updateHyperedgeContainerUponResetHyperedgeIds() {
  /*
   * Consider the containers of hyperedges in the root and sub-graphs,
   * where X corresponds to a nullptr and a number to the id of the
   * hyperedge pointed by the entry:
   *
   * graph:          |X|1|2|3|X|
   * subgraph1:      |X|X|2|3|X|
   * subsubgraph1_1: |X|X|X|3|X|
   * subgraph2:      |X|1|X|X|X|
   *
   * If the method resetHyperedgeIds was called on the graph we expect the
   * following:
   *
   * graph:          |0|1|2|
   * subgraph1:      |X|1|2|
   * subsubgraph1_1: |X|X|2|
   * subgraph2:      |0|X|X|
   *
   * The method updateHyperedgeContainerUponResetHyperedgeIds in
   * UndirectedSubHypergraph notify its subgraphs about this change, before
   * changing its own container of vertices.
   */
  throwExceptionOnNullptrParent();

  // Notify subgraphs of this change
  this->NotifyObservers(ObservableEvent::RESET_EDGE_IDS);

  if (whitelistedHyperedgesList_->empty()) {
    return;
  }
  std::vector<Hyperedge_t*> newWhitelistedHyperedgeContainer;
  auto it = whitelistedHyperedgesList_->data();
  auto end = whitelistedHyperedgesList_->data() +
          whitelistedHyperedgesList_->size();
  for (; it != end; ++it) {
    const auto& hyperarc = *it;
    if (hyperarc != nullptr) {
      if (whitelistedHyperedgesList_->filter_out(hyperarc)) {
        newWhitelistedHyperedgeContainer.push_back(nullptr);
      } else {
        newWhitelistedHyperedgeContainer.push_back(hyperarc);
      }
    }
  }

  whitelistedHyperedgesList_ = create_filtered_vector<Hyperedge_t*>(
          parent_->getRootHyperedgeContainerPtr(),
          std::make_unique<WhitelistFilter<Hyperedge_t*>>(
                  newWhitelistedHyperedgeContainer));
}


/// Return pointer to vertices container
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::VertexContainerPtr
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
getRootVerticesContainerPtr() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootVerticesContainerPtr();
}


/// Return pointer to hyperarc container
template<template<typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::HyperedgeContainerPtr
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
getRootHyperedgeContainerPtr() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootHyperedgeContainerPtr();
}
}  // namespace hglib
