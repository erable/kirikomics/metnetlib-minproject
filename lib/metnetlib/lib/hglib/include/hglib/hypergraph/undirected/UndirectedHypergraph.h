/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 03.10.17.
//

#ifndef HGLIB_UNDIRECTEDHYPERGRAPH_H
#define HGLIB_UNDIRECTEDHYPERGRAPH_H

#include <memory>
#include <type_traits>

#include "filtered_vector.h"

#include "UndirectedHypergraphInterface.h"
#include "hglib/hyperedge/undirected/UndirectedHyperedgeBase.h"
#include "hglib/hyperedge/undirected/UndirectedHyperedge.h"
#include "hglib/hyperedge/undirected/ArgumentsToCreateUndirectedHyperedge.h"
#include "hglib/utils/types.h"
#include "hglib/vertex/undirected/UndirectedVertex.h"

namespace hglib {

template <template <typename> class VertexTemplate_t = UndirectedVertex,
        typename Hyperedge_t = UndirectedHyperedge,
        typename VertexProperty = emptyProperty,
        typename HyperedgeProperty = emptyProperty,
        typename HypergraphProperty = emptyProperty>
class UndirectedHypergraph : public UndirectedHypergraphInterface<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>,
                                  public Hypergraph<VertexTemplate_t,
                                          Hyperedge_t, VertexProperty,
                                          HyperedgeProperty,
                                          HypergraphProperty> {
  // Check that the vertex and hyperedge type is correct
  // An undirected vertex is needed.
  static_assert(std::is_base_of<UndirectedVertex<Hyperedge_t>,
      VertexTemplate_t<Hyperedge_t>>::value, "A vertex of an undirected "
                    "hypergraph must be derived from UndirectedVertex<T>, "
      "where T is derived from HyperedgeBase, e.g. "
      "UndirectedVertex<UndirectedHyperedge> or "
      "UndirectedVertex<NamedUndirectedHyperedge>");
  // An undirected hyperedge is needed
  static_assert(
      std::is_base_of<UndirectedHyperedgeBase<VertexTemplate_t<Hyperedge_t>>,
          Hyperedge_t>::value, "A hyperedge of a hypergraph must be derived "
          "from HyperedgeBase, e.g. use as second template parameter "
          "UndirectedHyperedge or NamedUndirectedHyperedge");

 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::Vertex_t;
  /// Alias for Hyperedge type
  using Hyperedge_type = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::Hyperedge_type;
  /// Alias for Hyperedge property type
  using HyperedgeProperty_t = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeProperty_t;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperedge_t ptr
  using HyperedgeContainer = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeContainer;
  /// Alias for iterator over a container of vertices
  using vertex_iterator = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::vertex_iterator;
  /// Alias for iterator over a container of hyperedges
  using hyperedge_iterator = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::hyperedge_iterator;
  /// \brief Alias for the data container that stores the names of vertices
  /// of a hyperedge
  using VertexNames = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexNames;
  /// \brief Alias for the data container that stores the ids of vertices
  /// of a hyperedge
  using VertexIds = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexIds;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Hyperedge_t*>
  using HyperedgeContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<HyperedgeProperty*>
  using HyperedgePropertyContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgePropertyContainerPtr;

  /* **************************************************************************
   * **************************************************************************
   *                    Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// DirectedHypergraph constructor
  explicit UndirectedHypergraph(bool allowMultiHyperedges = true);
  /// Copy constructor
  explicit UndirectedHypergraph(
          const UndirectedHypergraph& undirectedHypergraph);
  /// Assignment operator
  UndirectedHypergraph& operator=(
          const UndirectedHypergraph& source);
  /// Destructor
  virtual ~UndirectedHypergraph();

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
  /// Check if multi-hyperedges are allowed in this hypergraph
  bool allowMultiHyperedges() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        Hyperedge access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Number of hyperedges in the hypergraph
  size_t nbHyperedges() const override;
  /// Highest assigned hyperedge id of the hypergraph
  size_t hyperedgeContainerSize() const override;

  /**
    * \brief Add a hyperedge with the provided vertices, and additional
    * parameters (if needed to create a hyperedge of type Hyperedge_t) to the
    * hypergraph
    */
  const Hyperedge_t* addHyperedge(
          decltype(hglib::NAME),
          const VertexNames& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {})
    override;

  /**
  * \brief Add a hyperedge with the provided vertex ids, and additional
  * parameters (if needed to create a hyperedge of type Hyperedge_t) to the
  * hypergraph
  */
  const Hyperedge_t* addHyperedge(
          const VertexIds& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {})
  override;

  /// Get arguments to create the hyperedge
  ArgumentsToCreateUndirectedHyperedge<Hyperedge_t> argumentsToCreateHyperedge(
          const hglib::HyperedgeIdType& hyperedgeId) const override;

  /// Remove a hyperedge with the given id from the hypergraph.
  void removeHyperedge(const HyperedgeIdType& hyperedgeId) override;

  /// Search hyperedge list for an hyperedge with the provided id
  const Hyperedge_t* hyperedgeById(
          const HyperedgeIdType & hyperedgeId) const override;

  /// Const hyperedge iterator pointing to begin of the hyperedges
  hyperedge_iterator hyperedgesBegin() const override;
  /// Const hyperedge iterator pointing to end of the hyperedges
  hyperedge_iterator hyperedgesEnd() const override;
  /// \brief Pair of const hyperedge iterator pointing to begin and end
  /// of the hyperedges
  std::pair<hyperedge_iterator,
          hyperedge_iterator> hyperedgesBeginEnd() const override;
  /// Return a reference to the container of hyperedges.
  const HyperedgeContainer& hyperedges() const override;

  // Vertices of a given hyperedge
  /// Return number of vertices in the hyperedge with given id.
  size_t nbImpliedVertices(const HyperedgeIdType& hyperedgeId) const override;
  /// Check if hyperedge with given id has vertices.
  bool hasVertices(const HyperedgeIdType& hyperedgeId) const override;
  /// \brief Const vertex iterator pointing to the begin of the vertices
  /// of the hyperedge with the given id
  vertex_iterator impliedVerticesBegin(
          const HyperedgeIdType& hyperedgeId) const override;
  /// \brief Const vertex iterator pointing to the end of the vertices of
  /// the hyperedge with the given id
  vertex_iterator impliedVerticesEnd(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Const vertex iterator pointing to begin and end of the vertices of
  /// the hyperedge with the given id
  std::pair<vertex_iterator, vertex_iterator> impliedVerticesBeginEnd(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Get container of vertices of given hyperedge id
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> impliedVertices(
          const HyperedgeIdType& hyperedgeId) const override;

  /// Establish consecutive hyperarc ids
  void resetHyperedgeIds();
  /// Establish consecutive vertex and hyperarc ids
  void resetIds();

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex access
   * **************************************************************************
   * *************************************************************************/
 public:
  // Hyperedges that contain a given vertex
  /// Number of hyperedges that contain the vertex with given id
  size_t nbHyperedges(const VertexIdType& vertexId) const override;
  /// Check if the vertex is part of at least one hyperedge
  bool isContainedInAnyHyperedge(const VertexIdType& vertexId) const override;
  /// \brief Const hyperedge iterator pointing to the begin of the
  /// hyperedges that contain the vertex with the given id
  hyperedge_iterator hyperedgesBegin(
          const VertexIdType& vertexId) const override;
  /// \brief Const hyperedge iterator pointing to the end of the
  /// hyperedges that contain the vertex with the given id
  hyperedge_iterator hyperedgesEnd(
          const VertexIdType& vertexId) const override;
  /// \brief Const hyperedge iterator pointing to begin and end of the
  /// hyperedges that contain the vertex with the given id
  std::pair<hyperedge_iterator, hyperedge_iterator> hyperedgesBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get container of hyperedges that contain the vertex with the given id
  std::shared_ptr<const GraphElementContainer<Hyperedge_t*>> hyperedges(
          const VertexIdType& vertexId) const override;

  /// Remove a vertex with given name from the hypergraph.
  void removeVertex(const std::string& vertexName,
                            bool removeImpliedHyperedges = true) override;
  /// Remove a vertex with given id from the hypergraph.
  void removeVertex(const VertexIdType& vertexId,
                            bool removeImpliedHyperedges = true) override;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/Hyperedge dependent access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if given vertex is a vertex of given hyperedge.
  bool isVertexOfHyperedge(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperedgeId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                        HyperedgeProperty access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get properties of the hyperedge with the provided id.
  const HyperedgeProperty* getHyperedgeProperties(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Get properties of the hyperedge with the provided id.
  HyperedgeProperty* getHyperedgeProperties_(
          const HyperedgeIdType& hyperedgeId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get root hypergraph
  const UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
          VertexProperty, HyperedgeProperty,
          HypergraphProperty>* getRootHypergraph() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected methods
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Construct a hyperarc and a HyperarcProperty object
  const Hyperedge_t* buildHyperedgeAndItsDependencies(
          std::unique_ptr<GraphElementContainer<Vertex_t*>> vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes);
  /// Remove vertex dependencies.
  void removeVertexDependencies(const Vertex_t& vertex);
  /// Remove hyperedge dependencies.
  void removeHyperedgeDependencies(const Hyperedge_t& hyperedge);
  /// Remove nullptr entries from vector of hyperedge properties.
  void removeNullEntriesFromHyperedgeProperties();
  /// Return pointer to hyperedge container
  HyperedgeContainerPtr getRootHyperedgeContainerPtr() const override;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Hyperedges of the graph
  HyperedgeContainerPtr hyperedges_;
  /// Property per hyperedge
  HyperedgePropertyContainerPtr hyperedgeProperties_;
  /// Hyperarc max Id
  HyperedgeIdType hyperedgeMaxId_;
};

/// Operator<<
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::ostream& operator<< (std::ostream& out,
                          const UndirectedHypergraph<VertexTemplate_t,
                                  Hyperedge_t, VertexProperty,
                                  HyperedgeProperty,
                                  HypergraphProperty>& hypergraph) {
  // print isolated vertices
  for (const auto& vertex : hypergraph.vertices()) {
    if (not hypergraph.isContainedInAnyHyperedge(vertex->id())) {
      out << *vertex << std::endl;
    }
  }

  // print hyperedges
  for (const auto& hyperedge : hypergraph.hyperedges()) {
    out << *hyperedge << std::endl;
  }

  return out;
}
}  // namespace hglib
#include "UndirectedHypergraph.hpp"
#endif  // HGLIB_UNDIRECTEDHYPERGRAPH_H
