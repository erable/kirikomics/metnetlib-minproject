/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.09.17.
//

#include "ArgumentsToCreateVertex.h"

namespace hglib {

/**
 * Constructor
 *
 * @param tailAndHeadIds
 * @param specificAttributes
 */
template<typename Vertex_t>
ArgumentsToCreateVertex<Vertex_t>::ArgumentsToCreateVertex(
        const std::string& name,
        const typename Vertex_t::SpecificAttributes& specificAttributes) :
        name_(name),
        specificAttributes_(specificAttributes) {}


/**
 * Get name of the vertex
 *
 * @return const std::string ref
 */
template<typename Vertex_t>
const std::string& ArgumentsToCreateVertex<Vertex_t>::
name() const {
  return name_;
}

/**
 * Get specific attributes of the vertex
 *
 * @return const ref to specific arguments
 */
template<typename Vertex_t>
const typename Vertex_t::SpecificAttributes&
ArgumentsToCreateVertex<Vertex_t>::
specificAttributes() const {
  return specificAttributes_;
}


/**
 * Set name of the vertex
 *
 * @param name
 */
template<typename Vertex_t>
void ArgumentsToCreateVertex<Vertex_t>::
setName(const std::string& name) {
  name_ = name;
}


/**
 * Set arguments that are specific to the vertex type.
 *
 * @param args
 */
template<typename Vertex_t>
void ArgumentsToCreateVertex<Vertex_t>::
setSpecificArguments(const typename Vertex_t::SpecificAttributes& args) {
  specificAttributes_ = args;
}
}  // namespace hglib
