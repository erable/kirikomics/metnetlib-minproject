/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#include <iostream>

#include "DirectedVertex.h"

#include "filtered_vector.h"

namespace hglib {

/**
 * DirectedVertex constructor
 *
 * @param id Identifier of the vertex
 * @param name Name of the vertex
 */
template <typename Hyperarc_t>
DirectedVertex<Hyperarc_t>::DirectedVertex(
        VertexIdType id, std::string name,
        const SpecificAttributes& specificAttributes) :
        VertexBase(id, name), specificAttributes_(specificAttributes) {
  inHyperarcs_ = create_filtered_vector<Hyperarc_t*>();
  outHyperarcs_ = create_filtered_vector<Hyperarc_t*>();
}


/**
 * DirectedVertex constructor
 *
 * @param id Identifier of the vertex
 */
/*template <typename Hyperarc_t>
DirectedVertex<Hyperarc_t>::DirectedVertex(
        VertexIdType id,
        const SpecificAttributes& specificAttributes) :
        VertexBase(id), specificAttributes_(specificAttributes) {
  name_ = "dv_" + std::to_string(id);
}*/

template <typename Hyperarc_t>
DirectedVertex<Hyperarc_t>::DirectedVertex(
        VertexIdType id,
        const SpecificAttributes& specificAttributes) :
        DirectedVertex(id, "dv_" + std::to_string(id), specificAttributes) {}

/**
 * DirectedVertex copy constructor
 *
 * @param vertex Vertex to be copied
 */
template <typename Hyperarc_t>
DirectedVertex<Hyperarc_t>::DirectedVertex(const DirectedVertex& vertex) :
        VertexBase(vertex) {
  inHyperarcs_ = create_filtered_vector<Hyperarc_t*>();
  outHyperarcs_ = create_filtered_vector<Hyperarc_t*>();
}

/* ****************************************************************************
 * ****************************************************************************
 *                        Protected methods
 * ****************************************************************************
 * ***************************************************************************/
/**
 * Add an outgoing hyperarc
 *
 * @param hyperarc Pointer to a hyperarc
 */
template <typename Hyperarc_t>
void DirectedVertex<Hyperarc_t>::addOutHyperarc(Hyperarc_t* hyperarc) {
  outHyperarcs_->push_back(hyperarc);
}


/**
 * Add an incoming hyperarc
 *
 * @param hyperarc Pointer to a hyperarc
 */
template <typename Hyperarc_t>
void DirectedVertex<Hyperarc_t>::addInHyperarc(Hyperarc_t* hyperarc) {
  inHyperarcs_->push_back(hyperarc);
}

/**
 * Remove a hyperarc from the outgoing hyperarcs.
 *
 * @param hyperarcToBeDeleted Hyperarc reference
 */
template <typename Hyperarc_t>
void DirectedVertex<Hyperarc_t>::
removeOutHyperarc(const Hyperarc_t& hyperarcToBeDeleted) {
  // We suppose that there is at most one occurrence of the given hyperarc
  // in the outgoing hyperarcs.
  auto newEnd = std::remove(outHyperarcs_->data(),
                            outHyperarcs_->data() + outHyperarcs_->size(),
                            &hyperarcToBeDeleted);
  // new end is different from the old one
  if (newEnd != (outHyperarcs_->data() + outHyperarcs_->size())) {
    outHyperarcs_->resize(outHyperarcs_->size() - 1);
  }
}


/**
 * Remove a hyperarc from the incoming hyperarcs.
 *
 * @param hyperarcToBeDeleted Hyperarc reference
 */
template <typename Hyperarc_t>
void DirectedVertex<Hyperarc_t>::
removeInHyperarc(const Hyperarc_t& hyperarcToBeDeleted) {
  // We suppose that there is at most one occurrence of the given hyperarc
  // in the incoming hyperarcs.
  auto newEnd = std::remove(inHyperarcs_->data(),
                            inHyperarcs_->data() + inHyperarcs_->size(),
                            &hyperarcToBeDeleted);
  // new end is different from
  if (newEnd != (inHyperarcs_->data() + inHyperarcs_->size())) {
    inHyperarcs_->resize(inHyperarcs_->size() - 1);
  }
}


/**
 * Get arguments needed to create this vertex
 *
 * @return tuple<string>
 */
template <typename Hyperarc_t>
ArgumentsToCreateVertex<DirectedVertex<Hyperarc_t>> DirectedVertex<
        Hyperarc_t>::
argumentsToCreateVertex() const {
  return ArgumentsToCreateVertex<DirectedVertex<Hyperarc_t>>(
          name_, specificAttributes_);
}
}  // namespace hglib
