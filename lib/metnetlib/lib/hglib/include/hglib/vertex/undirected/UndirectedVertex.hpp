/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.06.17.
//

#include "UndirectedVertex.h"

#include "filtered_vector.h"

namespace hglib {

/**
 * Constructor
 *
 * @param id vertex id
 * @param name vertex name
 * @param specificAttributes vertex specific arguments
 */
template <typename Hyperedge_t>
UndirectedVertex<Hyperedge_t>::
UndirectedVertex(VertexIdType id, std::string name,
                 const SpecificAttributes& specificAttributes) :
        VertexBase(id, name), specificAttributes_(specificAttributes) {
  hyperedges_ = create_filtered_vector<Hyperedge_t*>();
}


/**
 * UndirectedVertex constructor
 *
 * The vertex name is generated as 'udv_' + id
 *
 * @param id vertex id
 * @param specificAttributes vertex specific arguments
 */
/*template <typename Hyperedge_t>
UndirectedVertex<Hyperedge_t>::
UndirectedVertex(VertexIdType id,
                 const SpecificAttributes& specificAttributes) :
        VertexBase(id), specificAttributes_(specificAttributes) {
  name_ = "udv_" + std::to_string(id);
}*/

template <typename Hyperedge_t>
UndirectedVertex<Hyperedge_t>::
UndirectedVertex(VertexIdType id,
                 const SpecificAttributes& specificAttributes) :
        UndirectedVertex(id,
                         "udv_" + std::to_string(id),
                         specificAttributes) {}

/// UndirectedVertex copy constructor
template <typename Hyperedge_t>
UndirectedVertex<Hyperedge_t>::
UndirectedVertex(const UndirectedVertex& vertex) : VertexBase(vertex) {
  hyperedges_ = create_filtered_vector<Hyperedge_t*>();
}

/// Add a hyperedge
template <typename Hyperedge_t>
void UndirectedVertex<Hyperedge_t>::
addHyperedge(Hyperedge_t* hyperedge) {
  hyperedges_->push_back(hyperedge);
}

/// Remove a hyperedge
template <typename Hyperedge_t>
void UndirectedVertex<Hyperedge_t>::
removeHyperedge(const Hyperedge_t& hyperedgeToBeDeleted) {
  // We suppose that there is at most one occurrence of the given hyperedge
  // in the hyperedges.
  auto newEnd = std::remove(hyperedges_->data(),
                            hyperedges_->data() + hyperedges_->size(),
                            &hyperedgeToBeDeleted);
  // new end is different from the old one
  if (newEnd != (hyperedges_->data() + hyperedges_->size())) {
    hyperedges_->resize(hyperedges_->size() - 1);
  }
}

/// Get arguments needed to create this vertex
template <typename Hyperedge_t>
ArgumentsToCreateVertex<UndirectedVertex<Hyperedge_t>> UndirectedVertex<
        Hyperedge_t>::
argumentsToCreateVertex() const {
  return ArgumentsToCreateVertex<UndirectedVertex<Hyperedge_t>>(
          name_, specificAttributes_);
}
}  // namespace hglib
