/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#ifndef HGLIB_VERTEXBASE_H
#define HGLIB_VERTEXBASE_H

#include <string>

#include "hglib/utils/types.h"
#include "ArgumentsToCreateVertex.h"

namespace hglib {
/**
 * Base of a vertex.
 */
class VertexBase {
  template <template <typename> class Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class Hypergraph;

 protected:
  /// VertexBase constructor
  VertexBase(VertexIdType id, std::string name);
  /// VertexBase constructor
  VertexBase(VertexIdType id);
  /// VertexBase copy constructor
  VertexBase(const VertexBase& vertex);
  /// Destructor
  virtual ~VertexBase() {}

 public:
  /// Get Id of the vertex
  inline VertexIdType id() const;
  /// Get name of the vertex
  inline std::string name() const;
  /// Print the vertex
  virtual void print(std::ostream& out) const;

 protected:
  /// Assign an ID to the vertex
  void setId(const VertexIdType& id);

 protected:
  /// Numerical identifier of the vertex
  VertexIdType id_;
  /// Name of the vertex
  std::string name_;
};

/// Operator<<
inline std::ostream& operator<< (std::ostream& out,
                                 const VertexBase& vertex) {
  vertex.print(out);
  return out;
}

VertexIdType VertexBase::id() const {
  return id_;
}

std::string VertexBase::name() const {
  return name_;
}

}  // namespace hglib
#endif  // HGLIB_VERTEXBASE_H
