/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.06.17.
//

#include "hglib/hyperedge/directed/NamedDirectedHyperedge.h"
#include "hglib/hyperedge/directed/ArgumentsToCreateDirectedHyperedge.h"

namespace hglib {
NamedDirectedHyperedge::
NamedDirectedHyperedge(
        int id,
        std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> tails,
        std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> heads,
        const SpecificAttributes& specificAttributes) :
        DirectedHyperedgeBase(id, tails, heads),
        specificAttributes_(specificAttributes) {}


/**
 * Copy constructor
 *
 * @param hyperarc Hyperarc to be copied
 */
NamedDirectedHyperedge::
NamedDirectedHyperedge(const NamedDirectedHyperedge& hyperarc) :
        DirectedHyperedgeBase(hyperarc),
        specificAttributes_(hyperarc.specificAttributes_) {}


/**
 * Get arguments to create this NamedHyperarc
 *
 * @return tuple<pair<vector<string>,vector<string>>,string>
 */
ArgumentsToCreateDirectedHyperedge<NamedDirectedHyperedge>
NamedDirectedHyperedge::
argumentsToCreateHyperarc() const {
  std::vector<hglib::VertexIdType> tails, heads;
  for (const auto vertex : *(tails_.get())) {
    tails.push_back(vertex->id());
  }
  for (const auto vertex : *(heads_.get())) {
    heads.push_back(vertex->id());
  }
  return ArgumentsToCreateDirectedHyperedge<NamedDirectedHyperedge>(
          std::make_pair(tails, heads), specificAttributes_);
}


/**
 * Print the named hyperarc
 *
 * Prints the hyperarc in the following format:
 * Hyperarc_name: Tail_1 + ... + Tail_n -> Head_1 + ... + Head_m
 *
 * @param out std::ostream
 */
void NamedDirectedHyperedge::
print(std::ostream& out) const {
  out << specificAttributes_.name_ << ": ";
  DirectedHyperedgeBase<DirectedVertex_t>::printTailsAndHeads(out);
}


/**
 * Compare if the provided name, and the tail and head vertex names
 * are identical to the name, tail and head vertex names of this
 * NamedHyperarc
 *
 * @param tails_heads names of tail and head vertices
 * @param name name of the hyperarc
 * @return true if the hyperarc has the same name, and tail and head vertex
 * names, false otherwise
 */
bool NamedDirectedHyperedge::
compare(const std::vector<const DirectedVertex_t*>& tails,
        const std::vector<const DirectedVertex_t*>& heads,
        const std::string& name) const {
  return name.compare(specificAttributes_.name_) == 0 and
          DirectedHyperedgeBase<DirectedVertex_t>::compare(tails, heads);
}
}  // namespace hglib
