/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.06.17.
//

#include "hglib/hyperedge/undirected/UndirectedHyperedge.h"

namespace hglib {
/// UndirectedHyperedge constructor
UndirectedHyperedge::
UndirectedHyperedge(int id,
                    std::shared_ptr<GraphElementContainer<Vertex_t*>> vertices,
                    const SpecificAttributes& specificAttributes) :
        UndirectedHyperedgeBase(id, vertices),
        specificAttributes_(specificAttributes) {}

/// Copy constructor
UndirectedHyperedge::
UndirectedHyperedge(const UndirectedHyperedge& hyperedge) :
        UndirectedHyperedgeBase(hyperedge),
        specificAttributes_(hyperedge.specificAttributes_) {}

/// Get arguments to create this Hyperedge
ArgumentsToCreateUndirectedHyperedge<UndirectedHyperedge> UndirectedHyperedge::
argumentsToCreateHyperedge() const {
  std::vector<hglib::VertexIdType> vertices;
  for (const auto vertex : *(vertices_.get())) {
    vertices.push_back(vertex->id());
  }
  return ArgumentsToCreateUndirectedHyperedge<UndirectedHyperedge>(vertices,
                                                         specificAttributes_);
}

/// Print the hyperedge
void UndirectedHyperedge::
print(std::ostream& out) const {
  UndirectedHyperedgeBase<Vertex_t>::printVertices(out);
}

}  // namespace hglib
