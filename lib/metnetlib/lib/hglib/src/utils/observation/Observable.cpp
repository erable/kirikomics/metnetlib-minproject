/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 31.08.17.
//

#include "hglib/utils/observation/Observable.h"

namespace hglib {

/**
 *  Attach an observer o for a given event e
 *
 * @param o Observer
 * @param e Event
 */
void Observable::AddObserver(Observer* o, ObservableEvent e) {
  observers_[e].emplace_back(o);
}


/**
 * Release an observer o for a given event e
 *
 * @param o Observer
 * @param e Event
 */
void Observable::DeleteObserver(Observer* o, ObservableEvent e) {
  observers_[e].remove(o);
}


/**
 * Notify all observers of event e providing optional argument
 *
 * @param e Event
 * @param arg Optional arguments provided to the observer
 */
void Observable::NotifyObservers(ObservableEvent e, void* arg) {
  for (auto& o : observers_[e]) {
    o->Update(*this, e, arg);
  }
}
}  // namespace hglib
