/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 13.10.17.
//

#include <iostream>

#include "FilteredVector.h"

/// Constructor
template<typename T>
FilteredVector<T>::
FilteredVector(std::shared_ptr<Filter<T>> filter) :
    filter_(filter) {
}


/// Add item to the vector
template<typename T>
void FilteredVector<T>::
removeFilteredOutItem(const T& item) {
  filter_->removeFilteredOutItem(item);
}


/// Filter item out of the vector
template<typename T>
void FilteredVector<T>::
addFilteredOutItem(const T& item) {
  filter_->addFilterOutItem(item);
}


/// Whether the item is filtered out
template<typename T>
bool FilteredVector<T>::
filter_out(T item) const {
  return filter_->filter_out(item);
}


/// Return filter
template<typename T>
std::shared_ptr<Filter<T>> FilteredVector<T>::
filter() const {
  return filter_;
}
