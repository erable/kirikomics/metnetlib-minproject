/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#ifndef FILTERED_VECTOR_FILTERED_VECTOR_ITERATOR_H
#define FILTERED_VECTOR_FILTERED_VECTOR_ITERATOR_H

#include <iterator>

/**
 * Iterator class of a filtered_vector.
 *
 * @tparam T type of items stored in the filtered_vector. Has to be a pointer.
 * @tparam Container container type, e.g. filtered_vector<T>
 */
template<typename T, typename Container>
class FilteredVectorIterator :
        public std::iterator<std::random_access_iterator_tag, T> {
 public:
  /// typedef Base
  using Base = std::iterator<std::random_access_iterator_tag, T>;

  using typename Base::value_type;
  using typename Base::pointer;
  using typename Base::reference;
  using typename Base::difference_type;

  /// Default ctor
  explicit FilteredVectorIterator() = default;
  /// Constructor
  FilteredVectorIterator(const pointer&, std::shared_ptr<const Container>);
  /// Constructor
  /// Allow iterator to const_iterator conversion
  // "Making New Friends" idiom
  template <typename U>
  FilteredVectorIterator(
          const FilteredVectorIterator<U, Container>& other) :
          current_(other.base()), cont_(other.getContainerPtr()) {
  }
  /// Destructor
  virtual ~FilteredVectorIterator() = default;
  /// Assignment operator
  FilteredVectorIterator& operator=(
          const FilteredVectorIterator&) = default;

  /// Equal operator
  bool operator==(const FilteredVectorIterator&) const noexcept;
  /// Unequal operator
  bool operator!=(const FilteredVectorIterator&) const noexcept;
  /// Dereference operator
  reference operator*() const noexcept;
  /// Operator->
  pointer operator->() const noexcept;
  /// Increment operator
  FilteredVectorIterator& operator++() noexcept;
  /// Increment operator
  FilteredVectorIterator operator++(int) noexcept;
  /// Decrement operator
  FilteredVectorIterator& operator--() noexcept(false);
  /// Decrement operator
  FilteredVectorIterator operator--(int) noexcept(false);
  /// Arithmetic operator
  FilteredVectorIterator operator+(difference_type n) noexcept;
  /// Arithmetic operator
  FilteredVectorIterator operator-(difference_type n) noexcept;
  /// Arithmetic operator
  difference_type operator-(FilteredVectorIterator rhs) noexcept;
  /// Inequality comparison
  bool operator<(FilteredVectorIterator rhs) noexcept;
  /// Inequality comparison
  bool operator>(FilteredVectorIterator rhs) noexcept;
  /// Inequality comparison
  bool operator<=(FilteredVectorIterator rhs) noexcept;
  /// Inequality comparison
  bool operator>=(FilteredVectorIterator rhs) noexcept;
  /// Compound assignment operations +=
  FilteredVectorIterator& operator+=(difference_type n) noexcept;
  /// Compound assignment operations -=
  FilteredVectorIterator& operator-=(difference_type n) noexcept;
  /// Offset dereference operator
  reference operator[](difference_type n) const noexcept;
  /// Get pointer to current element
  const pointer& base() const noexcept;
  /// Get pointer to constainer
  std::shared_ptr<const Container> getContainerPtr() const noexcept;

  /// Arithmetic operator
  // "Making New Friends" idiom
  friend FilteredVectorIterator<T, Container>
  operator+(typename FilteredVectorIterator<T, Container>::difference_type n,
            const FilteredVectorIterator<T, Container>& rhs) noexcept {
    return FilteredVectorIterator<T, Container>(rhs.base() + n, rhs.cont_);
  };

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Pointer to current element
  pointer current_ = nullptr;
  /// \brief Pointer to container which provides the begin and end position
  /// as well as the filter
  std::shared_ptr<const Container> cont_;
};

#include "FilteredVectorIterator.hpp"
#endif  // FILTERED_VECTOR_FILTERED_VECTOR_ITERATOR_H
