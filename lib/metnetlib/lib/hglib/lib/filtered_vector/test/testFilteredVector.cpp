/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#include <iostream>
#include <memory>

#include "gtest/gtest.h"


#include "../include/filtered_vector/FilteredVector.h"
#include "../include/filtered_vector/Filter.h"
#include "../include/filtered_vector/NullptrFilter.h"
#include "../include/filtered_vector/create_filtered_vector.h"
#include "../include/filtered_vector/WhitelistFilter.h"

using std::make_shared;
using std::make_unique;

/**
 * Test fixture
 */
class TestFilteredVector : public testing::Test {
 protected:
  virtual void SetUp() {
    intVec = create_filtered_vector<int*>();
    charVec = create_filtered_vector<char*>();
    aVec = create_filtered_vector<A*>();
  }

  virtual void TearDown() {
    for (auto item : *(intVec.get())) {
      delete item;
    }
    for (auto item : *(charVec.get())) {
      delete item;
    }
    for (auto item : (*aVec.get())) {
      delete item;
    }
  }

  std::vector<int> intVector = {1, 2, 3};
  std::vector<char> charVector = {65, 66, 67};

  class A {
   public:
    A() {}
    A(int an_int, char a_char) : an_int(an_int), a_char(a_char) {}

    int id() {
      return an_int;
    }

    friend std::ostream& operator<<(std::ostream& os, const A& a) {
      os << "an_int: " << a.an_int << " a_char: " << a.a_char;
      return os;
    }

   public:
    int an_int = 1;
    char a_char = 'a';
  };

 protected:
  std::shared_ptr<FilteredVector<int*>> intVec;
  std::shared_ptr<FilteredVector<char*>> charVec;
  std::shared_ptr<FilteredVector<A*>> aVec;
};


TEST_F(TestFilteredVector, Test_factory) {
  auto intVecWithNullptrFilter = create_filtered_vector<int*>(
          intVec, make_unique<NullptrFilter<int*>>());

  ASSERT_EQ(intVecWithNullptrFilter->size(), (size_t) 0);

  std::shared_ptr<FilteredVector<A*>> aVecNullptr_filter =
      create_filtered_vector<A*>(aVec, make_unique<NullptrFilter<A*>>());
  auto aVecWhitelist_filter = create_filtered_vector<A*>(
      aVecNullptr_filter,
      make_unique<WhitelistFilter<A*>>());

  // line below does not compile:
  // error: static_assert failed "The template parameter is expected to be a
  // pointer"
  // auto test = create_filtered_vector<int>();
}

TEST_F(TestFilteredVector, Test_push_back_int) {
  ASSERT_EQ(intVec->size(), (size_t) 0);

  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  ASSERT_EQ(intVec->size(), (size_t) 3);
  intVec->push_back(nullptr);
  ASSERT_EQ(intVec->size(), (size_t) 4);

  auto intVecWithNullptrFilter = create_filtered_vector<int*>(
          intVec, make_unique<NullptrFilter<int*>>());
  ASSERT_EQ(intVecWithNullptrFilter->size(), (size_t) 4);
  intVecWithNullptrFilter->push_back(nullptr);
  ASSERT_EQ(intVecWithNullptrFilter->size(), (size_t) 5);
}

TEST_F(TestFilteredVector, Test_push_back_char) {
  ASSERT_EQ(charVec->size(), (size_t) 0);

  for (auto c : charVector) {
    charVec->push_back(new char(c));
  }
  ASSERT_EQ(charVec->size(), (size_t) 3);
  charVec->push_back(nullptr);
  ASSERT_EQ(charVec->size(), (size_t) 4);

  auto charVecWithNullptrFilter = create_filtered_vector<char*>(
          charVec, make_unique<NullptrFilter<char*>>());
  ASSERT_EQ(charVecWithNullptrFilter->size(), (size_t) 4);
  charVecWithNullptrFilter->push_back(nullptr);
  ASSERT_EQ(charVecWithNullptrFilter->size(), (size_t) 5);
}

TEST_F(TestFilteredVector, Test_push_back_A) {
  ASSERT_EQ(aVec->size(), (size_t) 0);

  aVec->push_back(new A());
  aVec->push_back(new A(66, 'Z'));
  aVec->push_back(new A(5, 't'));
  ASSERT_EQ(aVec->size(), (size_t) 3);
  aVec->push_back(nullptr);
  ASSERT_EQ(aVec->size(), (size_t) 4);

  auto aVecWithNullptrFilter = create_filtered_vector<A*>(
          aVec, make_unique<NullptrFilter<A*>>());
  ASSERT_EQ(aVecWithNullptrFilter->size(), (size_t) 4);
  aVecWithNullptrFilter->push_back(nullptr);
  ASSERT_EQ(aVecWithNullptrFilter->size(), (size_t) 5);
}

TEST_F(TestFilteredVector, Test_begin_nominal) {
  ASSERT_EQ(aVec->begin(), aVec->end());
  ASSERT_EQ(aVec->cbegin(), aVec->end());
  ASSERT_EQ(aVec->begin().base(), nullptr);

  aVec->push_back(new A());
  ASSERT_NE(aVec->begin(), aVec->end());
  ASSERT_NE(aVec->cbegin(), aVec->end());
  ASSERT_NE(aVec->begin().base(), nullptr);
}

TEST_F(TestFilteredVector, Test_begin_firstnull) {
  // create a FilteredVector where nullptr are filtered out
  std::shared_ptr<FilteredVector<A*>> aVecNull_filter =
      create_filtered_vector<A*>(aVec, make_unique<NullptrFilter<A*>>());

  EXPECT_EQ(aVecNull_filter->begin(), aVecNull_filter->end());
  // insert a null_ptr
  aVecNull_filter->push_back(nullptr);
  /*EXPECT_NE(aVec->begin(), aVec->end());
  EXPECT_NE(aVec->cbegin(), aVec->end());
  EXPECT_EQ(++(aVec->begin()), aVec->end());*/
  EXPECT_EQ(aVecNull_filter->begin(), aVecNull_filter->end());
  EXPECT_EQ(aVecNull_filter->cbegin(), aVecNull_filter->cend());
  // insert another null_ptr
  aVecNull_filter->push_back(nullptr);
  /*EXPECT_NE(aVec->begin(), aVec->end());
  auto it = aVec->begin();
  ++it;
  ++it;
  EXPECT_EQ(it, aVec->end());*/
  EXPECT_EQ(aVecNull_filter->begin(), aVecNull_filter->end());
  // insert pointer to A
  aVecNull_filter->push_back(new A());
  EXPECT_NE(aVecNull_filter->begin(), aVecNull_filter->end());
  EXPECT_EQ(++(aVecNull_filter->begin()), aVecNull_filter->end());
}

TEST_F(TestFilteredVector, Test_resize) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  ASSERT_THROW(intVec->resize(-1), std::exception);

  for (auto elt : *intVec) {
    delete elt;
  }
  intVec->resize(0);
  ASSERT_TRUE(intVec->empty());
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  size_t originalSize = intVec->size();
  intVec->resize(2 * intVec->size());
  ASSERT_EQ(2 * originalSize, intVec->size());
  intVec->resize(3);
  ASSERT_EQ(originalSize, intVec->size());
}

TEST_F(TestFilteredVector, Test_capacity) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }

  ASSERT_TRUE(intVec->capacity() >= intVec->size());
  ASSERT_TRUE(intVec->capacity() <= intVec->max_size());
}

TEST_F(TestFilteredVector, Test_empty) {
  ASSERT_TRUE(intVec->empty());
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  ASSERT_FALSE(intVec->empty());
}

TEST_F(TestFilteredVector, Test_reserve) {
  ASSERT_THROW(intVec->reserve(-1), std::exception);
  ASSERT_THROW(intVec->reserve(2 * intVec->max_size()), std::exception);

  size_t originalCapacity = intVec->capacity();
  size_t originalSize = intVec->size();
  // capacity is not affected if n is smaller than current capacity
  intVec->reserve(0.5 * originalCapacity);
  ASSERT_EQ(originalCapacity, intVec->capacity());
  // vector size is nt affected
  ASSERT_EQ(originalSize, intVec->size());

  intVec->reserve(2 * originalCapacity);
  // capacity is at least n
  ASSERT_TRUE(intVec->capacity() >= 2 * originalCapacity);
  ASSERT_EQ(originalSize, intVec->size());
}

TEST_F(TestFilteredVector, Test_shrink_to_fit) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  size_t originalCapacity = intVec->capacity();
  intVec->shrink_to_fit();
  ASSERT_TRUE(intVec->capacity() >= intVec->size());
  ASSERT_TRUE(intVec->capacity() <= originalCapacity);
}

TEST_F(TestFilteredVector, Test_offsetDereferenceOperator) {
  intVec->resize(intVector.size());
  int idx(0);
  for (auto i : intVector) {
    (*(intVec.get()))[idx] = new int(i);
    ASSERT_EQ(i, *(*(intVec.get()))[idx]);
    ++idx;
  }
  ASSERT_EQ(intVec->size(), intVector.size());
}

TEST_F(TestFilteredVector, Test_at) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  int idx(0);
  for (auto i : intVector) {
    delete intVec->at(idx);
    intVec->at(idx) = new int(2 * i);
    ASSERT_EQ(2 * i, *intVec->at(idx));
    ++idx;
  }
  ASSERT_THROW(intVec->at(idx), std::out_of_range);
}

TEST_F(TestFilteredVector, Test_front) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  ASSERT_EQ(*intVec->front(), intVector[0]);
  int newFront = 5;
  *intVec->front() = newFront;
  ASSERT_EQ(*intVec->front(), newFront);
}

TEST_F(TestFilteredVector, Test_back) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
    delete intVec->back();
    intVec->back() = new int(2 * i);
    ASSERT_EQ(*intVec->back(), 2 * i);
  }
}

TEST_F(TestFilteredVector, Test_assign_fill) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  std::shared_ptr<FilteredVector<int*>> intVec2 =
          create_filtered_vector<int*>();
  int i = 0;
  for (auto it = intVec->begin(); it != intVec->end(); ++it) {
    intVec2->assign(i, *it);
    ASSERT_EQ(intVec2->size(), (unsigned) i);
    for (auto item : *(intVec2.get())) {
      ASSERT_EQ(*item, *(*it));
    }
    ++i;
  }
}

TEST_F(TestFilteredVector, Test_assign_range) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  std::shared_ptr<FilteredVector<int*>> intVec2 =
          create_filtered_vector<int*>();
  auto first = intVec->begin();
  for (auto last = intVec->begin(); last != intVec->end(); ++last) {
    auto diff = last - first;
    intVec2->assign(first, last);
    ASSERT_EQ((unsigned) diff, intVec2->size());
    auto it = intVec->begin();
    for (auto item : *(intVec2.get())) {
      ASSERT_EQ(*item, *(*it));
      ++it;
    }
  }
}

TEST_F(TestFilteredVector, Test_assign_range2) {
  // TODO(mw): Find a way to take this function a template parameter
  // InputIterator.
  /*const int size = 3;
  int* myints[size] = {new int(1), new int(2), new int(3)};
  std::unique_ptr<FilteredVector<int*>> intVec2 =
          create_filtered_vector<int*>();
  for (unsigned i = 0; i < size; ++i) {
    intVec2->assign(myints, myints + i);
    ASSERT_EQ(intVec2->size(), (unsigned) i);
    int j = 0;
    for (auto item : *(intVec2.get())) {
      ASSERT_EQ(*item, *(myints[j++]));
    }
    ++i;
  }

  for (unsigned i = 0; i < size; ++i) {
    delete myints[i];
  }*/
}

TEST_F(TestFilteredVector, Test_pop_back) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }

  unsigned i = intVec->size();
  while (not intVec->empty()) {
    int* last = intVec->back();
    delete last;
    intVec->pop_back();
    ASSERT_EQ(--i, intVec->size());
  }
}

TEST_F(TestFilteredVector, Test_swap) {
  // TODO(mw) Need working swap implementation
  /*for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  std::unique_ptr<FilteredVector<int*>> intVec2 =
          create_filtered_vector<int*>();
  int first = 4;
  int last = 10;
  for (int i = first; i < last; ++i) {
    intVec2->push_back(new int(i));
  }
  auto sizeVec1 = intVec->size();
  auto sizeVec2 = intVec2->size();
  intVec->swap(*(intVec2.get()));

  ASSERT_EQ(intVec->size(), sizeVec2);
  ASSERT_EQ(intVec2->size(), sizeVec1);
  auto it = intVec->begin();
  for (int i = first; i < last; ++i) {
    ASSERT_EQ(*(*it), i);
    ++it;
  }
  it = intVec2->begin();
  for (auto i : intVector) {
    ASSERT_EQ(*(*it), i);
    ++it;
  }

  for (auto item : *(intVec2.get())) {
    delete item;
  }*/
}

TEST_F(TestFilteredVector, Test_constIterator) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  FilteredVector<int*>::iterator it = intVec->begin();
  FilteredVector<int*>::const_iterator itConst = intVec->cbegin();
  while (itConst != intVec->cend()) {
    ASSERT_EQ(*it, *itConst);
    ++it;
    ++itConst;
  }
  ASSERT_EQ(*itConst, *intVec->cend());
  ASSERT_EQ(*it, *intVec->end());

  while (itConst != intVec->cbegin()) {
    ASSERT_EQ(*it, *itConst);
    --it;
    --itConst;
  }
  ASSERT_EQ(*itConst, *intVec->cbegin());
  ASSERT_EQ(*it, *intVec->begin());

  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }

  it = intVec->begin();
  for (auto cit = intVec->cbegin() ; cit != intVec->cend() ; ++cit, ++it) {
    ASSERT_EQ(*it, *cit);
  }

  const decltype(intVec.get()) cintVec = intVec.get();
  FilteredVector<int*>::const_iterator cit = cintVec->begin();
  ASSERT_STREQ(typeid(decltype(cit)).name(),
               typeid(FilteredVector<int*>::const_iterator).name());
  it = intVec->begin();
  for (auto obj : *cintVec) {
    ASSERT_EQ(*it++, obj);
  }

  // Conversion iterator -> const-iterator
  FilteredVector<int*>::const_iterator cit2 = it;
  cit2 = it;
}

TEST_F(TestFilteredVector, Test_reverseIterator) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }
  auto it = intVec->begin();
  auto it2 = intVec->rend();
  --it2;
  ASSERT_EQ(*it, *it2);

  it = intVec->end();
  --it;
  it2 = intVec->rbegin();
  ASSERT_EQ(*it, *it2);
}

TEST_F(TestFilteredVector, Test_const_reverse_iterator) {
  for (auto i : intVector) {
    intVec->push_back(new int(i));
  }

  auto itRev = intVec->rbegin();
  auto itConstRev = intVec->crbegin();
  while (itConstRev != intVec->crend()) {
    ASSERT_EQ(*itRev, *itConstRev);
    ++itRev;
    ++itConstRev;
  }
  ASSERT_EQ(itConstRev, intVec->crend());
  ASSERT_EQ(itRev, intVec->rend());

  do {
    --itRev;
    --itConstRev;
    ASSERT_EQ(*itRev, *itConstRev);
  } while (itConstRev != intVec->crbegin());

  ASSERT_EQ(*itConstRev, *intVec->crbegin());
  ASSERT_EQ(*itRev, *intVec->rbegin());
}
