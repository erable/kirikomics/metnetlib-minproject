/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more deimpliedVertices.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 04.10.17.
//

#include "hglib.h"

#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <list>
#include <deque>
#include <unordered_set>

#include "gtest/gtest.h"

using std::pair;
using std::string;
using std::vector;
using hglib::UndirectedVertex;
using hglib::DirectedHyperedge;
using hglib::NamedUndirectedHyperedge;
using hglib::UndirectedVertex;
using hglib::UndirectedHyperedge;
using hglib::NamedUndirectedHyperedge;

/**
 * Test fixture
 */
class TestUndirectedHyperGraph : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

  vector<string> vertices {"A", "B", "Foo", "Bar", "", "6"};
  vector<pair<vector<string>, string>> hyperedges {
    {{"A", "Foo"}, "edge1"},
    {{"A", "B", "Bar", ""}, "edge2"},
    {{"6", ""}, "edge3"}
  };
  vector<pair<vector<string>, string>> hyperedgesWithUndeclaredVertices {
    {{"A", "Foo"}, "edge1"},
    {{"A", "B", "Bar", ""}, "edge2"},
    {{"6", ""}, "edge3"},
    {{"A", "C"}, "edge4"},
    {{"D", "C"}, "edge5"}
  };
  vector<pair<vector<string>, string>> hyperedgesDuplicateEntries {
    {{"A", "Foo"}, "edge1"},
    {{"A2", "Foo2"}, "edge1"},
    {{"A", "B", "Bar", ""}, "edge2"},
    {{"6", ""}, "edge3"}
  };
  vector<string> verticesDuplicateEntries {"A", "B", "Foo", "Bar", "", "6",
                                           "A"};

  vector<string> vertices2 {"X", "Y", "Z", "U", "V", "A", "Foo"};
  vector<pair<vector<string>, string>> hyperedges2 {
    {{"7", "X"}, "edge1"},
    {{"Z", "X", "A"}, "edge2"},
    {{"Y", "V", "Foo"}, "edge3"},
    {{"X", "Y"}, "edge4"},
    {{"X", "Z", "U", "V"}, "edge5"},
    {{"A", "Foo"}, "edge6"}
  };

 private:
  std::streambuf* storedStreambuf_;
};

TEST_F(TestUndirectedHyperGraph, TestCtor) {
// legal calls of a UnUndirectedHypergraph_impl
hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge> g;
hglib::UndirectedHypergraph<> g1;  // same as above
hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge> g2;


// Uncommenting the following lines won't compile anymore due to
// incorrect template parameters
// hglib::UndirectedHypergraph<UndirectedVertex, DirectedHyperedge> g4;
// hglib::UndirectedHypergraph<DirectedVertex, UndirectedHyperedge> g5;
/* hglib::UndirectedHypergraph<DirectedVertex,
 * NamedUndirectedHyperedge> g6;*/
}

TEST_F(TestUndirectedHyperGraph, Test_CopyCtor) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      std::string label = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      std::string label;
      Colour colour = Colour::red;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first);
  }

  // remove vertex "6" and associated hyperedge "edge3"
  g.removeVertex("6");

  // change some default properties in hypergraph with unnamed hyperedges
  g.getVertexProperties_(g.vertexByName("A")->id())->label = "Test";
  hglib::HyperedgeIdType hyperedgeId(0);  // hyperedge "edge1"
  g.getHyperedgeProperties_(hyperedgeId)->label = "TestHyperarc";
  g.getHyperedgeProperties_(hyperedgeId)->weight = 1.0;
  g.getHypergraphProperties_()->colour = Colour::blue;

  // Copy hypergraph with unnamed hyperedges
  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> gCopy(g);
  // Check
  ASSERT_EQ(g.nbVertices(), gCopy.nbVertices());
  ASSERT_EQ(g.nbHyperedges(), gCopy.nbHyperedges());
  // check graph properties
  ASSERT_STREQ(gCopy.getHypergraphProperties()->label.c_str(), "");
  ASSERT_EQ(gCopy.getHypergraphProperties()->colour, Colour::blue);
  // check vertices
  for (const auto& vertex : g.vertices()) {
    const auto& copiedVertex = gCopy.vertexByName(vertex->name());
    ASSERT_NE(copiedVertex, nullptr);
    ASSERT_EQ(vertex->id(), copiedVertex->id());
    const auto& vertexId = vertex->id();
    // compare implied hyperedges
    ASSERT_EQ(g.nbHyperedges(vertexId), gCopy.nbHyperedges(vertexId));
    auto hyperedgeIt = g.hyperedgesBegin(vertexId);
    auto hyperedgeEnd = g.hyperedgesEnd(vertexId);
    auto hyperedgeCopyIt = gCopy.hyperedgesBegin(vertexId);
    for (; hyperedgeIt != hyperedgeEnd; ++hyperedgeIt) {
      const auto& hyperedge = *hyperedgeIt;
      ASSERT_EQ(hyperedge->id(), (*hyperedgeCopyIt)->id());
      ++hyperedgeCopyIt;
    }
    // compare properties
    const auto vertexProperty = g.getVertexProperties(vertexId);
    const auto copiedVertexProperty = gCopy.getVertexProperties(vertexId);
    ASSERT_EQ(vertexProperty->label.compare(copiedVertexProperty->label), 0);
    ASSERT_EQ(vertexProperty->colour, copiedVertexProperty->colour);
  }

  // check hyperedges
  for (const auto& hyperedge : g.hyperedges()) {
    const auto& copiedHyperarc = gCopy.hyperedgeById(hyperedge->id());
    ASSERT_NE(copiedHyperarc, nullptr);
    ASSERT_EQ(hyperedge->id(), copiedHyperarc->id());
    const auto& hyperedgeId = hyperedge->id();
    // compare impliedVertices
    ASSERT_EQ(g.nbImpliedVertices(hyperedgeId),
              gCopy.nbImpliedVertices(hyperedgeId));
    auto impliedVerticesIt = g.impliedVerticesBegin(hyperedgeId);
    auto impliedVerticesEnd = g.impliedVerticesEnd(hyperedgeId);
    auto vertexCopyIt = gCopy.impliedVerticesBegin(hyperedgeId);
    for (; impliedVerticesIt != impliedVerticesEnd; ++impliedVerticesIt) {
      const auto& v = *impliedVerticesIt;
      ASSERT_EQ(v->id(), (*vertexCopyIt)->id());
      ASSERT_EQ(v->name().compare((*vertexCopyIt)->name()), 0);
      ++vertexCopyIt;
    }

    // compare properties
    const auto hyperedgeProperty = g.getHyperedgeProperties(hyperedgeId);
    const auto copiedHyperarcProperty =
            gCopy.getHyperedgeProperties(hyperedgeId);
    ASSERT_EQ(hyperedgeProperty->label.compare(copiedHyperarcProperty->label),
              0);
    ASSERT_EQ(hyperedgeProperty->weight, copiedHyperarcProperty->weight);
  }

  // Add a vertex and hyperedge to gCopy to check their correct id
  const auto& addedvertex = gCopy.addVertex("Test2");
  const auto& addedHyperedge = gCopy.addHyperedge(hglib::NAME,
                                                 {"A", "Test2"});
  ASSERT_EQ(addedvertex->id(), 6);
  ASSERT_EQ(addedHyperedge->id(), 3);
}

TEST_F(TestUndirectedHyperGraph, Test_CopyCtorWithNamedUndirectedHyperedge) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      std::string label = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      std::string label;
      Colour colour = Colour::red;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> g;

  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  // remove vertex "6" and associated hyperedge "edge3" from both hypergraphs
  g.removeVertex("6");

  // change some default properties in hypergraph with unnamed hyperedges
  g.getVertexProperties_(g.vertexByName("A")->id())->label = "Test";
  hglib::HyperedgeIdType hyperedgeId(0);  // hyperedge "edge1"
  g.getHyperedgeProperties_(hyperedgeId)->label = "TestHyperarc";
  g.getHyperedgeProperties_(hyperedgeId)->weight = 1.0;
  g.getHypergraphProperties_()->colour = Colour::blue;

  // Copy hypergraph with unnamed hyperedges
  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> gCopy(g);
  // Check
  ASSERT_EQ(g.nbVertices(), gCopy.nbVertices());
  ASSERT_EQ(g.nbHyperedges(), gCopy.nbHyperedges());
  // check graph properties
  ASSERT_EQ(gCopy.getHypergraphProperties()->label.compare(""), 0);
  ASSERT_EQ(gCopy.getHypergraphProperties()->colour, Colour::blue);
  // check vertices
  for (const auto& vertex : g.vertices()) {
    const auto& copiedVertex = gCopy.vertexByName(vertex->name());
    ASSERT_NE(copiedVertex, nullptr);
    ASSERT_EQ(vertex->id(), copiedVertex->id());
    const auto& vertexId = vertex->id();
    // compare in-hyperedges
    ASSERT_EQ(g.nbHyperedges(vertexId), gCopy.nbHyperedges(vertexId));
    auto it = g.hyperedgesBegin(vertexId);
    auto end = g.hyperedgesEnd(vertexId);
    auto hyperedgeCopyIt = gCopy.hyperedgesBegin(vertexId);
    for (; it != end; ++it) {
      const auto& hyperedge = *it;
      ASSERT_EQ(hyperedge->id(), (*hyperedgeCopyIt)->id());
      ASSERT_EQ(hyperedge->name().compare((*hyperedgeCopyIt)->name()), 0);
      ++hyperedgeCopyIt;
    }
    // compare properties
    const auto vertexProperty = g.getVertexProperties(vertexId);
    const auto copiedVertexProperty = gCopy.getVertexProperties(vertexId);
    ASSERT_EQ(vertexProperty->label.compare(copiedVertexProperty->label), 0);
    ASSERT_EQ(vertexProperty->colour, copiedVertexProperty->colour);
  }

  // check hyperedges
  for (const auto& hyperedge : g.hyperedges()) {
    const auto& copiedHyperarc = gCopy.hyperedgeById(hyperedge->id());
    ASSERT_NE(copiedHyperarc, nullptr);
    ASSERT_EQ(hyperedge->id(), copiedHyperarc->id());
    ASSERT_EQ(hyperedge->name().compare(copiedHyperarc->name()), 0);
    const auto& hyperedgeId = hyperedge->id();
    // compare impliedVertices
    ASSERT_EQ(g.nbImpliedVertices(hyperedgeId),
              gCopy.nbImpliedVertices(hyperedgeId));
    auto it = g.impliedVerticesBegin(hyperedgeId);
    auto end = g.impliedVerticesEnd(hyperedgeId);
    auto vertexCopyIt = gCopy.impliedVerticesBegin(hyperedgeId);
    for (; it != end; ++it) {
      const auto& v = *it;
      ASSERT_EQ(v->id(), (*vertexCopyIt)->id());
      ASSERT_EQ(v->name().compare((*vertexCopyIt)->name()), 0);
      ++vertexCopyIt;
    }

    // compare properties
    const auto hyperedgeProperty = g.getHyperedgeProperties(hyperedgeId);
    const auto copiedHyperarcProperty =
            gCopy.getHyperedgeProperties(hyperedgeId);
    ASSERT_EQ(hyperedgeProperty->label.compare(copiedHyperarcProperty->label),
              0);
    ASSERT_EQ(hyperedgeProperty->weight, copiedHyperarcProperty->weight);
  }
}

TEST_F(TestUndirectedHyperGraph, Test_AssignmentOperator) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      string name = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      string name = "Default";
      Colour colour = Colour::blue;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, HyperarcProperty,
          HypergraphProperty> graph, graph2, graph3;

  for (auto vertexName : vertices) {
    graph.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    graph.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  for (auto vertexName : vertices2) {
    graph2.addVertex(vertexName);
    graph3.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges2) {
    try {
      graph2.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
      graph3.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
    } catch (const std::invalid_argument& invalid_argument) {}
  }

  graph.getVertexProperties_(graph.vertexByName("A")->id())->name = "Test";
  graph.getHyperedgeProperties_(0)->name = "Test";  // "edge1"
  graph.getHypergraphProperties_()->name = "Test";
  graph.getHypergraphProperties_()->colour = Colour::red;
  graph = graph;  // nothing happens in a self assignment
  graph2 = graph3 = graph;
  // check if both graphs are identical
  // check graph properties
  ASSERT_EQ(graph.getHypergraphProperties()->name.compare("Test"), 0);
  ASSERT_EQ(graph.getHypergraphProperties()->colour, Colour::red);
  ASSERT_EQ(graph2.getHypergraphProperties()->name.compare("Test"), 0);
  ASSERT_EQ(graph2.getHypergraphProperties()->colour, Colour::red);
  ASSERT_EQ(graph3.getHypergraphProperties()->name.compare("Test"), 0);
  ASSERT_EQ(graph3.getHypergraphProperties()->colour, Colour::red);
  // check vertices
  auto vertexItGraph2 = graph2.verticesBegin();
  auto vertexItGraph3 = graph3.verticesBegin();
  for (const auto& vertex : graph.vertices()) {
    ASSERT_EQ(vertex->id(), (*vertexItGraph2)->id());
    ASSERT_EQ(vertex->name().compare((*vertexItGraph2)->name()), 0);
    ASSERT_EQ(vertex->id(), (*vertexItGraph3)->id());
    ASSERT_EQ(vertex->name().compare((*vertexItGraph3)->name()), 0);
    // check implied edges
    auto it = graph.hyperedgesBegin(vertex->id());
    auto end = graph.hyperedgesEnd(vertex->id());
    auto hyperedgeItGraph2 = graph2.hyperedgesBegin(vertex->id());
    auto hyperedgeItGraph3 = graph3.hyperedgesBegin(vertex->id());
    for (; it != end; ++it) {
      const auto& hyperedge = *it;
      ASSERT_EQ(hyperedge->id(), (*hyperedgeItGraph2)->id());
      ASSERT_EQ(hyperedge->name().compare((*hyperedgeItGraph2)->name()), 0);
      ASSERT_EQ(hyperedge->id(), (*hyperedgeItGraph3)->id());
      ASSERT_EQ(hyperedge->name().compare((*hyperedgeItGraph3)->name()), 0);
      ++hyperedgeItGraph2;
      ++hyperedgeItGraph3;
    }

    // check properties
    auto vertexProperty = graph.getVertexProperties(vertex->id());
    auto vertex2Property = graph2.getVertexProperties((*vertexItGraph2)->id());
    auto vertex3Property = graph3.getVertexProperties((*vertexItGraph3)->id());
    ASSERT_EQ(vertexProperty->name.compare(vertex2Property->name), 0);
    ASSERT_EQ(vertexProperty->name.compare(vertex3Property->name), 0);
    ASSERT_EQ(vertexProperty->colour, vertex2Property->colour);
    ASSERT_EQ(vertexProperty->colour, vertex3Property->colour);

    ++vertexItGraph2;
    ++vertexItGraph3;
  }

  // check hyperedges
  auto hyperedgeItGraph2 = graph2.hyperedgesBegin();
  auto hyperedgeItGraph3 = graph3.hyperedgesBegin();
  for (const auto& hyperedge : graph.hyperedges()) {
    ASSERT_EQ(hyperedge->id(), (*hyperedgeItGraph2)->id());
    ASSERT_EQ(hyperedge->name().compare((*hyperedgeItGraph2)->name()), 0);
    ASSERT_EQ(hyperedge->id(), (*hyperedgeItGraph3)->id());
    ASSERT_EQ(hyperedge->name().compare((*hyperedgeItGraph3)->name()), 0);
    // Check impliedVertices
    auto it = graph.impliedVerticesBegin(hyperedge->id());
    auto end = graph.impliedVerticesEnd(hyperedge->id());
    auto tailItGraph2 = graph2.impliedVerticesBegin(hyperedge->id());
    auto tailItGraph3 = graph3.impliedVerticesBegin(hyperedge->id());
    for (; it != end; ++it) {
      const auto& vertex = *it;
      ASSERT_EQ(vertex->id(), (*tailItGraph2)->id());
      ASSERT_EQ(vertex->name().compare((*tailItGraph2)->name()), 0);
      ASSERT_EQ(vertex->id(), (*tailItGraph3)->id());
      ASSERT_EQ(vertex->name().compare((*tailItGraph3)->name()), 0);
      ++tailItGraph2;
      ++tailItGraph3;
    }

    // Check properties
    const auto hyperedgeProperty = graph.getHyperedgeProperties(
            hyperedge->id());
    const auto hyperedgeProperty2 = graph2.getHyperedgeProperties(
            (*hyperedgeItGraph2)->id());
    const auto hyperedgeProperty3 = graph3.getHyperedgeProperties(
            (*hyperedgeItGraph3)->id());
    ASSERT_EQ(hyperedgeProperty->name.compare(hyperedgeProperty2->name), 0);
    ASSERT_EQ(hyperedgeProperty->name.compare(hyperedgeProperty3->name), 0);
    ASSERT_EQ(hyperedgeProperty->weight, hyperedgeProperty2->weight);
    ASSERT_EQ(hyperedgeProperty->weight, hyperedgeProperty3->weight);

    ++hyperedgeItGraph2;
    ++hyperedgeItGraph3;
  }

  // Add a vertex and hyperedge to graph2 to check their correct id
  const auto& addedvertex = graph2.addVertex("Test2");
  const auto& addedHyperedge = graph2.addHyperedge(hglib::NAME,
                                                   {"A", "Test2"},
                                                   {"Test_edge"});
  ASSERT_EQ(addedvertex->id(), 6);
  ASSERT_EQ(addedHyperedge->id(), 3);

  // Add a vertex and hyperedge to graph3 to check their correct id
  const auto& addedvertex2 = graph3.addVertex("Test2");
  const auto& addedHyperedge2 = graph3.addHyperedge(hglib::NAME,
                                                    {"A", "Test2"},
                                                    {"Test_edge"});
  ASSERT_EQ(addedvertex2->id(), 6);
  ASSERT_EQ(addedHyperedge2->id(), 3);
}

TEST_F(TestUndirectedHyperGraph, Test_addVertex) {
  hglib::UndirectedHypergraph<> g;
  size_t nbVertices = 0;
  hglib::VertexIdType vertexCounter = 0;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    ASSERT_EQ(++nbVertices, g.nbVertices());
    ASSERT_EQ(vertexCounter++, g.vertexByName(vertexName)->id());
  }
  // re-insertion of the same vertex (name) is not possible
  for (auto vertexName : vertices) {
    ASSERT_THROW(g.addVertex(vertexName), std::invalid_argument);
    ASSERT_EQ(nbVertices, g.nbVertices());
  }

  // insert vertices in another graph and check if the vertex ids start by zero
  hglib::UndirectedHypergraph<> g2;
  vertexCounter = 0;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
    ASSERT_EQ(vertexCounter++, g2.vertexByName(vertexName)->id());
  }

  // Add vertices without specifying the name
  hglib::UndirectedHypergraph<> g3;
  vertexCounter = 0;
  for (auto vertexName : vertices) {
    g3.addVertex();
    std::string expectedName = "udv_" + std::to_string(vertexCounter);
    const auto& vertex = g3.vertexByName(expectedName);
    ASSERT_NE(vertex, nullptr);
    ASSERT_EQ(vertex->name().compare(expectedName), 0);
    ASSERT_EQ(vertex->id(), vertexCounter++);
  }

  // TODO(mw): test with other types of vertices (more parameter than just a
  // name)
  // TODO(mw): test with vertex property (access functions have to be
  // implemented)
}

TEST_F(TestUndirectedHyperGraph, Test_addHyperedge) {
  hglib::UndirectedHypergraph<> g;  // graph with multi-hyperedges
  // graph without multi-hyperedges
  hglib::UndirectedHypergraph<> g2(false);
  // graph with multi-hyperedges
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g3;
  // graph without multi-hyperedges
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g4(false);

  // insert vertices in all hypergraphs
  for (const auto& vertexName : vertices) {
    g.addVertex(vertexName);
    g2.addVertex(vertexName);
    g3.addVertex(vertexName);
    g4.addVertex(vertexName);
  }
  // add hyperedges in all hypergraphs
  size_t nbHyperedges(0);
  hglib::HyperedgeIdType hyperedgeCounter(0);
  for (auto hyperedge : hyperedges) {
    const auto& arc1 = g.addHyperedge(hglib::NAME, hyperedge.first);
    const auto& arc2 = g2.addHyperedge(hglib::NAME, hyperedge.first);
    const auto& arc3 = g3.addHyperedge(hglib::NAME, hyperedge.first,
                                      hyperedge.second);
    const auto& arc4 = g4.addHyperedge(hglib::NAME, hyperedge.first,
                                      hyperedge.second);
    ++nbHyperedges;
    ASSERT_EQ(nbHyperedges, g.nbHyperedges());
    ASSERT_EQ(nbHyperedges, g2.nbHyperedges());
    ASSERT_EQ(nbHyperedges, g3.nbHyperedges());
    ASSERT_EQ(nbHyperedges, g4.nbHyperedges());
    ASSERT_EQ(hyperedgeCounter, arc1->id());
    ASSERT_EQ(hyperedgeCounter, arc2->id());
    ASSERT_EQ(hyperedgeCounter, arc3->id());
    ASSERT_EQ(hyperedgeCounter, arc4->id());
    ++hyperedgeCounter;
  }

  // check in/out degree of vertices in hypergraph g
  for (const auto& vertexName : vertices) {
    const auto& vertex = g.vertexByName(vertexName);
    ASSERT_NE(vertex, nullptr);
    switch (vertex->id()) {
      case 0:  // vertex A
        ASSERT_EQ(g.nbHyperedges(vertex->id()), (size_t) 2);
        break;
      case 1:  // vertex B
        ASSERT_EQ(g.nbHyperedges(vertex->id()), (size_t) 1);
        break;
      case 2:  // vertex Foo
        ASSERT_EQ(g.nbHyperedges(vertex->id()), (size_t) 1);
        break;
      case 3:  // vertex Bar
        ASSERT_EQ(g.nbHyperedges(vertex->id()), (size_t) 1);
        break;
      case 4:  // vertex ''
        ASSERT_EQ(g.nbHyperedges(vertex->id()), (size_t) 2);
        break;
      case 5:  // vertex 6
        ASSERT_EQ(g.nbHyperedges(vertex->id()), (size_t) 1);
        break;
    }
  }


  size_t nbCurrentHyperarcs(nbHyperedges);
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first);
    ASSERT_THROW(g2.addHyperedge(hglib::NAME, hyperedge.first),
                 std::invalid_argument);
    g3.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
    ASSERT_THROW(g4.addHyperedge(hglib::NAME, hyperedge.first,
                                 hyperedge.second),
                 std::invalid_argument);
    ASSERT_EQ(++nbHyperedges, g.nbHyperedges());
    ASSERT_EQ(nbCurrentHyperarcs, g2.nbHyperedges());
    ASSERT_EQ(nbHyperedges, g3.nbHyperedges());
    ASSERT_EQ(nbCurrentHyperarcs, g4.nbHyperedges());
  }

  // test undeclared vertices in a hyperedge
  hglib::UndirectedHypergraph<> g5;
  for (auto vertexName : vertices) {
    g5.addVertex(vertexName);
  }
  // 2 out of 5 hyperedges have undeclared vertices
  for (auto hyperedge : hyperedgesWithUndeclaredVertices) {
    try {
      g5.addHyperedge(hglib::NAME, hyperedge.first);
    } catch (std::invalid_argument& invalidArgument) {}
  }
  ASSERT_EQ(g5.nbHyperedges(), (size_t) 3);
}

TEST_F(TestUndirectedHyperGraph, Test_vertexByName) {
  hglib::UndirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    ASSERT_EQ(g.vertexByName(vertexName)->name().compare(vertexName), 0);
  }
  ASSERT_EQ(g.vertexByName("UnknownVertex"), nullptr);
}

TEST_F(TestUndirectedHyperGraph, Test_vertexById) {
  hglib::UndirectedHypergraph<> g;
  hglib::VertexIdType vertexCounter = 0;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    ASSERT_EQ(g.vertexById(vertexCounter)->id(), vertexCounter);
    ASSERT_EQ(g.vertexByName(vertexName), g.vertexById(vertexCounter++));
  }
  // vertex with given id does not exist -> exception
  ASSERT_THROW(g.vertexById(vertexCounter), std::out_of_range);

  // Check that a nullptr is returned after the removal of a vertex
  vertexCounter = 0;
  for (auto vertexName : vertices) {
    g.removeVertex(vertexCounter);
    ASSERT_EQ(g.vertexById(vertexCounter), nullptr);
    ++vertexCounter;
  }
}

TEST_F(TestUndirectedHyperGraph, Test_verticesContainer) {
  hglib::UndirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  std::vector<const UndirectedVertex<UndirectedHyperedge>*> verticesContainer;
  hglib::vertices(g, std::back_inserter(verticesContainer));
  auto it = vertices.begin();
  for (auto& vertex : verticesContainer) {
    EXPECT_EQ(vertex->name().compare(*it), 0);
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
    ++it;
  }

  // test with std:list
  // std::back_inserter
  std::list<const UndirectedVertex<UndirectedHyperedge>*> l;
  hglib::vertices(g, std::back_inserter(l));
  EXPECT_EQ(l.size(), g.nbVertices());
  for (auto& vertex : l) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }
  l.clear();
  // std::inserter
  hglib::vertices(g, std::inserter(l, l.begin()));
  EXPECT_EQ(l.size(), g.nbVertices());
  for (auto& vertex : l) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }

  // test with std::deque
  std::deque<const UndirectedVertex<UndirectedHyperedge>*> d;
  hglib::vertices(g, std::back_inserter(d));
  EXPECT_EQ(d.size(), g.nbVertices());
  for (auto& vertex : d) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }

  // test with std::set
  std::set<const UndirectedVertex<UndirectedHyperedge>*> s;
  hglib::vertices(g, std::inserter(s, s.begin()));
  EXPECT_EQ(s.size(), g.nbVertices());
  for (auto& vertex : s) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }
// test with std::multiset
  std::multiset<const UndirectedVertex<UndirectedHyperedge>*> ms;
  hglib::vertices(g, std::inserter(ms, ms.begin()));
  EXPECT_EQ(ms.size(), g.nbVertices());
  for (auto& vertex : ms) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }

  // test with std::unordered_set
  std::unordered_set<const UndirectedVertex<UndirectedHyperedge>*> us;
  hglib::vertices(g, std::inserter(us, us.begin()));
  EXPECT_EQ(us.size(), g.nbVertices());
  for (auto& vertex : us) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }

  // test with std::unordered_multiset
  std::unordered_multiset<const UndirectedVertex<UndirectedHyperedge>*> ums;
  hglib::vertices(g, std::inserter(ums, ums.begin()));
  EXPECT_EQ(ums.size(), g.nbVertices());
  for (auto& vertex : ums) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }
}


TEST_F(TestUndirectedHyperGraph, Test_hyperedgesContainer) {
  hglib::UndirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto arc : hyperedges) {
    g.addHyperedge(hglib::NAME, arc.first);
  }

  std::vector<const UndirectedHyperedge*> hyperedgeContainer;
  hglib::hyperedges(g, std::back_inserter(hyperedgeContainer));
  ASSERT_EQ(hyperedges.size(), hyperedgeContainer.size());
  ASSERT_EQ(hyperedges.size(), g.nbHyperedges());
  for (auto& hyperedge : hyperedgeContainer) {
    EXPECT_NE(g.hyperedgeById(hyperedge->id()), nullptr);
  }

  // test with std:list
  // std::back_inserter
  std::list<const UndirectedHyperedge*> l;
  hglib::hyperedges(g, std::back_inserter(l));
  EXPECT_EQ(l.size(), g.nbHyperedges());
  for (auto& hyperedge : l) {
    EXPECT_NE(g.hyperedgeById(hyperedge->id()), nullptr);
  }
  l.clear();
  // std::inserter
  hglib::hyperedges(g, std::inserter(l, l.begin()));
  EXPECT_EQ(l.size(), g.nbHyperedges());
  for (auto& hyperedge : l) {
    EXPECT_NE(g.hyperedgeById(hyperedge->id()), nullptr);
  }

  // test with std::deque
  std::deque<const UndirectedHyperedge*> d;
  hglib::hyperedges(g, std::back_inserter(d));
  EXPECT_EQ(d.size(), g.nbHyperedges());
  for (auto& hyperedge : d) {
    EXPECT_NE(g.hyperedgeById(hyperedge->id()), nullptr);
  }

  // test with std::set
  std::set<const UndirectedHyperedge*> s;
  hglib::hyperedges(g, std::inserter(s, s.begin()));
  EXPECT_EQ(s.size(), g.nbHyperedges());
  for (auto& hyperedge : s) {
    EXPECT_NE(g.hyperedgeById(hyperedge->id()), nullptr);
  }
// test with std::multiset
  std::multiset<const UndirectedHyperedge*> ms;
  hglib::hyperedges(g, std::inserter(ms, ms.begin()));
  EXPECT_EQ(ms.size(), g.nbHyperedges());
  for (auto& hyperedge : ms) {
    EXPECT_NE(g.hyperedgeById(hyperedge->id()), nullptr);
  }

  // test with std::unordered_set
  std::unordered_set<const UndirectedHyperedge*> us;
  hglib::hyperedges(g, std::inserter(us, us.begin()));
  EXPECT_EQ(us.size(), g.nbHyperedges());
  for (auto& hyperedge : us) {
    EXPECT_NE(g.hyperedgeById(hyperedge->id()), nullptr);
  }

  // test with std::unordered_multiset
  std::unordered_multiset<const UndirectedHyperedge*> ums;
  hglib::hyperedges(g, std::inserter(ums, ums.begin()));
  EXPECT_EQ(ums.size(), g.nbHyperedges());
  for (auto& hyperedge : ums) {
    EXPECT_NE(g.hyperedgeById(hyperedge->id()), nullptr);
  }
}

TEST_F(TestUndirectedHyperGraph, Test_vertexIterator) {
  hglib::UndirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  // test verticesBeginEnd (pair of begin/end iter)
  hglib::UndirectedHypergraph<>::vertex_iterator it, end;
  std::tie(it, end) = g.verticesBeginEnd();
  ASSERT_EQ(it, g.verticesBegin());
  ASSERT_EQ(end, g.verticesEnd());
  int pos(0);
  for (; it != end; ++it) {
    ASSERT_EQ((*it)->id(), (hglib::VertexIdType) pos);
    ASSERT_STREQ((*it)->name().c_str(), vertices[pos].c_str());
    ++pos;
  }

  // test verticesBegin/verticesEnd
  pos = 0;
  it = g.verticesBegin();
  for (; it != g.verticesEnd(); ++it) {
    ASSERT_EQ((*it)->id(), (hglib::VertexIdType) pos);
    ASSERT_STREQ((*it)->name().c_str(), vertices[pos].c_str());
    ++pos;
  }

  // test ranged based for loop
  pos = 0;
  for (const auto& vertex : g.vertices()) {
    ASSERT_EQ(vertex->id(), (hglib::VertexIdType) pos);
    ASSERT_STREQ(vertex->name().c_str(), vertices[pos].c_str());
    ++pos;
  }

  // test iterators after the removal of vertices
  g.removeVertex(1);
  g.removeVertex(3);
  g.removeVertex(5);

  std::tie(it, end) = g.verticesBeginEnd();
  auto it2 = g.verticesBegin();
  for (const auto& vertex : g.vertices()) {
    ASSERT_EQ(vertex, (*it));
    ASSERT_EQ(vertex, (*it2));
    ++it;
    ++it2;
  }
}

TEST_F(TestUndirectedHyperGraph, Test_getVertexProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (const auto& vertex : g.vertices()) {
    auto vProp = g.getVertexProperties_(vertex->id());
    ASSERT_EQ(vProp->name.compare("Default"), 0);
    ASSERT_EQ(vProp->colour, Colour::green);
    vProp->name = vertex->name() + "_" + std::to_string(vertex->id());
    vProp->colour = Colour::red;
    // Verify that updated properties are stored in the hypergraph
    const auto constVProp = g.getVertexProperties(vertex->id());
    string expected = vertex->name() + "_" + std::to_string(vertex->id());
    ASSERT_EQ(constVProp->name.compare(expected), 0);
    ASSERT_EQ(constVProp->colour, Colour::red);
  }

  hglib::VertexIdType outOfRange = 10;
  ASSERT_THROW(g.getVertexProperties_(outOfRange), std::out_of_range);
  ASSERT_THROW(g.getVertexProperties(outOfRange), std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(g.getVertexProperties_(outOfRange), std::out_of_range);
  ASSERT_THROW(g.getVertexProperties(outOfRange), std::out_of_range);

  // without VertexProperty (empty struct)
  hglib::UndirectedHypergraph<> g2;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
  }

  for (const auto& vertex : g2.vertices()) {
    auto vProp = g2.getVertexProperties(vertex->id());
    EXPECT_EQ(typeid(hglib::emptyProperty), typeid(*vProp));
    EXPECT_NE(typeid(VertexProperty), typeid(*vProp));
  }
}

TEST_F(TestUndirectedHyperGraph, Test_verticesWithPropertyContainer) {
  enum Colour {
      red, blue, green, black
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  // Get all (green) vertices
  std::vector<const UndirectedVertex<UndirectedHyperedge>*> verticesContainer;
  hglib::vertices(g, std::back_inserter(verticesContainer),
                  [&] (hglib::VertexIdType vertexId) -> bool {
                      return g.getVertexProperties(vertexId)->colour ==
                             Colour::green;});
  // All vertices are green by default
  ASSERT_EQ(verticesContainer.size(), vertices.size());
  auto it = vertices.begin();
  for (auto& vertex : verticesContainer) {
    EXPECT_EQ(vertex->name().compare(*it), 0);
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
    ++it;
  }
  // set colour of vertex 'A' to red
  g.getVertexProperties_(
          g.vertexByName("A")->id())->colour = Colour::red;
  // set colour of vertex 'B' and 'Foo' to blue
  g.getVertexProperties_(
          g.vertexByName("B")->id())->colour = Colour::blue;
  g.getVertexProperties_(
          g.vertexByName("Foo")->id())->colour = Colour::blue;
  // Get vertices by colour
  for ( int idx = red; idx != black; idx++ ) {
    verticesContainer.clear();  // empty container
    // Get vertices of specified colour
    Colour colour = static_cast<Colour>(idx);
    hglib::vertices(g, std::back_inserter(verticesContainer),
                    [&] (hglib::VertexIdType vertexId) ->
                            bool {
                        return g.getVertexProperties(vertexId)->colour ==
                               colour;});
    switch (colour) {
      case red:
        ASSERT_EQ(verticesContainer.size(), (size_t) 1);
        ASSERT_EQ(verticesContainer[0]->name().compare("A"), 0);
        break;
      case blue:
        ASSERT_EQ(verticesContainer.size(), (size_t) 2);
        ASSERT_EQ(verticesContainer[0]->name().compare("B"), 0);
        ASSERT_EQ(verticesContainer[1]->name().compare("Foo"), 0);
        break;
      case green:
        ASSERT_EQ(verticesContainer.size(), (size_t) 3);
        ASSERT_EQ(verticesContainer[0]->name().compare("Bar"), 0);
        ASSERT_EQ(verticesContainer[1]->name().compare(""), 0);
        ASSERT_EQ(verticesContainer[2]->name().compare("6"), 0);
        break;
      case black:
        ASSERT_TRUE(verticesContainer.empty());
    }
  }
}

TEST_F(TestUndirectedHyperGraph, Test_GetHypergraphProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      std::string name = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      std::string name;
      Colour colour = Colour::red;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, EdgeProperty, HypergraphProperty> g;

  ASSERT_EQ(g.getHypergraphProperties()->name.compare(""), 0);
  ASSERT_EQ(g.getHypergraphProperties()->colour, Colour::red);
  // change hypergraph properties
  g.getHypergraphProperties_()->name = "Test";
  g.getHypergraphProperties_()->colour = Colour::blue;
  // check changed properties
  ASSERT_EQ(g.getHypergraphProperties()->name.compare("Test"), 0);
  ASSERT_EQ(g.getHypergraphProperties()->colour, Colour::blue);

  // without HypergraphProperty (empty struct)
  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, EdgeProperty> g2;
  EXPECT_EQ(typeid(hglib::emptyProperty),
            typeid(*g2.getHypergraphProperties()));
  EXPECT_NE(typeid(HypergraphProperty),
            typeid(*g2.getHypergraphProperties()));
}

TEST_F(TestUndirectedHyperGraph, Test_hyperedgeById) {
  hglib::UndirectedHypergraph<> g;
  // insert vertices
  for (const auto& vertexName : vertices) {
    g.addVertex(vertexName);
  }
  // add hyperedges
  hglib::HyperedgeIdType hyperedgeCounter(0);
  for (auto hyperedge : hyperedges) {
    const auto& hyperedge1 = g.addHyperedge(hglib::NAME, hyperedge.first);
    const UndirectedHyperedge* hyperedge2 = g.hyperedgeById(hyperedgeCounter);
    ASSERT_EQ(hyperedge1, hyperedge2);
    ++hyperedgeCounter;
  }

  // Check that a nullptr is returned after the removal of a hyperedge
  hyperedgeCounter = 0;
  for (auto hyperedge : hyperedges) {
    g.removeHyperedge(hyperedgeCounter);
    ASSERT_EQ(g.hyperedgeById(hyperedgeCounter), nullptr);
    ++hyperedgeCounter;
  }

  hglib::HyperedgeIdType outOfRange(10);
  ASSERT_THROW(g.hyperedgeById(outOfRange), std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(g.hyperedgeById(outOfRange), std::out_of_range);
}

TEST_F(TestUndirectedHyperGraph, Test_hyperedgeIterator) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  // test hyperedgesBeginEnd (pair of begin/end iter)
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge>::hyperedge_iterator it, end;
  std::tie(it, end) = g.hyperedgesBeginEnd();
  ASSERT_EQ(it, g.hyperedgesBegin());
  ASSERT_EQ(end, g.hyperedgesEnd());
  int pos(0);
  for (; it != end; ++it) {
    ASSERT_EQ((*it)->id(), (hglib::HyperedgeIdType) pos);
    ASSERT_STREQ((*it)->name().c_str(), hyperedges[pos].second.c_str());
    ++pos;
  }

  // test hyperedgesBegin/hyperedgesEnd
  pos = 0;
  auto it2 = g.hyperedgesBegin();
  for (; it2 != g.hyperedgesEnd(); ++it2) {
    ASSERT_EQ((*it2)->id(), (hglib::HyperedgeIdType) pos);
    ASSERT_STREQ((*it2)->name().c_str(), hyperedges[pos].second.c_str());
    ++pos;
  }

  // test ranged based for loop
  pos = 0;
  for (const auto& hyperedge : g.hyperedges()) {
    ASSERT_EQ(hyperedge->id(), (hglib::HyperedgeIdType) pos);
    ASSERT_STREQ(hyperedge->name().c_str(), hyperedges[pos].second.c_str());
    ++pos;
  }

  // Test iterator after the removal of vertices/hyperedges
  size_t idx(0);
  for (; idx < hyperedges.size() ; ++idx) {
    g.removeHyperedge(idx);
    // using begin
    it2 = g.hyperedgesBegin();
    // using begin/end-pair
    std::tie(it, end) = g.hyperedgesBeginEnd();
    for (const auto& hyperedge : g.hyperedges()) {
      ASSERT_EQ((*it2), hyperedge);
      ASSERT_EQ((*it), hyperedge);
      ++it2;
      ++it;
    }
  }
}

TEST_F(TestUndirectedHyperGraph, Test_impliedVerticesIterator) {
  typedef hglib::UndirectedHypergraph<UndirectedVertex,
      NamedUndirectedHyperedge> hypergraph;
  hypergraph g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  for (auto hyperedge : hyperedges) {
    const auto& edge = g.addHyperedge(
        hglib::NAME, hyperedge.first, hyperedge.second);
    const auto& edgeId = edge->id();
    // test implied vertices iterator
    // iter begin
    auto it = g.impliedVerticesBegin(edgeId);
    // pair begin/end
    hypergraph::vertex_iterator it2, end;
    std::tie(it2, end) = g.impliedVerticesBeginEnd(edgeId);
    // range based for
    auto verticesPtr = g.impliedVertices(edgeId);
    // vertex names
    auto itNames = hyperedge.first.begin();
    for (const auto& vertex : *verticesPtr) {
      ASSERT_STREQ(vertex->name().c_str(), (*itNames).c_str());
      ASSERT_STREQ((*it)->name().c_str(), (*itNames).c_str());
      ASSERT_STREQ((*it2)->name().c_str(), (*itNames).c_str());
      ++it;
      ++it2;
      ++itNames;
    }
    // check nb of vertices in hyperedge
    ASSERT_EQ(g.nbImpliedVertices(edgeId), hyperedge.first.size());
    ASSERT_TRUE(g.hasVertices(edgeId));
  }
}

TEST_F(TestUndirectedHyperGraph, Test_getHyperedgeProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, HyperarcProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  for (const auto& hyperedge : g.hyperedges()) {
    auto hyperedgeProp = g.getHyperedgeProperties_(hyperedge->id());
    ASSERT_EQ(hyperedgeProp->name.compare("Default"), 0);
    ASSERT_EQ(hyperedgeProp->weight, 0.0);
    hyperedgeProp->name = hyperedge->name() + "_" +
                         std::to_string(hyperedge->id());
    hyperedgeProp->weight = 1.0;
    // Verify that updated properties are stored in the hypergraph
    const auto constVProp = g.getHyperedgeProperties(hyperedge->id());
    string expected = hyperedge->name() + "_" + std::to_string(
            hyperedge->id());
    ASSERT_EQ(constVProp->name.compare(expected), 0);
    ASSERT_EQ(constVProp->weight, 1.0);
  }

  hglib::HyperedgeIdType outOfRange(10);
  ASSERT_THROW(g.getHyperedgeProperties(outOfRange), std::out_of_range);
  ASSERT_THROW(g.getHyperedgeProperties_(outOfRange), std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(g.getHyperedgeProperties(outOfRange), std::out_of_range);
  ASSERT_THROW(g.getHyperedgeProperties_(outOfRange), std::out_of_range);

  // without HyperarcProperty (empty struct)
  hglib::UndirectedHypergraph<> g2;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g2.addHyperedge(hglib::NAME, hyperedge.first);
  }

  for (const auto& hyperedge : g2.hyperedges()) {
    auto hyperedgeProp = g2.getHyperedgeProperties(hyperedge->id());
    EXPECT_EQ(typeid(hglib::emptyProperty), typeid(*hyperedgeProp));
    EXPECT_NE(typeid(HyperarcProperty), typeid(*hyperedgeProp));
  }
}

TEST_F(TestUndirectedHyperGraph, Test_hyperedgesWithPropertyContainer) {
  enum Colour {
      red, blue, green, black
  };
  struct VertexProperty {
      string name = "Default";
  };
  struct EdgeProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  // Get all (green) vertices
  std::vector<const NamedUndirectedHyperedge*> hyperedgesContainer;
  hglib::hyperedges(g, std::back_inserter(hyperedgesContainer),
                   [&] (hglib::HyperedgeIdType hyperedgeId) -> bool {
                       return g.getHyperedgeProperties(hyperedgeId)->colour ==
                              Colour::green;});
  // All vertices are green by default
  ASSERT_EQ(hyperedgesContainer.size(), g.nbHyperedges());
  for (auto& hyperedge : hyperedgesContainer) {
    EXPECT_NE(g.hyperedgeById(hyperedge->id()), nullptr);
  }
  // set colour of hyperedge 'edge1' to red
  g.getHyperedgeProperties_(0)->colour = Colour::red;
  // set colour of hyperedge 'edge2' and 'edge3' to blue
  g.getHyperedgeProperties_(1)->colour = Colour::blue;
  g.getHyperedgeProperties_(2)->colour = Colour::blue;
  // Get hyperedges by colour
  for ( int idx = red; idx != black; idx++ ) {
    hyperedgesContainer.clear();  // empty container
    // Get vertices of specified colour
    Colour colour = static_cast<Colour>(idx);
    hglib::hyperedges(g, std::back_inserter(hyperedgesContainer),
                     [&] (hglib::HyperedgeIdType arcId) ->
                             bool {
                         return g.getHyperedgeProperties(arcId)->colour ==
                                colour;});
    switch (colour) {
      case red:
        ASSERT_EQ(hyperedgesContainer.size(), (size_t) 1);
        ASSERT_EQ(hyperedgesContainer[0]->name().compare("edge1"), 0);
        break;
      case blue:
        ASSERT_EQ(hyperedgesContainer.size(), (size_t) 2);
        ASSERT_EQ(hyperedgesContainer[0]->name().compare("edge2"), 0);
        ASSERT_EQ(hyperedgesContainer[1]->name().compare("edge3"), 0);
        break;
      case green:
        ASSERT_TRUE(hyperedgesContainer.empty());
        break;
      case black:
        ASSERT_TRUE(hyperedgesContainer.empty());
        break;
    }
  }
}

TEST_F(TestUndirectedHyperGraph, Test_edgeContainsVertex) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  ASSERT_EQ(hyperedges.size(), g.nbHyperedges());
  ASSERT_EQ(vertices.size(), g.nbVertices());

  const auto& hyperedge1 = g.hyperedgeById(0);
  const auto& hyperedge2 = g.hyperedgeById(1);
  const auto& hyperedge3 = g.hyperedgeById(2);
  for (const auto& vertex : g.vertices()) {
    if (vertex->name().compare("A") == 0) {
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge1->id()));
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge1->id()));
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge2->id()));
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge2->id()));
    } else if (vertex->name().compare("B") == 0) {
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge2->id()));
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge2->id()));
    } else if (vertex->name().compare("Foo") == 0) {
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge1->id()));
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge1->id()));
    } else if (vertex->name().compare("Bar") == 0) {
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge2->id()));
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge2->id()));
    } else if (vertex->name().compare("") == 0) {
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge2->id()));
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge2->id()));
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge3->id()));
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge3->id()));
    } else if (vertex->name().compare("6") == 0) {
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge3->id()));
      ASSERT_TRUE(g.isVertexOfHyperedge(vertex->id(), hyperedge3->id()));
    }
  }

  for (const auto& edge : g.hyperedges()) {
    const auto& edgeId = edge->id();
    if (edge->name().compare("edge1") == 0) {
      ASSERT_TRUE(g.isVertexOfHyperedge(g.vertexByName("A")->id(), edgeId));
      ASSERT_TRUE(g.isVertexOfHyperedge(g.vertexByName("Foo")->id(),
                                           edgeId));
      ASSERT_FALSE(g.isVertexOfHyperedge(g.vertexByName("B")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperedge(g.vertexByName("Bar")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperedge(g.vertexByName("")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperedge(g.vertexByName("6")->id(), edgeId));
    } else if (edge->name().compare("edge2") == 0) {
      ASSERT_TRUE(g.isVertexOfHyperedge(g.vertexByName("A")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperedge(g.vertexByName("Foo")->id(), edgeId));
      ASSERT_TRUE(g.isVertexOfHyperedge(g.vertexByName("B")->id(), edgeId));
      ASSERT_TRUE(g.isVertexOfHyperedge(g.vertexByName("Bar")->id(),
                                           edgeId));
      ASSERT_TRUE(g.isVertexOfHyperedge(g.vertexByName("")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperedge(g.vertexByName("6")->id(), edgeId));
    } else if (edge->name().compare("edge3") == 0) {
      ASSERT_FALSE(g.isVertexOfHyperedge(g.vertexByName("A")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperedge(g.vertexByName("Foo")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperedge(g.vertexByName("B")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperedge(g.vertexByName("Bar")->id(), edgeId));
      ASSERT_TRUE(g.isVertexOfHyperedge(g.vertexByName("")->id(), edgeId));
      ASSERT_TRUE(g.isVertexOfHyperedge(g.vertexByName("6")->id(), edgeId));
    }
  }
}

TEST_F(TestUndirectedHyperGraph, Test_impliedHyperedges) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  typedef hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> hypergraph_t;
  for (const auto &vertex : g.vertices()) {
    const auto id = vertex->id();
    std::vector<hglib::HyperedgeIdType> hyperedgeIds;
    if (vertex->name().compare("A") == 0) {
      // hyperedge ids: 0, 1
      ASSERT_TRUE(g.isContainedInAnyHyperedge(id));
      ASSERT_EQ(g.nbHyperedges(id), (size_t) 2);
      hyperedgeIds = {0, 1};
    } else if (vertex->name().compare("B") == 0) {
      // hyperedge ids: 1
      ASSERT_TRUE(g.isContainedInAnyHyperedge(id));
      ASSERT_EQ(g.nbHyperedges(id), (size_t) 1);
      hyperedgeIds = {1};
    } else if (vertex->name().compare("Foo") == 0) {
      // hyperedge ids: 0
      ASSERT_TRUE(g.isContainedInAnyHyperedge(id));
      ASSERT_EQ(g.nbHyperedges(id), (size_t) 1);
      hyperedgeIds = {0};
    } else if (vertex->name().compare("Bar") == 0) {
      // hyperedge ids: 1
      ASSERT_TRUE(g.isContainedInAnyHyperedge(id));
      ASSERT_EQ(g.nbHyperedges(id), (size_t) 1);
      hyperedgeIds = {1};
    } else if (vertex->name().compare("") == 0) {
      // hyperedge ids: 1, 2
      ASSERT_TRUE(g.isContainedInAnyHyperedge(id));
      ASSERT_EQ(g.nbHyperedges(id), (size_t) 2);
      hyperedgeIds = {1, 2};
    } else if (vertex->name().compare("6") == 0) {
      // outgoing hyperedge ids: 2
      ASSERT_TRUE(g.isContainedInAnyHyperedge(id));
      ASSERT_EQ(g.nbHyperedges(id), (size_t) 1);
      hyperedgeIds = {2};
    }
    auto it = hyperedgeIds.begin();
    // using begin/end-pair
    hypergraph_t::hyperedge_iterator it2, end;
    std::tie(it2, end) = g.hyperedgesBeginEnd(id);
    // using begin
    auto itHyperedges = g.hyperedgesBegin(id);
    // range based for
    auto hyperedgesPtr = g.hyperedges(id);
    for (const auto& hyperedge : *hyperedgesPtr) {
      ASSERT_EQ((*it), hyperedge->id());
      ASSERT_EQ((*it), (*itHyperedges)->id());
      ASSERT_EQ((*it), (*it2)->id());
      ++it;
      ++it2;
      ++itHyperedges;
    }
  }

  // test exceptions
  std::vector<int> outOfRangeIds = {10, -1};
  for (const auto &outOfRangeId : outOfRangeIds) {
    ASSERT_THROW(g.nbHyperedges(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.isContainedInAnyHyperedge(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.hyperedgesBegin(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.hyperedgesEnd(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.hyperedgesBeginEnd(outOfRangeId), std::out_of_range);
    // ASSERT_THROW(g.hyperedges(outOfRangeId), std::out_of_range);
  }

  // test exception after the removal of a vertex
  hglib::VertexIdType removedVertexId(2);
  g.removeVertex(removedVertexId);
  ASSERT_THROW(g.nbHyperedges(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.isContainedInAnyHyperedge(removedVertexId),
               std::invalid_argument);
  ASSERT_THROW(g.hyperedgesBegin(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.hyperedgesEnd(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.hyperedgesBeginEnd(removedVertexId), std::invalid_argument);
  // ASSERT_THROW(g.hyperedges(removedVertexId), std::invalid_argument);
}

TEST_F(TestUndirectedHyperGraph, Test_removeVertex) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      std::string name = "Default";
      double weight = 0.0;
  };


  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, HyperarcProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }
  size_t nb_v_init = g.nbVertices();
  size_t nb_e_init = g.nbHyperedges();

  // X does not exist in the hypergraph
  ASSERT_THROW(g.removeVertex("X"), std::invalid_argument);
  ASSERT_EQ(nb_v_init, g.nbVertices());

  auto vertexAId = g.vertexByName("A")->id();
  g.removeVertex("A");
  ASSERT_EQ(nb_v_init - 1, g.nbVertices());
  ASSERT_EQ(nb_e_init - 2, g.nbHyperedges());
  ASSERT_EQ(g.vertexByName("A"), nullptr);
  ASSERT_FALSE(g.isContainedInAnyHyperedge(g.vertexByName("Foo")->id()));
  ASSERT_FALSE(g.isContainedInAnyHyperedge(g.vertexByName("B")->id()));
  ASSERT_FALSE(g.isContainedInAnyHyperedge(g.vertexByName("Bar")->id()));
  ASSERT_EQ(g.nbHyperedges(g.vertexByName("")->id()), (size_t) 1);
  // check property entry
  ASSERT_EQ(g.getVertexProperties(vertexAId), nullptr);
  // Check if exception is thrown if we retry to delete the vertex
  ASSERT_THROW(g.removeVertex("A"), std::invalid_argument);

  // remove edge3 in two steps: first remove vertex '6', second remove ''
  // -> edges are only removed if they have neither impliedVertices nor
  // impliedVertices
  hglib::HyperedgeIdType edge3Id(2);
  auto vertex6Id = g.vertexByName("6")->id();
  g.removeVertex("6", false);
  ASSERT_EQ(g.vertexByName("6"), nullptr);
  ASSERT_NE(g.hyperedgeById(edge3Id), nullptr);
  ASSERT_TRUE(g.isContainedInAnyHyperedge(g.vertexByName("")->id()));
  ASSERT_FALSE(g.isContainedInAnyHyperedge(g.vertexByName("B")->id()));
  ASSERT_FALSE(g.isContainedInAnyHyperedge(g.vertexByName("Foo")->id()));
  ASSERT_FALSE(g.isContainedInAnyHyperedge(g.vertexByName("Bar")->id()));
  // check property entry
  ASSERT_EQ(g.getVertexProperties(vertex6Id), nullptr);
  // Check if exception is thrown if we retry to delete the vertex
  ASSERT_THROW(g.removeVertex("6"), std::invalid_argument);

  // remove vertex ''
  g.removeVertex("", false);
  ASSERT_EQ(g.vertexByName(""), nullptr);
  ASSERT_EQ(g.hyperedgeById(edge3Id), nullptr);

  // Vertices "B", "Foo", "Bar" still exists in the hypergraph...
  ASSERT_EQ(g.nbVertices(), nb_v_init - 3);
  // ...but they are isolated
  ASSERT_EQ(g.nbHyperedges(), (size_t) 0);

  // Check that the remaining vertices still have a property entry
  for (const auto& vertex : g.vertices()) {
    const auto& prop = g.getVertexProperties(vertex->id());
    ASSERT_NE(prop, nullptr);
    ASSERT_EQ(prop->name.compare("Default"), 0);
    ASSERT_EQ(prop->colour, Colour::green);
  }
}

TEST_F(TestUndirectedHyperGraph, Test_removeHyperedge) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      std::string name = "Default";
      double weight = 0.0;
  };

  typedef hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge, VertexProperty,
          HyperarcProperty> hypergraph_t;
  hypergraph_t g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }
  size_t nb_v_init = g.nbVertices();

  for (const auto& hyperedge : g.hyperedges()) {
    auto hyperedgeId = hyperedge->id();
    if (hyperedge->name().compare("edge1") == 0) {
      size_t nb_e_init = g.nbHyperedges();
      size_t nb_out_init = g.nbHyperedges(g.vertexByName("A")->id());
      size_t nb_in_init = g.nbHyperedges(g.vertexByName("Foo")->id());
      g.removeHyperedge(hyperedgeId);
      ASSERT_EQ(g.nbHyperedges(), nb_e_init - 1);
      ASSERT_EQ(g.nbHyperedges(g.vertexByName("A")->id()), nb_out_init - 1);
      ASSERT_EQ(g.nbHyperedges(g.vertexByName("Foo")->id()), nb_in_init - 1);
    } else if (hyperedge->name().compare("edge2") == 0) {
      size_t nb_e_init = g.nbHyperedges();
      size_t nb_out_A_init = g.nbHyperedges(g.vertexByName("A")->id());
      size_t nb_out_B_init = g.nbHyperedges(g.vertexByName("B")->id());
      size_t nb_in_Foo_init = g.nbHyperedges(g.vertexByName("Bar")->id());
      size_t nb_in__init = g.nbHyperedges(g.vertexByName("")->id());
      g.removeHyperedge(hyperedgeId);
      ASSERT_EQ(g.nbHyperedges(), nb_e_init - 1);
      ASSERT_EQ(g.nbHyperedges(g.vertexByName("A")->id()), nb_out_A_init - 1);
      ASSERT_EQ(g.nbHyperedges(g.vertexByName("B")->id()), nb_out_B_init - 1);
      ASSERT_EQ(g.nbHyperedges(g.vertexByName("Bar")->id()),
                nb_in_Foo_init - 1);
      ASSERT_EQ(g.nbHyperedges(g.vertexByName("")->id()), nb_in__init - 1);
    } else if (hyperedge->name().compare("edge3") == 0) {
      size_t nb_e_init = g.nbHyperedges();
      size_t nb_out_init = g.nbHyperedges(g.vertexByName("6")->id());
      size_t nb_in_init = g.nbHyperedges(g.vertexByName("")->id());
      g.removeHyperedge(hyperedgeId);
      ASSERT_EQ(g.nbHyperedges(), nb_e_init - 1);
      ASSERT_EQ(g.nbHyperedges(g.vertexByName("6")->id()), nb_out_init - 1);
      ASSERT_EQ(g.nbHyperedges(g.vertexByName("")->id()), nb_in_init - 1);
    }
    // Verify if respective property was deleted
    ASSERT_EQ(g.getHyperedgeProperties(hyperedgeId), nullptr);
  }
  ASSERT_EQ(nb_v_init, g.nbVertices());
  ASSERT_EQ((size_t) 0, g.nbHyperedges());
  ASSERT_EQ(g.hyperedgesBegin(), g.hyperedgesEnd());
  hypergraph_t::hyperedge_iterator it, end;
  std::tie(it, end) = g.hyperedgesBeginEnd();
  ASSERT_EQ(it, end);
  ASSERT_EQ(it, g.hyperedgesBegin());
  ASSERT_EQ(end, g.hyperedgesEnd());
  for (const auto& vertex : g.vertices()) {
    ASSERT_EQ(g.nbHyperedges(vertex->id()), (size_t) 0);
  }
}

TEST_F(TestUndirectedHyperGraph, Test_ResetVertexIds) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, HyperarcProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  // change default property values
  for (const auto& vertex : g.vertices()) {
    auto vProp = g.getVertexProperties_(vertex->id());
    vProp->name = vertex->name() + "_" + std::to_string(vertex->id());
  }
  // reset after no vertex was deleted
  g.resetVertexIds();
  // Verify that nothing has changed
  ASSERT_EQ(g.nbVertices(), vertices.size());
  for (const auto& vertex : g.vertices()) {
    auto vProp = g.getVertexProperties_(vertex->id());
    ASSERT_EQ(vProp->name.compare(
            vertex->name() + "_" + std::to_string(vertex->id())), 0);
  }

  // remove a section of vertices at the begin of the vector
  g.removeVertex("A");  // with Id = 0
  g.removeVertex("B");  // with Id = 1
  g.resetVertexIds();

  // Verify that Ids shifted by 2
  ASSERT_EQ(g.vertexByName("Foo")->id(), (size_t) 0);
  ASSERT_EQ(g.vertexByName("Bar")->id(), (size_t) 1);
  ASSERT_EQ(g.vertexByName("")->id(), (size_t) 2);
  ASSERT_EQ(g.vertexByName("6")->id(), (size_t) 3);

  // remove a section of vertices at the end of the vector
  g.removeVertex("");  // with Id = 2
  g.removeVertex("6");  // with Id = 3
  g.resetVertexIds();
  // Verify the Ids of vertices Foo (0) and Bar (1)
  ASSERT_EQ(g.vertexByName("Foo")->id(), (size_t) 0);
  ASSERT_EQ(g.vertexByName("Bar")->id(), (size_t) 1);

  // Add some new vertices and verify correct Ids
  g.addVertex("U");
  ASSERT_EQ(g.vertexByName("U")->id(), (size_t) 2);
  g.addVertex("X");
  ASSERT_EQ(g.vertexByName("X")->id(), (size_t) 3);
  g.addVertex("Y");
  ASSERT_EQ(g.vertexByName("Y")->id(), (size_t) 4);
  g.addVertex("Z");
  ASSERT_EQ(g.vertexByName("Z")->id(), (size_t) 5);


  // remove a section of vertices in the middle of the vector
  g.removeVertex("Bar");  // with Id = 1
  g.removeVertex("U");  // with Id = 2
  g.removeVertex("Y");  // with Id = 4
  g.resetVertexIds();
  // Verify the Ids of vertices Foo (0), X (1) and Z (2)
  ASSERT_EQ(g.vertexByName("Foo")->id(), (size_t) 0);
  ASSERT_EQ(g.vertexByName("X")->id(), (size_t) 1);
  ASSERT_EQ(g.vertexByName("Z")->id(), (size_t) 2);

  // Verify that vertexMaxId_ was set correctly
  g.addVertex("U");
  ASSERT_EQ(g.vertexByName("U")->id(), (size_t) 3);

  // remove all vertices
  g.removeVertex("Foo");
  g.removeVertex("X");
  g.removeVertex("Z");
  g.removeVertex("U");
  g.resetVertexIds();
  // Verify that vertexMaxId_ was set correctly
  g.addVertex("V");
  ASSERT_EQ(g.vertexByName("V")->id(), (size_t) 0);
}

TEST_F(TestUndirectedHyperGraph, Test_ResetHyperarcIds) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, HyperarcProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  // nothing changed if no hyperedge was deleted
  g.resetHyperedgeIds();
  ASSERT_EQ(g.hyperedgeById(0)->id(), (size_t) 0);  // "edge1"
  ASSERT_EQ(g.hyperedgeById(1)->id(), (size_t) 1);  // "edge2"
  ASSERT_EQ(g.hyperedgeById(2)->id(), (size_t) 2);  // "edge3"

  // add some hyperedges
  g.addHyperedge(hglib::NAME, {{"A"}, {"B"}}, {"edge4"});
  g.addHyperedge(hglib::NAME, {{"A"}, {"B"}}, {"edge5"});
  g.addHyperedge(hglib::NAME, {{"A"}, {"B"}}, {"edge6"});
  g.addHyperedge(hglib::NAME, {{"A"}, {"B"}}, {"edge7"});

  // remove hyperedges at the begin of the vector
  g.removeHyperedge(0);  // "edge1"
  g.removeHyperedge(1);  // "edge2"
  g.resetHyperedgeIds();
  // Verify remaining Ids
  ASSERT_EQ(g.hyperedgeById(0)->name().compare("edge3"), 0);
  ASSERT_EQ(g.hyperedgeById(1)->name().compare("edge4"), 0);
  ASSERT_EQ(g.hyperedgeById(2)->name().compare("edge5"), 0);
  ASSERT_EQ(g.hyperedgeById(3)->name().compare("edge6"), 0);
  ASSERT_EQ(g.hyperedgeById(4)->name().compare("edge7"), 0);
  // Verify hyperedgeMaxId_
  g.addHyperedge(hglib::NAME, {{"A"}, {"B"}}, {"edge8"});
  ASSERT_EQ(g.hyperedgeById(5)->name().compare("edge8"), 0);

  // remove hyperedges at the end of the vector
  g.removeHyperedge(4);  // "edge7"
  g.removeHyperedge(5);  // "edge8"
  g.resetHyperedgeIds();
  // Verify remaining Ids
  ASSERT_EQ(g.hyperedgeById(0)->name().compare("edge3"), 0);
  ASSERT_EQ(g.hyperedgeById(1)->name().compare("edge4"), 0);
  ASSERT_EQ(g.hyperedgeById(2)->name().compare("edge5"), 0);
  ASSERT_EQ(g.hyperedgeById(3)->name().compare("edge6"), 0);
  // Verify hyperedgeMaxId_
  g.addHyperedge(hglib::NAME, {{"A"}, {"B"}}, {"edge9"});
  ASSERT_EQ(g.hyperedgeById(4)->name().compare("edge9"), 0);
  g.addHyperedge(hglib::NAME, {{"A"}, {"B"}}, {"edge10"});
  ASSERT_EQ(g.hyperedgeById(5)->name().compare("edge10"), 0);

  // remove hyperedges in the middle of the vector
  g.removeHyperedge(1);  // "edge4"
  g.removeHyperedge(2);  // "edge5"
  g.removeHyperedge(4);  // "edge9"
  g.resetHyperedgeIds();
  // Verify remaining Ids
  ASSERT_EQ(g.hyperedgeById(0)->name().compare("edge3"), 0);
  ASSERT_EQ(g.hyperedgeById(1)->name().compare("edge6"), 0);
  ASSERT_EQ(g.hyperedgeById(2)->name().compare("edge10"), 0);
  // Verify hyperedgeMaxId_
  g.addHyperedge(hglib::NAME, {{"A"}, {"B"}}, {"edge11"});
  ASSERT_EQ(g.hyperedgeById(3)->name().compare("edge11"), 0);
}

TEST_F(TestUndirectedHyperGraph, Test_ResetIds) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  g.removeVertex("Bar");
  g.resetIds();

  // check vertex ids
  hglib::VertexIdType vertexId(0);
  for (const auto& vertex : g.vertices()) {
    ASSERT_EQ(vertexId++, vertex->id());
  }
  // check hyperedge ids
  hglib::HyperedgeIdType hyperedgeId(0);
  for (const auto& hyperedge : g.hyperedges()) {
    ASSERT_EQ(hyperedgeId++, hyperedge->id());
  }

  // check if vertexMaxId_ and hyperedgeMaxId_ were set correctly
  size_t expectedVertexId = g.nbVertices();
  size_t expectedHyperarcId = g.nbHyperedges();
  // should add two vertices and one hyperedge
  g.addVertex("X");
  g.addVertex("Y");
  g.addHyperedge(hglib::NAME, {{"X"}, {"Y"}}, {"edge4"});
  const auto& newHyperarc = g.hyperedgeById(
          (hglib::HyperedgeIdType) expectedHyperarcId);
  ASSERT_NE(newHyperarc, nullptr);
  ASSERT_EQ(newHyperarc->id(), (hglib::HyperedgeIdType) expectedHyperarcId);
  const auto& vertex_X = g.vertexByName("X");
  const auto& vertex_Y = g.vertexByName("Y");
  ASSERT_NE(vertex_X, nullptr);
  ASSERT_NE(vertex_Y, nullptr);
  ASSERT_EQ(vertex_X->id(), (hglib::VertexIdType) expectedVertexId);
  ASSERT_EQ(vertex_Y->id(), (hglib::VertexIdType) expectedVertexId + 1);
}


TEST_F(TestUndirectedHyperGraph, Test_print) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  // Print isolated vertices
  std::ostringstream output;
  output << g;
  std::string observedString(output.str());
  std::string expectedString = "A\nB\nFoo\nBar\n\n6\n";
  ASSERT_EQ(observedString, expectedString);

  // Add hyperedges
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  // clear output
  output.str("");
  output.clear();
  // Print hypergraph
  output << g;
  observedString = output.str();
  expectedString = "edge1: {A, Foo}\n"
          "edge2: {A, B, Bar, }\n"
          "edge3: {6, }\n";
  ASSERT_EQ(observedString, expectedString);

  // The same with unnamed hyperedges
  hglib::UndirectedHypergraph<> g2;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
  }
  // Add hyperedges
  for (auto hyperedge : hyperedges) {
    g2.addHyperedge(hglib::NAME, hyperedge.first);
  }

  // clear output
  output.str("");
  output.clear();
  // Print hypergraph
  output << g2;
  observedString = output.str();
  expectedString = "{A, Foo}\n"
          "{A, B, Bar, }\n"
          "{6, }\n";
  ASSERT_EQ(observedString, expectedString);
}

TEST_F(TestUndirectedHyperGraph, Test_argumentsToCreateVertexHyperarc) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g, g2;
  hglib::UndirectedHypergraph<> g3;
  hglib::UndirectedHypergraph<> g4;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    g3.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
    g3.addHyperedge(hglib::NAME, hyperedge.first);
  }

  for (const auto& vertex : g.vertices()) {
    auto args = g.argumentsToCreateVertex(vertex->id());
    const auto vertex2 = g2.addVertex(args.name(), args.specificAttributes());
    ASSERT_EQ(vertex->name().compare(vertex2->name()), 0);
  }

  for (const auto& hyperedge : g.hyperedges()) {
    auto args = g.argumentsToCreateHyperedge(hyperedge->id());
    const auto& hyerparc2 = g2.addHyperedge(args.verticesIds(),
                                           args.specificAttributes());
    ASSERT_EQ(hyperedge->name().compare(hyerparc2->name()), 0);
    ASSERT_EQ(g.nbImpliedVertices(hyperedge->id()),
              g2.nbImpliedVertices(hyerparc2->id()));
    auto it = g.impliedVerticesBegin(hyperedge->id());
    auto end = g.impliedVerticesEnd(hyperedge->id());
    for (; it != end; ++it) {
      ASSERT_NE(g2.vertexByName((*it)->name()), nullptr);
    }
    ASSERT_EQ(g.nbImpliedVertices(hyperedge->id()),
              g2.nbImpliedVertices(hyerparc2->id()));
  }

  for (const auto& vertex : g3.vertices()) {
    auto args = g3.argumentsToCreateVertex(vertex->id());
    const auto vertex4 = g4.addVertex(args.name(), args.specificAttributes());
    ASSERT_EQ(vertex->name().compare(vertex4->name()), 0);
  }

  for (const auto& hyperedge : g3.hyperedges()) {
    auto args = g3.argumentsToCreateHyperedge(hyperedge->id());
    const auto& hyerparc4 = g4.addHyperedge(args.verticesIds(),
                                           args.specificAttributes());
    ASSERT_EQ(g3.nbImpliedVertices(hyperedge->id()),
              g4.nbImpliedVertices(hyerparc4->id()));
    auto it = g3.impliedVerticesBegin(hyperedge->id());
    auto end = g3.impliedVerticesEnd(hyperedge->id());
    for (; it != end; ++it) {
      ASSERT_NE(g4.vertexByName((*it)->name()), nullptr);
    }
    ASSERT_EQ(g3.nbImpliedVertices(hyperedge->id()),
              g4.nbImpliedVertices(hyerparc4->id()));
  }
}
