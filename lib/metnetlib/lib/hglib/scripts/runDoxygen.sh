#!/bin/bash

# Run doxygen on the specified input file. The doxygen logfile is
# a parameter inside the doxygen input file. We check if the logfile
# is empty. In this case we remove the logfile. Otherwise, the location
# of the logfile is printed, and the script exit with an error code 1.
# Argument 1: doxygen input file
# Argument 2: doxygen logfile
# Return: 1 if there is an error, 0 otherwise

# run doxygen
doxygen $1
# check logfile
if grep -q "error" $2; then
    printf "\nCheck the doxygen logfile $2 for errors.\n\n";
    exit 1
fi;
