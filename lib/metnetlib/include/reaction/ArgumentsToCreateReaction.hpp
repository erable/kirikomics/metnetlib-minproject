/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 04.12.17.
//

#include "ArgumentsToCreateReaction.h"

namespace metnetlib {
/// Constructor
template<typename Reaction_t>
ArgumentsToCreateReaction<Reaction_t>::
ArgumentsToCreateReaction(const SubstrateAndProductNames &subtrateProductNames,
                          const typename Reaction_t::SpecificAttributes &attr) :
    substrateAndProductNames_(subtrateProductNames),
    specificAttributes_(attr) {
}

/// Get substrate and product names
template<typename Reaction_t>
const typename ArgumentsToCreateReaction<
    Reaction_t>::SubstrateAndProductNames& ArgumentsToCreateReaction<
    Reaction_t>::
substrateAndProductNames() const {
  return substrateAndProductNames_;
}


/// Get specific arguments
template<typename Reaction_t>
const typename Reaction_t::SpecificAttributes &ArgumentsToCreateReaction<
    Reaction_t>::
specificAttributes() const {
  return specificAttributes_;
}


/// Set substrate and product names
template<typename Reaction_t>
void ArgumentsToCreateReaction<Reaction_t>::
setSubstrateAndProductNames(
    const SubstrateAndProductNames &substrateProductNames) {
  substrateAndProductNames_ = substrateProductNames;
}


/// Set arguments that are specific to the reaction type (Reaction_t)
template<typename Reaction_t>
void ArgumentsToCreateReaction<Reaction_t>::
setSpecificArguments(
    const typename Reaction_t::SpecificAttributes &attributes) {
  specificAttributes_ = attributes;
}
}  // namespace metnetlib
