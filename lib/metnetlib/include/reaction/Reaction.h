/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 07.11.17.
//

#ifndef METNET_REACTION_H
#define METNET_REACTION_H

#include <unordered_map>

#include "hglib/hyperedge/directed/ArgumentsToCreateDirectedHyperedge.h"

#include "ArgumentsToCreateReaction.h"
#include "compound/Compound.h"
#include "utils/types.h"

// Forward declaration of hglib classes to be befriended
namespace hglib {
template <template <typename> class Compound_t,
                              typename Reac_t,
                              typename CompoundProperty,
                              typename ReactionProperty,
                              typename MetNetProperty>
class Hypergraph;

template <template <typename> class Compound_t,
                              typename Reac_t,
                              typename CompoundProperty,
                              typename ReactionProperty,
                              typename MetNetProperty>
class DirectedHypergraph;

}  // namespace hglib

namespace metnetlib {

class Reaction final : public hglib::DirectedHyperedgeBase<Compound<Reaction>> {
  // Hypergraph is a friend class
  template <template <typename> class Compound_t, typename Reac_t,
      typename CompoundProperty, typename ReactionProperty,
      typename MetNetProperty>
  friend class hglib::Hypergraph;
  // DirectedHypergraph is a friend class
  template <template <typename> class Compound_t, typename Reac_t,
      typename CompoundProperty, typename ReactionProperty,
      typename MetNetProperty>
  friend class hglib::DirectedHypergraph;
  // MetabolicNetwork is a friend class
  template <template <typename> class Compound_t, typename Reac_t,
    typename CompoundProperty, typename ReactionProperty,
    typename MetNetProperty>
  friend class MetabolicNetwork;
  // SubMetabolicNetwork is a friend class
  template <template <typename> class Compound_t, typename Reac_t,
    typename CompoundProperty, typename ReactionProperty,
    typename MetNetProperty>
  friend class SubMetabolicNetwork;

 public:
  /// Alias for compound type
  using Compound_t = Compound<Reaction>;
  /**
   * \brief Alias for substrate or product stoichiometries
   *
   * \details
   * In the SBML format a stoichiometric value is assigned to every compound
   * that takes part in a reaction. It is also specified whether this
   * stoichiometric value is constant or not. These information are expressed
   * through a pair of double and bool. We further put all substrate (product)
   * stoichiometries into a vector, where the order corresponds to the order
   * of compounds in the container of substrates (products).
   */
  using Stoichiometry = std::vector<std::pair<double, bool>>;

  /* **************************************************************************
   * **************************************************************************
   *                        Nested class SpecificAttributes
   * **************************************************************************
   ***************************************************************************/
  /**
   * SpecificAttributes class
   *
   * A basic reaction has no specific arguments, but the class is needed to
   * call the addReaction function of a (Sub)MetabolicNetwork.
   */
  class SpecificAttributes {
    /* ************************************************************************
     * ************************************************************************
     *                        SpecificAttributes Constructor
     * ************************************************************************
     *************************************************************************/
   public:
    /// Constructor
    SpecificAttributes(const std::string& sbmlId, bool reversible, bool fast,
                       const Stoichiometry& substrateStoichiometry,
                       const Stoichiometry& productStoichiometry,
                       double lowerBound, double upperBound) :
        sbmlId_(sbmlId), reversible_(reversible), fast_(fast),
        substrateStoichiometry_(substrateStoichiometry),
        productStoichiometry_(productStoichiometry), lowerBound_(lowerBound),
        upperBound_(upperBound) {
    }
    /// Copy constructor
    SpecificAttributes(const SpecificAttributes& other) :
        sbmlId_(other.sbmlId_), reversible_(other.reversible_),
        fast_(other.fast_),
        substrateStoichiometry_(other.substrateStoichiometry_),
        productStoichiometry_(other.productStoichiometry_),
        lowerBound_(other.lowerBound_), upperBound_(other.upperBound_) {
    }
    /* ************************************************************************
     * ************************************************************************
     *                        SpecificAttributes member functions
     * ************************************************************************
     *************************************************************************/
   public:
    /// Get all substrate stoichiometries
    const Stoichiometry& substrateStoichiometries() const {
      return substrateStoichiometry_;
    }
    /// Get all product stoichiometries
    const Stoichiometry& productStoichiometries() const {
      return productStoichiometry_;
    }

    /* ************************************************************************
     * ************************************************************************
     *                        SpecificAttributes member functions
     * ************************************************************************
     *************************************************************************/
   public:
    /// Sbml Id
    std::string sbmlId_;
    /// Indicate whether the reaction is reversible
    bool reversible_;
    /// Indicate whether the reaction is fast
    bool fast_;
    /// substrate stoichiometry and flag whether
    Stoichiometry substrateStoichiometry_;
    /// product stoichiometry
    Stoichiometry productStoichiometry_;
    /// Lower bound
    double lowerBound_;
    /// Upper bound
    double upperBound_;
    /// Reversible reaction
    Reaction* reversibleReaction = nullptr;
  };

  /* **************************************************************************
   * **************************************************************************
   *                        Constructor/Destructor (Rule of five)
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Constructor
  Reaction(ReactionIdType,
           std::shared_ptr<MetNetElementContainer<Compound_t*>>,
           std::shared_ptr<MetNetElementContainer<Compound_t*>>,
           const SpecificAttributes&);
  /// Default constructor
  Reaction() = delete;
  /// Destructor
  ~Reaction() = default;
  /// Copy constructor
  Reaction(const Reaction&);
  /// Move constructor
  Reaction(Reaction&&) = default;
  /// Copy assignment
  Reaction& operator=(const Reaction&) = default;
  /// Move assignment
  Reaction& operator=(Reaction&&) = default;

  /* **************************************************************************
   * **************************************************************************
   *                        Member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Get the Sbml Id
  const std::string& sbmlId() const;
  /// Is the reaction fast?
  bool isFast() const;
  /// Get lower bound of the reaction
  double lowerBound() const;
  /// Get upper bound of the reaction
  double upperBound() const;
  /// Print the reaction
  void print(std::ostream& out) const;

 protected:
  /// Get arguments to create this reaction
  hglib::ArgumentsToCreateDirectedHyperedge<Reaction>
    argumentsToCreateHyperarc() const;
  /// Get arguments to create the reverse sense of this reaction
  hglib::ArgumentsToCreateDirectedHyperedge<Reaction>
    argumentsToCreateReverseReaction() const;
  /// Set reversible reaction
  void setReversibleReaction(Reaction*);
  /// Get reverse reaction
  const Reaction* reversibleReaction() const;
  /// Is the reaction reversible?
  bool isReversible() const;
  /// Set the reaction irreversible
  void setIrreversible();
  /// Check if this reaction (if reversible) is the backward reaction
  bool isBackwardReaction() const;
  /// Get substrate stoichiometry of given substrate
  double substrateStoichiometry(const Compound_t*) const;
  /// Get product stoichiometry of given product
  double productStoichiometry(const Compound_t*) const;

  /// Provides default arguments to create an irreversible reaction
  static ArgumentsToCreateReaction<Reaction>
  argumentsToCreateDefaultIrreversibleReaction(
      const std::string& reactionName,
      const std::vector<std::string>& substrateNames,
      const std::vector<std::string>& productNames);

  /* **************************************************************************
   * **************************************************************************
   *                        Member variables
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Specific attributes
  SpecificAttributes specificAttributes_;
};

inline std::ostream& operator<< (std::ostream& out, const Reaction& reaction) {
  reaction.print(out);
  return out;
}

}  // namespace metnetlib

#endif  // METNET_REACTION_H
