/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 18.12.17.
//

#include "SbmlFileWriter.h"

#include <iostream>
#include <fstream>

namespace metnetlib {

template <typename MetabolicNetwork_t>
void SbmlFileWriter<MetabolicNetwork_t>::
writeMetabolicNetworkToFile(const MetabolicNetwork_t& network,
                            const char* filename) {
  std::ofstream outFile(filename);
  if (outFile.is_open()) {
    writeHeader(outFile);
    writeListOfCompartments(outFile, network);
    writeListOfCompounds(outFile, network);
    writeListOfReactions(outFile, network);
    writeTail(outFile);
    outFile.close();
  } else {
    std::cout << "Could not open file " << filename << '\n';
  }
}


/// Write XML header
template <typename MetabolicNetwork_t>
void SbmlFileWriter<MetabolicNetwork_t>::
writeHeader(std::ofstream& file) {
  file << "<?xml version=\"1.0\"  encoding=\"UTF-8\"?>\n";
  file << "<sbml xmlns=\"http://www.sbml.org/sbml/level2\" version=\"1\""
      " level=\"2\" xmlns:html=\"http://www.w3.org/1999/xhtml\">\n";
  file << "<model id=\"automatically_generated\" name=\"\">\n";
}


/// Write list of compartments
template <typename MetabolicNetwork_t>
void SbmlFileWriter<MetabolicNetwork_t>::
writeListOfCompartments(std::ofstream& file,
                        const MetabolicNetwork_t& network) {
  file << "<listOfCompartments>\n";
  // Print each compartment
  for (const auto& compartment : network.compartments()) {
    file << "\t<compartment id=\"" << compartment->sbmlId()
         << "\" size=\"" << compartment->size() << "\" constant=\"";
    file << (compartment->isConstant() ? "true" : "false");
    file << "\"/>\n";
  }
  file << "</listOfCompartments>\n";
}



/// Write list of compounds
template <typename MetabolicNetwork_t>
void SbmlFileWriter<MetabolicNetwork_t>::
writeListOfCompounds(std::ofstream& file, const MetabolicNetwork_t& network) {
  file << "<listOfSpecies>\n";
  // Print each compound
  for (const auto& compound : network.compounds()) {
    file << "\t<species id=\"" << compound->sbmlId()
         << "\" name=\"" << compound->sbmlId()
         << "\" compartment=\"" << compound->getCompartment()->sbmlId()
         << "\" hasOnlySubstanceUnits=\"";
    file << (compound->hasOnlySubstanceUnits() ? "true" : "false");
    file << "\" boundaryCondition=\"";
    file << (compound->hasBoundaryCondition() ? "true" : "false");
    file << "\" constant=\"";
    file << (compound->isConstant() ? "true" : "false");
    file << "\"/>\n";
  }
  file << "</listOfSpecies>\n";
}


/// Write list of reactions
template <typename MetabolicNetwork_t>
void SbmlFileWriter<MetabolicNetwork_t>::
writeListOfReactions(std::ofstream& file, const MetabolicNetwork_t& network) {
  file << "<listOfReactions>\n";
  for (const auto& reaction : network.reactions()) {
    bool reversible = network.isReversible(reaction->id());
    if (reversible) {
      const auto& sbmlId = reaction->sbmlId();
      if (sbmlId.length() >= suffixReverseReactionLength and
          0 == sbmlId.compare(sbmlId.length() - suffixReverseReactionLength,
                              suffixReverseReactionLength,
                              suffixReverseReaction)) {
        continue;
      }
    }
    // Print reaction
    file << "\t<reaction id=\"" << reaction->sbmlId()
         << "\" name=\"" << reaction->sbmlId() << "\" reversible=\"";
    file << (reversible ? "true" : "false");
    file << "\" fast=\"";
    file << (reaction->isFast() ? "true" : "false");
    file << "\">\n";
    // Print substrates
    if (network.hasSubstrates(reaction->id())) {
      file << "\t\t<listOfReactants>\n";
      auto it = network.substratesBegin(reaction->id());
      auto end = network.substratesEnd(reaction->id());
      for (; it != end; ++it) {
        file << "\t\t\t<speciesReference species=\"" << (*it)->sbmlId()
             << "\" stoichiometry=\""
             << network.substrateStoichiometry((*it)->id(), reaction->id())
             << "\"/>\n";
      }
      file << "\t\t</listOfReactants>\n";
    }
    // Print products
    if (network.hasProducts(reaction->id())) {
      file << "\t\t<listOfProducts>\n";
      auto it = network.productsBegin(reaction->id());
      auto end = network.productsEnd(reaction->id());
      for (; it != end; ++it) {
        file << "\t\t\t<speciesReference species=\"" << (*it)->sbmlId()
             << "\" stoichiometry=\""
             << network.productStoichiometry((*it)->id(), reaction->id())
             << "\"/>\n";
      }
      file << "\t\t</listOfProducts>\n";
    }
    // Kinetic law for lower and upper bound
    file << "\t\t<kineticLaw>\n";
    file << "\t\t\t<listOfParameters>\n";
    file << "\t\t\t\t<parameter id=\"LOWER_BOUND\" value=\"";
    // Get lower bound
    double lb(reaction->lowerBound());
    const auto& reversibleReaction(network.reversibleReaction(reaction->id()));
    // If the reaction is reversible and because we split reversible reactions,
    // the lower bound is the negative of the upper bound of the reverse
    // reaction
    if (reversibleReaction != nullptr) {
      lb = - reversibleReaction->upperBound();
    }
    file << lb << "\" ";
    file << "units=\"mmol_per_gDW_per_hr\"/>\n";
    file << "\t\t\t\t<parameter id=\"UPPER_BOUND\" value=\""
         << reaction->upperBound();
    file << "\" units=\"mmol_per_gDW_per_hr\"/>\n";
    file << "\t\t\t</listOfParameters>\n";
    file << "\t\t</kineticLaw>\n";
    file << "\t</reaction>\n";
  }
  file << "</listOfReactions>\n";
}


/// Write XML tail
template <typename MetabolicNetwork_t>
void SbmlFileWriter<MetabolicNetwork_t>::
writeTail(std::ofstream& file) {
  file << "</model>\n";
  file << "</sbml>\n";
}
}  // namespace metnetlib
