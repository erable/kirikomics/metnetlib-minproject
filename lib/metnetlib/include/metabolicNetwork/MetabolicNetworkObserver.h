/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 17.11.17.
//

#ifndef HGLIB_METABOLICNETWORKOBSERVER_H
#define HGLIB_METABOLICNETWORKOBSERVER_H

#include "MetabolicNetworkObservableEvents.h"

namespace metnetlib {

class MetabolicNetworkObservable;

class MetabolicNetworkObserver {
 public :
  // ==========================================================================
  //                               Constructors
  // ==========================================================================
  /// Default ctor
  MetabolicNetworkObserver() = default;
  /// Copy ctor
  MetabolicNetworkObserver(const MetabolicNetworkObserver&) = delete;
  /// Move ctor
  MetabolicNetworkObserver(MetabolicNetworkObserver&&) = delete;

  // ==========================================================================
  //                                Destructor
  // ==========================================================================
  /// Destructor
  virtual ~MetabolicNetworkObserver() = default;

  // ==========================================================================
  //                                Operators
  // ==========================================================================
  /// Copy assignment
  MetabolicNetworkObserver& operator=(
      const MetabolicNetworkObserver& other) = delete;
  /// Move assignment
  MetabolicNetworkObserver& operator=(
      const MetabolicNetworkObserver&& other) = delete;

  // ==========================================================================
  //                              Public Methods
  // ==========================================================================
  /// This method is called whenever the observed object is changed.
  virtual void Update(const MetabolicNetworkObservable& o,
                      MetabolicNetworkObservableEvent e, void* arg) = 0;

  // ==========================================================================
  //                                 Getters
  // ==========================================================================

  // ==========================================================================
  //                                 Setters
  // ==========================================================================

 protected :
  // ==========================================================================
  //                            Protected Methods
  // ==========================================================================

  // ==========================================================================
  //                               Attributes
  // ==========================================================================
};

}  // namespace metnetlib

#endif  // HGLIB_METABOLICNETWORKOBSERVER_H
