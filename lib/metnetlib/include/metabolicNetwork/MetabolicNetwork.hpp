/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 07.11.17.
//

#include "MetabolicNetwork.h"
#include "SubMetabolicNetwork.h"

#include <string>
#include <regex>

#include "reaction/ArgumentsToCreateReaction.h"

namespace metnetlib {

/// Default constructor
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
MetabolicNetwork() : compartmentMaxId_(-1) {
  directedHypergraph_ = new DirectedHypergraph_t(true);
  compartments_ = create_filtered_vector<Compartment*>(
      create_filtered_vector<Compartment*>(),
      std::make_unique<NullptrFilter<Compartment*>>());
}


/// Copy constructor
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
MetabolicNetwork(const MetabolicNetwork& other) :
    MetabolicNetworkInterface<CompoundTemplate_t, Reaction_type,
        CompoundProperty, ReactionProperty, MetabolicNetworkProperty>(),
    compartmentMaxId_(-1) {
  try {
    // Copy underlying directed hypergraph
    directedHypergraph_ = new DirectedHypergraph_t(
        *(other.directedHypergraph_));

    // Assign reversible reactions
    for (const auto& reaction : other.reactions()) {
      if (other.isReversible(reaction->id())) {
        // get reversible reaction in other network
        const auto revReaction = other.reversibleReaction(reaction->id());
        // Get pointers to forward and backward reaction in this network
        auto nonConstForwardReaction = const_cast<Reaction_type*>(
            reactionById(reaction->id()));
        auto nonConstBackwardReaction = const_cast<Reaction_type*>(
            reactionById(revReaction->id()));
        // Set reversible reaction of forward reaction in this network
        nonConstForwardReaction->setReversibleReaction(
            nonConstBackwardReaction);
      }
    }

    // create compartments_
    compartments_ = create_filtered_vector<Compartment*>(
        create_filtered_vector<Compartment*>(),
        std::make_unique<NullptrFilter<Compartment*>>());
    // copy compartments
    for (std::size_t i = 0; i < other.compartments_->size(); ++i) {
      Compartment* compartment = other.compartments_->operator[](i);
      ++compartmentMaxId_;
      // compartment was deleted in other network
      if (compartment == nullptr) {
        compartments_->push_back(nullptr);
      } else {
        Compartment* copy = new Compartment(*compartment);
        // add compartment copy to hypergraph
        compartments_->push_back(copy);
      }
    }

    // Update compartment pointers of compounds
    for (const auto& compound : compounds()) {
      const auto& compartment = compound->getCompartment();
      const auto& copiedCompartment = compartmentById(compartment->id());
      compound->setCompartment(copiedCompartment);
    }
  } catch (std::bad_alloc& e) {
    for (auto& compartment : *compartments_) {
      delete compartment;
    }
    throw;
  }
}


/// Destructor
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
~MetabolicNetwork() {
  for (auto& compartment : *(compartments_.get())) {
    delete compartment;
  }
  delete directedHypergraph_;
}


/// Copy assignment
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>& MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
operator=(const MetabolicNetwork& source) {
  // check for self assignment
  if (this != &source) {
    // Set the parent of all childs in the sub-network-hierarchy to null
    std::unordered_set<SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
        CompoundProperty, ReactionProperty,
        MetabolicNetworkProperty>*> observers;
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver : itEvent->second) {
        auto subNetwork = dynamic_cast<SubMetabolicNetwork<CompoundTemplate_t,
            Reaction_type, CompoundProperty, ReactionProperty,
            MetabolicNetworkProperty>*>(itObserver);
        observers.insert(subNetwork);
      }
    }
    for (auto& subNetwork : observers) {
      subNetwork->setParentOfChildsToNull();
    }

    // deallocate compartments_
    for (auto& compartments : *compartments_) {
      delete compartments;
    }
    compartments_->clear();

    // re-init the compartments max Id
    compartmentMaxId_ = -1;

    // Release and reset directed hypergraph
    delete directedHypergraph_;

    try {
      // Copy underlying directed hypergraph
      directedHypergraph_ = new DirectedHypergraph_t(
          *(source.directedHypergraph_));

      // Assign reversible reactions
      for (const auto& reaction : source.reactions()) {
        if (source.isReversible(reaction->id())) {
          // get reversible reaction in other network
          const auto revReaction = source.reversibleReaction(reaction->id());
          // Get pointers to forward and backward reaction in this network
          auto nonConstForwardReaction = const_cast<Reaction_type*>(
              reactionById(reaction->id()));
          auto nonConstBackwardReaction = const_cast<Reaction_type*>(
              reactionById(revReaction->id()));
          // Set reversible reaction of forward reaction in this network
          nonConstForwardReaction->setReversibleReaction(
              nonConstBackwardReaction);
        }
      }

      // copy compartments
      for (std::size_t i = 0; i < source.compartments_->size(); ++i) {
        Compartment* compartment = source.compartments_->operator[](i);
        ++compartmentMaxId_;
        // compartment was deleted in other network
        if (compartment == nullptr) {
          compartments_->push_back(nullptr);
        } else {
          Compartment* copy = new Compartment(*compartment);
          // add compartment copy to hypergraph
          compartments_->push_back(copy);
        }
      }

      // Update compartment pointers of compounds
      for (const auto& compound : compounds()) {
        const auto& compartment = compound->getCompartment();
        const auto& copiedCompartment = compartmentById(compartment->id());
        compound->setCompartment(copiedCompartment);
      }
    } catch (std::bad_alloc& e) {
      for (auto& compartment : *compartments_) {
        delete compartment;
      }
      throw;
    }
  }
  return *this;
}

/* **************************************************************************
 * **************************************************************************
 *                        Member functions
 * **************************************************************************
 ***************************************************************************/
// related to compartment
/**
 * Add a compartment
 *
 * Add a compartment if:
 * * the SbmlId follows the SBML specifications. The Id must match the regular
 *   expression [a-zA-Z_][a-zA-Z0-9_]*
 * * the size is not negative.
 *
 * @param sbmlId SBML Id of the compartment
 * @param size Size of the compartment
 * @param constant Flag that indicates whether the compartment is constant over
 * time
 * @return On success a pointer to the created compartment is returned,
 * otherwise an invalid_argument exception is thrown
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const Compartment* MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
addCompartment(const std::string& sbmlId, double size, bool constant) {
  // Check that the size of the compartment is >= 0
  // Check if sbmlId follows the SBML specification
  // Check that there is no compartment with the same Sbml Id in the network
  // Create Compartment and add it to the network
  // Return pointer to Compartment

  // Check that the size of the compartment is >= 0
  if (size < 0.0) {
    std::string errorMessage = "The size of the compartment must be >= 0, "
        "you provided the value: ";
    errorMessage += std::to_string(size);
    errorMessage += " for the compartment " + sbmlId;
    throw std::invalid_argument(errorMessage);
  }
  // Check if sbmlId follows the SBML specification
  std::regex regEx("[a-zA-Z_][a-zA-Z0-9_]*");
  if (not std::regex_match(sbmlId.begin(), sbmlId.end(), regEx)) {
    std::string errorMessage = "The compartment Sbml Id ";
    errorMessage += sbmlId;
    errorMessage += " is not valid. The Sbml Id must match the following"
        " regular expression: [a-zA-Z_][a-zA-Z0-9_]*";
    throw std::invalid_argument(errorMessage);
  }

  // Check that there is no compartment with the same Sbml Id in the network
  for (const auto& compartment : *compartments_) {
    if (compartment->sbmlId().compare(sbmlId) == 0) {
      std::string errorMessage = "The compartment Sbml Id ";
      errorMessage += sbmlId;
      errorMessage += " already exists in the network";
      throw std::invalid_argument(errorMessage);
    }
  }

  // Create Compartment and add it to the network
  Compartment* comp = new Compartment(++compartmentMaxId_, sbmlId, size,
                                      constant);
  compartments_->push_back(comp);

  // Return pointer to Compartment
  return comp;
}


/**
 * Get pointer to compartment
 *
 * Get pointer to compartment if there exists a compartment with the given Sbml
 * Id.
 *
 * @param sbmlId Sbml Id of the compartment
 * @return Pointer to compartment if present in the network, nullptr otherwise
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const Compartment* MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
compartmentBySbmlId(const std::string& sbmlId) const {
  // Return pointer to compartment with given Sbml Id if found,
  // nullptr otherwise
  for (const auto& compartment : *compartments_) {
    if (compartment->sbmlId().compare(sbmlId) == 0) {
      return compartment;
    }
  }
  return nullptr;
}

/**
 * Get pointer to compartment
 *
 * Get pointer to compartment if there exists a compartment with the given
 * internal numerical Id.
 *
 * @param sbmlId Internal numerical Id of the compartment
 * @return Pointer to compartment if present in the network,
 * otherwise throw std::out_of_range
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const Compartment* MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
compartmentById(const CompartmentIdType& compartmentId) const {
  try {
    return compartments_->at(compartmentId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in MetabolicNetwork::"
        "compartmentById: Id = " << std::to_string(compartmentId)
              << "; min = 0; max = "
              << (compartments_->size() - 1) << '\n';
    throw;
  }
}


/**
 * Get number of compartments
 *
 * @return Number of compartments
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
nbCompartments() const {
  size_t nbCompartments = 0;
  auto it = compartments_->cbegin();
  auto end = compartments_->cend();
  for (; it != end; ++it) {
    ++nbCompartments;
  }
  return nbCompartments;
}


/**
 * Const compartment iterator pointing to begin of the compartments
 *
 * @return compartment_iterator
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compartment_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compartmentsBegin() const {
  return compartments_->cbegin();
}


/**
 * Const compartment iterator pointing to end of the compartments
 *
 * @return compartment_iterator
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compartment_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compartmentsEnd() const {
  return compartments_->cend();
}


/**
 * Return a reference to the container of compartments.
 *
 * @return reference to the container of compartments
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::CompartmentContainer& MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compartments() const {
  return *compartments_;
}


/**
 * Remove a compartment and all its compounds from the network
 *
 * The compartment and all its compounds are removed from the network. Reactions
 * that are entirely in the compartment are also removed. Reactions that are on
 * the boundary of the compartment (reactions with either a substrate or product
 * in another compartment) are removed if the second parameter is set to true.
 *
 * @param id Internal Id of the compartment
 * @param removeImpliedReactions Flag to signal whether reactions with a
 * compound from the given compartment Id should also be removed.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
removeCompartment(const CompartmentIdType& id, bool removeImpliedReactions) {
  // * Get pointer to compartment with specified id
  // * Remove all compounds in the compartment
  // * remove the compartment from the network
  try {
    // Get pointer to compartment with specified id
    const auto& compartment = this->compartmentById(id);
    if (compartment == nullptr) {
      std::string errorMessage("Called MetabolicNetwork::removeCompartment"
                                   " with the compartment id ");
      errorMessage += std::to_string(id);
      errorMessage += " which does not exists in the metabolic network.\n";
      throw std::invalid_argument(errorMessage);
    }

    // Remove all compounds in the compartment
    std::vector<Compound_t*> removeCompounds;
    for (const auto& compound : directedHypergraph_->vertices()) {
      if (compound->getCompartment() == compartment) {
        removeCompounds.push_back(compound);
      }
    }
    for (const auto& compound : removeCompounds) {
      directedHypergraph_->removeVertex(compound->id(), removeImpliedReactions);
    }

    CompartmentIdType idx(id);
    // Notify observers of change
    Remove_compartment_args args;
    args.compartmentId_ = idx;
    args.removeImpliedReactions = removeImpliedReactions;
    this->NotifyObservers(MetabolicNetworkObservableEvent::COMPARTMENT_REMOVED,
                          &args);

    // Remove the compartment from the network
    // CompartmentIdType idx(id);
    delete compartments_->operator[](id);
    compartments_->operator[](id) = nullptr;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in MetabolicNetwork::"
        "removeCompartment:"
        " index = " << id << "; min = 0; max = "
              << (compartments_->size() -1) << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
* Establish consecutive internal compartment ids
*
* After deleting compartments from the network, the internal compartment ids
* are not consecutive anymore. This function reassigns
* consecutive ids to the compartments.
*
* Complexity: Linear
*/
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
resetCompartmentIds() {
  // Notify observers
  this->NotifyObservers(MetabolicNetworkObservableEvent::RESET_COMPARTMENT_IDS);
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(
      compartments_->data(), compartments_->data() + compartments_->size(),
      static_cast<Compartment*>(NULL));

  // Compute nb of nullptr
  size_t nbNullptr = (compartments_->data() + compartments_->size()) -
      posFirstNullptr;
  // Resize
  compartments_->resize(compartments_->size() - nbNullptr);

  // assign new Ids
  CompartmentIdType id(-1);
  for (auto it = compartments_->begin(); it != compartments_->end(); ++it) {
    (*it)->setId(++id);  // assign new Id
  }

  // set compartmentMaxId_ to the highest assigned id
  compartmentMaxId_ = id;
}


/**
 * Highest assigned compartment id of the network
 *
 * @return
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
compartmentContainerSize() const {
  return compartments_->size();
}


// related to compounds
/**
 * \brief Add a compound with a given Sbml Id and several additional arguments
 * specific to the compound type
 *
 * A Sbml Id and specific arguments are provided. The compound is modeled as a
 * vertex in a directed hypergraph. All vertex types have in common that they
 * possess a name. A compound is a special kind of vertex and
 * all attributes of the compound other than the Sbml Id (vertex name), e.g.
 * pointer to the compartment, flags for hasOnlySubstanceUnits,
 * boundaryCondition, constant are provided as 'specific' attributes (they are
 * specific to this kind of vertex). It is mandatory that the compound type
 * (CompoundTemplate_t<Reaction_type>), has a nested class SpecificAttributes
 * (see as example class Compound).
 *
 * Check if sbmlId follows the SBML specification.
 * Check that there is no compound with the same Sbml Id in the network.
 * Check if there exists the compartment ptr (inside specificAttributes) in
 * this network.
 * If those checks are passed, a compound is added to the network
 *
 * @param sbmlId Sbml Id of the compound
 * @param specificAttributes specific arguments of the compound type
 * @return Pointer to compound if added with success, otherwise throw exception
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Compound_t* MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
addCompound(const std::string& sbmlId,
            const typename Compound_t::SpecificAttributes& specificAttributes) {
  // * Check if sbmlId follows the SBML specification
  // * Check that there is no compound with the same Sbml Id in the network
  // (done in addVertex)
  // * Check if there exists the compartment ptr (inside specificAttributes)
  // in this network
  // * Add compound to underlying directed hypergraph
  // * Return pointer to compound

  // Check if sbmlId follows the SBML specification
  std::regex regEx("[a-zA-Z_][a-zA-Z0-9_]*");
  if (not std::regex_match(sbmlId.begin(), sbmlId.end(), regEx)) {
    std::string errorMessage = "The compound Sbml Id ";
    errorMessage += sbmlId;
    errorMessage += " is not valid. The Sbml Id must match the following"
        " regular expression: [a-zA-Z_][a-zA-Z0-9_]*";
    throw std::invalid_argument(errorMessage);
  }

  // Check if there exists the compartment ptr (inside specificAttributes)
  // in this network
  try {
    if (specificAttributes.compartment_ == nullptr) {
      throw std::invalid_argument("Exception in MetabolicNetwork::addCompound:"
                                      " Provide a valid compartment pointer."
                                      " Not a nullptr!\n");
    }
    auto compartmentPtr = specificAttributes.compartment_;
    if (compartmentById(compartmentPtr->id()) != compartmentPtr) {
      throw std::invalid_argument("Exception in MetabolicNetwork::addCompound:"
                                      " Provide a valid compartment pointer"
                                      " from this metabolic network.\n");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "Exception in MetabolicNetwork::addCompound: "
        "Provide a valid compartment pointer from this network.\n";
    throw oor;
  }

  // Create compound
  try {
    const auto& compound = directedHypergraph_->addVertex(sbmlId,
                                                          specificAttributes);
    // Return pointer to compound on success
    return directedHypergraph_->vertexById(compound->id());
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument("Compound " + sbmlId +
                                " already exists in this hypergraph.\n");
  }
}


/**
 * Get pointer to compound
 *
 * Get pointer to compound with specified internal numerical Id.
 *
 * @param compoundId Internal numerical Id.
 * @return Pointer to compound if present, otherwise throw out_of_range
 * exception
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Compound_t* MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compoundById(const CompoundIdType& compoundId) const {
  try {
    return directedHypergraph_->vertexById(compoundId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in MetabolicNetwork::compoundById: "
        "index = " << compoundId << "; min = 0; max = " <<
              (directedHypergraph_->vertexContainerSize() - 1) << '\n';
    throw;
  }
}


/**
 * Get pointer to compound
 *
 * Get pointer to compound if there exists a compound with the given Sbml
 * Id.
 *
 * @param sbmlId Sbml Id of the compound
 * @return Pointer to compound if present in the network, nullptr otherwise
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Compound_t* MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compoundBySbmlId(const std::string& sbmlId) const {
  // Return pointer to compound with given Sbml Id if found,
  // nullptr otherwise
  return directedHypergraph_->vertexByName(sbmlId);
}


/**
 * Get number of compounds
 *
 * @return number of compounds
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
nbCompounds() const {
  return directedHypergraph_->nbVertices();
}


/**
 * Const compound iterator pointing to begin of the compounds
 *
 * @return Const compound iterator pointing to begin of the compounds
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compoundsBegin() const {
  return directedHypergraph_->verticesBegin();
}


/**
 * Const compound iterator pointing to end of the compounds
 *
 * @return Const compound iterator pointing to end of the compounds
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compoundsEnd() const {
  return directedHypergraph_->verticesEnd();
}


/**
 * Return a reference to the container of compounds.
 *
 * @return Return a reference to the container of compounds.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::CompoundContainer& MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compounds() const {
  return directedHypergraph_->vertices();
}


/**
 * Get arguments to create the compound
 *
 * @param compoundId Id of the compound
 * @return Arguments to add the compound to a metabolic network
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
hglib::ArgumentsToCreateVertex<typename MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Compound_t> MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
argumentsToCreateCompound(const CompoundIdType& compoundId) const {
  return directedHypergraph_->argumentsToCreateVertex(compoundId);
}


/**
 * Remove a compound with the given Id
 *
 * @param id Internal Id of a compound
 * @param removeImpliedReactions Flag to signal whether reactions with the
 * given compound id should also be removed.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
removeCompound(const CompoundIdType& id, bool removeImpliedReactions) {
  directedHypergraph_->removeVertex(id, removeImpliedReactions);
}


/**
* Establish consecutive internal compound ids
*
* After deleting compounds from the network, the internal compound ids
* are not consecutive anymore. This function reassigns
* consecutive ids to the compounds.
*
* Complexity: Linear
*/
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
resetCompoundIds() {
  return directedHypergraph_->resetVertexIds();
}


// consuming reactions of a compound
/**
 * Check whether the compound with given Id is consumed by a reaction
 *
 * @param id Internal Id of a compound
 * @return true if the compound is consumed by at least one reaction, false
 * otherwise.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
isConsumed(const CompoundIdType& id) const {
  return directedHypergraph_->hasOutHyperarcs(id);
}


/**
 * Get number of consuming reactions
 *
 * @param id Internal Id of a compound
 * @return Number of consuming reactions of the compound with the given Id.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
nbConsumingReactions(const CompoundIdType& id) const {
  return directedHypergraph_->outDegree(id);
}


/**
 * \brief Const iterator pointing to begin of the consuming reactions of the
 * compound with given Id
 *
 * @param id Internal Id of a compound
 * @return Const iterator pointing to begin of the consuming reactions of the
 * compound with given Id
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
consumingReactionsBegin(const CompoundIdType& id) const {
  return directedHypergraph_->outHyperarcsBegin(id);
}


/**
 * \brief Const iterator pointing to end of the consuming reactions of the
 * compound with given Id
 *
 * @param id Internal Id of a compound
 * @return Const iterator pointing to end of the consuming reactions of the
 * compound with given Id
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
consumingReactionsEnd(const CompoundIdType& id) const {
  return directedHypergraph_->outHyperarcsEnd(id);
}


/**
 * \brief Return a shared_ptr to the container of consuming reactions of the
 * compound with given Id
 *
 * @param id Internal Id of a compound
 * @return shared_ptr to the container of consuming reactions of the
 * compound with given Id
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
std::shared_ptr<const hglib::GraphElementContainer<
    typename MetabolicNetwork<CompoundTemplate_t,
                              Reaction_type,
                              CompoundProperty,
                              ReactionProperty,
                              MetabolicNetworkProperty>::Reaction_t*>>
MetabolicNetwork<CompoundTemplate_t,
                 Reaction_type,
                 CompoundProperty,
                 ReactionProperty,
                 MetabolicNetworkProperty>::
consumingReactions(const CompoundIdType& id) const {
  return directedHypergraph_->outHyperarcs(id);
}


// producing reactions of a compound
/**
 * Check whether the compound with given Id is produced by a reaction
 *
 * @param id Internal Id of a compound
 * @return true if at least one reaction produces the compound with the given
 * id, false otherwise
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
isProduced(const CompoundIdType& id) const {
  return directedHypergraph_->hasInHyperarcs(id);
}


/**
 * Get number of producing reactions
 *
 * @param id Internal Id of a compound
 * @return number of producing reactions
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
nbProducingReactions(const CompoundIdType& id) const {
  return directedHypergraph_->inDegree(id);
}


/**
 * \brief Const iterator pointing to begin of the producing reactions of the
 * compound with given Id
 *
 * @param id Internal Id of a compound
 * @return Const iterator pointing to begin of the producing reactions of the
 * compound with given Id
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
producingReactionsBegin(const CompoundIdType& id) const {
  return directedHypergraph_->inHyperarcsBegin(id);
}


/**
 * \brief Const iterator pointing to end of the producing reactions of the
 * compound with given Id
 *
 * @param id Internal Id of a compound
 * @return Const iterator pointing to end of the producing reactions of the
 * compound with given Id
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
producingReactionsEnd(const CompoundIdType& id) const {
  return directedHypergraph_->inHyperarcsEnd(id);
}


/**
 * \brief Return a shared_ptr to the container of producing reactions of the
 * compound with given Id
 *
 * @param id Internal Id of a compound
 * @return shared_ptr to the container of producing reactions of the compound
 * with given Id
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
std::shared_ptr<const hglib::GraphElementContainer<
    typename MetabolicNetwork<CompoundTemplate_t,
                              Reaction_type,
                              CompoundProperty,
                              ReactionProperty,
                              MetabolicNetworkProperty>::Reaction_t*>>
MetabolicNetwork<CompoundTemplate_t,
                 Reaction_type,
                 CompoundProperty,
                 ReactionProperty,
                 MetabolicNetworkProperty>::
producingReactions(const CompoundIdType& id) const {
  return directedHypergraph_->inHyperarcs(id);
}


// Compound property getters
/**
 * Get properties of the compound with the provided id.
 *
 * Complexity: Constant
 *
 * Throws an out_of_range exception if the id is smaller than 0
 * or greater than the highest compound id attributed in this network.
 *
 * @param id id of the compound
 * @return pointer to property struct of the compound if the compound exists,
 * nullptr otherwise
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const CompoundProperty* MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
getCompoundProperties(const CompoundIdType& id) const {
  return directedHypergraph_->getVertexProperties(id);
}


/**
 * Get properties of the compound with the provided id.
 *
 * Complexity: Constant
 *
 * Throws an out_of_range exception if the id is smaller than 0
 * or greater than the highest compound id attributed in this network.
 *
 * @param id id of the compound
 * @return pointer to property struct of the compound if the compound exists,
 * nullptr otherwise
 *
 * Non-const version of getVertexProperties
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
CompoundProperty* MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
getCompoundProperties_(const CompoundIdType& id) const {
  return directedHypergraph_->getVertexProperties_(id);
}


// related to reactions
/**
 * Add a reaction with the provided arguments
 *
 * Add a reaction with the provided substrates, products, and
 * additional parameters specific to the reaction type to the
 * network
 *
 * @param substratesProducts substrate and product names
 * @param attributes additional arguments needed to create a reaction of type
 * Reaction_type
 * @return If the reaction is irreversible a pointer to the reaction is
 * returned. If the reaction is reversible, two reactions are created (forward
 * and backward direction) and a pointer to the forward reaction is returned.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t* MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
addReaction(decltype(hglib::NAME),
            const SubstrateAndProductNames& substratesProducts,
            const typename Reaction_t::SpecificAttributes& attributes) {
  // * Check that the Sbml Id follows the Sbml specifications
  // * Check that the number of substrates corresponds to the number of
  // substrate stoichiometry entries
  // * Check that the number of products corresponds to the number of
  // product stoichiometry entries
  // * Check that the lower bound is greater than or equal to zero if the
  // reaction is not reversible
  // * If the reaction is irreversible, add the reaction and return pointer to
  // it
  // * If the reaction is reversible, add a forward and backward reaction and
  // return a pointer to the forward reaction

  // Check that the Sbml Id follows the Sbml specifications
  std::regex regEx("[a-zA-Z_][a-zA-Z0-9_]*");
  std::string sbmlId = attributes.sbmlId_;
  if (not std::regex_match(sbmlId.begin(), sbmlId.end(), regEx)) {
    std::string errorMessage = "The reaction Sbml Id ";
    errorMessage += sbmlId;
    errorMessage += " is not valid. The Sbml Id must match the following"
        " regular expression: [a-zA-Z_][a-zA-Z0-9_]*";
    throw std::invalid_argument(errorMessage);
  }

  // Check that there is no reaction in the network with the same Sbml Id
  for (const auto& reaction : directedHypergraph_->hyperarcs()) {
    if (reaction->sbmlId().compare(sbmlId) == 0) {
      std::string errorMessage = "The reaction Sbml Id ";
      errorMessage += sbmlId;
      errorMessage += " already exists in the network.\n";
      throw std::invalid_argument(errorMessage);
    }
  }

  // Check that the number of substrates corresponds to the number of
  // substrate stoichiometry entries
  auto nbStoichioEntries = attributes.substrateStoichiometries().size();
  if (substratesProducts.first.size() != nbStoichioEntries) {
    std::string errorMessage = "Number of substrates (";
    errorMessage += std::to_string(substratesProducts.first.size());
    errorMessage += ") and number of substrate stoichiometry entries (";
    errorMessage += std::to_string(nbStoichioEntries);
    errorMessage += ") must be equal.\n";
    throw std::invalid_argument(errorMessage);
  }

  // Check that the number of products corresponds to the number of
  // product stoichiometry entries
  nbStoichioEntries = attributes.productStoichiometries().size();
  if (substratesProducts.second.size() != nbStoichioEntries) {
    std::string errorMessage = "Number of products (";
    errorMessage += std::to_string(substratesProducts.second.size());
    errorMessage += ") and number of product stoichiometry entries (";
    errorMessage += std::to_string(nbStoichioEntries);
    errorMessage += ") must be equal.\n";
    throw std::invalid_argument(errorMessage);
  }

  // Check that the lower bound is greater than or equal to zero if the
  // reaction is not reversible
  if (attributes.reversible_ == false and attributes.lowerBound_ < 0) {
    std::string errorMessage = "The lower flux bound of an irreversible"
        " reaction must be greater than or equal to zero: ";
    errorMessage += attributes.sbmlId_;
    errorMessage += " -> " + std::to_string(attributes.lowerBound_);
    errorMessage += " Do not insert the reaction.\n";
    throw std::invalid_argument(errorMessage);
  }

  // Create reaction or forward and backward reaction if reversible
  try {
    // If the reaction is irreversible, add the reaction
    if (attributes.reversible_ == false) {
      return directedHypergraph_->addHyperarc(
          hglib::NAME, substratesProducts, attributes);
    } else {
      const Reaction_t* forwardReaction;
      // As we split reversible reactions, we set the lower bound to zero if a
      // negative value is provided.
      if (attributes.lowerBound_ < 0) {
        // A copy is needed as the attributes are passed by const ref
        auto attributesCopy(attributes);
        attributesCopy.lowerBound_ = 0;
        forwardReaction = directedHypergraph_->addHyperarc(
            hglib::NAME, substratesProducts, attributesCopy);
      } else {
        forwardReaction = directedHypergraph_->addHyperarc(
            hglib::NAME, substratesProducts, attributes);
      }

      // Get arguments to create backward reaction (contain substrate/product
      // names and reaction type specific attributes)
      auto argsForBackwardReaction =
          forwardReaction->argumentsToCreateReverseReaction();
      // Recover reaction type specific attributes of the backward reaction
      auto backwardReactionSpecificAttributes =
          argsForBackwardReaction.specificAttributes();
      // set lower and upper bound of the backward reaction
      backwardReactionSpecificAttributes.lowerBound_ = 0;
      if (attributes.lowerBound_ >= 0) {
        backwardReactionSpecificAttributes.upperBound_ = 0;
      } else {
        backwardReactionSpecificAttributes.upperBound_ =
            -attributes.lowerBound_;
      }
      // Set arguments to create the backward reaction
      argsForBackwardReaction.setSpecificArguments(
          backwardReactionSpecificAttributes);
      // create the backward reaction
      const auto& backwardReaction =
          directedHypergraph_->addHyperarc(
              argsForBackwardReaction.tailAndHeadIds(),
              argsForBackwardReaction.specificAttributes());
      // Add each other as reverse reaction
      auto nonConstForwardReaction = const_cast<Reaction_type *>(
          forwardReaction);
      auto nonConstBackwardReaction = const_cast<Reaction_type *>(
          backwardReaction);
      nonConstForwardReaction->setReversibleReaction(nonConstBackwardReaction);
      nonConstBackwardReaction->setReversibleReaction(nonConstForwardReaction);

      // return pointer to forward reaction
      return directedHypergraph_->hyperarcById(forwardReaction->id());
    }
  } catch (const std::invalid_argument& ia) {
    // An exception is only thrown if a substrate or product is not declared in
    // the network. In this case no reaction is created. Thus, also in the case
    // of a reversible reaction, it is not possible to create the forward but
    // not the backward reaction. No pointer to a reaction has to be deleted.

    // Check which substrate/product was not declared
    std::string errorMessage = "";
    // Check substrates
    auto it = substratesProducts.first.begin();
    for (; it != substratesProducts.first.end(); ++it) {
      const auto& compound = directedHypergraph_->vertexByName(*it);
      if (compound == nullptr) {
        errorMessage += "Substrate " + *it + " in reaction " + sbmlId +
                         " is not declared in the network. Do not insert the"
                             " reaction.\n";
      }
    }
    // Check products
    it = substratesProducts.second.begin();
    for (; it != substratesProducts.second.end(); ++it) {
      const auto& compound = directedHypergraph_->vertexByName(*it);
      if (compound == nullptr) {
        errorMessage += "Product " + *it + " in reaction " + sbmlId +
                        " is not declared in the network. Do not insert the"
                            " reaction.\n";
      }
    }
    throw std::invalid_argument(errorMessage);
  }
}


/**
 * Get pointer to reaction
 *
 * Get pointer to reaction with given internal id
 *
 * @param id Internal numerical Id.
 * @return Pointer to reaction, nullptr if the given id is between 0
 * and maximum attributed reaction id in this network, throw out_of_range
 * exception otherwise.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t* MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reactionById(const ReactionIdType& id) const {
  return directedHypergraph_->hyperarcById(id);
}


/**
 * Get pointer to reaction
 *
 * Get pointer to reaction with given Sbml id
 *
 * @param sbmlId Sbml Id
 * @return Pointer to reaction, nullptr if there is no reaction with the given
 * Sbml id in the network.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t* MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reactionBySbmlId(const std::string& sbmlId) const {
  // Return pointer to reaction with given Sbml Id if found,
  // nullptr otherwise
  for (const auto& reaction : directedHypergraph_->hyperarcs()) {
    if (reaction->sbmlId().compare(sbmlId) == 0) {
      return reaction;
    }
  }
  return nullptr;
}


/**
 * Get number of reactions
 *
 * @return Number of reactions
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
nbReactions() const {
  // number of all reactions (reversible reactions are counted twice)
  size_t nbReac(directedHypergraph_->nbHyperarcs());
  // Get number of reversible reactions
  size_t nbRevReac(nbReversibleReactions());
  return (nbReac - nbRevReac);
}


/**
 * Get number of reversible reactions
 *
 * @return number of reversible reactions
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
nbReversibleReactions() const {
  size_t nbRevReac(0);
  for (const auto& reac : directedHypergraph_->hyperarcs()) {
    if (reac->isReversible()) {
      ++nbRevReac;
    }
  }
  return (nbRevReac / 2);
}

/**
 * Is the reaction reversible?
 *
 * @param id Id of the reaction
 * @return true if the reaction is reversible, false otherwise
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
isReversible(const ReactionIdType& id) const {
  try {
    const auto& reaction = directedHypergraph_->hyperarcById(id);
    if (reaction == nullptr) {
      std::string errorMessage = "Error in MetabolicNetwork::";
      errorMessage += "isReversible: The reaction with";
      errorMessage += " given Id: ";
      errorMessage += std::to_string(id);
      errorMessage += " is not in the metabolic network anymore.";
      errorMessage += "It was deleted before.\n";
      throw std::invalid_argument(errorMessage);
    }

    // return whether given reaction is reversible or not
    return reaction->isReversible();
  } catch (const std::out_of_range& oor) {
    throw oor;
  } catch (const std::invalid_argument& ia) {
    throw ia;
  }
}

/**
 * Get reverse reaction
 *
 * @param id Id of the reaction
 * @return If given reaction is reversible, a pointer to the reverse reaction
 * is returned, otherwise a nullptr is returned
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t* MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reversibleReaction(const ReactionIdType& id) const {
  try {
    const auto& reaction = directedHypergraph_->hyperarcById(id);
    if (reaction == nullptr) {
      std::string errorMessage = "Error in MetabolicNetwork::";
      errorMessage += "reversibleReaction: The reaction with";
      errorMessage += " given Id: ";
      errorMessage += std::to_string(id);
      errorMessage += " is not in the metabolic network anymore.";
      errorMessage += "It was deleted before.\n";
      throw std::invalid_argument(errorMessage);
    }

    // return whether given reaction is reversible or not
    return reaction->reversibleReaction();
  } catch (const std::out_of_range& oor) {
    throw oor;
  } catch (const std::invalid_argument& ia) {
    throw ia;
  }
}


/**
 * Const reaction iterator pointing to begin of the reactions
 *
 * @return Const reaction iterator pointing to begin of the reactions
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reactionsBegin() const {
  return directedHypergraph_->hyperarcsBegin();
}


/**
 * Const reaction iterator pointing to end of the reactions
 *
 * @return Const reaction iterator pointing to end of the reactions
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reactionsEnd() const {
  return directedHypergraph_->hyperarcsEnd();
}


/**
 * Return a reference to the container of reactions.
 *
 * @return Reference to the container of reactions.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::ReactionContainer& MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reactions() const {
  return directedHypergraph_->hyperarcs();
}


/**
 * Add a default irreversible reaction
 *
 * @param reactionId Id of the reaction
 * @return Arguments to add this reaction to a metabolic network
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t* MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
addDefaultIrreversibleReaction(const std::string& reactionName,
                               const std::vector<std::string>& substrateNames,
                               const std::vector<std::string>& productNames) {
  // Get arguments to build a default irreversible reaction
  auto args = Reaction_t::argumentsToCreateDefaultIrreversibleReaction(
      reactionName, substrateNames, productNames);
  // Add reaction
  return addReaction(hglib::NAME, args.substrateAndProductNames(),
                     args.specificAttributes());
}


/**
 * Remove a reaction with the given id from the network.
 *
 * @param id Internal Id of a reaction
 * @param removeReverseReaction In case that the reaction with the given id is
 * reversible, this flag signals whether the reverse direction of the reaction
 * should also be removed from the network.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
removeReaction(const ReactionIdType& id, bool removeReverseReaction) {
  // * Get reaction with provided Id
  // * If the reaction is reversible and
  //  (i)  removeReverseReaction == true, then the reverse reaction is
  //       removed from the network, or
  //  (ii) removeReverseReaction == false, then the reverse reaction is set to
  //       be irreversible.
  // * Remove the reaction with the given Id from the network
  try {
    // Get reaction with provided Id
    const auto& reaction = directedHypergraph_->hyperarcById(id);
    if (reaction == nullptr) {
      std::string errorMessage("Called MetabolicNetwork::removeReaction"
                                   " with "
                                   "the reaction id ");
      errorMessage += std::to_string(id);
      errorMessage += " which does not exists in the metabolic network.\n";
      throw std::invalid_argument(errorMessage);
    }

    // If the reaction is reversible and
    if (reaction->isReversible()) {
      auto reversibleReaction = const_cast<Reaction_t*>(
          reaction->reversibleReaction());
      //  (i)  removeReverseReaction == true, then the reverse reaction is
      //       removed from the network, or
      if (removeReverseReaction) {
        directedHypergraph_->removeHyperarc(reversibleReaction->id());
      } else {  // (ii) the reverse reaction is set to be irreversible.
        reversibleReaction->setIrreversible();
      }
    }
    // Remove the reaction with the given Id from the network
    directedHypergraph_->removeHyperarc(id);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in MetabolicNetwork::"
        "removeReaction: index = " << id << "; min = 0; max = "
              << (directedHypergraph_->nbHyperarcs() - 1) << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}

/**
 * Establish consecutive internal reaction ids
 *
 * After deleting reactions from the network, the reaction ids
 * are not consecutive anymore. This function reassigns
 * consecutive ids to the reactions.
 *
 * Complexity: Linear
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
resetReactionIds() {
  directedHypergraph_->resetHyperarcIds();
}


/**
 * Check whether the reaction with given Id has substrates
 *
 * @param id Internal Id of a reaction
 * @return true if the reaction with given Id has substrates, otherwise false is
 * returned. Throws invalid_argument exception if there is no reaction with the
 * given Id.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
hasSubstrates(const ReactionIdType& id) const {
  return directedHypergraph_->hasTailVertices(id);
}


/**
 * Get number of substrates of the reaction with given Id
 *
 * @param id Internal Id of a reaction
 * @return Number of substrates of the reaction. Throws invalid_argument
 * exception if there is no reaction with the given Id.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
nbSubstrates(const ReactionIdType& id) const {
  return directedHypergraph_->nbTailVertices(id);
}


/**
 * \brief Const compound iterator pointing to the begin of the substrates of
 * the reaction with the given id
 *
 * @param id Internal Id of a reaction
 * @return Iterator pointing to the begin of the substrates of the given
 * reaction.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
substratesBegin(const ReactionIdType& id) const {
  return directedHypergraph_->tailsBegin(id);
}


/**
 * \brief Const compound iterator pointing to the end of the substrates of
 * the reaction with the given id
 *
 * @param id Internal Id of a reaction
 * @return Iterator pointing to the end of the substrates of the given
 * reaction.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
substratesEnd(const ReactionIdType& id) const {
  return directedHypergraph_->tailsEnd(id);
}


/**
 * Get shared_ptr of container of substrates of given reaction id
 *
 * @param id Internal Id of a reaction
 * @return shared_ptr of container of substrates of given reaction id
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
std::shared_ptr<const hglib::GraphElementContainer<
    typename MetabolicNetwork<CompoundTemplate_t,
                              Reaction_type,
                              CompoundProperty,
                              ReactionProperty,
                              MetabolicNetworkProperty>::Compound_t*>>
MetabolicNetwork<CompoundTemplate_t,
                 Reaction_type,
                 CompoundProperty,
                 ReactionProperty,
                 MetabolicNetworkProperty>::
substrates(const ReactionIdType& id) const {
  return directedHypergraph_->tails(id);
}


/**
 * Check if given compound is a substrate of given reaction.
 *
 * @param compoundId Id of the compound
 * @param reactionId Id of the reaction
 * @return true if the compound is a substrate of the reaction, false otherwise
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
isSubstrateOfReaction(const CompoundIdType& compoundId,
                      const ReactionIdType& reactionId) const {
  return directedHypergraph_->isTailVertexOfHyperarc(compoundId, reactionId);
}


/**
 * Get substrate stoichiometry of given compound in given reaction
 *
 * @param compoundId Id of the compound
 * @param reactionId Id of the reaction
 * @return Substrate stoichiometry if given compound is a substrate of the
 * given reaction. Otherwise throw invalid_argument exception if the compound
 * or reaction does not exist in the network or if the compound is not a
 * substrate of the reaction.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
double MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
substrateStoichiometry(const CompoundIdType& compoundId,
                       const ReactionIdType& reactionId) const {
  // Check first if there are the given compound and reaction
  // If yes, call function to retrieve the substrate stoichiometry which may
  // throw an invalid_argument exception if the compound is not a substrate of
  // the reaction
  try {
    const auto& compound = directedHypergraph_->vertexById(compoundId);
    if (compound == nullptr) {
      std::string errorMessage = "Error in MetabolicNetwork::";
      errorMessage += "substrateStoichiometry: The compound with";
      errorMessage += " given Id: ";
      errorMessage += std::to_string(compoundId);
      errorMessage += " is not in the metabolic network anymore.";
      errorMessage += "It was deleted before.\n";
      throw std::invalid_argument(errorMessage);
    }
    const auto& reaction = directedHypergraph_->hyperarcById(reactionId);
    if (reaction == nullptr) {
      std::string errorMessage = "Error in MetabolicNetwork::";
      errorMessage += "substrateStoichiometry: The reaction with";
      errorMessage += " given Id: ";
      errorMessage += std::to_string(reactionId);
      errorMessage += " is not in the metabolic network anymore.";
      errorMessage += "It was deleted before.\n";
      throw std::invalid_argument(errorMessage);
    }
    // Return stoichiometry
    return reaction->substrateStoichiometry(compound);
  } catch (const std::out_of_range& oor) {
    throw oor;
  } catch (const std::invalid_argument& ia) {
    throw ia;
  }
}

// products of a reaction
/**
 * Check whether the reaction with given Id has products
 *
 * @param id Internal Id of a reaction
 * @return true if the reaction with given Id has products, otherwise false is
 * returned. Throws invalid_argument exception if there is no reaction with the
 * given Id.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
hasProducts(const ReactionIdType& id) const {
  return directedHypergraph_->nbHeadVertices(id);
}


/**
 * Get number of products of the reaction with given Id
 *
 * @param id Internal Id of a reaction
 * @return Number of products of the reaction. Throws invalid_argument
 * exception if there is no reaction with the given Id.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
nbProducts(const ReactionIdType& id) const {
  return directedHypergraph_->nbHeadVertices(id);
}


/**
 * \brief Const compound iterator pointing to the begin of the products of the
 * reaction with the given id
 *
 * @param id Internal Id of a reaction
 * @return Const compound iterator pointing to the begin of the products of the
 * reaction with the given id.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
productsBegin(const ReactionIdType& id) const {
  return directedHypergraph_->headsBegin(id);
}


/**
 * \brief Const compound iterator pointing to the end of the products of the
 * reaction with the given id
 *
 * @param id Internal Id of a reaction
 * @return Const compound iterator pointing to the end of the products of the
 * reaction with the given id.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
productsEnd(const ReactionIdType& id) const {
  return directedHypergraph_->headsEnd(id);
}


/**
 * Get shared_ptr of container of products of given reaction id
 *
 * @param id Internal Id of a reaction
 * @return shared_ptr of container of products of given reaction id
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
std::shared_ptr<const hglib::GraphElementContainer<
    typename MetabolicNetwork<CompoundTemplate_t,
                              Reaction_type,
                              CompoundProperty,
                              ReactionProperty,
                              MetabolicNetworkProperty>::Compound_t *>>
MetabolicNetwork<CompoundTemplate_t,
                 Reaction_type,
                 CompoundProperty,
                 ReactionProperty,
                 MetabolicNetworkProperty>::
products(const ReactionIdType& id) const {
  return directedHypergraph_->heads(id);
}


/**
 * Check if given compound is a product of given reaction.
 *
 * @param compoundId Id of the compound
 * @param reactionId Id of the reaction
 * @return true if the compound is a product of the reaction, false otherwise
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
isProductOfReaction(const CompoundIdType& compoundId,
                    const ReactionIdType& reactionId) const {
  return directedHypergraph_->isHeadVertexOfHyperarc(compoundId, reactionId);
}


/**
 * Get product stoichiometry of given compound in given reaction
 *
 * @param compoundId Id of the compound
 * @param reactionId Id of the reaction
 * @return Product stoichiometry if given compound is a product of the
 * given reaction. Otherwise throw invalid_argument exception if the compound
 * or reaction does not exist in the network or if the compound is not a
 * product of the reaction.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
double MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
productStoichiometry(const CompoundIdType& compoundId,
                     const ReactionIdType& reactionId) const {
  // Check first if there are the given compound and reaction
  // If yes, call function to retrieve the product stoichiometry which may
  // throw an invalid_argument exception if the compound is not a product of
  // the reaction
  try {
    const auto& compound = directedHypergraph_->vertexById(compoundId);
    if (compound == nullptr) {
      std::string errorMessage = "Error in MetabolicNetwork::";
      errorMessage += "productStoichiometry: The compound with";
      errorMessage += " given Id: ";
      errorMessage += std::to_string(compoundId);
      errorMessage += " is not in the metabolic network anymore.";
      errorMessage += "It was deleted before.\n";
      throw std::invalid_argument(errorMessage);
    }
    const auto& reaction = directedHypergraph_->hyperarcById(reactionId);
    if (reaction == nullptr) {
      std::string errorMessage = "Error in MetabolicNetwork::";
      errorMessage += "productStoichiometry: The reaction with";
      errorMessage += " given Id: ";
      errorMessage += std::to_string(reactionId);
      errorMessage += " is not in the metabolic network anymore.";
      errorMessage += "It was deleted before.\n";
      throw std::invalid_argument(errorMessage);
    }
    // Return stoichiometry
    return reaction->productStoichiometry(compound);
  } catch (const std::out_of_range& oor) {
    throw oor;
  } catch (const std::invalid_argument& ia) {
    throw ia;
  }
}


// reaction property related
/**
 * Get properties of the reaction with the provided id.
 *
 * Complexity: Constant
 *
 * Throws an out_of_range exception if the id is smaller than 0
 * or greater than the highest reaction id attributed in this network.
 *
 * @param id Id of the reaction
 * @return pointer to property struct of the reaction if the reaction exists,
 * nullptr otherwise
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const ReactionProperty* MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
getReactionProperties(const ReactionIdType& id) const {
  return directedHypergraph_->getHyperarcProperties(id);
}


/**
 * Get properties of the reaction with the provided id.
 *
 * Complexity: Constant
 *
 * Throws an out_of_range exception if the id is smaller than 0
 * or greater than the highest reaction id attributed in this network.
 *
 * @param id Id of the reaction
 * @return pointer to property struct of the reaction if the reaction exists,
 * nullptr otherwise
 *
 * Non-const version of getReactionProperties
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
ReactionProperty* MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
getReactionProperties_(const ReactionIdType& id) const {
  return directedHypergraph_->getHyperarcProperties_(id);
}


// Related to the metabolic network property
/**
 * Get properties of the metabolic network.
 *
 * Complexity: Constant
 *
 * @return Pointer to MetabolicNetworkProperty struct/class.
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const MetabolicNetworkProperty* MetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
getMetabolicNetworkProperties() const {
  return directedHypergraph_->getHypergraphProperties();
}


/**
 * Get properties of the metabolic network.
 *
 * Complexity: Constant
 *
 * @return Pointer to MetabolicNetworkProperty struct/class.
 *
 * Non-const version of getMetabolicNetworkProperties
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
MetabolicNetworkProperty* MetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
getMetabolicNetworkProperties_() const {
  return directedHypergraph_->getHypergraphProperties_();
}

/* **************************************************************************
 * **************************************************************************
 *                        Protected member functions
 * **************************************************************************
 ***************************************************************************/
/**
 * Get underlying directed hypergraph
 *
 * @return Pointer to DirectedHypergraphInterface
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty,
    MetabolicNetworkProperty>::DirectedHypergraphInterface* MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
getUnderlyingDirectedHypergraph() const {
  return directedHypergraph_;
}


/**
 * Return pointer to compartment container in the root metabolic network
 *
 * @return pointer to compartment container
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename MetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty,
    MetabolicNetworkProperty>::CompartmentContainerPtr MetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
getRootCompartmentContainerPtr() const {
  return compartments_;
}
}  // namespace metnetlib
