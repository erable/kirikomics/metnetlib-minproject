/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 17.11.17.
//

#ifndef HGLIB_METABOLICNETWORKOBSERVABLE_H
#define HGLIB_METABOLICNETWORKOBSERVABLE_H

#include "MetabolicNetworkObserver.h"
#include "MetabolicNetworkObservableEvents.h"

#include <list>
#include <map>

namespace metnetlib {

class MetabolicNetworkObservable {
 public :
  // ==========================================================================
  //                               Constructors
  // ==========================================================================
  /// Default ctor
  MetabolicNetworkObservable() = default;
  /// Copy ctor
  MetabolicNetworkObservable(const MetabolicNetworkObservable&) = delete;
  /// Move ctor
  MetabolicNetworkObservable(MetabolicNetworkObservable&&) = delete;

  // ==========================================================================
  //                                Destructor
  // ==========================================================================
  /// Destructor
  virtual ~MetabolicNetworkObservable() = default;

  // ==========================================================================
  //                                Operators
  // ==========================================================================
  /// Copy assignment
  MetabolicNetworkObservable& operator=(
      const MetabolicNetworkObservable& other) = delete;
  /// Move assignment
  MetabolicNetworkObservable& operator=(
      const MetabolicNetworkObservable&& other) = delete;

  // ==========================================================================
  //                              Public Methods
  // ==========================================================================
  /// Attach an observer o for a given event e
  void AddObserver(MetabolicNetworkObserver* o,
                   MetabolicNetworkObservableEvent e);
  /// Release an observer o for a given event e
  void DeleteObserver(MetabolicNetworkObserver* o,
                      MetabolicNetworkObservableEvent e);
  /// Notify all observers of event e providing optional argument
  void NotifyObservers(MetabolicNetworkObservableEvent e, void* arg = nullptr);

  // ==========================================================================
  //                                 Getters
  // ==========================================================================

  // ==========================================================================
  //                                 Setters
  // ==========================================================================

 protected :
  // ==========================================================================
  //                            Protected Methods
  // ==========================================================================

  // ==========================================================================
  //                               Attributes
  // ==========================================================================
  std::map<MetabolicNetworkObservableEvent,
           std::list<MetabolicNetworkObserver*>> observers_;
};

}  // namespace metnetlib

#endif  // HGLIB_METABOLICNETWORKOBSERVABLE_H
