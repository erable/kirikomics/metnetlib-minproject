/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 07.11.17.
//

#ifndef METNET_COMPOUND_H
#define METNET_COMPOUND_H

#include <memory>

#include "hglib/vertex/directed/DirectedVertex.h"

#include "compartment/Compartment.h"

// Forward declaration of hglib classes to be befriended
namespace hglib {
template<template <typename> class Compound_t,
                            typename Reac_t,
                            typename CompoundProperty,
                            typename ReactionProperty,
                            typename MetNetProperty>
class Hypergraph;

template <template <typename> class Compound_t,
                             typename Reac_t,
                             typename CompoundProperty,
                             typename ReactionProperty,
                             typename MetNetProperty>
class DirectedHypergraph;

template <template <typename> class Compound_t,
                             typename Reac_t,
                             typename CompoundProperty,
                             typename ReactionProperty,
                             typename MetNetProperty>
class DirectedSubHypergraph;
}  // namespace hglib

namespace metnetlib {

/**
 * Compound class
 *
 * @tparam Reaction_t Type of consuming/producing reactions
 */
template <typename Reaction_t>
class Compound : private hglib::DirectedVertex<Reaction_t> {
  // Hypergraph is a friend class
  template <template <typename> class Compound_t, typename Reac_t,
      typename CompoundProperty, typename ReactionProperty,
      typename MetNetProperty>
  friend class hglib::Hypergraph;
  // DirectedHypergraph is a friend class
  template <template <typename> class Compound_t, typename Reac_t,
      typename CompoundProperty, typename ReactionProperty,
      typename MetNetProperty>
  friend class hglib::DirectedHypergraph;
  // DirectedSubHypergraph is a friend class
  template <template <typename> class Compound_t, typename Reac_t,
      typename CompoundProperty, typename ReactionProperty,
      typename MetNetProperty>
  friend class hglib::DirectedSubHypergraph;
  // MetabolicNetwork is a friend
  template <template <typename> class Compound_t, typename Reac_t,
      typename CompoundProperty, typename ReactionProperty,
      typename MetNetProperty>
  friend class MetabolicNetwork;

 public:
  // Assert that Reaction_t is derived from DirectedHyperedgeBase
  static_assert(hglib::is_parent_of<hglib::DirectedHyperedgeBase,
                                    Reaction_t>::value,
                "Template parameter of Compound must be a class derived "
                    "from DirectedHyperedgeBase");

  /* **************************************************************************
   * **************************************************************************
   *                        Nested class SpecificAttributes
   * **************************************************************************
   ***************************************************************************/
  /**
   * SpecificAttributes class
   *
   * A basic Compound has no specific arguments, but the class is needed to
   * call the addCompound function of a (Sub)MetabolicNetwork.
   */
  class SpecificAttributes {
   public:
    SpecificAttributes(const Compartment* compartment,
                       bool hasOnlySubstanceUnits, bool boundaryCondition,
                       bool constant) :
        compartment_(compartment),
        hasOnlySubstanceUnits_(hasOnlySubstanceUnits),
        boundaryCondition_(boundaryCondition), constant_(constant) {
    }

    /* ************************************************************************
     * ************************************************************************
     *                        Member variables of nested class
     * ************************************************************************
     *************************************************************************/
   public:
    /// Compartment
    const Compartment* compartment_;
    /// \brief If the attribute has the value “false”, then the unit of
    /// measurement associated with the value of the species is
    /// {unit of amount}/{unit of size} (i.e., concentration or density). If
    /// hasOnlySubstanceUnits has the value “true”, then the value is
    /// interpreted as having a unit of amount only.
    bool hasOnlySubstanceUnits_;
    /// \brief Indicate whether there is a constraint for this compound in
    /// constraint-based methods.
    bool boundaryCondition_;
    /// Indicate whether the amount of a compound can vary
    bool constant_;
  };

  /* **************************************************************************
   * **************************************************************************
   *                        Constructor/Destructor (Rule of five)
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Constructor
  Compound(const CompoundIdType&, const std::string&,
           const SpecificAttributes&);
  /// Constructor
  Compound(const CompoundIdType&, const SpecificAttributes&);
  /// Default constructor
  Compound() = delete;
  /// Destructor
  virtual ~Compound() = default;
  /// Copy constructor
  Compound(const Compound&);
  /// Move constructor
  Compound(Compound&&) = default;
  /// Copy assignment
  Compound& operator=(const Compound&) = default;
  /// Move assignment
  Compound& operator=(Compound&&) = default;

  /* **************************************************************************
   * **************************************************************************
   *                        Member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Get internal Id of the compartment
  CompoundIdType id() const;
  /// Get the Sbml Id of the compound.
  std::string sbmlId() const;
  /// Get pointer to compartment this compound lies in
  const Compartment* getCompartment() const;
  /// Get the value of the hasOnlySubstanceUnits attribute flag
  bool hasOnlySubstanceUnits() const;
  /// Check whether the boundary condition is set
  bool hasBoundaryCondition() const;
  /// Check whether the amount of a compound can vary
  bool isConstant() const;
  /// Set boundary condition flag
  void setBoundaryCondition(bool);

 protected:
  /// Get arguments needed to create this compound
  hglib::ArgumentsToCreateVertex<Compound<Reaction_t>>
      argumentsToCreateVertex() const;
  /// Set the compartment pointer
  void setCompartment(const Compartment*);

  /* **************************************************************************
   * **************************************************************************
   *                        Member variables
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Specific attributes
  SpecificAttributes specificAttributes_;
};

/// Operator<<
template <typename Reaction_t>
inline std::ostream& operator<< (std::ostream& out,
                                 const Compound<Reaction_t>& compound) {
  out << compound.sbmlId();
  return out;
}

}  // namespace metnetlib

#include "Compound.hpp"
#endif  // METNET_COMPOUND_H
