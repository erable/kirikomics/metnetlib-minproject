/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 01.12.17.
//

#ifndef METNETLIB_REMOVE_ISOLATED_COMPOUNDS_H
#define METNETLIB_REMOVE_ISOLATED_COMPOUNDS_H

#include <list>

namespace metnetlib {

/**
 * Remove isolated compounds from the network
 *
 * A compound is isolated if it is neither consumed nor produced.
 *
 * @tparam MetabolicNetwork
 * @param network
 */
template<typename MetabolicNetwork>
void remove_isolated_compounds(MetabolicNetwork* network) {
  std::list<CompoundIdType> compoundsToBeRemoved;
  // Get isolated compounds
  for (const auto& compound : network->compounds()) {
    // check if the compound has consuming or producing reactions
    if (not network->isConsumed(compound->id()) and
        not network->isProduced(compound->id())) {
      compoundsToBeRemoved.push_back(compound->id());
    }
  }

  // Remove isolated compounds
  for (const auto& compoundId : compoundsToBeRemoved) {
    network->removeCompound(compoundId);
  }
}
}  // namespace metnetlib
#endif  // METNETLIB_REMOVE_ISOLATED_COMPOUNDS_H
