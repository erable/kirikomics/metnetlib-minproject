/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 30.11.17.
//

#ifndef METNETLIB_REMOVE_DUPLICATE_REACTIONS_H
#define METNETLIB_REMOVE_DUPLICATE_REACTIONS_H

#include <list>

#include "are_identical_reactions.hpp"

namespace metnetlib {

/**
 * Remove from the network duplicate reactions
 *
 * A reaction is a duplicate of another reactions if they have the same
 * substrates, products and the same stoichiometry. Consider a pair of identical
 * reactions: If only one of them is irreversible, remove the irreversible one,
 * otherwise remove the second reaction.
 *
 * @tparam MetabolicNetwork
 * @param network
 */
template<typename MetabolicNetwork>
void remove_duplicate_reactions(MetabolicNetwork* network) {
  // TODO(mw) implement
  std::list<ReactionIdType> reactionsToBeRemoved;
  // Get duplicate reactions
  auto it = network->reactionsBegin();
  auto end = network->reactionsEnd();
  for (; it != end; ++it) {
    auto it2 = it;
    for (++it2; it2 != end; ++it2) {
      // continue if it2 is already in the list of to be removed reactions
      if (std::find(reactionsToBeRemoved.begin(), reactionsToBeRemoved.end(),
                    (*it2)->id()) != reactionsToBeRemoved.end()) {
        continue;
      }
      // reactions are identical (substrates, products, stoichiometry)
      if (are_identical_reactions(*network, *(*it), *(*it2)) == true) {
        // if only one reaction is irreversible, remove the irreversible one,
        // otherwise remove the second one (it2)
        if (not network->isReversible((*it)->id()) and
            network->isReversible((*it2)->id())) {
          reactionsToBeRemoved.push_back((*it)->id());
        } else {
          reactionsToBeRemoved.push_back((*it2)->id());
        }
        break;
      }
    }
  }

  // Remove reactions from network
  for (const auto& reactionId : reactionsToBeRemoved) {
    network->removeReaction(reactionId);
  }
}
}  // namespace metnetlib

#endif  // METNETLIB_REMOVE_DUPLICATE_REACTIONS_H
