/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 30.11.17.
//

#ifndef METNETLIB_REMOVE_REACTIONS_WITHOUT_SUBSTRATE_OR_PRODUCT_H
#define METNETLIB_REMOVE_REACTIONS_WITHOUT_SUBSTRATE_OR_PRODUCT_H

#include <list>

namespace metnetlib {

/**
 * Remove from the network those reactions without substrates or products
 *
 * @tparam MetabolicNetwork
 * @param network
 */
template<typename MetabolicNetwork>
void remove_reactions_without_substrate_or_product(
    MetabolicNetwork* network) {
  std::list<typename MetabolicNetwork::Reaction_t*> reactionsToBeRemoved;
  // Get reactions without substrate or product
  for (const auto& reaction : network->reactions()) {
    if (not (network->hasSubstrates(reaction->id()) and
        network->hasProducts(reaction->id()))) {
      reactionsToBeRemoved.push_back(reaction);
    }
  }

  // Remove reactions from network
  for (const auto& reaction : reactionsToBeRemoved) {
    network->removeReaction(reaction->id());
  }
}
}  // namespace metnetlib
#endif  // METNETLIB_REMOVE_REACTIONS_WITHOUT_SUBSTRATE_OR_PRODUCT_H
