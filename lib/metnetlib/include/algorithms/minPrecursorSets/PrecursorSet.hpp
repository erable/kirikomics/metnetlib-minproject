/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 30.11.17.
//

#include "PrecursorSet.h"

namespace metnetlib {


/// Copy constructor
template <typename Compound_t, typename Reaction_t>
PrecursorSet<Compound_t, Reaction_t>::
PrecursorSet(const PrecursorSet& other) :
    precursors_(other.precursors_),
    bootstrapCompounds_(other.bootstrapCompounds_),
    factoryReactions_(other.factoryReactions_) {
}


/// Copy assignment
template <typename Compound_t, typename Reaction_t>
PrecursorSet<Compound_t, Reaction_t>& PrecursorSet<Compound_t, Reaction_t>::
operator=(const PrecursorSet<Compound_t, Reaction_t>& other) {
  if (this != &other) {
    precursors_ = other.precursors_;
    bootstrapCompounds_ = other.bootstrapCompounds_;
    factoryReactions_ = other.factoryReactions_;
  }
  return *this;
}


/// Add a precursor to the precursor set
template <typename Compound_t, typename Reaction_t>
void PrecursorSet<Compound_t, Reaction_t>::
addPrecursor(const Compound_t& compound) {
  precursors_.insert(&compound);
}


/// Add a compound as a bootstrap compound to the precursor set
template <typename Compound_t, typename Reaction_t>
void PrecursorSet<Compound_t, Reaction_t>::
addBootstrapCompound(const Compound_t& compound) {
  bootstrapCompounds_.insert(&compound);
}


/// Add a reaction as part of the factory from the precursor set to the target
template <typename Compound_t, typename Reaction_t>
void PrecursorSet<Compound_t, Reaction_t>::
addFactoryReaction(const Reaction_t& reaction) {
  factoryReactions_.insert(&reaction);
}


/// Get begin of precursors
template <typename Compound_t, typename Reaction_t>
typename std::set<const Compound_t*>::const_iterator PrecursorSet<Compound_t,
    Reaction_t>::
precursorsBegin() const {
  return precursors_.cbegin();
}


/// Get end of precursors
template <typename Compound_t, typename Reaction_t>
typename std::set<const Compound_t*>::const_iterator PrecursorSet<Compound_t,
    Reaction_t>::
precursorsEnd() const {
  return precursors_.cend();
}


/// Get number of precursors
template <typename Compound_t, typename Reaction_t>
size_t PrecursorSet<Compound_t, Reaction_t>::
nbPrecursors() const {
  return precursors_.size();
}


/// Get begin of bootstrap compounds
template <typename Compound_t, typename Reaction_t>
typename std::set<const Compound_t*>::const_iterator PrecursorSet<Compound_t,
    Reaction_t>::
bootstrapCompoundsBegin() const {
  return bootstrapCompounds_.cbegin();
}


/// Get end of bootstrap compounds
template <typename Compound_t, typename Reaction_t>
typename std::set<const Compound_t*>::const_iterator PrecursorSet<Compound_t,
    Reaction_t>::
bootstrapCompoundsEnd() const {
  return bootstrapCompounds_.cend();
}


/// Get begin of factory reactions
template <typename Compound_t, typename Reaction_t>
typename std::set<const Reaction_t*>::const_iterator PrecursorSet<Compound_t,
    Reaction_t>::
factoryReactionsBegin() const {
  return factoryReactions_.cbegin();
}


/// Get end of factory reactions
template <typename Compound_t, typename Reaction_t>
typename std::set<const Reaction_t*>::const_iterator PrecursorSet<Compound_t,
    Reaction_t>::
factoryReactionsEnd() const {
  return factoryReactions_.cend();
}
}  // namespace metnetlib
