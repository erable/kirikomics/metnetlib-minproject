/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 11.12.17.
//

#include "MinimalPrecursorSetsInput.h"

namespace metnetlib {
/// Constructor
template<typename MetabolicNetwork>
MinimalPrecursorSetsInput<MetabolicNetwork>::
MinimalPrecursorSetsInput(const std::shared_ptr<MetabolicNetwork>& network,
                          const std::set<CompoundIdType>& targets,
                          const std::set<CompoundIdType>& userDefinedSources,
                          const std::set<CompoundIdType>& bootstrapCompounds,
                          bool considerTopologicalSources,
                          bool considerBoundaryCompoundsAsSources,
                          const ConstraintType& constraintType, double epsilon,
                          bool addFactoryReactions) :
    network_(network), targets_(targets),
    userDefinedSources_(userDefinedSources),
    bootstrapCompounds_(bootstrapCompounds),
    considerTopologicalSources_(considerTopologicalSources),
    considerBoundaryCompoundsAsSources_(considerBoundaryCompoundsAsSources),
    constraintType_(constraintType),
    epsilon_(epsilon), addFactoryReactions_(addFactoryReactions) {
}


/// Constructor
template<typename MetabolicNetwork>
MinimalPrecursorSetsInput<MetabolicNetwork>::
MinimalPrecursorSetsInput(const std::shared_ptr<MetabolicNetwork>& network,
                          const std::set<CompoundIdType>& targets,
                          const std::set<CompoundIdType>& userDefinedSources,
                          const std::set<CompoundIdType>& bootstrapCompounds) :
    network_(network), targets_(targets),
    userDefinedSources_(userDefinedSources),
    bootstrapCompounds_(bootstrapCompounds),
    considerTopologicalSources_(false),
    considerBoundaryCompoundsAsSources_(false),
    constraintType_(ConstraintType::STEADYSTATE),
    epsilon_(0.1), addFactoryReactions_(false) {
}

/// Copy constructor
template<typename MetabolicNetwork>
MinimalPrecursorSetsInput<MetabolicNetwork>::
MinimalPrecursorSetsInput(const MinimalPrecursorSetsInput& other) :
    network_(other.network_), targets_(other.targets_),
    userDefinedSources_(other.userDefinedSources_),
    bootstrapCompounds_(other.bootstrapCompounds_),
    considerTopologicalSources_(other.considerTopologicalSources_),
    considerBoundaryCompoundsAsSources_(
        other.considerBoundaryCompoundsAsSources_),
    constraintType_(other.constraintType_),
    epsilon_(other.epsilon_), addFactoryReactions_(other.addFactoryReactions_) {
}


/// Copy assignment
template<typename MetabolicNetwork>
MinimalPrecursorSetsInput <MetabolicNetwork>& MinimalPrecursorSetsInput<
    MetabolicNetwork>::
operator=(const MinimalPrecursorSetsInput& other) {
  if (this != &other) {  // avoid self assignment
    network_ = other.network_;
    targets_ = other.targets_;
    userDefinedSources_ = other.userDefinedSources_;
    bootstrapCompounds_ = other.bootstrapCompounds_;
    considerTopologicalSources_ = other.considerTopologicalSources_;
    considerBoundaryCompoundsAsSources_ =
        other.considerBoundaryCompoundsAsSources_;
    constraintType_ = other.constraintType_;
    epsilon_ = other.epsilon_;
    addFactoryReactions_ = other.addFactoryReactions_;
  }
  return *this;
}


/// Add a target
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
addTarget(const CompoundIdType& compoundId) {
  targets_.insert(compoundId);
}


/// Add an user defined source
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
addUserDefinedSource(const CompoundIdType& compoundId) {
  userDefinedSources_.insert(compoundId);
}


/// Add a bootstrap compounds
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
addBootstrapCompound(const CompoundIdType& compoundId) {
  bootstrapCompounds_.insert(compoundId);
}


/// Remove a target
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
removeTarget(const CompoundIdType& compoundId) {
  targets_.erase(compoundId);
}


/// Remove an user defined source
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
removeUserDefinedSource(const CompoundIdType& compoundId) {
  userDefinedSources_.erase(compoundId);
}


/// Remove a bootstrap compounds
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
removeBootstrapCompound(const CompoundIdType& compoundId) {
  bootstrapCompounds_.erase(compoundId);
}


/// Set consider-topological-sources flag
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
considerTopologicalSources(bool considerTopologicalSources) {
  considerTopologicalSources_ = considerTopologicalSources;
}


/// Set consider-boundary-compounds-as-sources flag
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
considerBoundaryCompoundsAsSources(bool considerBoundaryCompoundsAsSources) {
  considerBoundaryCompoundsAsSources_ = considerBoundaryCompoundsAsSources;
}


/// Set constraint type
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
constraintType(const ConstraintType& constraintType) {
  constraintType_ = constraintType;
}


/// Set epsilon for the target constraint
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
epsilon(const double& epsilon) {
  epsilon_ = epsilon;
}


/// Set add-factory-reactions flag
template<typename MetabolicNetwork>
void MinimalPrecursorSetsInput<MetabolicNetwork>::
addFactoryReactions(bool addFactoryReactions) {
  addFactoryReactions_ = addFactoryReactions;
}

// Getter
/// Get the metabolic network
template<typename MetabolicNetwork>
std::shared_ptr<const MetabolicNetwork>
MinimalPrecursorSetsInput<MetabolicNetwork>::
metabolicNetwork() const {
  return network_;
}


/// Iterator pointing to the begin of the targets
template<typename MetabolicNetwork>
std::set<CompoundIdType>::const_iterator MinimalPrecursorSetsInput<
    MetabolicNetwork>::
targetsBegin() const {
  return targets_.cbegin();
}


/// Iterator pointing to the end of the targets
template<typename MetabolicNetwork>
std::set<CompoundIdType>::const_iterator MinimalPrecursorSetsInput<
    MetabolicNetwork>::
targetsEnd() const {
  return targets_.cend();
}


/// Whether a given compound is a target
template<typename MetabolicNetwork>
bool MinimalPrecursorSetsInput<MetabolicNetwork>::
isTarget(CompoundIdType id) const {
  return (not (std::find(targets_.cbegin(),
                         targets_.cend(),
                         id) == targets_.cend()));
}


/// Iterator pointing to the begin of the user defined sources
template<typename MetabolicNetwork>
std::set<CompoundIdType>::const_iterator MinimalPrecursorSetsInput<
    MetabolicNetwork>::
userDefinedSourcesBegin() const {
  return userDefinedSources_.cbegin();
}


/// Iterator pointing to the end of the user defined sources
template<typename MetabolicNetwork>
std::set<CompoundIdType>::const_iterator MinimalPrecursorSetsInput<
    MetabolicNetwork>::
userDefinedSourcesEnd() const {
  return userDefinedSources_.cend();
}


/// Get number of user-defined sources
template<typename MetabolicNetwork>
auto MinimalPrecursorSetsInput<MetabolicNetwork>::
nbUserDefinedSources() const {
  return userDefinedSources_.size();
}


/// Whether a given compound is a user-defined source
template<typename MetabolicNetwork>
bool MinimalPrecursorSetsInput<MetabolicNetwork>::
isUserDefinedSource(CompoundIdType id) const {
  return (not (std::find(userDefinedSources_.cbegin(),
                        userDefinedSources_.cend(),
                        id) == userDefinedSources_.cend()));
}


/// Iterator pointing to the begin of the bootstrap compounds
template<typename MetabolicNetwork>
std::set<CompoundIdType>::const_iterator MinimalPrecursorSetsInput<
    MetabolicNetwork>::
bootstrapCompoundsBegin() const {
  return bootstrapCompounds_.cbegin();
}


/// Iterator pointing to the end of the bootstrap compounds
template<typename MetabolicNetwork>
std::set<CompoundIdType>::const_iterator
MinimalPrecursorSetsInput<MetabolicNetwork>::
bootstrapCompoundsEnd() const {
  return bootstrapCompounds_.cend();
}


/// Get number of bootstrap compounds
template<typename MetabolicNetwork>
auto MinimalPrecursorSetsInput<MetabolicNetwork>::
nbBootstrapCompounds() const {
  return bootstrapCompounds_.size();
}


/// Whether a given compound is a bootstrap compound
template<typename MetabolicNetwork>
bool MinimalPrecursorSetsInput<MetabolicNetwork>::
isBootstrapCompound(CompoundIdType id) const {
  return (not (std::find(bootstrapCompounds_.cbegin(),
                         bootstrapCompounds_.cend(),
                         id) == bootstrapCompounds_.cend()));
}


/// Get consider-topological-sources flag
template<typename MetabolicNetwork>
bool MinimalPrecursorSetsInput<MetabolicNetwork>::
considerTopologicalSources() const {
  return considerTopologicalSources_;
}


/// Get consider-boundary-compounds-as-sources flag
template<typename MetabolicNetwork>
bool MinimalPrecursorSetsInput<MetabolicNetwork>::
considerBoundaryCompoundsAsSources() const {
  return considerBoundaryCompoundsAsSources_;
}


/// Get constraint type
template<typename MetabolicNetwork>
const ConstraintType& MinimalPrecursorSetsInput<MetabolicNetwork>::
constraintType() const {
  return constraintType_;
}


/// Get epsilon for the target constraint
template<typename MetabolicNetwork>
const double& MinimalPrecursorSetsInput<MetabolicNetwork>::
epsilon() const {
  return epsilon_;
}


/// Get add-factory-reactions flag
template<typename MetabolicNetwork>
bool MinimalPrecursorSetsInput<MetabolicNetwork>::
addFactoryReactions() const {
  return addFactoryReactions_;
}
}  // namespace metnetlib
