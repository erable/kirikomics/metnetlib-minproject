/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 11.12.17.
//

#ifndef METNETLIB_MINIMALPRECURSORSETSINPUT_H
#define METNETLIB_MINIMALPRECURSORSETSINPUT_H

#include <memory>
#include <set>

#include "algorithms/utils/ConstraintType.h"
#include "utils/types.h"

namespace metnetlib {

/**
 * Stores the inputs needed for the computation of minimal precursor sets.
 *
 * These parameter contain:
 * * the metabolic network
 * * the set of targets
 * * the set of user defiend sources
 * * the set of bootstrap compounds
 * * a flag to show whether topological sources should be considered
 * * a flag to show whether boundary compounds should be considered as sources
 * * the constraint type {ACCUMULATION, STEADYSTATE, DUPMACH}
 * * a epsilon for the target constraint
 * * a flag to show whether factory reaction are added to the minPS solution
 *
 * @tparam MetabolicNetwork
 */
template<typename MetabolicNetwork>
class MinimalPrecursorSetsInput {
  /* **************************************************************************
   * **************************************************************************
   *                        Constructor/Destructor (Rule of five)
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Default constructor
  MinimalPrecursorSetsInput() = delete;
  /// Constructor
  MinimalPrecursorSetsInput(const std::shared_ptr<MetabolicNetwork>&,
                            const std::set<CompoundIdType>&,
                            const std::set<CompoundIdType>&,
                            const std::set<CompoundIdType>&, bool, bool,
                            const ConstraintType&, double, bool);

  /// Constructor
  MinimalPrecursorSetsInput(const std::shared_ptr<MetabolicNetwork>&,
                            const std::set<CompoundIdType>&,
                            const std::set<CompoundIdType>&,
                            const std::set<CompoundIdType>&);
  /// Destructor
  virtual ~MinimalPrecursorSetsInput() = default;
  /// Copy constructor
  MinimalPrecursorSetsInput(const MinimalPrecursorSetsInput&);
  /// Move constructor
  MinimalPrecursorSetsInput(MinimalPrecursorSetsInput&&) = default;
  /// Copy assignment
  MinimalPrecursorSetsInput& operator=(const MinimalPrecursorSetsInput&);
  /// Move assignment
  MinimalPrecursorSetsInput& operator=(MinimalPrecursorSetsInput&&) = default;

  /* **************************************************************************
   * **************************************************************************
   *                        Member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  // Modify sets of target, source, bootstrap compounds
  /// Add a target
  void addTarget(const CompoundIdType&);
  /// Add an user defined source
  void addUserDefinedSource(const CompoundIdType&);
  /// Add a bootstrap compounds
  void addBootstrapCompound(const CompoundIdType&);
  /// Remove a target
  void removeTarget(const CompoundIdType&);
  /// Remove an user defined source
  void removeUserDefinedSource(const CompoundIdType&);
  /// Remove a bootstrap compounds
  void removeBootstrapCompound(const CompoundIdType&);
  /// Set consider-topological-sources flag
  void considerTopologicalSources(bool);
  /// Set consider-boundary-compounds-as-sources flag
  void considerBoundaryCompoundsAsSources(bool);
  /// Set constraint type
  void constraintType(const ConstraintType&);
  /// Set epsilon for the target constraint
  void epsilon(const double&);
  /// Set add-factory-reactions flag
  void addFactoryReactions(bool);

  // Getter
  /// Get the metabolic network
  std::shared_ptr<const MetabolicNetwork> metabolicNetwork() const;
  /// Iterator pointing to the begin of the targets
  std::set<CompoundIdType>::const_iterator targetsBegin() const;
  /// Iterator pointing to the end of the targets
  std::set<CompoundIdType>::const_iterator targetsEnd() const;
  /// Whether a given compound is a target
  bool isTarget(CompoundIdType id) const;
  /// Iterator pointing to the begin of the user defined sources
  std::set<CompoundIdType>::const_iterator userDefinedSourcesBegin() const;
  /// Iterator pointing to the end of the user defined sources
  std::set<CompoundIdType>::const_iterator userDefinedSourcesEnd() const;
  /// Get number of user-defined sources
  auto nbUserDefinedSources() const;
  /// Whether a given compound is a user-defined source
  bool isUserDefinedSource(CompoundIdType id) const;
  /// Iterator pointing to the begin of the bootstrap compounds
  std::set<CompoundIdType>::const_iterator bootstrapCompoundsBegin() const;
  /// Iterator pointing to the end of the bootstrap compounds
  std::set<CompoundIdType>::const_iterator bootstrapCompoundsEnd() const;
  /// Get number of bootstrap compounds
  auto nbBootstrapCompounds() const;
  /// Whether a given compound is a bootstrap compound
  bool isBootstrapCompound(CompoundIdType id) const;
  /// Get consider-topological-sources flag
  bool considerTopologicalSources() const;
  /// Get consider-boundary-compounds-as-sources flag
  bool considerBoundaryCompoundsAsSources() const;
  /// Get constraint type
  const ConstraintType& constraintType() const;
  /// Get epsilon for the target constraint
  const double& epsilon() const;
  /// Get add-factory-reactions flag
  bool addFactoryReactions() const;

  /* **************************************************************************
   * **************************************************************************
   *                        Data variables
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// The metabolic network
  std::shared_ptr<const MetabolicNetwork> network_;
  /// The set of targets
  std::set<CompoundIdType> targets_;
  /// The set of user defined sources
  std::set<CompoundIdType> userDefinedSources_;
  /// The set of bootstrap compounds
  std::set<CompoundIdType> bootstrapCompounds_;
  /// Flag to show whether topological sources should be considered
  bool considerTopologicalSources_ = false;
  /// Flag to show whether boundary compounds should be considered as sources
  bool considerBoundaryCompoundsAsSources_ = false;
  /// Constraint type
  ConstraintType constraintType_ = ConstraintType::STEADYSTATE;
  /// Epsilon for the target constraint
  double epsilon_ = 0.1;
  /// Flag to show whether factory reaction are added to the minPS solution
  bool addFactoryReactions_ = false;
};
}  // namespace metnetlib

#include "MinimalPrecursorSetsInput.hpp"
#endif  // METNETLIB_MINIMALPRECURSORSETSINPUT_H
