/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 13.12.17.
//

#ifndef METNETLIB_MINIMALPRECURSORSETENUMERATOR_H
#define METNETLIB_MINIMALPRECURSORSETENUMERATOR_H

#include <vector>

#include "utils/MinimalPrecursorSetsInput.h"
#include "algorithms/utils/MIPSolver.h"
#include "PrecursorSet.h"

namespace metnetlib {

template<typename MetabolicNetwork>
class MinimalPrecursorSetEnumerator {
  /* **************************************************************************
   * **************************************************************************
   *                             Alias
   * **************************************************************************
   ***************************************************************************/
  using PrecursorSet_t = PrecursorSet<typename MetabolicNetwork::Compound_t,
                                      typename MetabolicNetwork::Reaction_t>;

  /* **************************************************************************
   * **************************************************************************
   *                        Constructor/Destructor (Rule of five)
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Default constructor
  MinimalPrecursorSetEnumerator() = delete;
  /// Constructor
  MinimalPrecursorSetEnumerator(
      const MIPSolver& mipSolver,
      const MinimalPrecursorSetsInput<MetabolicNetwork>& input);
  /// Destructor
  virtual ~MinimalPrecursorSetEnumerator() = default;
  /// Copy constructor
  MinimalPrecursorSetEnumerator(const MinimalPrecursorSetEnumerator&);
  /// Move constructor
  MinimalPrecursorSetEnumerator(MinimalPrecursorSetEnumerator&&) = delete;
  /// Copy assignment
  MinimalPrecursorSetEnumerator& operator=(
      const MinimalPrecursorSetEnumerator&) = delete;
  /// Move assignment
  MinimalPrecursorSetEnumerator& operator=(
      MinimalPrecursorSetEnumerator&&) = delete;

  /* **************************************************************************
   * **************************************************************************
   *                        Public member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Get optimization problem type
  const MIPSolver& mipSolver() const;
  /// Set optimization problem type
  void setMipSolver(const MIPSolver&);
  /// Enumerate all minimal precursor sets for the given input
  std::vector<PrecursorSet_t> enumerate();
  /// Retrieve preprocessed input
  const auto& prepocessed_input() const {return prepocessedInput_;}
  /// Retrieve preprocessed network
  const auto& preprocessedNetwork() const {return preprocessedNetwork_;}

  /* **************************************************************************
   * **************************************************************************
   *                        Protected member functions
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Preprocessing
  void preprocessingMinimalPrecursorSets();

  #ifdef USE_CPLEX
  /// Enumerate with cplex
  std::vector<PrecursorSet_t> enumerateWithCplex() const;
  #endif  // USE_CPLEX

  /* **************************************************************************
   * **************************************************************************
   *                        Data variables
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Used Mixed integer programming solver
  MIPSolver mipSolver_;
  /// MinimalPrecursorSetsInput
  const MinimalPrecursorSetsInput<MetabolicNetwork> input_;
  /// Copy of input_ that will be preprocessed
  MinimalPrecursorSetsInput<MetabolicNetwork> prepocessedInput_;
  /// Copy of the metabolic network that will be preprocessed
  MetabolicNetwork preprocessedNetwork_;
};
}  // namespace metnetlib

#include "MinimalPrecursorSetEnumerator.hpp"

#endif  // METNETLIB_MINIMALPRECURSORSETENUMERATOR_H
