#!/bin/sh

# Parse valgrind output file for a memory leak.
# If there is a memory leak, then one can find something like
# 'definitely lost: 1,088 bytes in 17 blocks'.
# If there is no memory leak, either one can find 'definitely lost: 0 bytes' or
# 'ERROR SUMMARY: 0 errors from 0 contexts' in the file. For this reason we parse
# the valgrind logfile for an error with grep -q "definitely lost: [1-9].* bytes".
# Argument: valgrind output file
# Return: 1 if there is a memory leak, 0 otherwise
if grep -q "definitely lost: [1-9].* bytes" $1; then
        printf "\nMemory leaks!!! Check valgrind output in $1\n"
        exit 1
fi

