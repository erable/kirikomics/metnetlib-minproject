/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include "gtest/gtest.h"

#include "metnetlib.h"

class TestAlgorithmUtils : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

 private:
  std::streambuf* storedStreambuf_;
};

TEST_F(TestAlgorithmUtils, Test_topologicalSources) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c", 3.0, true);
  // Add compounds
  const auto& c1 = metNet.addCompound("C1", {compartment, true, false, true});
  const auto& c2 = metNet.addCompound("C2", {compartment, true, false, true});
  const auto& c3 = metNet.addCompound("C3", {compartment, true, false, true});

  // Add irreversible reaction C1 -> C2
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  // Add reversible reaction C3 <-> C2
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});

  // Get topological sources of the network: C1, C3
  auto topologicalSources = metnetlib::topological_sources(metNet);
  ASSERT_EQ(topologicalSources.size(), (size_t) 2);
  // C1 is a topological source
  ASSERT_NE(std::find(topologicalSources.begin(), topologicalSources.end(), c1),
            topologicalSources.end());
  // C2 is NOT a topological source
  ASSERT_EQ(std::find(topologicalSources.begin(), topologicalSources.end(), c2),
            topologicalSources.end());
  // C3 is a topological source
  ASSERT_NE(std::find(topologicalSources.begin(), topologicalSources.end(), c3),
            topologicalSources.end());
}

TEST_F(TestAlgorithmUtils, Test_removeReactionsWithoutSubstratesOrProdcucts) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c", 3.0, true);
  // Add compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});

  // Add irreversible reaction C1 -> C2
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  // Add reversible reaction C3 <-> C2
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  // Add irreversible reaction C4 ->
  metNet.addReaction(hglib::NAME, {{"C4"}, {}},
                     {"R3", false, false, {{1, true}}, {}, 0, 1000});
  // Add irreversible reaction  -> C1
  metNet.addReaction(hglib::NAME, {{}, {"C1"}},
                     {"R4", false, false, {}, {{1, true}}, 0, 1000});
  // Add reversible reaction C3 <->
  metNet.addReaction(hglib::NAME, {{"C3"}, {}},
                     {"R5", true, false, {{1, true}}, {}, -1000, 1000});
  // Add reversible reaction <-> C2
  metNet.addReaction(hglib::NAME, {{}, {"C2"}},
                     {"R6", true, false, {}, {{1, true}}, -1000, 1000});

  // remove reactions without substrates or products (R1, R2 remain)
  metnetlib::remove_reactions_without_substrate_or_product(&metNet);
  ASSERT_EQ(metNet.nbReactions(), (size_t) 2);
  ASSERT_EQ(metNet.nbReversibleReactions(), (size_t) 1);
  ASSERT_NE(metNet.reactionBySbmlId("R1"), nullptr);
  ASSERT_NE(metNet.reactionBySbmlId("R2"), nullptr);
  ASSERT_NE(metNet.reactionBySbmlId(std::string("R2") +
                                        metnetlib::suffixReverseReaction),
            nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId("R3"), nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId("R4"), nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId("R5"), nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId(std::string("R5") +
                                        metnetlib::suffixReverseReaction),
            nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId("R6"), nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId(std::string("R6") +
                                        metnetlib::suffixReverseReaction),
            nullptr);
}

TEST_F(TestAlgorithmUtils, Test_removeReactionsWithoutSubstrate) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c", 3.0, true);
  // Add compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});

  // Add irreversible reaction C1 -> C2
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  // Add reversible reaction C3 <-> C2
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  // Add irreversible reaction C4 ->
  metNet.addReaction(hglib::NAME, {{"C4"}, {}},
                     {"R3", false, false, {{1, true}}, {}, 0, 1000});
  // Add irreversible reaction  -> C1
  metNet.addReaction(hglib::NAME, {{}, {"C1"}},
                     {"R4", false, false, {}, {{1, true}}, 0, 1000});
  // Add reversible reaction C3 <->
  metNet.addReaction(hglib::NAME, {{"C3"}, {}},
                     {"R5", true, false, {{1, true}}, {}, -1000, 1000});
  // Add reversible reaction <-> C2
  metNet.addReaction(hglib::NAME, {{}, {"C2"}},
                     {"R6", true, false, {}, {{1, true}}, -1000, 1000});

  // remove reactions without substrate
  metnetlib::remove_reactions_without_substrate(&metNet);
  ASSERT_EQ(metNet.nbReactions(), (size_t) 5);
  ASSERT_EQ(metNet.nbReversibleReactions(), (size_t) 1);
  ASSERT_NE(metNet.reactionBySbmlId("R1"), nullptr);
  ASSERT_NE(metNet.reactionBySbmlId("R2"), nullptr);
  ASSERT_NE(metNet.reactionBySbmlId(std::string("R2") +
                                        metnetlib::suffixReverseReaction),
            nullptr);
  ASSERT_NE(metNet.reactionBySbmlId("R3"), nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId("R4"), nullptr);
  ASSERT_NE(metNet.reactionBySbmlId("R5"), nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId(std::string("R5") +
                                        metnetlib::suffixReverseReaction),
            nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId("R6"), nullptr);
  ASSERT_NE(metNet.reactionBySbmlId(std::string("R6") +
                                        metnetlib::suffixReverseReaction),
            nullptr);
}

TEST_F(TestAlgorithmUtils, Test_are_identical_reactions) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c", 3.0, true);
  // Add compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});

  // Add irreversible reaction C1 -> C2
  const auto& r1 = metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  // Add reversible reaction C3 <-> C2
  const auto& r2 = metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  // Add reversible reaction C3 <-> C2 (duplicate of R2)
  const auto& r3 = metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R3", true, false, {{1, true}}, {{1, true}}, -100, 100});
  // Add reversible reaction C1 <-> C2 (duplicate of R1)
  const auto& r4 = metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                                      {"R4", true, false, {{1, true}},
                                       {{1, true}}, -100, 100});
  // Add reactions with different stoichiometries
  const auto& r5 = metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                                      {"R5", false, false, {{2, true}},
                                       {{1, true}}, 0, 1000});
  const auto& r6 = metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                                      {"R6", false, false, {{1, true}},
                                       {{2, true}}, 0, 1000});

  // Check are_identical_reactions
  ASSERT_FALSE(metnetlib::are_identical_reactions(metNet, *r1, *r2));
  ASSERT_TRUE(metnetlib::are_identical_reactions(metNet, *r2, *r3));
  ASSERT_TRUE(metnetlib::are_identical_reactions(
      metNet, *(metNet.reversibleReaction((r2)->id())),
      *(metNet.reversibleReaction((r3)->id()))));
  ASSERT_TRUE(metnetlib::are_identical_reactions(metNet, *r1, *r4));
  ASSERT_FALSE(metnetlib::are_identical_reactions(
      metNet, *r1, *(metNet.reversibleReaction((r4)->id()))));
  ASSERT_FALSE(metnetlib::are_identical_reactions(metNet, *r1, *r5));
  ASSERT_FALSE(metnetlib::are_identical_reactions(metNet, *r1, *r6));

  // Check that the order of the arguments plays no role
  ASSERT_FALSE(metnetlib::are_identical_reactions(metNet, *r2, *r1));
  ASSERT_TRUE(metnetlib::are_identical_reactions(metNet, *r3, *r2));
  ASSERT_TRUE(metnetlib::are_identical_reactions(
      metNet, *(metNet.reversibleReaction((r3)->id())),
      *(metNet.reversibleReaction((r2)->id()))));
  ASSERT_TRUE(metnetlib::are_identical_reactions(metNet, *r4, *r1));
  ASSERT_FALSE(metnetlib::are_identical_reactions(
      metNet, *(metNet.reversibleReaction((r4)->id())), *r1));
  ASSERT_FALSE(metnetlib::are_identical_reactions(metNet, *r5, *r1));
  ASSERT_FALSE(metnetlib::are_identical_reactions(metNet, *r6, *r1));

  // Check exceptions
  // Build another network
  metnetlib::MetabolicNetwork<> metNet2;
  // Add a compartment
  const auto &compartment2 = metNet2.addCompartment("c", 3.0, true);
  // Add compounds
  metNet2.addCompound("C1", {compartment2, true, false, true});
  metNet2.addCompound("C2", {compartment2, true, false, true});
  metNet2.addCompound("C3", {compartment2, true, false, true});

  // Add irreversible reaction C1 -> C2
  const auto& r1_2 = metNet2.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                                      {"R1", false, false, {{1, true}},
                                       {{1, true}}, 0, 1000});
  // Add reversible reaction C3 <-> C2
  const auto& r2_2 = metNet2.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                                      {"R2", true, false, {{1, true}},
                                       {{1, true}}, -1000, 1000});

  // Test exception
  ASSERT_THROW(metnetlib::are_identical_reactions(metNet, *r1, *r1_2),
               std::invalid_argument);
  ASSERT_THROW(metnetlib::are_identical_reactions(metNet, *r2_2, *r2),
               std::invalid_argument);
}

TEST_F(TestAlgorithmUtils, Test_remove_duplicate_reactions) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c", 3.0, true);
  // Add compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});

  // Add irreversible reaction C1 -> C2
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  // Add reversible reaction C3 <-> C2
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -100, 100});
  // Add reversible reaction C3 <-> C2 (duplicate of R2)
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R3", true, false, {{1, true}}, {{1, true}}, -100, 100});
  // Add reversible reaction C1 <-> C2 (duplicate of R1)
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R4", true, false, {{1, true}}, {{1, true}}, -100, 100});
  // Add reactions with different stoichiometries
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R5", false, false, {{2, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R6", false, false, {{1, true}}, {{2, true}}, 0, 1000});
  // Add a irreversible reaction C3 -> C2 (duplicate of R2, R3)
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R7", false, false, {{1, true}}, {{1, true}}, 0, 100});

  // Remove duplicate reactions
  metnetlib::remove_duplicate_reactions(&metNet);
  // Check that there are still R2, R4, R5, R6
  // R1, R3, R7 are removed; R1 is removed because it is irreversible and its
  // duplicate reaction R4 is reversible (irreversible reactions are removed in
  // this case); the same holds for R7 (duplicate of R2)
  ASSERT_EQ(metNet.reactionBySbmlId("R1"), nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId("R3"), nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId(std::string("R3") +
                                        metnetlib::suffixReverseReaction),
            nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId("R7"), nullptr);

  ASSERT_NE(metNet.reactionBySbmlId("R2"), nullptr);
  ASSERT_NE(metNet.reactionBySbmlId(std::string("R2") +
                                        metnetlib::suffixReverseReaction),
            nullptr);
  ASSERT_NE(metNet.reactionBySbmlId("R4"), nullptr);
  ASSERT_NE(metNet.reactionBySbmlId(std::string("R4") +
                                        metnetlib::suffixReverseReaction),
            nullptr);
  ASSERT_NE(metNet.reactionBySbmlId("R5"), nullptr);
  ASSERT_NE(metNet.reactionBySbmlId("R6"), nullptr);
}

TEST_F(TestAlgorithmUtils, Test_remove_isolated_compounds) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c", 3.0, true);
  // Add compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});

  // Add irreversible reaction C1 -> C2
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  // Add reversible reaction C3 <-> C2
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -100, 100});

  // Remove isolated compounds (C4)
  metnetlib::remove_isolated_compounds(&metNet);
  // Check that C4 is not present anymore
  ASSERT_EQ(metNet.compoundBySbmlId("C4"), nullptr);
  // The other compounds are still present
  ASSERT_NE(metNet.compoundBySbmlId("C1"), nullptr);
  ASSERT_NE(metNet.compoundBySbmlId("C2"), nullptr);
  ASSERT_NE(metNet.compoundBySbmlId("C3"), nullptr);

  // Remove R1 and remove again isolated compounds (C1)
  metNet.removeReaction(0);
  // Remove isolated compounds (C1)
  metnetlib::remove_isolated_compounds(&metNet);
  // Check that C1 is not present anymore
  ASSERT_EQ(metNet.compoundBySbmlId("C1"), nullptr);
  // The other compounds are still present
  ASSERT_NE(metNet.compoundBySbmlId("C2"), nullptr);
  ASSERT_NE(metNet.compoundBySbmlId("C3"), nullptr);
}
