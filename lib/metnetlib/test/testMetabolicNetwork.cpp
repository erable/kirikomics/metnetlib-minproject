/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 07.11.17.
//

#include <string>
#include <tuple>
#include <utility>      // std::pair
#include <vector>

#include "gtest/gtest.h"

#include "metnetlib.h"

class TestMetabolicNetwork : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

  // a compartment
  std::tuple<std::string, double, bool>
      compartment = std::make_tuple("c", 1.0, false);

 private:
  std::streambuf* storedStreambuf_;
};

TEST_F(TestMetabolicNetwork, Test_addCompartment) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  metNet.addCompartment(std::get<0>(compartment),
                        std::get<1>(compartment),
                        std::get<2>(compartment));
  // can't add a compartment with the same name
  ASSERT_THROW(metNet.addCompartment("c", 3.0, true),
               std::invalid_argument);
  // retrieve compartment and check its member variables
  const auto& compartment = metNet.compartmentBySbmlId("c");
  ASSERT_EQ(compartment->id(), (size_t) 0);
  ASSERT_EQ(compartment->sbmlId().compare("c"), 0);
  ASSERT_EQ(compartment->size(), (size_t) 1.0);
  ASSERT_FALSE(compartment->isConstant());
  // Test exception throw on invalid compartment names
  ASSERT_THROW(metNet.addCompartment("", 1.0, true),
               std::invalid_argument);
  ASSERT_THROW(metNet.addCompartment("1", 1.0, true),
               std::invalid_argument);
  ASSERT_THROW(metNet.addCompartment("1_c", 1.0, true),
               std::invalid_argument);
  ASSERT_THROW(metNet.addCompartment("-c", 1.0, true),
               std::invalid_argument);
  // Test exception throw on negative size
  ASSERT_THROW(metNet.addCompartment("negSize", -1.0, true),
               std::invalid_argument);
  // Test some valid compartment names
  // According to SBML specification, the name must fit this regular
  // expression: [a-zA-Z_][a-zA-Z0-9_]*
  ASSERT_NO_THROW(metNet.addCompartment("_c", 1.0, true));
  ASSERT_NO_THROW(metNet.addCompartment("__c", 1.0, true));
  ASSERT_NO_THROW(metNet.addCompartment("c_", 1.0, true));
  ASSERT_NO_THROW(metNet.addCompartment("C", 1.0, true));
  ASSERT_NO_THROW(metNet.addCompartment("C_", 1.0, true));
  ASSERT_NO_THROW(metNet.addCompartment("c_1", 1.0, true));
  ASSERT_NO_THROW(metNet.addCompartment("_c_a_b_c_0", 1.0, true));
  ASSERT_NO_THROW(metNet.addCompartment("_", 1.0, true));
  ASSERT_NO_THROW(metNet.addCompartment("__", 1.0, true));
}

TEST_F(TestMetabolicNetwork, Test_compartmentById) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  metNet.addCompartment(std::get<0>(compartment),
                        std::get<1>(compartment),
                        std::get<2>(compartment));
  metNet.addCompartment("c1", 1.0, false);
  metNet.addCompartment("c2", 2.0, true);
  // Get compartment by Id and SbmlId
  ASSERT_EQ(metNet.compartmentById(0),
            metNet.compartmentBySbmlId(std::get<0>(compartment)));
  ASSERT_EQ(metNet.compartmentById(1),
            metNet.compartmentBySbmlId("c1"));
  ASSERT_EQ(metNet.compartmentById(2),
            metNet.compartmentBySbmlId("c2"));
  // Test exceptions
  ASSERT_THROW(metNet.compartmentById(3), std::out_of_range);
  // Test unknown compartment Sbml Id
  ASSERT_EQ(metNet.compartmentBySbmlId("unknwownCompartment"), nullptr);
}

TEST_F(TestMetabolicNetwork, Test_nbCompartments) {
  metnetlib::MetabolicNetwork<> metNet;
  // Check number of compartments
  size_t expectedNbCompartments(0);
  ASSERT_EQ(metNet.nbCompartments(), expectedNbCompartments);
  metNet.addCompartment("c1", 1.0, false);
  ASSERT_EQ(metNet.nbCompartments(), ++expectedNbCompartments);
  metNet.addCompartment("c2", 2.0, true);
  ASSERT_EQ(metNet.nbCompartments(), ++expectedNbCompartments);
  // Number of compartments remains the same as the compartment c2 already
  // exists
  try {
    metNet.addCompartment("c2", 2.0, true);
  } catch (const std::invalid_argument&) {}
  ASSERT_EQ(metNet.nbCompartments(), expectedNbCompartments);
}

TEST_F(TestMetabolicNetwork, Test_compartmentIterator) {
  typedef metnetlib::MetabolicNetwork<> metNetwork;
  metnetlib::MetabolicNetwork<> metNet;
  metNet.addCompartment("c1", 0.0, false);
  metNet.addCompartment("c2", 1.0, false);
  metNet.addCompartment("c3", 2.0, false);
  metNet.addCompartment("c4", 3.0, false);

  // iterate over compartments
  metNetwork::compartment_iterator it, end;
  it = metNet.compartmentsBegin();
  end = metNet.compartmentsEnd();
  for (const auto& compartment : metNet.compartments()) {
    ASSERT_EQ(compartment->id(), (*it)->id());
    ASSERT_EQ(compartment->sbmlId(), (*it)->sbmlId());
    ASSERT_EQ(compartment->size(), (*it)->size());
    ASSERT_EQ(compartment->isConstant(), (*it)->isConstant());
    ++it;
  }
  ASSERT_EQ(it, end);
}

TEST_F(TestMetabolicNetwork, Test_addCompound) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  auto compartment = metNet.addCompartment("c1", 1.0, false);
  // Add a compound
  metNet.addCompound("C1", {compartment, true, false, true});
  const auto& compound = metNet.compoundById(0);
  ASSERT_EQ(compound->sbmlId().compare("C1"), 0);
  ASSERT_EQ(compound->getCompartment(), compartment);
  ASSERT_EQ(compound->hasOnlySubstanceUnits(), true);
  ASSERT_EQ(compound->hasBoundaryCondition(), false);
  ASSERT_EQ(compound->isConstant(), true);

  // Check exceptions
  // Compound with the same name already exists
  ASSERT_THROW(metNet.addCompound("C1", {compartment, false, true, false}),
               std::invalid_argument);
  // Exception throw on invalid compound names
  ASSERT_THROW(metNet.addCompound("", {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(metNet.addCompound("1", {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(metNet.addCompound("1_c", {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(metNet.addCompound("-c", {compartment, false, true, false}),
               std::invalid_argument);
  // Exception on invalid compartment pointer
  ASSERT_THROW(metNet.addCompound("C2", {nullptr, false, true, false}),
               std::invalid_argument);

  // compartment pointer taken from a different network
  metnetlib::MetabolicNetwork<> metNet2;
  // Add a compartment (identical to the one in metNet)
  auto compartment2 = metNet2.addCompartment("c1", 1.0, false);
  // Add a compartment
  auto compartment3 = metNet2.addCompartment("c2", 1.0, false);
  ASSERT_THROW(metNet.addCompound("C2", {compartment2, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(metNet.addCompound("C2", {compartment3, false, true, false}),
               std::out_of_range);
}

TEST_F(TestMetabolicNetwork, Test_compoundById) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compound
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // Get compound by Id and SbmlId
  ASSERT_EQ(metNet.compoundById(0),
            metNet.compoundBySbmlId("C1"));
  ASSERT_EQ(metNet.compoundById(1),
            metNet.compoundBySbmlId("C2"));
  ASSERT_EQ(metNet.compoundById(2),
            metNet.compoundBySbmlId("C3"));
  // Test exceptions
  ASSERT_THROW(metNet.compoundById(3), std::out_of_range);
  // Test unknown compound Sbml Id
  ASSERT_EQ(metNet.compoundBySbmlId("unknwownCompound"), nullptr);
}

TEST_F(TestMetabolicNetwork, Test_nbCompounds) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Check number of compounds
  size_t expectedNbCompounds(0);
  ASSERT_EQ(metNet.nbCompounds(), expectedNbCompounds);
  metNet.addCompound("C1", {compartment, true, false, true});
  ASSERT_EQ(metNet.nbCompounds(), ++expectedNbCompounds);
  metNet.addCompound("C2", {compartment, true, false, true});
  ASSERT_EQ(metNet.nbCompounds(), ++expectedNbCompounds);
  metNet.addCompound("C3", {compartment, true, false, true});
  ASSERT_EQ(metNet.nbCompounds(), ++expectedNbCompounds);

  // Number of compounds remains the same if addCompound does not add a compound
  try {
    metNet.addCompound("C1", {compartment, false, true, false});
    metNet.addCompound("", {compartment, false, true, false});
  } catch (const std::invalid_argument&) {}
  ASSERT_EQ(metNet.nbCompounds(), expectedNbCompounds);
}

TEST_F(TestMetabolicNetwork, Test_compoundIterator) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // iterate over compounds
  MetNetwork::compound_iterator it, end;
  it = metNet.compoundsBegin();
  end = metNet.compoundsEnd();
  for (const auto& compound : metNet.compounds()) {
    ASSERT_EQ(compound->id(), (*it)->id());
    ASSERT_EQ(compound->sbmlId(), (*it)->sbmlId());
    ASSERT_EQ(compound->getCompartment(), (*it)->getCompartment());
    ASSERT_EQ(compound->hasOnlySubstanceUnits(),
              (*it)->hasOnlySubstanceUnits());
    ASSERT_EQ(compound->hasBoundaryCondition(), (*it)->hasBoundaryCondition());
    ASSERT_EQ(compound->isConstant(), (*it)->isConstant());
    ++it;
  }
  ASSERT_EQ(it, end);
}

TEST_F(TestMetabolicNetwork, Test_addReaction) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  auto compartment = metNet.addCompartment("c1", 1.0, false);
  // Add some compounds
  const auto& c1 = metNet.addCompound("C1", {compartment, true, false, true});
  const auto& c2 = metNet.addCompound("C2", {compartment, true, false, true});
  const auto& c3 = metNet.addCompound("C3", {compartment, true, false, true});
  const auto& c4 = metNet.addCompound("C4", {compartment, true, false, true});
  const auto& c5 = metNet.addCompound("C5", {compartment, true, false, true});
  const auto& c6 = metNet.addCompound("C6", {compartment, true, false, true});
  const auto& c7 = metNet.addCompound("C7", {compartment, true, false, true});
  ASSERT_EQ(metNet.nbCompounds(), (size_t) 7);

  // add reaction
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  // check attributes of added reaction
  const auto& reaction = metNet.reactionById(0);
  ASSERT_NE(reaction, nullptr);
  ASSERT_EQ(reaction->id(), 0);
  ASSERT_EQ(reaction->sbmlId(), "R1");
  ASSERT_TRUE(metNet.isReversible(reaction->id()));
  ASSERT_FALSE(reaction->isFast());
  ASSERT_EQ(metNet.substrateStoichiometry(c1->id(), reaction->id()), 1);
  ASSERT_EQ(metNet.substrateStoichiometry(c2->id(), reaction->id()), 2);
  ASSERT_EQ(metNet.productStoichiometry(c3->id(), reaction->id()), 1);
  ASSERT_EQ(reaction->lowerBound(), 0);
  ASSERT_EQ(reaction->upperBound(), 1000);
  // reverse reaction
  const auto& revReaction = metNet.reactionById(1);
  ASSERT_NE(revReaction, nullptr);
  ASSERT_EQ(revReaction->id(), 1);
  ASSERT_EQ(revReaction->sbmlId(), std::string("R1") +
      metnetlib::suffixReverseReaction);
  ASSERT_TRUE(metNet.isReversible(revReaction->id()));
  ASSERT_FALSE(revReaction->isFast());
  ASSERT_EQ(metNet.productStoichiometry(c1->id(), revReaction->id()), 1);
  ASSERT_EQ(metNet.productStoichiometry(c2->id(), revReaction->id()), 2);
  ASSERT_EQ(metNet.substrateStoichiometry(c3->id(), revReaction->id()), 1);
  ASSERT_EQ(revReaction->lowerBound(), 0);
  ASSERT_EQ(revReaction->upperBound(), 1000);
  // Check that each reaction is the reverse reaction of the other one
  ASSERT_EQ(metNet.reversibleReaction(reaction->id()), revReaction);
  ASSERT_EQ(metNet.reversibleReaction(revReaction->id()), reaction);


  // add another reaction
  metNet.addReaction(hglib::NAME, {{"C4", "C5"}, {"C6", "C7"}},
                     {"R2", false, false, {{2, true}, {2, true}},
                                          {{3, true}, {1, true}}, 0, 1000});
  const auto& reaction2 = metNet.reactionById(2);
  ASSERT_NE(reaction2, nullptr);
  ASSERT_EQ(reaction2->id(), 2);
  ASSERT_EQ(reaction2->sbmlId(), "R2");
  ASSERT_FALSE(metNet.isReversible(reaction2->id()));
  ASSERT_FALSE(reaction2->isFast());
  ASSERT_EQ(metNet.substrateStoichiometry(c4->id(), reaction2->id()), 2);
  ASSERT_EQ(metNet.substrateStoichiometry(c5->id(), reaction2->id()), 2);
  ASSERT_EQ(metNet.productStoichiometry(c6->id(), reaction2->id()), 3);
  ASSERT_EQ(metNet.productStoichiometry(c7->id(), reaction2->id()), 1);
  ASSERT_EQ(reaction2->lowerBound(), 0);
  ASSERT_EQ(reaction2->upperBound(), 1000);
  // check that the reversible reaction is a nullptr
  ASSERT_EQ(metNet.reversibleReaction(reaction2->id()), nullptr);

  // Check exceptions of invalid reactions
  // Add a reaction with an already existing Sbml Id
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C4"}, {"C7"}},
                                  {"R2", true, true, {{1, true}}, {{2, true}},
                                   -100, 100}),
               std::invalid_argument);
  // Add a reaction with undefined compound (substrate)
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C8"}, {"C7"}},
                                  {"R3", true, true, {{1, true}}, {{2, true}},
                                   -100, 100}),
               std::invalid_argument);
  // Add a reaction with undefined compound (product)
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C7"}, {"C8"}},
                                  {"R3", true, true, {{1, true}}, {{2, true}},
                                   -100, 100}),
               std::invalid_argument);
  // Double check: Add 'C8' and insert reaction again (with success)
  metNet.addCompound("C8", {compartment, true, false, true});
  metNet.addReaction(hglib::NAME, {{"C8"}, {"C7"}},
                     {"R3", true, true, {{1, true}}, {{2, true}}, -100, 100});
  ASSERT_NE(metNet.reactionById(2), nullptr);
  // Add a reaction R4 with too less substrate stoichiometry entries
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                  {"R4", false, false, {{2, true}},
                                   {{3, true}, {1, true}}, 0, 1000}),
               std::invalid_argument);
  // Add a reaction R4 with too many substrate stoichiometry entries
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                  {"R4", false, false,
                                   {{1, true}, {2, true}, {2, true}},
                                   {{3, true}, {1, true}}, 0, 1000}),
               std::invalid_argument);
  // Add a reaction R4 with too less product stoichiometry entries
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                  {"R4", false, false, {{1, true}, {2, true}},
                                   {{1, true}}, 0, 1000}),
               std::invalid_argument);
  // Add a reaction R4 with too many product stoichiometry entries
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                  {"R4", false, false,
                                   {{2, true}, {2, true}},
                                   {{3, true}, {1, true}, {2, true}}, 0, 1000}),
               std::invalid_argument);
  // Add R4 with the right number of stoichiometry entries. Will have id=3.
  metNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                     {"R4", false, false,
                      {{2, true}, {2, true}}, {{3, true}, {1, true}}, 0, 1000});
  ASSERT_NE(metNet.reactionById(3), nullptr);
  // Add a irreversible reaction but with negative bound
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                  {"R5", false, false,
                                   {{2, true}}, {{3, true}}, -10, 1000}),
               std::invalid_argument);

  // Check exception on invalid Sbml Id
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                  {"", false, false,
                                   {{2, true}, {2, true}},
                                   {{3, true}, {1, true}, {2, true}}, 0, 1000}),
               std::invalid_argument);
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                  {"5", false, false,
                                   {{2, true}, {2, true}},
                                   {{3, true}, {1, true}, {2, true}}, 0, 1000}),
               std::invalid_argument);
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                  {"5_c", false, false,
                                   {{2, true}, {2, true}},
                                   {{3, true}, {1, true}, {2, true}}, 0, 1000}),
               std::invalid_argument);
  ASSERT_THROW(metNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                  {"-R5", false, false,
                                   {{2, true}, {2, true}},
                                   {{3, true}, {1, true}, {2, true}}, 0, 1000}),
               std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_reactionById) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compound
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});

  // Add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C3", "C4"}},
                     {"R2", false, false, {{1, true}}, {{2, true}, {1, true}},
                      0, 1000});
  metNet.addReaction(hglib::NAME, {{"C2", "C5"}, {"C1", "C3"}},
                     {"R3", true, false, {{1, true}, {2, true}},
                           {{1, true}, {1, true}}, -1000, 1000});

  // Get reaction by Id and SbmlId
  ASSERT_EQ(metNet.reactionById(0),
            metNet.reactionBySbmlId("R1"));
  ASSERT_EQ(metNet.reactionById(1),
            metNet.reactionBySbmlId(std::string("R1") +
                                        metnetlib::suffixReverseReaction));
  ASSERT_EQ(metNet.reactionById(2),
            metNet.reactionBySbmlId("R2"));
  ASSERT_EQ(metNet.reactionById(3),
            metNet.reactionBySbmlId("R3"));
  ASSERT_EQ(metNet.reactionById(4),
            metNet.reactionBySbmlId(std::string("R3") +
                                        metnetlib::suffixReverseReaction));

  // Test exceptions
  ASSERT_THROW(metNet.reactionById(5), std::out_of_range);
  // Test unknown compound Sbml Id
  ASSERT_EQ(metNet.reactionBySbmlId("unknwownReaction"), nullptr);
}

TEST_F(TestMetabolicNetwork, Test_nbReactions) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compound
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});

  // Check number of reactions
  size_t expectedNbReactions(0);
  ASSERT_EQ(metNet.nbReactions(), expectedNbReactions);
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  ASSERT_EQ(metNet.nbReactions(), ++expectedNbReactions);
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C3", "C4"}},
                     {"R2", true, false, {{1, true}}, {{2, true}, {1, true}},
                      -1000, 1000});
  ASSERT_EQ(metNet.nbReactions(), ++expectedNbReactions);
  metNet.addReaction(hglib::NAME, {{"C2", "C5"}, {"C1", "C3"}},
                     {"R3", true, false, {{1, true}, {2, true}},
                      {{1, true}, {1, true}}, -1000, 1000});
  ASSERT_EQ(metNet.nbReactions(), ++expectedNbReactions);
  metNet.addReaction(hglib::NAME, {{"C2", "C5"}, {"C1", "C3"}},
                     {"R4", false, false, {{1, true}, {2, true}},
                      {{1, true}, {1, true}}, 0, 1000});
  ASSERT_EQ(metNet.nbReactions(), ++expectedNbReactions);

  // Number of reactions remains the same if addReaction does not add a reaction
  try {
    metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                       {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                        -1000, 1000});
    metNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                       {"-R5", false, false, {{2, true}, {2, true}},
                        {{3, true}, {1, true}, {2, true}}, 0, 1000});
  } catch (const std::invalid_argument&) {}
  ASSERT_EQ(metNet.nbReactions(), expectedNbReactions);
}

TEST_F(TestMetabolicNetwork, Test_reactionIterator) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});

  // Add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C3", "C4"}},
                     {"R2", true, false, {{1, true}}, {{2, true}, {1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C2", "C5"}, {"C1", "C3"}},
                     {"R3", true, false, {{1, true}, {2, true}},
                      {{1, true}, {1, true}}, -1000, 1000});

  // iterate over reactions
  MetNetwork::reaction_iterator it, end;
  it = metNet.reactionsBegin();
  end = metNet.reactionsEnd();
  for (const auto& reaction : metNet.reactions()) {
    ASSERT_EQ(reaction->id(), (*it)->id());
    ASSERT_EQ(reaction->sbmlId(), (*it)->sbmlId());
    ASSERT_EQ(metNet.isReversible(reaction->id()),
              metNet.isReversible((*it)->id()));
    ASSERT_EQ(reaction->isFast(),
              (*it)->isFast());
    ASSERT_EQ(reaction->lowerBound(), (*it)->lowerBound());
    ASSERT_EQ(reaction->upperBound(), (*it)->upperBound());
    ++it;
  }
  ASSERT_EQ(it, end);
}

TEST_F(TestMetabolicNetwork, Test_hasSubstrates) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});

  // Add some reactions
  const auto& reac1 = metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", true, false, {{1, true}}, {{1, true}},
                      -1000, 1000});
  const auto& reac2 = metNet.addReaction(hglib::NAME, {{}, {"C1"}},
                     {"R2", true, false, {}, {{1, true}},
                      -1000, 1000});
  ASSERT_TRUE(metNet.hasSubstrates(reac1->id()));  // Id: 0; C1 -> C2
  ASSERT_TRUE(metNet.hasSubstrates(1));  // Id: 1; C2 -> C1
  ASSERT_FALSE(metNet.hasSubstrates(reac2->id()));  // Id: 2; -> C1
  ASSERT_TRUE(metNet.hasSubstrates(3));  // Id: 3; C1 ->

  // Test out_of_range exception
  ASSERT_THROW(metNet.hasSubstrates(4), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeReaction(0);
  ASSERT_THROW(metNet.hasSubstrates(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_nbSubstrates) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // Add some reactions
  // without substrates
  metNet.addReaction(hglib::NAME, {{}, {"C1"}},
                     {"R1", false, false, {}, {{1, true}}, 0, 1000});
  // one substrate
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R2", false, false, {{1, true}}, {{1, true}},
                      0, 1000});
  // two substrates
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R3", false, false, {{1, true}, {1, true}}, {{1, true}},
                      0, 1000});

  // Check number of substrates
  for (const auto& reaction : metNet.reactions()) {
    ASSERT_EQ(reaction->id(), metNet.nbSubstrates(reaction->id()));
  }

  // Add reversible reaction and check number of substrates in both directions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R4", true, false, {{1, true}, {1, true}}, {{1, true}},
                      -1000, 1000});
  // forward reaction should have two substrates
  ASSERT_EQ(metNet.nbSubstrates(3), (size_t) 2);
  // backward reaction should have one substrate
  ASSERT_EQ(metNet.nbSubstrates(4), (size_t) 1);

  // Test out_of_range exception
  ASSERT_THROW(metNet.nbSubstrates(5), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeReaction(0);
  ASSERT_THROW(metNet.nbSubstrates(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_substratesIterators) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // Add some reactions
  // without substrates
  metNet.addReaction(hglib::NAME, {{},
                                   {"C1"}},
                     {"R1", true, false, {}, {{1, true}}, -1000, 1000});
  // one substrate
  metNet.addReaction(hglib::NAME, {{"C1"},
                                   {"C2"}},
                     {"R2", true, false, {{1, true}}, {{1, true}},
                      -1000, 1000});
  // two substrates
  metNet.addReaction(hglib::NAME, {{"C1", "C2"},
                                   {"C3"}},
                     {"R3", true, false, {{1, true}, {1, true}}, {{1, true}},
                      -1000, 1000});

  // Iterate over the substrates of each reaction
  for (const auto &reaction : metNet.reactions()) {
    // with compound_iterator
    MetNetwork::compound_iterator it, end;
    it = metNet.substratesBegin(reaction->id());
    end = metNet.substratesEnd(reaction->id());
    // range based for loop
    auto substrates = metNet.substrates(reaction->id());
    for (const auto& substrate : *substrates) {
      ASSERT_EQ(substrate->id(), (*it)->id());
      ASSERT_EQ(substrate, *it);
      ++it;
    }
    ASSERT_EQ(it, end);
  }

  // Test out_of_range exception
  ASSERT_THROW(metNet.substratesBegin(10), std::out_of_range);
  ASSERT_THROW(metNet.substratesEnd(10), std::out_of_range);
  ASSERT_THROW(metNet.substrates(10), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeReaction(0);
  ASSERT_THROW(metNet.substratesBegin(0), std::invalid_argument);
  ASSERT_THROW(metNet.substratesEnd(0), std::invalid_argument);
  ASSERT_THROW(metNet.substrates(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_hasProducts) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});

  // Add some reactions
  const auto& reac1 = metNet.addReaction(
      hglib::NAME, {{"C1"}, {"C2"}},
      {"R1", true, false, {{1, true}}, {{1, true}}, -1000, 1000});
  const auto& reac2 = metNet.addReaction(hglib::NAME, {{"C1"}, {}},
                                         {"R2", true, false, {{1, true}}, {},
                                          -1000, 1000});
  // Check hasProducts
  ASSERT_TRUE(metNet.hasProducts(reac1->id()));  // Id: 0; C1 -> C2
  ASSERT_TRUE(metNet.hasProducts(1));  // Id: 1; C2 -> C1
  ASSERT_FALSE(metNet.hasProducts(reac2->id()));  // Id: 2; C1 ->
  ASSERT_TRUE(metNet.hasProducts(3));  // Id: 3;  -> C1

  // Test out_of_range exception
  ASSERT_THROW(metNet.hasProducts(10), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeReaction(0);
  ASSERT_THROW(metNet.hasProducts(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_nbProducts) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // Add some reactions
  // without products
  metNet.addReaction(hglib::NAME, {{"C1"}, {}},
                     {"R1", false, false, {{1, true}}, {}, 0, 1000});
  // one product
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R2", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  // two products
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2", "C3"}},
                     {"R3", false, false, {{2, true}}, {{1, true}, {1, true}},
                      0, 1000});

  // Check nbProducts
  for (const auto& reaction : metNet.reactions()) {
    ASSERT_EQ(reaction->id(), metNet.nbProducts(reaction->id()));
  }

  // Add reversible reaction and check number of products in both directions
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2", "C3"}},
                     {"R4", true, false, {{2, true}}, {{1, true}, {1, true}},
                      -1000, 1000});
  // Expected number of products in forward reaction: 2
  ASSERT_EQ(metNet.nbProducts(3), (size_t) 2);
  // Expected number of products in backward reaction: 1
  ASSERT_EQ(metNet.nbProducts(4), (size_t) 1);

  // Test out_of_range exception
  ASSERT_THROW(metNet.nbProducts(10), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeReaction(0);
  ASSERT_THROW(metNet.nbProducts(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_productsIterators) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // Add some reactions
  // without products
  metNet.addReaction(hglib::NAME, {{"C1"}, {}},
                     {"R1", true, false, {{1, true}}, {}, -1000, 1000});
  // one product
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  // two products
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2", "C3"}},
                     {"R3", true, false, {{2, true}}, {{1, true}, {1, true}},
                      -1000, 1000});

  // Iterate over the products of each reaction
  for (const auto& reaction : metNet.reactions()) {
    // with compound_iterator
    MetNetwork::compound_iterator it, end;
    it = metNet.productsBegin(reaction->id());
    end = metNet.productsEnd(reaction->id());
    // range based for loop
    auto products = metNet.products(reaction->id());
    for (const auto& product : *products) {
      ASSERT_EQ(product->id(), (*it)->id());
      ASSERT_EQ(product, *it);
      ++it;
    }
    ASSERT_EQ(it, end);
  }

  // Test out_of_range exception
  ASSERT_THROW(metNet.productsBegin(10), std::out_of_range);
  ASSERT_THROW(metNet.productsEnd(10), std::out_of_range);
  ASSERT_THROW(metNet.products(10), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeReaction(0);
  ASSERT_THROW(metNet.productsBegin(0), std::invalid_argument);
  ASSERT_THROW(metNet.productsEnd(0), std::invalid_argument);
  ASSERT_THROW(metNet.products(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_isConsumed) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  const auto& c1 = metNet.addCompound("C1", {compartment, true, false, true});
  const auto& c2 = metNet.addCompound("C2", {compartment, true, false, true});
  const auto& c3 = metNet.addCompound("C3", {compartment, true, false, true});
  const auto& c4 = metNet.addCompound("C4", {compartment, true, false, true});

  // Add a reaction
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C4"}},
                     {"R2", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  // Check isConsumed
  ASSERT_TRUE(metNet.isConsumed(c1->id()));
  ASSERT_TRUE(metNet.isConsumed(c2->id()));
  ASSERT_TRUE(metNet.isConsumed(c3->id()));
  ASSERT_FALSE(metNet.isConsumed(c4->id()));

  // Check out_of_range exceptions
  ASSERT_THROW(metNet.isConsumed(10), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeCompound(0);
  ASSERT_THROW(metNet.isConsumed(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_nbConsumingReactions) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // Add a reaction
  metNet.addReaction(hglib::NAME, {{"C2"}, {"C1"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C1"}},
                     {"R2", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R3", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  // Check nbConsumingReactions
  for (const auto& compound : metNet.compounds()) {
    ASSERT_EQ(compound->id(), metNet.nbConsumingReactions(compound->id()));
  }

  // Add reversible reaction and check number of consuming reactions
  const auto& c4 = metNet.addCompound("C4", {compartment, true, false, true});
  const auto& c5 = metNet.addCompound("C5", {compartment, true, false, true});
  metNet.addReaction(hglib::NAME, {{"C4"}, {"C5"}},
                     {"R4", true, false, {{1, true}}, {{1, true}}, -100, 100});
  ASSERT_EQ(metNet.nbConsumingReactions(c4->id()),
            metNet.nbConsumingReactions(c5->id()));

  // Check out_of_range exceptions
  ASSERT_THROW(metNet.nbConsumingReactions(10), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeCompound(0);
  ASSERT_THROW(metNet.nbConsumingReactions(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_consumingReactionsIterator) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // Add a reaction
  metNet.addReaction(hglib::NAME, {{"C2"}, {"C1"}},
                     {"R1", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C1"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C2"}},
                     {"R3", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  // Iterate over consuming reactions
  for (const auto& compound : metNet.compounds()) {
    // with reaction_iterator
    MetNetwork::reaction_iterator it, end;
    it = metNet.consumingReactionsBegin(compound->id());
    end = metNet.consumingReactionsEnd(compound->id());
    // range based for loop
    auto consumingReactions = metNet.consumingReactions(compound->id());
    for (const auto& reaction : *consumingReactions) {
      ASSERT_EQ(reaction, *it);
      ++it;
    }
    ASSERT_EQ(it, end);
  }

  // Check out_of_range exceptions
  ASSERT_THROW(metNet.consumingReactionsBegin(3), std::out_of_range);
  ASSERT_THROW(metNet.consumingReactionsEnd(3), std::out_of_range);
  ASSERT_THROW(metNet.consumingReactions(3), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeCompound(0);
  ASSERT_THROW(metNet.consumingReactionsBegin(0), std::invalid_argument);
  ASSERT_THROW(metNet.consumingReactionsEnd(0), std::invalid_argument);
  ASSERT_THROW(metNet.consumingReactions(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_isProduced) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  const auto& c1 = metNet.addCompound("C1", {compartment, true, false, true});
  const auto& c2 = metNet.addCompound("C2", {compartment, true, false, true});
  const auto& c3 = metNet.addCompound("C3", {compartment, true, false, true});
  const auto& c4 = metNet.addCompound("C4", {compartment, true, false, true});

  // Add a reaction
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C4"}},
                     {"R2", false, false, {{1, true}}, {{1, true}}, 0, 1000});

  // Check isProduced
  ASSERT_TRUE(metNet.isProduced(c1->id()));
  ASSERT_TRUE(metNet.isProduced(c2->id()));
  ASSERT_FALSE(metNet.isProduced(c3->id()));
  ASSERT_TRUE(metNet.isProduced(c4->id()));

  // Check out_of_range exceptions
  ASSERT_THROW(metNet.isProduced(10), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeCompound(0);
  ASSERT_THROW(metNet.isProduced(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_nbProducingReactions) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // Add some reactions
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C3"}},
                     {"R2", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C2"}, {"C3"}},
                     {"R3", false, false, {{1, true}}, {{1, true}}, 0, 1000});

  // Check nbProducingReactions
  for (const auto& compound : metNet.compounds()) {
    ASSERT_EQ(metNet.nbProducingReactions(compound->id()), compound->id());
  }

  // Add reversible reaction and check number of producing reactions
  const auto& c4 = metNet.addCompound("C4", {compartment, true, false, true});
  const auto& c5 = metNet.addCompound("C5", {compartment, true, false, true});
  metNet.addReaction(hglib::NAME, {{"C4"}, {"C5"}},
                     {"R4", true, false, {{1, true}}, {{1, true}}, -100, 100});
  ASSERT_EQ(metNet.nbProducingReactions(c4->id()),
            metNet.nbProducingReactions(c5->id()));

  // Check out_of_range exceptions
  ASSERT_THROW(metNet.nbProducingReactions(10), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeCompound(0);
  ASSERT_THROW(metNet.nbProducingReactions(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_producingReactionsIterator) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // Add some reactions
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C3"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C2"}, {"C3"}},
                     {"R3", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});

  // Check producing reactions iterator
  for (const auto& compound : metNet.compounds()) {
    // With reaction_iterator
    MetNetwork::reaction_iterator it, end;
    it = metNet.producingReactionsBegin(compound->id());
    end = metNet.producingReactionsEnd(compound->id());
    // With range based for loop
    auto producingReactions = metNet.producingReactions(compound->id());
    for (const auto& reaction : *producingReactions) {
      ASSERT_EQ(*it, reaction);
      ++it;
    }
    ASSERT_EQ(it, end);
  }

  // Check out_of_range exceptions
  ASSERT_THROW(metNet.producingReactionsBegin(3), std::out_of_range);
  ASSERT_THROW(metNet.producingReactionsEnd(3), std::out_of_range);
  ASSERT_THROW(metNet.producingReactions(3), std::out_of_range);
  // Check invalid_argument exceptions
  metNet.removeCompound(0);
  ASSERT_THROW(metNet.producingReactionsBegin(0), std::invalid_argument);
  ASSERT_THROW(metNet.producingReactionsEnd(0), std::invalid_argument);
  ASSERT_THROW(metNet.producingReactions(0), std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_removeCompound) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});

  // Add some reactions
  // create reactions with Id 0 and 1 (reverse reaction)
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  // create reactions with Id 2 and 3 (reverse reaction)
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C3"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  // create reactions with Id 4 and 5 (reverse reaction)
  metNet.addReaction(hglib::NAME, {{"C2"}, {"C3", "C4"}},
                     {"R3", true, false, {{2, true}}, {{1, true}, {1, true}},
                      -1000, 1000});

  // number of compounds/reactions
  size_t nbCompounds(metNet.nbCompounds());
  size_t nbReactions(metNet.nbReactions());
  // Remove compound 'C1' (Id: 0), keeping implied reactions
  metNet.removeCompound(0, false);
  ASSERT_EQ(metNet.compoundById(0), nullptr);
  ASSERT_EQ(--nbCompounds, metNet.nbCompounds());
  ASSERT_EQ(nbReactions, metNet.nbReactions());
  // reaction 'R1' has no substrates anymore
  ASSERT_EQ(metNet.substratesBegin(0), metNet.substratesEnd(0));
  // reaction 'R2' has no substrates anymore
  ASSERT_EQ(metNet.substratesBegin(2), metNet.substratesEnd(2));

  // Remove compound 'C2' (Id: 1), keeping implied reactions
  metNet.removeCompound(1, false);
  ASSERT_EQ(metNet.compoundById(1), nullptr);
  ASSERT_EQ(--nbCompounds, metNet.nbCompounds());
  // Reaction 'R1'(R1_REV') has no substrates and products anymore and is
  // removed from the network
  ASSERT_EQ(--nbReactions, metNet.nbReactions());
  // Compound 'C3' has still two incoming reactions: R2, R3
  ASSERT_EQ(metNet.nbProducingReactions(2), (size_t) 2);
  // Reaction R3 has no substrate anymore
  ASSERT_EQ(metNet.substratesBegin(4), metNet.substratesEnd(4));

  // Remove C3 and implied reactions
  // Thus, there is no reaction in the network anymore.
  // Compound 'C4' is the only (isolated) compound
  metNet.removeCompound(2, true);
  ASSERT_EQ(metNet.compoundById(2), nullptr);
  ASSERT_EQ((size_t) 1, metNet.nbCompounds());
  ASSERT_NE(metNet.compoundById(3), nullptr);
  ASSERT_EQ((size_t) 0, metNet.nbReactions());
  // check incoming reactions of 'C4' (should be empty)
  ASSERT_EQ(metNet.producingReactionsBegin(3), metNet.producingReactionsEnd(3));

  // Test exceptions
  ASSERT_THROW(metNet.removeCompound(0), std::invalid_argument);
  ASSERT_THROW(metNet.removeCompound(10), std::out_of_range);
}

TEST_F(TestMetabolicNetwork, Test_removeReaction) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});

  // Add some reactions
  // create reactions with Id 0 and 1 (reverse reaction)
  metNet.addReaction(hglib::NAME, {{"C1"},
                                   {"C2"}},
                     {"R1", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  // create reactions with Id 2 and 3 (reverse reaction)
  metNet.addReaction(hglib::NAME, {{"C1"},
                                   {"C3"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  // create reactions with Id 4 and 5 (reverse reaction)
  metNet.addReaction(hglib::NAME, {{"C2"},
                                   {"C3", "C4"}},
                     {"R3", true, false, {{2, true}}, {{1, true}, {1, true}},
                      -1000, 1000});

  // number of compounds/reactions
  size_t nbCompounds(metNet.nbCompounds());
  size_t nbReactions(metNet.nbReactions());

  // Remove reaction 'R1'
  metNet.removeReaction(0, true);
  // -> number of compounds remains the same
  ASSERT_EQ(nbCompounds, metNet.nbCompounds());
  // -> one reaction less in the network
  ASSERT_EQ(--nbReactions, metNet.nbReactions());

  // Remove reaction 'R2'
  metNet.removeReaction(2, true);
  // -> number of compounds remains the same
  ASSERT_EQ(nbCompounds, metNet.nbCompounds());
  // -> one reaction less in the network
  ASSERT_EQ(--nbReactions, metNet.nbReactions());
  // Compound 'C1' has no consuming reaction
  ASSERT_FALSE(metNet.isConsumed(0));

  // Remove reaction 'R3'
  metNet.removeReaction(4, true);
  // -> number of compounds remains the same
  ASSERT_EQ(nbCompounds, metNet.nbCompounds());
  // -> one reaction less in the network
  ASSERT_EQ(--nbReactions, metNet.nbReactions());
  // -> all reactions of the network are removed
  ASSERT_FALSE(metNet.isConsumed(0));
  ASSERT_FALSE(metNet.isConsumed(1));
  ASSERT_FALSE(metNet.isConsumed(2));
  ASSERT_FALSE(metNet.isConsumed(3));
  ASSERT_FALSE(metNet.isProduced(0));
  ASSERT_FALSE(metNet.isProduced(1));
  ASSERT_FALSE(metNet.isProduced(2));
  ASSERT_FALSE(metNet.isProduced(3));

  // Test removeReaction with boolean flag = false
  // create reactions with Id 6 and 7 (reverse reaction)
  metNet.addReaction(hglib::NAME, {{"C2"},
                                   {"C3", "C4"}},
                     {"R4", true, false, {{2, true}}, {{1, true}, {1, true}},
                      -1000, 1000});
  ++nbReactions;

  // Remove reaction 'R4' but not 'R4_REV'
  metNet.removeReaction(6);
  // -> number of compounds remains the same
  ASSERT_EQ(nbCompounds, metNet.nbCompounds());
  // -> number of reactions remains the same (the reversible became
  // irreversible)
  ASSERT_EQ(nbReactions, metNet.nbReactions());
  const auto& revReac = metNet.reactionById(7);
  ASSERT_NE(revReac, nullptr);
  ASSERT_FALSE(metNet.isReversible(revReac->id()));

  // Check exceptions
  ASSERT_THROW(metNet.removeReaction(0), std::invalid_argument);
  ASSERT_THROW(metNet.removeReaction(10), std::out_of_range);
}

TEST_F(TestMetabolicNetwork, Test_removeCompartment) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment1 = metNet.addCompartment("c1", 1.0, false);
  const auto& compartment2 = metNet.addCompartment("c2", 1.0, false);
  const auto& compartment3 = metNet.addCompartment("c3", 1.0, false);

  // Add some compounds in compartment1
  metNet.addCompound("C1", {compartment1, true, false, true});
  metNet.addCompound("C2", {compartment1, true, false, true});
  // Add some compounds in compartment2
  metNet.addCompound("C3", {compartment2, true, false, true});
  metNet.addCompound("C4", {compartment2, true, false, true});
  // Add a compound in compartment3
  metNet.addCompound("C5", {compartment3, true, false, true});

  // Add some reactions
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C3"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C4"}},
                     {"R3", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                     {"R4", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C5"}},
                     {"R5", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});

  // number of compounds/reactions
  size_t nbCompounds(metNet.nbCompounds());
  size_t nbReactions(metNet.nbReactions());

  // remove compartment1 without removing boundary reactions
  metNet.removeCompartment(0, false);
  ASSERT_EQ(metNet.compartmentById(0), nullptr);
  // -> Compounds C1 and C2 are removed
  nbCompounds -= 2;
  ASSERT_EQ(nbCompounds, metNet.nbCompounds());
  ASSERT_EQ(metNet.compoundById(0), nullptr);
  ASSERT_EQ(metNet.compoundById(1), nullptr);
  // -> Reaction R1 is removed
  ASSERT_EQ(--nbReactions, metNet.nbReactions());
  ASSERT_EQ(metNet.reactionById(0), nullptr);

  // remove compartment2 removing boundary reactions
  metNet.removeCompartment(1);
  ASSERT_EQ(metNet.compartmentById(1), nullptr);
  // -> Compounds C3 and C4 are removed
  nbCompounds -= 2;
  ASSERT_EQ(nbCompounds, metNet.nbCompounds());
  ASSERT_EQ(metNet.compoundById(2), nullptr);
  ASSERT_EQ(metNet.compoundById(3), nullptr);
  // -> all reactions are removed
  ASSERT_EQ((size_t) 0, metNet.nbReactions());
  ASSERT_EQ(metNet.reactionById(0), nullptr);
  ASSERT_EQ(metNet.reactionById(1), nullptr);
  ASSERT_EQ(metNet.reactionById(2), nullptr);
  ASSERT_EQ(metNet.reactionById(3), nullptr);
  ASSERT_EQ(metNet.reactionById(4), nullptr);
  // -> Compound C5 still exists as isolated compound in its compartment
  ASSERT_NE(metNet.compoundById(4), nullptr);
  ASSERT_NE(metNet.compartmentById(2), nullptr);

  // Test exceptions
  ASSERT_THROW(metNet.removeCompartment(0), std::invalid_argument);
  ASSERT_THROW(metNet.removeCompartment(10), std::out_of_range);
}

TEST_F(TestMetabolicNetwork, Test_resetCompoundIds) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment1 = metNet.addCompartment("c1", 1.0, false);

  // Add compounds
  metNet.addCompound("C_0", {compartment1, true, false, true});
  metNet.addCompound("C_1", {compartment1, true, false, true});
  metNet.addCompound("C_2", {compartment1, true, false, true});
  metNet.addCompound("C_3", {compartment1, true, false, true});
  metNet.addCompound("C_4", {compartment1, true, false, true});

  // resetCompoundIds changes nothing if no compound was deleted
  metNet.resetCompoundIds();
  for (const auto& compound : metNet.compounds()) {
    std::string expectedSbmlId = "C_" + std::to_string(compound->id());
    ASSERT_EQ(compound->sbmlId().compare(expectedSbmlId), 0);
  }

  // Remove compounds with Id 0 and 1
  metNet.removeCompound(0);
  metNet.removeCompound(1);
  ASSERT_EQ(metNet.compoundBySbmlId("C_0"), nullptr);
  ASSERT_EQ(metNet.compoundBySbmlId("C_1"), nullptr);
  // Reset compound Ids
  metNet.resetCompoundIds();
  // Remaining compounds should now have an internal Id decreased by 2 compared
  // to before the removal of the compounds.
  // The Id of a new inserted compound should be equal to expectedId.
  metnetlib::CompoundIdType expectedId(0);
  for (const auto& compound : metNet.compounds()) {
    std::string expectedSbmlId = "C_" + std::to_string(compound->id() + 2);
    ASSERT_EQ(compound->sbmlId().compare(expectedSbmlId), 0);
    ++expectedId;
  }
  // Check Id of the next inserted compound
  const auto& c5 = metNet.addCompound("C_5", {compartment1, true, false, true});
  ASSERT_EQ(c5->id(), expectedId);

  // Remove compounds in the middle (C_3, C_4)
  metNet.removeCompound(1);
  metNet.removeCompound(2);
  ASSERT_EQ(metNet.compoundBySbmlId("C_3"), nullptr);
  ASSERT_EQ(metNet.compoundBySbmlId("C_4"), nullptr);

  // Reset compound Ids
  metNet.resetCompoundIds();
  // Check Ids of remaining compounds
  ASSERT_EQ(metNet.nbCompounds(), (size_t) 2);
  ASSERT_EQ(metNet.compoundById(0), metNet.compoundBySbmlId("C_2"));
  ASSERT_EQ(metNet.compoundById(1), metNet.compoundBySbmlId("C_5"));

  // Remove last compound (C_5)
  metNet.removeCompound(1);
  // Reset compound Ids
  metNet.resetCompoundIds();
  // Check Ids of remaining compounds
  ASSERT_EQ(metNet.nbCompounds(), (size_t) 1);
  ASSERT_EQ(metNet.compoundById(0), metNet.compoundBySbmlId("C_2"));
  // Check that next inserted compound has the Id equal to 1
  const auto& c4 = metNet.addCompound("C_4", {compartment1, true, false, true});
  ASSERT_EQ(c4->id(), 1);
}

TEST_F(TestMetabolicNetwork, Test_resetReactionIds) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment1 = metNet.addCompartment("c1", 1.0, false);

  // Add compounds
  metNet.addCompound("C1", {compartment1, true, false, true});
  metNet.addCompound("C2", {compartment1, true, false, true});

  // Add some reactions
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R0", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R2", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R3", false, false, {{1, true}}, {{1, true}}, 0, 1000});

  // resetReactionIds changes nothing if no reaction was deleted
  metNet.resetReactionIds();
  for (const auto& reaction : metNet.reactions()) {
    std::string expectedSbmlId = "R" + std::to_string(reaction->id());
    ASSERT_EQ(reaction->sbmlId().compare(expectedSbmlId), 0);
  }

  // Remove reactions with Id 0 and 1
  metNet.removeReaction(0);
  metNet.removeReaction(1);
  ASSERT_EQ(metNet.reactionBySbmlId("R0"), nullptr);
  ASSERT_EQ(metNet.reactionBySbmlId("R1"), nullptr);
  // Reset reaction Ids
  metNet.resetReactionIds();
  // Remaining reactions should now have an internal Id decreased by 2 compared
  // to before the removal of the reactions.
  // The Id of a new inserted reaction should be equal to expectedId.
  metnetlib::ReactionIdType expectedId(0);
  for (const auto& reaction : metNet.reactions()) {
    std::string expectedSbmlId = "R" + std::to_string(reaction->id() + 2);
    ASSERT_EQ(reaction->sbmlId().compare(expectedSbmlId), 0);
    ++expectedId;
  }
  // Check Id of the next inserted reaction
  const auto& r4 = metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                                      {"R4", false, false, {{1, true}},
                                       {{1, true}}, 0, 1000});
  ASSERT_EQ(r4->id(), expectedId);

  // Remove reaction in the middle (R3)
  metNet.removeReaction(1);
  ASSERT_EQ(metNet.compoundBySbmlId("R3"), nullptr);

  // Reset reaction Ids
  metNet.resetReactionIds();
  // Check Ids of remaining reactions
  ASSERT_EQ(metNet.nbReactions(), (size_t) 2);
  ASSERT_EQ(metNet.reactionById(0), metNet.reactionBySbmlId("R2"));
  ASSERT_EQ(metNet.reactionById(1), metNet.reactionBySbmlId("R4"));

  // Remove last reaction (R4)
  metNet.removeReaction(1);
  // Reset reaction Ids
  metNet.resetReactionIds();
  // Check Ids of remaining reactions
  ASSERT_EQ(metNet.nbReactions(), (size_t) 1);
  ASSERT_EQ(metNet.reactionById(0), metNet.reactionBySbmlId("R2"));
  // Check that next inserted reaction has the Id equal to 1
  const auto& r5 = metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                                      {"R5", false, false, {{1, true}},
                                       {{1, true}}, 0, 1000});
  ASSERT_EQ(r5->id(), 1);
}

TEST_F(TestMetabolicNetwork, Test_resetCompartmentIds) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add some compartments
  metNet.addCompartment("c0", 1.0, false);
  metNet.addCompartment("c1", 1.0, false);
  metNet.addCompartment("c2", 1.0, false);
  metNet.addCompartment("c3", 1.0, false);

  // resetCompartmentIds changes nothing if no compartment was deleted
  metNet.resetCompartmentIds();
  for (const auto& compartment : metNet.compartments()) {
    std::string expectedSbmlId = "c" + std::to_string(compartment->id());
    ASSERT_EQ(compartment->sbmlId().compare(expectedSbmlId), 0);
  }

  // Remove compartments with Id 0 and 1
  metNet.removeCompartment(0);
  metNet.removeCompartment(1);
  ASSERT_EQ(metNet.compartmentBySbmlId("c0"), nullptr);
  ASSERT_EQ(metNet.compartmentBySbmlId("c1"), nullptr);
  // Reset compartment Ids
  metNet.resetCompartmentIds();
  // Remaining compartments should now have an internal Id decreased by 2
  // compared to before the removal of the compartments.
  // The Id of a new inserted compartment should be equal to expectedId.
  metnetlib::CompartmentIdType expectedId(0);
  for (const auto& compartment : metNet.compartments()) {
    std::string expectedSbmlId = "c" + std::to_string(compartment->id() + 2);
    ASSERT_EQ(compartment->sbmlId().compare(expectedSbmlId), 0);
    ++expectedId;
  }
  // Check Id of the next inserted compartment
  const auto& compartment4 = metNet.addCompartment("c4", 1.0, false);
  ASSERT_EQ(compartment4->id(), expectedId);

  // Remove compartment in the middle (c3)
  metNet.removeCompartment(1);
  ASSERT_EQ(metNet.compartmentBySbmlId("c3"), nullptr);

  // Reset compartment Ids
  metNet.resetCompartmentIds();
  // Check Ids of remaining compartments
  ASSERT_EQ(metNet.nbCompartments(), (size_t) 2);
  ASSERT_EQ(metNet.compartmentById(0), metNet.compartmentBySbmlId("c2"));
  ASSERT_EQ(metNet.compartmentById(1), metNet.compartmentBySbmlId("c4"));

  // Remove last compartment (c4)
  metNet.removeCompartment(1);
  // Reset compartment Ids
  metNet.resetCompartmentIds();
  // Check Ids of remaining compartments
  ASSERT_EQ(metNet.nbCompartments(), (size_t) 1);
  ASSERT_EQ(metNet.compartmentById(0), metNet.compartmentBySbmlId("c2"));
  // Check that next inserted compartment has the Id equal to 1
  const auto& compartment5 = metNet.addCompartment("c3", 1.0, false);
  ASSERT_EQ(compartment5->id(), 1);
}

TEST_F(TestMetabolicNetwork, Test_getCompoundProperties) {
  enum Colour {
      red, blue, green
  };
  struct CompoundProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct ReactionProperty {
      std::string name = "Default";
      double weight = 0.0;
  };

  typedef metnetlib::MetabolicNetwork<metnetlib::Compound,
                                      metnetlib::Reaction,
                                      CompoundProperty,
                                      ReactionProperty> MetNetwork;
  MetNetwork metNet;

  // Add a compartment
  const auto& compartment1 = metNet.addCompartment("c1", 1.0, false);

  // Add compounds
  metNet.addCompound("C_0", {compartment1, true, false, true});
  metNet.addCompound("C_1", {compartment1, true, false, true});
  metNet.addCompound("C_2", {compartment1, true, false, true});
  metNet.addCompound("C_3", {compartment1, true, false, true});
  metNet.addCompound("C_4", {compartment1, true, false, true});

  for (const auto& compound : metNet.compounds()) {
    auto compoundProp = metNet.getCompoundProperties_(compound->id());
    ASSERT_EQ(compoundProp->name.compare("Default"), 0);
    ASSERT_EQ(compoundProp->colour, Colour::green);
    compoundProp->name = compound->sbmlId() + "_" +
        std::to_string(compound->id());
    compoundProp->colour = Colour::red;
    // Verify that updated properties are stored in the network
    const auto constCompoundProp = metNet.getCompoundProperties(compound->id());
    std::string expected = compound->sbmlId() + "_" +
        std::to_string(compound->id());
    ASSERT_EQ(constCompoundProp->name.compare(expected), 0);
    ASSERT_EQ(constCompoundProp->colour, Colour::red);
  }

  metnetlib::CompoundIdType outOfRange = 10;
  ASSERT_THROW(metNet.getCompoundProperties(outOfRange), std::out_of_range);
  ASSERT_THROW(metNet.getCompoundProperties_(outOfRange), std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(metNet.getCompoundProperties(outOfRange), std::out_of_range);
  ASSERT_THROW(metNet.getCompoundProperties_(outOfRange), std::out_of_range);

  // without CompoundProperty (empty struct)
  metnetlib::MetabolicNetwork<> metNet2;
  // Add a compartment
  const auto& compartment2 = metNet2.addCompartment("c1", 1.0, false);

  // Add compounds
  metNet2.addCompound("C_0", {compartment2, true, false, true});
  metNet2.addCompound("C_1", {compartment2, true, false, true});
  metNet2.addCompound("C_2", {compartment2, true, false, true});
  metNet2.addCompound("C_3", {compartment2, true, false, true});
  metNet2.addCompound("C_4", {compartment2, true, false, true});

  // Check compound properties
  for (const auto& compound : metNet2.compounds()) {
    auto compoundProp = metNet2.getCompoundProperties(compound->id());
    EXPECT_EQ(typeid(hglib::emptyProperty), typeid(*compoundProp));
    EXPECT_NE(typeid(CompoundProperty), typeid(*compoundProp));
  }
}

TEST_F(TestMetabolicNetwork, Test_getReactionProperties) {
  enum Colour {
      red, blue, green
  };
  struct CompoundProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct ReactionProperty {
      std::string name = "Default";
      double weight = 0.0;
  };

  typedef metnetlib::MetabolicNetwork<metnetlib::Compound, metnetlib::Reaction,
      CompoundProperty, ReactionProperty> MetNetwork;
  MetNetwork metNet;

  // Add a compartment
  const auto &compartment1 = metNet.addCompartment("c1", 1.0, false);

  // Add compounds
  metNet.addCompound("C1", {compartment1, true, false, true});
  metNet.addCompound("C2", {compartment1, true, false, true});

  // Add some reactions
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R0", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R2", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R3", false, false, {{1, true}}, {{1, true}}, 0, 1000});

  for (const auto& reaction : metNet.reactions()) {
    auto reactionProp = metNet.getReactionProperties_(reaction->id());
    ASSERT_EQ(reactionProp->name.compare("Default"), 0);
    ASSERT_EQ(reactionProp->weight, 0.0);
    reactionProp->name = reaction->sbmlId() + "_" +
                         std::to_string(reaction->id());
    reactionProp->weight = 1.0;
    // Verify that updated properties are stored in the network
    const auto constReactionProp = metNet.getReactionProperties(reaction->id());
    std::string expected = reaction->sbmlId() + "_" +
                           std::to_string(reaction->id());
    ASSERT_EQ(constReactionProp->name.compare(expected), 0);
    ASSERT_EQ(constReactionProp->weight, 1.0);
  }

  metnetlib::ReactionIdType outOfRange = 10;
  ASSERT_THROW(metNet.getReactionProperties(outOfRange), std::out_of_range);
  ASSERT_THROW(metNet.getReactionProperties_(outOfRange), std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(metNet.getReactionProperties(outOfRange), std::out_of_range);
  ASSERT_THROW(metNet.getReactionProperties_(outOfRange), std::out_of_range);

  // without ReactionProperty (empty struct)
  metnetlib::MetabolicNetwork<> metNet2;
  // Add a compartment
  const auto& compartment2 = metNet2.addCompartment("c1", 1.0, false);

  // Add compounds
  metNet2.addCompound("C1", {compartment2, true, false, true});
  metNet2.addCompound("C2", {compartment2, true, false, true});

  // Add some reactions
  metNet2.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R0", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet2.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet2.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R2", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet2.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R3", false, false, {{1, true}}, {{1, true}}, 0, 1000});

  // Check reaction properties
  for (const auto& reaction : metNet2.reactions()) {
    auto reactionProp = metNet2.getReactionProperties(reaction->id());
    EXPECT_EQ(typeid(hglib::emptyProperty), typeid(*reactionProp));
    EXPECT_NE(typeid(ReactionProperty), typeid(*reactionProp));
  }
}

TEST_F(TestMetabolicNetwork, Test_getMetabolicNetworkProperties) {
  enum Colour {
      red, blue, green
  };
  struct CompoundProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct ReactionProperty {
      std::string name = "Default";
      double weight = 0.0;
  };
  struct MetabolicNetworkProperty {
      std::string name;
      Colour colour = Colour::red;
  };

  typedef metnetlib::MetabolicNetwork<metnetlib::Compound, metnetlib::Reaction,
      CompoundProperty, ReactionProperty, MetabolicNetworkProperty> MetNetwork;
  MetNetwork metNet;

  ASSERT_EQ(metNet.getMetabolicNetworkProperties()->name.compare(""), 0);
  ASSERT_EQ(metNet.getMetabolicNetworkProperties()->colour, Colour::red);
  // change metabolic network properties
  metNet.getMetabolicNetworkProperties_()->name = "Test";
  metNet.getMetabolicNetworkProperties_()->colour = Colour::blue;
  // check changed properties
  ASSERT_EQ(metNet.getMetabolicNetworkProperties()->name.compare("Test"), 0);
  ASSERT_EQ(metNet.getMetabolicNetworkProperties()->colour, Colour::blue);

  // without MetabolicNetworkProperty (empty struct)
  metnetlib::MetabolicNetwork<metnetlib::Compound, metnetlib::Reaction,
      CompoundProperty, ReactionProperty>  metNet2;
  EXPECT_EQ(typeid(hglib::emptyProperty),
            typeid(*metNet2.getMetabolicNetworkProperties()));
  EXPECT_NE(typeid(MetabolicNetworkProperty),
            typeid(*metNet2.getMetabolicNetworkProperties()));
}

TEST_F(TestMetabolicNetwork, Test_isSubstrateOrProductOfReaction) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment1 = metNet.addCompartment("c1", 1.0, false);

  // Add compounds
  const auto& c1 = metNet.addCompound("C1", {compartment1, true, false, true});
  const auto& c2 = metNet.addCompound("C2", {compartment1, true, false, true});

  // Add a reaction
  const auto& r1 = metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                                      {"R0", true, false, {{1, true}},
                                       {{1, true}}, -1000, 1000});
  // Get pointer to backward reaction of r1
  const auto& r1Rev = metNet.reactionById(1);

  // Check isSubstrateOfReaction
  ASSERT_TRUE(metNet.isSubstrateOfReaction(c1->id(), r1->id()));
  ASSERT_FALSE(metNet.isSubstrateOfReaction(c2->id(), r1->id()));
  ASSERT_TRUE(metNet.isSubstrateOfReaction(c2->id(), r1Rev->id()));
  ASSERT_FALSE(metNet.isSubstrateOfReaction(c1->id(), r1Rev->id()));

  // Check isProductOfReaction
  ASSERT_FALSE(metNet.isProductOfReaction(c1->id(), r1->id()));
  ASSERT_TRUE(metNet.isProductOfReaction(c2->id(), r1->id()));
  ASSERT_FALSE(metNet.isProductOfReaction(c2->id(), r1Rev->id()));
  ASSERT_TRUE(metNet.isProductOfReaction(c1->id(), r1Rev->id()));

  // Test out_of_range exception
  ASSERT_THROW(metNet.isSubstrateOfReaction(10, r1->id()),
               std::out_of_range);
  ASSERT_THROW(metNet.isSubstrateOfReaction(c1->id(), 10),
               std::out_of_range);
  ASSERT_THROW(metNet.isProductOfReaction(10, r1->id()),
               std::out_of_range);
  ASSERT_THROW(metNet.isProductOfReaction(c1->id(), 10),
               std::out_of_range);

  // remove compound 0
  metNet.removeCompound(0, false);
  // Test invalid_argument exception
  // Check first that reaction R0 still exists and is not the reason for the
  // exception
  ASSERT_NE(metNet.reactionById(r1->id()), nullptr);
  ASSERT_THROW(metNet.isSubstrateOfReaction(0, r1->id()),
               std::invalid_argument);
  ASSERT_THROW(metNet.isProductOfReaction(0, r1->id()),
               std::invalid_argument);
  // remove reaction 0
  metNet.removeReaction(0);
  // Test invalid_argument exception
  // Check first that compound c2 still exists and is not the reason for the
  // exception
  ASSERT_NE(metNet.compoundById(c2->id()), nullptr);
  ASSERT_THROW(metNet.isSubstrateOfReaction(c2->id(), 0),
               std::invalid_argument);
  ASSERT_THROW(metNet.isProductOfReaction(c2->id(), 0),
               std::invalid_argument);
}

TEST_F(TestMetabolicNetwork, Test_CopyCtor) {
  enum Colour {red, blue, green};
  struct CompoundProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };
  struct ReactionProperty {
      std::string label = "Default";
      double weight = 0.0;
  };
  struct MetabolicNetworkProperty {
      std::string label;
      Colour colour = Colour::red;
  };

  typedef metnetlib::MetabolicNetwork<metnetlib::Compound, metnetlib::Reaction,
      CompoundProperty, ReactionProperty, MetabolicNetworkProperty> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment1 = metNet.addCompartment("c1", 1.0, false);

  // Add compounds
  metNet.addCompound("C1", {compartment1, true, false, true});
  metNet.addCompound("C2", {compartment1, true, false, true});
  metNet.addCompound("C3", {compartment1, true, false, true});
  metNet.addCompound("C4", {compartment1, true, false, true});

  // Add some reactions
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R0", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C4"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});

  // remove compound "C3" and associated reaction "R1"
  metNet.removeCompound(metNet.compoundBySbmlId("C3")->id());

  // change some default properties in the network
  metNet.getCompoundProperties_(
      metNet.compoundBySbmlId("C1")->id())->label = "Test";
  metnetlib::ReactionIdType reactionId(0);  // reaction "R0"
  metNet.getReactionProperties_(reactionId)->label = "TestReaction";
  metNet.getReactionProperties_(reactionId)->weight = 1.0;
  metNet.getMetabolicNetworkProperties_()->colour = Colour::blue;

  // Copy metabolic network
  MetNetwork metNetCopy(metNet);

  // Check
  ASSERT_EQ(metNet.nbCompartments(), metNetCopy.nbCompartments());
  ASSERT_EQ(metNet.nbCompounds(), metNetCopy.nbCompounds());
  ASSERT_EQ(metNet.nbReactions(), metNetCopy.nbReactions());
  ASSERT_EQ(metNet.nbReversibleReactions(), metNetCopy.nbReversibleReactions());
  // check metabolic network properties
  ASSERT_EQ(metNetCopy.getMetabolicNetworkProperties_()->label.compare(""), 0);
  ASSERT_EQ(metNetCopy.getMetabolicNetworkProperties_()->colour, Colour::blue);

  // Check compartments
  auto compartmentIt = metNet.compartmentsBegin();
  auto compartmentsEnd = metNet.compartmentsEnd();
  for (; compartmentIt != compartmentsEnd; ++compartmentIt) {
    const auto& compartment = *compartmentIt;
    const auto& copiedCompartment = metNetCopy.compartmentBySbmlId(
        compartment->sbmlId());
    ASSERT_NE(copiedCompartment, nullptr);
    ASSERT_EQ(compartment->id(), copiedCompartment->id());
    ASSERT_EQ(compartment->isConstant(), copiedCompartment->isConstant());
    ASSERT_EQ(compartment->size(), copiedCompartment->size());
  }

  // check compounds
  // Get first the only compartment in the copied network
  const auto& compartmentCopy = metNetCopy.compartmentBySbmlId("c1");
  auto compoundIt = metNet.compoundsBegin();
  auto compoundEnd = metNet.compoundsEnd();
  for (; compoundIt != compoundEnd; ++compoundIt) {
    const auto& compound = *compoundIt;
    const auto& copiedCompound = metNetCopy.compoundBySbmlId(
        compound->sbmlId());
    ASSERT_NE(copiedCompound, nullptr);
    ASSERT_EQ(compound->id(), copiedCompound->id());

    // Check compartment pointer of the copiedCompound
    ASSERT_EQ(copiedCompound->getCompartment(), compartmentCopy);
    ASSERT_EQ(compound->getCompartment(), compartment1);
    ASSERT_NE(copiedCompound->getCompartment(), compartment1);

    const auto& compoundId = compound->id();
    // compare producing reactions
    ASSERT_EQ(metNet.nbProducingReactions(compoundId),
              metNetCopy.nbProducingReactions(compoundId));
    auto reactionIt = metNet.producingReactionsBegin(compoundId);
    auto reactionEnd = metNet.producingReactionsEnd(compoundId);
    auto reactionCopyIt = metNetCopy.producingReactionsBegin(compoundId);
    for (; reactionIt != reactionEnd; ++reactionIt) {
      const auto& reaction = *reactionIt;
      ASSERT_EQ(reaction->id(), (*reactionCopyIt)->id());
      ++reactionCopyIt;
    }
    // compare consuming reactions
    ASSERT_EQ(metNet.nbConsumingReactions(compoundId),
              metNetCopy.nbConsumingReactions(compoundId));
    reactionIt = metNet.consumingReactionsBegin(compoundId);
    reactionEnd = metNet.consumingReactionsEnd(compoundId);
    reactionCopyIt = metNetCopy.consumingReactionsBegin(compoundId);
    for (; reactionIt != reactionEnd; ++reactionIt) {
      const auto& reaction = *reactionIt;
      ASSERT_EQ(reaction->id(), (*reactionCopyIt)->id());
      ++reactionCopyIt;
    }
    // compare properties
    const auto compoundProperty = metNet.getCompoundProperties(compoundId);
    const auto copiedCompoundProperty = metNetCopy.getCompoundProperties(
        compoundId);
    ASSERT_EQ(compoundProperty->label.compare(copiedCompoundProperty->label),
              0);
    ASSERT_EQ(compoundProperty->colour, copiedCompoundProperty->colour);
  }

  // check reactions
  auto itReactions = metNet.reactionsBegin();
  auto itReactionsEnd = metNet.reactionsEnd();
  for (; itReactions != itReactionsEnd; ++itReactions) {
    const auto& reaction = *itReactions;
    const auto& copiedReaction = metNetCopy.reactionById(reaction->id());
    ASSERT_NE(copiedReaction, nullptr);
    ASSERT_EQ(reaction->id(), copiedReaction->id());
    const auto& reactionId = reaction->id();
    // compare substrates
    ASSERT_EQ(metNet.nbSubstrates(reactionId),
              metNetCopy.nbSubstrates(reactionId));
    auto compoundCopyIt = metNetCopy.substratesBegin(reactionId);
    auto it = metNet.substratesBegin(reactionId);
    auto end = metNet.substratesEnd(reactionId);
    for (; it != end; ++it) {
      const auto& substrate = *it;
      ASSERT_EQ(substrate->id(), (*compoundCopyIt)->id());
      ASSERT_EQ(substrate->sbmlId().compare((*compoundCopyIt)->sbmlId()), 0);
      ++compoundCopyIt;
    }
    // compare products
    ASSERT_EQ(metNet.nbProducts(reactionId),
              metNetCopy.nbProducts(reactionId));
    compoundCopyIt = metNetCopy.productsBegin(reactionId);
    it = metNet.productsBegin(reactionId);
    end = metNet.productsEnd(reactionId);
    for (; it != end; ++it) {
      const auto& product = *it;
      ASSERT_EQ(product->id(), (*compoundCopyIt)->id());
      ASSERT_EQ(product->sbmlId().compare((*compoundCopyIt)->sbmlId()), 0);
      ++compoundCopyIt;
    }

    // Check pointer to reverse reaction
    if (metNetCopy.isReversible(reactionId)) {
      const auto& revReaction = metNetCopy.reversibleReaction(reactionId);
      ASSERT_NE(revReaction, nullptr);
      // Check that the pointer to the reverse reaction is from the network
      // copy and not from the original network
      ASSERT_EQ(revReaction, metNetCopy.reactionById(revReaction->id()));
      ASSERT_NE(revReaction, metNet.reactionById(revReaction->id()));
    } else {
      ASSERT_EQ(metNetCopy.reversibleReaction(reactionId), nullptr);
    }

    // compare properties
    const auto reactionProperty = metNet.getReactionProperties(reactionId);
    const auto copiedReactionProperty = metNetCopy.getReactionProperties(
        reactionId);
    ASSERT_EQ(reactionProperty->label.compare(copiedReactionProperty->label),
              0);
    ASSERT_EQ(reactionProperty->weight, copiedReactionProperty->weight);
  }

  // Add compartment to the network copy and check its Id
  const auto& compartment2 = metNetCopy.addCompartment("c2", 1.0, false);
  ASSERT_EQ(compartment2->id(), 1);
}

TEST_F(TestMetabolicNetwork, Test_AssignmentOperator) {
  enum Colour {red, blue, green};
  struct CompoundProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct ReactionProperty {
      std::string name = "Default";
      double weight = 0.0;
  };
  struct MetabolicNetworkProperty {
      std::string name = "Default";
      Colour colour = Colour::blue;
  };

  typedef metnetlib::MetabolicNetwork<metnetlib::Compound, metnetlib::Reaction,
      CompoundProperty, ReactionProperty, MetabolicNetworkProperty> MetNetwork;
  MetNetwork metNet, metNet2, metNet3;
  // Add a compartment to metNet
  const auto& compartment1 = metNet.addCompartment("c1", 1.0, false);

  // Add compounds to metNet
  metNet.addCompound("C1", {compartment1, true, false, true});
  metNet.addCompound("C2", {compartment1, true, false, true});
  metNet.addCompound("C3", {compartment1, true, false, true});
  metNet.addCompound("C4", {compartment1, true, false, true});

  // Add some reactions to metNet
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R0", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C3"}, {"C4"}},
                     {"R1", false, false, {{1, true}}, {{1, true}}, 0, 1000});
  metNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                     {"R2", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});

  // Add different compartments, compounds, reactions to metNet2
  const auto& compartment2 = metNet2.addCompartment("c2", 1.0, false);
  metNet2.addCompound("C1_2", {compartment2, true, false, true});
  metNet2.addCompound("C2_2", {compartment2, true, false, true});
  metNet2.addReaction(hglib::NAME, {{"C1_2"}, {"C2_2"}},
                      {"R0_2", false, false, {{1, true}}, {{1, true}}, 0,
                       1000});
  // Add different compartments, compounds, reactions to metNet3
  const auto& compartment3 = metNet3.addCompartment("c3", 1.0, false);
  metNet3.addCompound("C1_3", {compartment3, true, false, true});
  metNet3.addCompound("C2_3", {compartment3, true, false, true});
  metNet3.addReaction(hglib::NAME, {{"C1_3"}, {"C2_3"}},
                      {"R0_3", false, false, {{1, true}}, {{1, true}}, 0,
                       1000});

  // Change some properties in metNet
  metNet.getCompoundProperties_(
      metNet.compoundBySbmlId("C1")->id())->name = "Test";
  metNet.getReactionProperties_(0)->name = "Test";  // "edge1"
  metNet.getMetabolicNetworkProperties_()->name = "Test";
  metNet.getMetabolicNetworkProperties_()->colour = Colour::red;

  // Make assignments
  metNet = metNet;  // nothing happens in a self assignment
  metNet2 = metNet3 = metNet;
  // check if both metabolic networks are identical
  // check metabolic networks properties
  ASSERT_EQ(metNet.getMetabolicNetworkProperties()->name.compare("Test"), 0);
  ASSERT_EQ(metNet.getMetabolicNetworkProperties()->colour, Colour::red);
  ASSERT_EQ(metNet2.getMetabolicNetworkProperties()->name.compare("Test"), 0);
  ASSERT_EQ(metNet2.getMetabolicNetworkProperties()->colour, Colour::red);
  ASSERT_EQ(metNet3.getMetabolicNetworkProperties()->name.compare("Test"), 0);
  ASSERT_EQ(metNet3.getMetabolicNetworkProperties()->colour, Colour::red);

  // check compartments
  auto CompartmentItMetNet2 = metNet2.compartmentsBegin();
  auto CompartmentItMetNet3 = metNet3.compartmentsBegin();
  for (const auto& compartment : metNet.compartments()) {
    ASSERT_EQ(compartment->id(), (*CompartmentItMetNet2)->id());
    ASSERT_EQ(compartment->sbmlId().compare((*CompartmentItMetNet2)->sbmlId()),
              0);
    ASSERT_EQ(compartment->size(), (*CompartmentItMetNet2)->size());
    ASSERT_EQ(compartment->isConstant(), (*CompartmentItMetNet2)->isConstant());
    ASSERT_EQ(compartment->id(), (*CompartmentItMetNet3)->id());
    ASSERT_EQ(compartment->sbmlId().compare((*CompartmentItMetNet3)->sbmlId()),
              0);
    ASSERT_EQ(compartment->size(), (*CompartmentItMetNet3)->size());
    ASSERT_EQ(compartment->isConstant(), (*CompartmentItMetNet3)->isConstant());
  }

  // check compounds
  // Get first the only compartment in metNet2 and metNet3
  const auto& compartmentMetNet2 = metNet2.compartmentBySbmlId("c1");
  const auto& compartmentMetNet3 = metNet3.compartmentBySbmlId("c1");
  auto CompoundItMetNet2 = metNet2.compoundsBegin();
  auto CompoundItMetNet3 = metNet3.compoundsBegin();
  for (const auto& compound : metNet.compounds()) {
    ASSERT_EQ(compound->id(), (*CompoundItMetNet2)->id());
    ASSERT_EQ(compound->sbmlId().compare((*CompoundItMetNet2)->sbmlId()), 0);
    ASSERT_EQ(compound->id(), (*CompoundItMetNet3)->id());
    ASSERT_EQ(compound->sbmlId().compare((*CompoundItMetNet3)->sbmlId()), 0);

    // Check compartment pointer of the compound in metNet2
    ASSERT_EQ((*CompoundItMetNet2)->getCompartment(), compartmentMetNet2);
    ASSERT_EQ(compound->getCompartment(), compartment1);
    ASSERT_NE((*CompoundItMetNet2)->getCompartment(), compartment1);
    // Check compartment pointer of the compound in metNet3
    ASSERT_EQ((*CompoundItMetNet3)->getCompartment(), compartmentMetNet3);
    ASSERT_NE((*CompoundItMetNet3)->getCompartment(), compartment1);

    // check producing reactions
    auto reactionItMetNet2 = metNet2.producingReactionsBegin(compound->id());
    auto reactionItMetNet3 = metNet3.producingReactionsBegin(compound->id());
    auto inArcsIt = metNet.producingReactionsBegin(compound->id());
    auto inArcsEnd = metNet.producingReactionsEnd(compound->id());
    for (; inArcsIt != inArcsEnd; ++inArcsIt) {
      const auto& reaction = *inArcsIt;
      ASSERT_EQ(reaction->id(), (*reactionItMetNet2)->id());
      ASSERT_EQ(reaction->sbmlId().compare((*reactionItMetNet2)->sbmlId()), 0);
      ASSERT_EQ(reaction->id(), (*reactionItMetNet3)->id());
      ASSERT_EQ(reaction->sbmlId().compare((*reactionItMetNet3)->sbmlId()), 0);
      ++reactionItMetNet2;
      ++reactionItMetNet3;
    }
    // check consuming reactions
    reactionItMetNet2 = metNet2.consumingReactionsBegin(compound->id());
    reactionItMetNet3 = metNet3.consumingReactionsBegin(compound->id());
    auto outArcIt = metNet.consumingReactionsBegin(compound->id());
    auto outArcEnd = metNet.consumingReactionsEnd(compound->id());
    for (; outArcIt != outArcEnd; ++outArcIt) {
      const auto& reaction = *outArcIt;
      ASSERT_EQ(reaction->id(), (*reactionItMetNet2)->id());
      ASSERT_EQ(reaction->sbmlId().compare((*reactionItMetNet2)->sbmlId()), 0);
      ASSERT_EQ(reaction->id(), (*reactionItMetNet3)->id());
      ASSERT_EQ(reaction->sbmlId().compare((*reactionItMetNet3)->sbmlId()), 0);
      ++reactionItMetNet2;
      ++reactionItMetNet3;
    }
    // check properties
    auto compoundProperty = metNet.getCompoundProperties(compound->id());
    auto compound2Property = metNet2.getCompoundProperties(
        (*CompoundItMetNet2)->id());
    auto compound3Property = metNet3.getCompoundProperties(
        (*CompoundItMetNet3)->id());
    ASSERT_EQ(compoundProperty->name.compare(compound2Property->name), 0);
    ASSERT_EQ(compoundProperty->name.compare(compound3Property->name), 0);
    ASSERT_EQ(compoundProperty->colour, compound2Property->colour);
    ASSERT_EQ(compoundProperty->colour, compound3Property->colour);

    ++CompoundItMetNet2;
    ++CompoundItMetNet3;
  }

  // check reactions
  auto reactionItMetNet2 = metNet2.reactionsBegin();
  auto reactionItMetNet3 = metNet3.reactionsBegin();
  for (const auto& reaction : metNet.reactions()) {
    ASSERT_EQ(reaction->id(), (*reactionItMetNet2)->id());
    ASSERT_EQ(reaction->sbmlId().compare((*reactionItMetNet2)->sbmlId()), 0);
    ASSERT_EQ(metNet.isReversible(reaction->id()),
              metNet.isReversible((*reactionItMetNet2)->id()));
    ASSERT_EQ(reaction->isFast(), (*reactionItMetNet2)->isFast());
    ASSERT_EQ(reaction->lowerBound(), (*reactionItMetNet2)->lowerBound());
    ASSERT_EQ(reaction->upperBound(), (*reactionItMetNet2)->upperBound());
    ASSERT_EQ(reaction->id(), (*reactionItMetNet3)->id());
    ASSERT_EQ(reaction->sbmlId().compare((*reactionItMetNet3)->sbmlId()), 0);
    ASSERT_EQ(metNet.isReversible(reaction->id()),
              metNet.isReversible((*reactionItMetNet3)->id()));
    ASSERT_EQ(reaction->isFast(), (*reactionItMetNet3)->isFast());
    ASSERT_EQ(reaction->lowerBound(), (*reactionItMetNet3)->lowerBound());
    ASSERT_EQ(reaction->upperBound(), (*reactionItMetNet3)->upperBound());
    // Check substrates
    auto substrateItMetNet2 = metNet2.substratesBegin(reaction->id());
    auto substrateItMetNet3 = metNet3.substratesBegin(reaction->id());
    auto it = metNet.substratesBegin(reaction->id());
    auto end = metNet.substratesEnd(reaction->id());
    for (; it != end; ++it) {
      const auto& compound = *it;
      ASSERT_EQ(compound->id(), (*substrateItMetNet2)->id());
      ASSERT_EQ(compound->sbmlId().compare((*substrateItMetNet2)->sbmlId()), 0);
      ASSERT_EQ(compound->id(), (*substrateItMetNet3)->id());
      ASSERT_EQ(compound->sbmlId().compare((*substrateItMetNet3)->sbmlId()), 0);
      ++substrateItMetNet2;
      ++substrateItMetNet3;
    }
    // Check products
    auto productItMetNet2 = metNet2.productsBegin(reaction->id());
    auto productItMetNet3 = metNet3.productsBegin(reaction->id());
    it = metNet.productsBegin(reaction->id());
    end = metNet.productsEnd(reaction->id());
    for (; it != end; ++it) {
      const auto& compound = *it;
      ASSERT_EQ(compound->id(), (*productItMetNet2)->id());
      ASSERT_EQ(compound->sbmlId().compare((*productItMetNet2)->sbmlId()), 0);
      ASSERT_EQ(compound->id(), (*productItMetNet3)->id());
      ASSERT_EQ(compound->sbmlId().compare((*productItMetNet3)->sbmlId()), 0);
      ++productItMetNet2;
      ++productItMetNet3;
    }

    // Check pointer to reverse reaction in metNet2
    if (metNet2.isReversible(reaction->id())) {
      const auto& revReaction = metNet2.reversibleReaction(reaction->id());
      ASSERT_NE(revReaction, nullptr);
      // Check that the pointer to the reverse reaction is from the network
      // and not from the original network
      ASSERT_EQ(revReaction, metNet2.reactionById(revReaction->id()));
      ASSERT_NE(revReaction, metNet.reactionById(revReaction->id()));
      ASSERT_NE(revReaction, metNet3.reactionById(revReaction->id()));
    } else {
      ASSERT_EQ(metNet2.reversibleReaction(reaction->id()), nullptr);
    }
    // Check pointer to reverse reaction in metNet3
    if (metNet3.isReversible(reaction->id())) {
      const auto& revReaction = metNet3.reversibleReaction(reaction->id());
      ASSERT_NE(revReaction, nullptr);
      // Check that the pointer to the reverse reaction is from the network
      // and not from the original network
      ASSERT_EQ(revReaction, metNet3.reactionById(revReaction->id()));
      ASSERT_NE(revReaction, metNet.reactionById(revReaction->id()));
      ASSERT_NE(revReaction, metNet2.reactionById(revReaction->id()));
    } else {
      ASSERT_EQ(metNet3.reversibleReaction(reaction->id()), nullptr);
    }

    // Check properties
    const auto reactionProperty = metNet.getReactionProperties(reaction->id());
    const auto reactionProperty2 = metNet2.getReactionProperties(
        (*reactionItMetNet2)->id());
    const auto reactionProperty3 = metNet3.getReactionProperties(
        (*reactionItMetNet3)->id());
    ASSERT_EQ(reactionProperty->name.compare(reactionProperty2->name), 0);
    ASSERT_EQ(reactionProperty->name.compare(reactionProperty3->name), 0);
    ASSERT_EQ(reactionProperty->weight, reactionProperty2->weight);
    ASSERT_EQ(reactionProperty->weight, reactionProperty3->weight);

    ++reactionItMetNet2;
    ++reactionItMetNet3;
  }

  // Add compartment to metNet2 and check its Id
  const auto& newCompartment2 = metNet2.addCompartment("c2", 1.0, false);
  ASSERT_EQ(newCompartment2->id(), 1);

  // Add compartment to metNet3 and check its Id
  const auto& newCompartment3 = metNet3.addCompartment("c2", 1.0, false);
  ASSERT_EQ(newCompartment3->id(), 1);
}

TEST_F(TestMetabolicNetwork, Test_print) {
  typedef metnetlib::MetabolicNetwork<> MetNetwork;
  MetNetwork metNet;
  // Add a compartment
  const auto& compartment1 = metNet.addCompartment("c1", 1.0, false);

  // Add compounds
  metNet.addCompound("C1", {compartment1, true, false, true});
  metNet.addCompound("C2", {compartment1, true, false, true});
  metNet.addCompound("C3", {compartment1, true, false, true});
  metNet.addCompound("C4", {compartment1, true, false, true});
  metNet.addCompound("C5", {compartment1, true, false, true});
  metNet.addCompound("C6", {compartment1, true, false, true});


  // Print isolated compounds
  std::ostringstream output;
  output << metNet;
  std::string observedString(output.str());
  std::string expectedString = "C1\nC2\nC3\nC4\nC5\nC6\n";
  ASSERT_EQ(observedString, expectedString);

  // Add some reactions
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2"}},
                     {"R0", true, false, {{1, true}}, {{1, true}}, -1000,
                      1000});
  metNet.addReaction(hglib::NAME, {{"C3", "C1"}, {"C4"}},
                     {"R1", true, false, {{1, true}, {1, true}}, {{2, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C2", "C5"}},
                     {"R2", false, false, {{1, true}}, {{1, true}, {2, true}},
                      0, 1000});

  // clear output
  output.str("");
  output.clear();
  // Print metabolic network
  output << metNet;
  observedString = output.str();
  expectedString = "C6\n"
      "R0: 1 C1 <-> 1 C2\n"
      "R1: 1 C3 + 1 C1 <-> 2 C4\n"
      "R2: 1 C1 -> 1 C2 + 2 C5\n";
  ASSERT_EQ(observedString, expectedString);
}
