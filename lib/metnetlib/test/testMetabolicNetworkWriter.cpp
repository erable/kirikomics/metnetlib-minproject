/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.12.17.
//

#include "gtest/gtest.h"

#include "metnetlib.h"

#include <fstream>

class TestMetabolicNetworkWriter : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

  // a compartment
  std::tuple<std::string, double, bool>
      compartment = std::make_tuple("c", 1.0, false);

 private:
  std::streambuf* storedStreambuf_;
};

TEST_F(TestMetabolicNetworkWriter, Test_writeSbmlFile) {
  using MetabolicNetwork_t = metnetlib::MetabolicNetwork<>;
  const char* inFileName = "data/metabolicNetworks/valid/sbml_1.xml";
  const char* outFileName = "sbml_1_output.xml";
  auto metNet = metnetlib::SbmlFileReader<>::readFromFile(inFileName);
  metnetlib::SbmlFileWriter<MetabolicNetwork_t>::writeMetabolicNetworkToFile(
      *metNet, outFileName);

  // read in the written SBML file and check if both networks are the same
  auto metNet2 = metnetlib::SbmlFileReader<>::readFromFile(outFileName);

  // Compare compartments
  ASSERT_EQ(metNet->nbCompartments(), metNet2->nbCompartments());
  auto it = metNet->compartmentsBegin();
  for (const auto& compartment : metNet2->compartments()) {
    ASSERT_EQ(compartment->id(), (*it)->id());
    ASSERT_EQ(compartment->isConstant(), (*it)->isConstant());
    ASSERT_EQ(compartment->sbmlId(), (*it)->sbmlId());
    ASSERT_EQ(compartment->size(), (*it)->size());
    ++it;
  }
  ASSERT_EQ(it, metNet->compartmentsEnd());

  // Compare compounds
  ASSERT_EQ(metNet->nbCompounds(), metNet2->nbCompounds());
  auto compoundIt = metNet->compoundsBegin();
  for (const auto& compound : metNet2->compounds()) {
    ASSERT_EQ(compound->id(), (*compoundIt)->id());
    ASSERT_EQ(compound->sbmlId(), (*compoundIt)->sbmlId());
    ASSERT_EQ(compound->getCompartment()->id(),
              (*compoundIt)->getCompartment()->id());
    ASSERT_EQ(compound->hasOnlySubstanceUnits(),
              (*compoundIt)->hasOnlySubstanceUnits());
    ASSERT_EQ(compound->hasBoundaryCondition(),
              (*compoundIt)->hasBoundaryCondition());
    ASSERT_EQ(compound->isConstant(), (*compoundIt)->isConstant());

    const auto& compoundId = compound->id();
    // compare producing reactions
    ASSERT_EQ(metNet->nbProducingReactions(compoundId),
              metNet2->nbProducingReactions(compoundId));
    auto reactionIt = metNet->producingReactionsBegin(compoundId);
    auto reactionEnd = metNet->producingReactionsEnd(compoundId);
    auto reactionIt2 = metNet2->producingReactionsBegin(compoundId);
    for (; reactionIt != reactionEnd; ++reactionIt) {
      const auto& reaction = *reactionIt;
      ASSERT_EQ(reaction->id(), (*reactionIt2)->id());
      ++reactionIt2;
    }
    ASSERT_EQ(reactionIt2, metNet2->producingReactionsEnd(compoundId));

    // compare consuming reactions
    ASSERT_EQ(metNet->nbConsumingReactions(compoundId),
              metNet2->nbConsumingReactions(compoundId));
    reactionIt = metNet->consumingReactionsBegin(compoundId);
    reactionEnd = metNet->consumingReactionsEnd(compoundId);
    reactionIt2 = metNet2->consumingReactionsBegin(compoundId);
    for (; reactionIt != reactionEnd; ++reactionIt) {
      const auto& reaction = *reactionIt;
      ASSERT_EQ(reaction->id(), (*reactionIt2)->id());
      ++reactionIt2;
    }
    ASSERT_EQ(reactionIt2, metNet2->consumingReactionsEnd(compoundId));

    ++compoundIt;
  }
  ASSERT_EQ(compoundIt, metNet->compoundsEnd());

  // Compare reactions
  ASSERT_EQ(metNet->nbReactions(), metNet2->nbReactions());
  ASSERT_EQ(metNet->nbReversibleReactions(), metNet2->nbReversibleReactions());
  auto itReactions = metNet->reactionsBegin();
  for (const auto& reaction : metNet2->reactions()) {
    ASSERT_EQ(reaction->id(), (*itReactions)->id());
    ASSERT_EQ(reaction->sbmlId(), (*itReactions)->sbmlId());
    ASSERT_EQ(reaction->isFast(), (*itReactions)->isFast());
    ASSERT_EQ(reaction->lowerBound(), (*itReactions)->lowerBound());
    ASSERT_EQ(reaction->upperBound(), (*itReactions)->upperBound());

    const auto& reactionId = reaction->id();
    // compare substrates
    ASSERT_EQ(metNet->nbSubstrates(reactionId),
              metNet2->nbSubstrates(reactionId));
    auto it = metNet->substratesBegin(reactionId);
    auto substratesPtr = metNet2->substrates(reactionId);
    for (const auto& substrate : *substratesPtr) {
      ASSERT_EQ(substrate->id(), (*it)->id());
      ASSERT_EQ(substrate->sbmlId().compare((*it)->sbmlId()), 0);
      ++it;
    }
    ASSERT_EQ(it, metNet->substratesEnd(reactionId));

    // compare products
    ASSERT_EQ(metNet->nbProducts(reactionId),
              metNet2->nbProducts(reactionId));
    it = metNet->productsBegin(reactionId);
    auto productsPtr = metNet2->products(reactionId);
    for (const auto& product : *productsPtr) {
      ASSERT_EQ(product->id(), (*it)->id());
      ASSERT_EQ(product->sbmlId().compare((*it)->sbmlId()), 0);
      ++it;
    }
    ASSERT_EQ(it, metNet->productsEnd(reactionId));

    // Check reversibility
    ASSERT_EQ(metNet->isReversible(reactionId),
              metNet2->isReversible(reactionId));

    ++itReactions;
  }
  ASSERT_EQ(itReactions, metNet->reactionsEnd());
}
