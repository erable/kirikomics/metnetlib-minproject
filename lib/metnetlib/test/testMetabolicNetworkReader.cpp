/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 27.11.17.
//

#include <unistd.h>
#include <stdio.h>

#include "gtest/gtest.h"

#include "metnetlib.h"

class TestMetabolicNetworkReader : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

  // a compartment
  std::tuple<std::string, double, bool>
      compartment = std::make_tuple("c", 1.0, false);

 private:
  std::streambuf* storedStreambuf_;
};

TEST_F(TestMetabolicNetworkReader, Test_readSbmlFile) {
  // File does not exists
  ASSERT_THROW(metnetlib::SbmlFileReader<>::readFromFile(
      "test.xml"), std::invalid_argument);

  // Undeclared product
  try {
    metnetlib::DefaultSbmlFileReader::readFromFile(
        "data/metabolicNetworks/invalid/undeclaredProduct.xml");
    FAIL() << "Expected std::invalid_argument\n";
  } catch (const std::invalid_argument& err) {
    std::string expected("Product M_13dpg_c in reaction R_GAPD is not declared "
                             "in the network. Do not insert the reaction.\n"
                             "Product M_13dpg_c in reaction R_PGK is not "
                             "declared in the network. Do not insert the "
                             "reaction.\n");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::invalid_argument";
  }

  // Wrong compartment Ids (catched by libsbml)
  try {
    metnetlib::DefaultSbmlFileReader::readFromFile(
        "data/metabolicNetworks/invalid/invalidCompartmentIds.xml");
    FAIL() << "Expected std::invalid_argument\n";
  } catch (const std::invalid_argument& err) {
    std::string expected("Error reading your Sbml file:\n"
                             "line 5: (10103 [Error]) An SBML XML document "
                             "must conform to the XML Schema for the "
                             "corresponding SBML Level, Version and Release. "
                             "The XML Schema for SBML defines the basic SBML "
                             "object structure, the data types used by those "
                             "objects, and the order in which the objects may "
                             "appear in an SBML document.\n "
                             "Attribute 'id' on an <compartment> must not be "
                             "an empty string.\n\n"
                             "line 6: (10310 [Error]) The syntax of 'id' "
                             "attribute values must conform to the syntax of "
                             "the SBML type 'SId'.\nReference: L3V1 Section "
                             "3.1.7\n The id '1' does not conform to the "
                             "syntax.\n\n"
                             "line 7: (10310 [Error]) The syntax of 'id' "
                             "attribute values must conform to the syntax of "
                             "the SBML type 'SId'.\nReference: L3V1 Section "
                             "3.1.7\n The id '1_c' does not conform to the "
                             "syntax.\n\n"
                             "line 8: (10310 [Error]) The syntax of 'id' "
                             "attribute values must conform to the syntax of "
                             "the SBML type 'SId'.\nReference: L3V1 Section "
                             "3.1.7\n The id '-c' does not conform to the "
                             "syntax.\n\n");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::invalid_argument";
  }

  // Negative compartment size
  try {
    metnetlib::DefaultSbmlFileReader::readFromFile(
        "data/metabolicNetworks/invalid/invalidCompartmentIds2.xml");
    FAIL() << "Expected std::invalid_argument\n";
  } catch (const std::invalid_argument& err) {
    std::string expected("The size of the compartment must be >= 0, you "
                             "provided the value: -1.000000 for the compartment"
                             " negSize");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::invalid_argument";
  }


  // Invalid compound Ids
  try {
    metnetlib::DefaultSbmlFileReader::readFromFile(
        "data/metabolicNetworks/invalid/invalidCompoundIds.xml");
    FAIL() << "Expected std::invalid_argument\n";
  } catch (const std::invalid_argument& err) {
    std::string expected("Error reading your Sbml file:\n"
                             "line 9: (10103 [Error]) An SBML XML document "
                             "must conform to the XML Schema for the "
                             "corresponding SBML Level, Version and Release. "
                             "The XML Schema for SBML defines the basic SBML "
                             "object structure, the data types used by those "
                             "objects, and the order in which the objects may "
                             "appear in an SBML document.\n "
                             "Attribute 'id' on an <species> must not be an "
                             "empty string.\n\n"
                             "line 11: (10310 [Error]) The syntax of 'id' "
                             "attribute values must conform to the syntax of "
                             "the SBML type 'SId'.\nReference: L3V1 Section "
                             "3.1.7\n The id '1' does not conform to the "
                             "syntax.\n\n"
                             "line 13: (10310 [Error]) The syntax of 'id' "
                             "attribute values must conform to the syntax of "
                             "the SBML type 'SId'.\nReference: L3V1 Section "
                             "3.1.7\n The id '1_c' does not conform to the "
                             "syntax.\n\n"
                             "line 15: (10310 [Error]) The syntax of 'id' "
                             "attribute values must conform to the syntax of "
                             "the SBML type 'SId'.\nReference: L3V1 Section "
                             "3.1.7\n The id '-c' does not conform to the "
                             "syntax.\n\n");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::invalid_argument";
  }

  // Invalid compound Ids Part 2: duplicate compounds, undefined compartment
  try {
    metnetlib::DefaultSbmlFileReader::readFromFile(
        "data/metabolicNetworks/invalid/invalidCompoundIds2.xml");
    FAIL() << "Expected std::invalid_argument\n";
  } catch (const std::invalid_argument& err) {
    std::string expected("Compound C1 already exists in this hypergraph.\n"
                             "The compartment Sbml Id c2 is not defined.\n");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::invalid_argument";
  }

  // Invalid reaction Sbml Ids
  try {
    metnetlib::DefaultSbmlFileReader::readFromFile(
        "data/metabolicNetworks/invalid/invalidReactions.xml");
    FAIL() << "Expected std::invalid_argument\n";
  } catch (const std::invalid_argument& err) {
    std::string expected("Error reading your Sbml file:\n"
                             "line 14: (10103 [Error]) An SBML XML document "
                             "must conform to the XML Schema for the "
                             "corresponding SBML Level, Version and Release. "
                             "The XML Schema for SBML defines the basic SBML "
                             "object structure, the data types used by those "
                             "objects, and the order in which the objects may "
                             "appear in an SBML document.\n "
                             "Attribute 'id' on an <reaction> must not be an "
                             "empty string.\n\n"
                             "line 22: (10310 [Error]) The syntax of 'id' "
                             "attribute values must conform to the syntax of "
                             "the SBML type 'SId'.\nReference: L3V1 Section "
                             "3.1.7\n The id '5' does not conform to the "
                             "syntax.\n\n"
                             "line 30: (10310 [Error]) The syntax of 'id' "
                             "attribute values must conform to the syntax of "
                             "the SBML type 'SId'.\nReference: L3V1 Section "
                             "3.1.7\n The id '5_c' does not conform to the "
                             "syntax.\n\n"
                             "line 38: (10310 [Error]) The syntax of 'id' "
                             "attribute values must conform to the syntax of "
                             "the SBML type 'SId'.\nReference: L3V1 Section "
                             "3.1.7\n The id '-c' does not conform to the "
                             "syntax.\n\n");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::invalid_argument";
  }

  // Invalid reaction Sbml Ids part 2: duplicate reaction Sbml Ids, undeclared
  // compounds, negative lower bound of an irreversible reactions
  try {
    metnetlib::DefaultSbmlFileReader::readFromFile(
        "data/metabolicNetworks/invalid/invalidReactions2.xml");
    FAIL() << "Expected std::invalid_argument\n";
  } catch (const std::invalid_argument& err) {
    std::string expected("The reaction Sbml Id R1 already exists in the "
                             "network.\n"
                             "The lower flux bound of an irreversible reaction"
                             " must be greater than or equal to zero: "
                             "R2 -> -1000.000000 Do not insert the reaction.\n"
                             "Substrate C3 in reaction R3 is not declared in "
                             "the network. Do not insert the reaction.\n"
                             "Product C3 in reaction R4 is not declared in the"
                             " network. Do not insert the reaction.\n");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::invalid_argument";
  }

  // Read a valid Sbml file
  auto metNet = metnetlib::DefaultSbmlFileReader::
  readFromFile(
      "data/metabolicNetworks/valid/Ec_core_flux1.xml");
  ASSERT_EQ(metNet->nbCompartments(), (size_t) 2);
  ASSERT_EQ(metNet->nbCompounds(), (size_t) 78);
  ASSERT_EQ(metNet->nbReactions(), (size_t) 77);
  // constant flag is set to false by default
  ASSERT_FALSE(metNet->compoundBySbmlId("M_xu5p_D_c")->isConstant());
  ASSERT_TRUE(metNet->compoundBySbmlId("M_ac_b")->hasBoundaryCondition());
  // Check boundary condition
  ASSERT_FALSE(metNet->compoundBySbmlId("M_xu5p_D_c")->hasBoundaryCondition());
  ASSERT_FALSE(metNet->compoundBySbmlId("M_ac_b")->isConstant());
}
