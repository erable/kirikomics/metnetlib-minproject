#
# - Try to find Sbml
#
# Once done this will define
#
#   SBML_FOUND - system has Sbml
#   SBML_INCLUDE_DIRS - the Sbml (and deps) include directories
#   SBML_LIBRARIES - Link these to use Sbml
#   SBML_LIBRARY_PATH - Path to Sbml library
#
find_path(SBML_INCLUDE_DIR "sbml/SBMLTypes.h"
    PATHS $ENV{LIBSBML_DIR})
find_library(SBML_LIBRARY NAMES "sbml"
    PATHS $ENV{LIBSBML_DIR})

if (NOT SBML_LIBRARY)
    message(SEND_ERROR "\
    \nCould not find sbml \
    \nIf you do have sbml on this system, try setting the environment \
    variable LIBSBML_DIR to the path of your sbml installation, e.g. \
    export LIBSBML_DIR=path_to_sbml\n\
    ")
endif (NOT SBML_LIBRARY)

set(SBML_INCLUDE_DIRS ${SBML_INCLUDE_DIR})
set(SBML_LIBRARIES ${SBML_LIBRARY})
get_filename_component(SBML_LIBRARY_PATH ${SBML_LIBRARY} DIRECTORY)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Sbml DEFAULT_MSG
    SBML_INCLUDE_DIR SBML_LIBRARY)
