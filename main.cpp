#include <iostream>

#include "metnetlib.h"

int main() {
  // Read metabolic network from sbml file
  std::shared_ptr<metnetlib::DefaultMetabolicNetwork> metNet =
      metnetlib::DefaultSbmlFileReader::readFromFile("../sample_data/sbml.xml");

  // Print compounds
  std::cout << "Network compounds" << std::endl
            << "-----------------" << std::endl;
  for (auto compound : metNet->compounds()) {
    std::cout << *compound << std::endl;
  }

  // Print reactions
  std::cout << std::endl
            << "Network reactions" << std::endl
            << "-----------------" << std::endl;
  for (const auto& reaction : metNet->reactions()) {
    std::cout << *reaction;
  }

  return 0;
}
