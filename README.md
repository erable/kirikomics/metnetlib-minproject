###### MetNetLib – Copyright © by INRIA – All rights reserved – Arnaud Mary, David Parsons, Martin Wannagat, 2018

# MetNetLib Minimal Project
This is a minimal project to get you bootstrapped with the use of metnetlib (https://gitlab.inria.fr/kirikomics/metnetlib).

MetNetLib is a library that provides you with ...

## To run the minimal program (main.cpp):
mkdir build
cd build
cmake ..
make
./metnet_minproject

## To run metnetlib tests:
mkdir build
cd build
cmake ..
make check-metnetlib
